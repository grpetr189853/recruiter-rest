<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%customers}}`.
 */
class m190827_182205_create_customers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%customers}}', [
            'id' => $this->char(36)->notNull(),
            'lastname'  => $this->string()->notNull(),
            'name'  => $this->string()->notNull(),
            'fathername'  => $this->string()->null(),
            'location'  => $this->integer()->null(),
            'created_at'  => $this->integer()->notNull(),
            'updated_at'  => $this->integer()->notNull()
        ], $tableOptions);

        $this->addPrimaryKey('pk-customers', '{{%customers}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%customers}}');
    }
}