<?php

use yii\db\Migration;

/**
 * Class m190329_133032_candidate_table
 */
class m190329_133032_candidate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%candidates}}', [
            'id'            => $this->char(36)->notNull(),
            'lastname'      => $this->string()->notNull(),
            'name'          => $this->string()->notNull(),
            'fathername'    => $this->string(),
            'location'      => $this->integer(),
            'current_status'=> $this->integer(),
            'company'       => $this->string(),
            'position'      => $this->string(),
            'salary'        => $this->integer(),
            'date_birth'    => $this->dateTime(),
            'date_created'  => $this->dateTime(),
        ], $tableOptions);

        $this->addPrimaryKey('pk-candidates', '{{%candidates}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%candidates}}');
    }

}
