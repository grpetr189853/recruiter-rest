<?php

use yii\db\Migration;

/**
 * Class m200304_095755_add_is_resume_field_to_files_uploaded_table
 */
class m200304_095755_add_is_resume_field_to_files_uploaded_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%files_uploaded}}', 'is_resume', $this->tinyInteger(1)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropcolumn('{{%files_uploaded}}', 'is_resume');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200304_095755_add_is_resume_field_to_files_uploaded_table cannot be reverted.\n";

        return false;
    }
    */
}
