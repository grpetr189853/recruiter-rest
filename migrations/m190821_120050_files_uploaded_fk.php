<?php

use yii\db\Migration;

/**
 * Class m190821_120050_files_uploaded_fk
 */
class m190821_120050_files_uploaded_fk extends Migration
{
    private $tableName = 'files_uploaded';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'file_type', 'integer');
        $this->addForeignKey(
            'fk_file_type',
            $this->tableName,
            'file_type',
            'file_type',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_file_type',$this->tableName);
    }


}
