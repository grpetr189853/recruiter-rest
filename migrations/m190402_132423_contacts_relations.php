<?php

use yii\db\Migration;

/**
 * Class m190402_132423_contacts_relations
 */
class m190402_132423_contacts_relations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createIndex(
            'index-candidate_contacts-candidate_id',
            '{{%contacts_candidate}}',
            'candidate_id');
        $this->addForeignKey(
            'fk-candidate_contacts-candidate',
            '{{%contacts_candidate}}',
            'candidate_id',
            '{{%candidates}}',
            'id',
            'CASCADE',
            'RESTRICT');

        $this->createIndex(
            'index-candidate_contacts-contact_type_id',
            '{{%contacts_candidate}}',
            'contact_type');
        $this->addForeignKey(
            'fk-candidate_contacts-contact_type',
            '{{%contacts_candidate}}',
            'contact_type',
            '{{%contact_type}}',
            'id',
            'CASCADE',
            'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%contacts_candidate}}','fk-candidate_contacts-candidate');
        $this->dropIndex('{{%contacts_candidate}}', 'index-candidate_contacts-candidate_id');

        $this->dropForeignKey('{{%contacts_candidate}}', 'fk-candidate_contacts-contact_type');
        $this->dropIndex('{{%contacts_candidate}}', 'index-candidate_contacts-contact_type_id');
    }


}
