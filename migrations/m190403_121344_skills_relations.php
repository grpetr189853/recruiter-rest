<?php

use yii\db\Migration;

/**
 * Class m190403_121344_skills_relations
 */
class m190403_121344_skills_relations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            'index-candidate_skills-skill_id',
            '{{%skills_candidate}}',
            'skill_id');
        $this->addForeignKey(
            'fk-candidate_skills-skill',
            '{{%skills_candidate}}',
            'skill_id',
            '{{%skills}}',
            'id',
            'CASCADE',
            'RESTRICT');

        $this->createIndex(
            'index-candidate_skills-candidate_id',
            '{{%skills_candidate}}',
            'candidate_id');

        $this->addForeignKey(
            'fk-candidate_skills-candidate',
            '{{%skills_candidate}}',
            'candidate_id',
            '{{%candidates}}',
            'id',
            'CASCADE',
            'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%skills_candidate}}','fk-candidate_skills-skill');
        $this->dropIndex('{{%skills_candidate}}', 'index-candidate_skills-skill_id');
        $this->dropForeignKey('{{%skills_candidate}}','fk-candidate_skills-candidate');
        $this->dropIndex('{{%skills_candidate}}', 'index-candidate_skills-candidate_id');
    }

}
