<?php

use yii\db\Migration;

/**
 * Class m200526_043503_additional_statuses_relations
 */
class m200526_043503_additional_statuses_relations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            'index-additional_statuses-candidate_id',
            '{{%additional_statuses}}',
            'candidate_id');
        $this->addForeignKey(
            'fk-additional_statuses-candidate',
            '{{%additional_statuses}}',
            'candidate_id',
            '{{%candidates}}',
            'id',
            'CASCADE',
            'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-additional_statuses-candidate','{{%additional_statuses}}');
        $this->dropIndex('index-additional_statuses-candidate_id','{{%additional_statuses}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200526_043503_additional_statuses_relations cannot be reverted.\n";

        return false;
    }
    */
}
