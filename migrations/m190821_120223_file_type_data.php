<?php

use yii\db\Migration;

/**
 * Class m190821_120223_file_type_data
 */
class m190821_120223_file_type_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('file_type', ['name','dir','table'],[
                ['candidate','candidate','candidate'],
                ['vacancy','vacancy','vacancy'],
                ['client','client','client']]
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

}
