<?php

use yii\db\Migration;

/**
 * Class m190418_103406_data
 */
class m190418_103406_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%skills}}',['name'],[
            ['HTML'],
            ['CSS'],
            ['JavaScript'],
            ['PHP'],
            ['C#'],
            ['C++'],
            ['OOP'],
            ['DDD'],
            ['TDD'],
            ['MySQL'],
            ['Redis'],
            ['Angular JS'],
            ['Angular 2'],
            ['React'],
            ['Vue'],
            ['Java'],
        ]);

        $this->batchInsert('{{%countries}}',['name'],[
            ['Украина']
        ]);

        $this->batchInsert('{{%cities}}',['country_id','name'],[
            ['1','Киев'],
            ['1','Харьков'],
            ['1','Днепропетровск'],
            ['1','Львов'],
            ['1','Одесса'],
            ['1','Полтава']
        ]);

        $this->batchInsert('{{%contact_type}}',['name'],[
            ['PHONE'],
            ['EMAIL'],
            ['SKYPE'],
            ['LINKEDIN'],
        ]);

        $this->batchInsert('{{%candidates}}',['id','lastname','name','fathername','location','current_status','company','position','salary'],[
            ['f61e8b51-d16c-464d-9929-44d1b05dbab1','Чумак','Виктор','Александрович','1','1','EPAM','Senir C# developer','5000'],
            ['f61e8b51-d16c-464d-9929-44d1b05dbab2','Олифер','Александр','Валентинович','2','1','Logitek','Fullstack Dev','300'],
            ['f61e8b51-d16c-464d-9929-44d1b05dbab3','Олифер','Геннадий','Валентинович','3','1','Cisco','Frontend developer (.Net)','2500'],
            ['f61e8b51-d16c-464d-9929-44d1b05dbab4','Абрамян','Гамлет','Аванесович','4','2','Cisco','Frontend Dev','1800'],
            ['f61e8b51-d16c-464d-9929-44d1b05dbab5','Дума','Павел','Дмитриевич','5','2','Magnifico','HR','1500'],
            ['f61e8b51-d16c-464d-9929-44d1b05dbab6','Киць','Армен','Обрадович','1','3','BADOO','IT Recruiter','1000'],
            ['f61e8b51-d16c-464d-9929-44d1b05dbab7','Петров','Андрей','Владимирович','2','4','Playtica','Senior PM','950'],
            ['f61e8b51-d16c-464d-9929-44d1b05dbab8','Чумак','Виктор','Александрович','3','5','Amazon','Angular Developer','700'],
            ['f61e8b51-d16c-464d-9929-44d1b05dbab9','Керолл','Алиса','Сергеевна','4','6','BADOO','Php dev','750'],
            ['f61e8b51-d16c-464d-9929-44d1b05dba10','Надь','Виктор','Сергеевич','5','3','BADOO','Middle JS Dev Inc','700'],
            ['f61e8b51-d16c-464d-9929-44d1b05dba11','Федорков','Андрей','Дмитриевич','1','6','Logitek INC','Trainee','5000'],
            ['f61e8b51-d16c-464d-9929-44d1b05dba12','Абраменко','Сергей','Александрович','1','7','Svitla Systems','Trainee','1800'],
        ]);




    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190418_103406_data cannot be reverted.\n";

        return false;
    }

}
