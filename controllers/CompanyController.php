<?php


namespace app\controllers;


use yii\rest\ActiveController;

class CompanyController extends ActiveController
{
    public $modelClass = 'app\crm\entities\company\Company';

    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }
}