<?php


namespace app\controllers;

use app\crm\entities\candidate\ContactCandidate;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

class ContactsCandidateController extends ActiveController
{
    public $modelClass = 'app\crm\entities\candidate\ContactCandidate';
    public $updateScenario = ContactCandidate::SCENARIO_UPDATE;
    public $createScenario = ContactCandidate::SCENARIO_CREATE;
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }
    public function actions(){
        $actions = parent::actions();
        $actions['create']['scenario'] = ContactCandidate::SCENARIO_CREATE;
        $actions['update']['scenario'] = ContactCandidate::SCENARIO_UPDATE;
        unset($actions['index'],$actions['create']);
        return $actions;
    }

    public function actionIndex()
    {
        $candidate_id = \Yii::$app->request->get('candidate_id');
        $activeData = new ActiveDataProvider([
            'query' => ContactCandidate::find()->where('candidate_id = :candidate_id', [':candidate_id' => $candidate_id]),
            'pagination' => [
                'defaultPageSize' => -1,
                'pageSizeLimit' => -1,
            ],
        ]);
        return $activeData;
    }

    public function actionCreate()
    {
        $candidate_id = \Yii::$app->request->get('candidate_id');
        $model = new $this->modelClass([
            'scenario' => ContactCandidate::SCENARIO_CREATE,
        ]);

        $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save()) {
            $response = \Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute(['actionView', 'id' => $candidate_id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }

}