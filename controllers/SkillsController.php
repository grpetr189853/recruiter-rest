<?php


namespace app\controllers;

use yii\rest\ActiveController;

class SkillsController extends ActiveController
{
    public $modelClass = 'app\crm\entities\Skill';
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }

}