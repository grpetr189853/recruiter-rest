<?php


namespace app\controllers;

use app\crm\entities\customer\ContactCustomer;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

class ContactsCustomerController extends ActiveController
{

    public $modelClass = 'app\crm\entities\customer\ContactCustomer';

    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }

    public function actions(){
        $actions = parent::actions();

        unset($actions['index'],$actions['create']);
        return $actions;
    }

    public function actionIndex()
    {
        $customer_id = \Yii::$app->request->get('customer_id');
        $activeData = new ActiveDataProvider([
            'query' => ContactCustomer::find()->where('customer_id = :customer_id', [':customer_id' => $customer_id]),
            'pagination' => [
                'defaultPageSize' => -1,
                'pageSizeLimit' => -1,
            ],
        ]);
        return $activeData;
    }
}