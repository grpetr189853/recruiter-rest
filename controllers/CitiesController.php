<?php


namespace app\controllers;


use yii\rest\ActiveController;

class CitiesController extends ActiveController
{
    public $modelClass = 'app\crm\entities\City';
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }

}