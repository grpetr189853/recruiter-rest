<?php

namespace app\controllers;

use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;

class MailController extends ActiveController 
{
    public $modelClass = '';

    public $createScenario = '';

    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }

    public function actions(){
        $actions = parent::actions();
        unset($actions['create']);
        return $actions;
    }

    public function actionCreate()
    {
        $params['customer'] = \Yii::$app->request->getBodyParam('customer');
        $params['vacancy'] = \Yii::$app->request->getBodyParam('vacancy');
        $params['contact'] = \Yii::$app->request->getBodyParam('contact_name');
        $params['address'] = \Yii::$app->request->getBodyParam('contact_value');
        $params['document'] = \Yii::$app->request->getBodyParam('attached_document');
        $params['note'] = \Yii::$app->request->getBodyParam('letter_body');
        return \Yii::$app->mailer->compose(
            ['html' => 'sendForm-html', 'text' => 'sendForm-text'],
            [
                'customer' => $params['customer'],
                'vacancy' => $params['vacancy'],
                'contact' => $params['contact'],
                'address' => $params['address'],
                'document' => $params['document'],
                'note' => $params['note'],
            ]
        )
            ->setFrom('from@domain.com')
            ->setTo('to@domain.com')
            ->setSubject('Тема сообщения')
//            ->setTextBody('Текст сообщения')
//            ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
            ->send();
    }

}