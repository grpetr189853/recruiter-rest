<?php


namespace app\controllers;


use yii\rest\ActiveController;

class ContactsTypeController extends ActiveController
{
    public $modelClass = 'app\crm\entities\candidate\ContactCandidateType';
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }
}