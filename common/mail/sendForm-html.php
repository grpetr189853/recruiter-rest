<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $customer */
/* @var $vacancy */
/* @var $contact */
/* @var $address */
/* @var $document */
/* @var $note */
?>
<div class="password-reset">
    <p>Customer <?= Html::encode($customer) ?>,</p>
    <p>Vacancy <?= Html::encode($vacancy) ?>,</p>
    <p>Contact <?= Html::encode($contact) ?>,</p>
    <p>Address <?= Html::encode($address) ?>,</p>
    <p>Document <?= Html::encode($document) ?>,</p>
    <p>Note <?= Html::encode($note) ?>,</p>
</div>
