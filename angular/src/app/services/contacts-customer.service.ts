import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ContactsCustomer } from '../models/contactsCustomer';

@Injectable({
  providedIn: 'root'
})
export class ContactsCustomerService {
  
  private headers = new Headers({'Content-Type': 'application/json'});
  private candidateUrl = `web/customers/`;
  private contactsCandidateUrl = `/contacts-customer`;
  constructor(private http: Http) { }


  getCustomerContacts(customer_id: string): Promise<ContactsCustomer[]> {
    return this.http
      .get(this.candidateUrl + `${customer_id}` + this.contactsCandidateUrl)
      .toPromise()
      .then(response => response.json() as ContactsCustomer[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
