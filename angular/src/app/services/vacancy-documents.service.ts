import { Injectable } from '@angular/core';
import {Headers, Http} from "@angular/http";
import {DocumentVacancy} from "../models/DocumentVacancy";

@Injectable({
  providedIn: 'root'
})
export class VacancyDocumentsService {
  private headers = new Headers();
  constructor(private http: Http) { }
  public getVacancyDocuments(vacancy_id): Promise<[DocumentVacancy]> {
    const vacancyUrl = 'web/vacancies';
    return this.http.get(vacancyUrl + '/' + `${vacancy_id}` + '/documents-vacancy')
      .toPromise()
      .then(response => response.json() as DocumentVacancy)
      .catch(this.handleError);
  }
  public uploadFile(file: File, vacancy_id) {
    const formData = new FormData();
    const vacancyDocumentCreateUrl = 'web/vacancies';
    formData.append('file', file);
    return this.http.post(vacancyDocumentCreateUrl + '/' + `${vacancy_id}` + '/documents-vacancy', formData, {headers: this.headers});
  }
  public deleteFileByName(vacancy_id, deletedDocumentId: number) {
    const fileDeleteUrl = 'web/vacancies';
    return this.http.delete(fileDeleteUrl  + '/' + `${vacancy_id}` + '/documents-vacancy/' + `${deletedDocumentId}`,{headers: this.headers});
  }


  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
