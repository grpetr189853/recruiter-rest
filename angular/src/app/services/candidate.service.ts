import { Injectable } from '@angular/core';
// import { Headers, Http } from '@angular/http';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import { Candidate } from '../models/candidate';
import { Observable } from 'rxjs';
import { Headers, Http } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class CandidateService {


  private headers = new Headers({'Content-Type': 'application/json'});
  private candidateUrl = 'web/candidates';  // URL to web api
  private expandRelatedStatuses = '?expand=relatedStatuses';

  constructor(private http: Http) { }

  getData(): Promise<Candidate[]> {
    return this.http.get(this.candidateUrl)
      .toPromise()
      .then(response => response.json() as Candidate[])
      .catch(this.handleError);
  }

  getCandidatesWithStatuses(): Promise<Candidate[]>  {
    return this.http.get(`${this.candidateUrl}${this.expandRelatedStatuses}`)
      .toPromise()
      .then(response => response.json() as Candidate[])
      .catch(this.handleError);
  }

  getDetail(id: string): Promise<Candidate> {
    return this.http.get(`${this.candidateUrl}/${id}`)
      .toPromise()
      .then(response => response.json() as Candidate)
      .catch(this.handleError);
  }

  update(candidate: Candidate): Promise<Candidate> {
    const url = `${this.candidateUrl}/${candidate.id}`;
    return this.http
      .put(url, JSON.stringify(candidate), {headers: this.headers})
      .toPromise()
      .then(() => candidate)
      .catch(this.handleError);
  }
  create(candidate: Candidate): Promise<Candidate> {
    const url = `${this.candidateUrl}`;
    return this.http
      .post(url, JSON.stringify({
        lastname: candidate.lastname,
        name: candidate.name,
        fathername: candidate.fathername,
        location: candidate.location,
        company: candidate.company,
        position: candidate.position,
        salary: candidate.salary,
        date_birth: candidate.date_birth,
        wish: candidate.wish,
        notes: candidate.notes
      }),{headers: this.headers})
      .toPromise()
      .then(res => res.json() as Candidate)
      .catch(this.handleError);
  }
  delete(candidate_id: string) {
    const url = `${this.candidateUrl}/${candidate_id}`;
    return this.http
      .delete(url,{headers: this.headers})
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
