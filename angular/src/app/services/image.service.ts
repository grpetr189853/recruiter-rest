import { Injectable } from '@angular/core';
import {Headers, Http} from "@angular/http";
import {forkJoin} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private http: Http) { }

  public uploadPhoto(photo: File, candidate_id) {
    const formData = new FormData();
    const photoCreateUrl = 'web/candidates';
    formData.append('photo', photo);
    return this.http.post(photoCreateUrl + '/' + `${candidate_id}` + '/photo', formData);
  }

  public deletePhoto(candidate_id) {
    const photoDeleteUrl = 'web/candidates';
    return this.http.delete(photoDeleteUrl + '/' + `${candidate_id}` + '/photo');
  }

}
