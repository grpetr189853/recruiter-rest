import { Injectable } from '@angular/core';
import {Headers, Http} from "@angular/http";

@Injectable({
  providedIn: 'root'
})
export class MailService {
  private headers = new Headers({'Content-Type': 'application/json'});
  constructor(private http: Http) { }

  public sendMail(customer: string = '', vacancy: string = '', contact_name: string, contact_value: string, attached_document: string, letter_body: string) {
    const mailUrl = 'web/mail';
    return this.http
      .post(
        mailUrl, JSON.stringify({
          customer,
          vacancy,
          contact_name,
          contact_value,
          attached_document,
          letter_body,
        })
    ,{headers: this.headers})
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
