import { Injectable } from '@angular/core';
import {Headers, Http} from "@angular/http";
import {DocumentCandidate} from "../models/DocumentCandidate";

@Injectable({
  providedIn: 'root'
})
export class FileService {
  private headers = new Headers();
  constructor(private http: Http) { }

  public getCandidateResume(candidate_id): Promise<[DocumentCandidate]> {
    const resumeUrl = 'web/candidates';
    return this.http.get(resumeUrl + '/' + `${candidate_id}` + '/documents-candidate')
      .toPromise()
      .then(response => response.json() as DocumentCandidate)
      .catch(this.handleError);
  }

  public getCandidateDocuments(candidate_id): Promise<[DocumentCandidate]> {
    const resumeUrl = 'web/candidates';
    return this.http.get(resumeUrl + '/' + `${candidate_id}` + '/documents-candidate')
      .toPromise()
      .then(response => response.json() as DocumentCandidate)
      .catch(this.handleError);
  }
  public uploadFile(file: File, candidate_id, fileType: number) {
    const formData = new FormData();
    const resumeCreateUrl = 'web/candidates';
    formData.append('file', file);
    this.headers.set('file-type', fileType.toString());
    return this.http.post(resumeCreateUrl + '/' + `${candidate_id}` + '/documents-candidate', formData, {headers: this.headers});
  }

  public deleteFile(candidate_id) {
    const fileDeleteUrl = 'web/candidates';
    return this.http.delete(fileDeleteUrl  + '/' + `${candidate_id}` + '/documents-candidate',{headers: this.headers});
  }

  public deleteFileByName(candidate_id, deletedDocumentId: number) {
    const fileDeleteUrl = 'web/candidates';
    return this.http.delete(fileDeleteUrl  + '/' + `${candidate_id}` + '/documents-candidate/' + `${deletedDocumentId}`,{headers: this.headers});
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
