import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CandidateComponent } from './components/candidate/candidate.component';
import { DetailComponent} from './components/detail/detail.component';
import {CandidateUpdateComponent} from './components/candidate-update/candidate-update.component';
import {AddCandidateComponent} from "./components/add-candidate/add-candidate.component";
import { CandidateInfoComponent } from './components/candidate-info/candidate-info.component';
import { CandidateListComponent } from './components/candidate-list/candidate-list.component';
import { AllVacanciesComponent } from './components/all-vacancies/all-vacancies.component';
import { VacancyListComponent } from './components/vacancy-list/vacancy-list.component';
import { VacancyInfoComponent } from './components/vacancy-info/vacancy-info.component';
import { AddVacancyComponent } from './components/add-vacancy/add-vacancy.component';

const routes: Routes = [
  { path: 'web/candidates', component: CandidateComponent },
  { path: 'detail/:id',     component: DetailComponent },
  { path: 'update/:id', component: CandidateUpdateComponent},
  { path: 'add-candidate', component: AddCandidateComponent},
  { path: 'candidate-list', component: CandidateListComponent},
  { path: 'candidate-info', component: CandidateInfoComponent},
  { path: 'all-vacancies', component: AllVacanciesComponent},
  { path: 'vacancy-list', component: VacancyListComponent},
  { path: 'vacancy-info', component: VacancyInfoComponent},
  { path: 'add-vacancy', component: AddVacancyComponent},

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
