import { Component, OnInit } from '@angular/core';

import {CandidateService} from './services/candidate.service';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CandidateService]
})
export class AppComponent {
  constructor(private router: Router) {
  };
}
