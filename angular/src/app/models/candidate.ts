export class Candidate {
  id?: string;
  lastname: string;
  name: string;
  fathername: string;
  location: number;
  current_status: number;
  company: string;
  position: string;
  salary: number;
  date_birth: string;
  date_created: string;
  photo: string;
  wish: string;
  documents: string;
  notes: string;
  relatedStatuses: any;
}
