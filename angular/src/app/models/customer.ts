export class Customer {
    id?: string;
    name: string;
    location: number;
    contacts: string;
  }
  