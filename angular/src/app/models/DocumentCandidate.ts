export class DocumentCandidate {
  file_name : string;
  file_extension: string;
  entity_id: string;
  is_resume: number;
  id?: number;
}
