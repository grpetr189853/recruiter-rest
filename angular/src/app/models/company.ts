export class Company {
    id?: string;
    name: string;
    full_name: string;
    condition: string;
    about: string;
    logo: string;
    image: string;
}