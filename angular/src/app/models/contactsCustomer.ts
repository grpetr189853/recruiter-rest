export class ContactsCustomer 
{
    id?: number;
    customer_id: string;
    contact_type: string;
    value: string;
}