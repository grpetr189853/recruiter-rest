import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendCandidateModalComponent } from './send-candidate-modal.component';

describe('SendCandidateModalComponent', () => {
  let component: SendCandidateModalComponent;
  let fixture: ComponentFixture<SendCandidateModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendCandidateModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendCandidateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
