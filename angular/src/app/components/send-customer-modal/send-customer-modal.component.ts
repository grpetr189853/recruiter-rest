import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Customer } from 'src/app/models/customer';
import { CustomerService } from 'src/app/services/customer.service';
import { ContactsCustomerService } from 'src/app/services/contacts-customer.service';
import { Candidate } from 'src/app/models/candidate';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { FileService } from 'src/app/services/file.service';
import { CandidateComponent } from '../candidate/candidate.component';
import { NgForm } from '@angular/forms';
import { MailService } from 'src/app/services/mail.service';
import { ContactsCandidateService } from 'src/app/services/contacts-candidate.service';
import { VacancyService } from 'src/app/services/vacancy.service';
class FileSnippet {
  pending: boolean = false;
  status: string = 'init';
  constructor(public src: string, public file: File) {}
}
@Component({
  selector: 'app-send-customer-modal',
  templateUrl: './send-customer-modal.component.html',
  styleUrls: ['./send-customer-modal.component.css']
})
export class SendCustomerModalComponent implements OnInit {
  title: string;
  closeBtnName: string;

  customers_names: string[] = [];
  vacancies_names: string[] = [];
  selected_customer: string;
  customer_contact: string[] = [];
  customer_contact_value: string[] = [];
  attached_document: string[] = [];
  customer_letter_body: string;
  selectedCandidateId: string;
  customerId: string = '';
  customer_contacts_values: any = [];
  customer_contacts_names: any;
  transformed_customer_contacts_names: any = [];
  attached_documents: any;
  selected_contact_name: any;
  selected_contact_value: any;
  selected_attached_document: any;
  selected_vacancy: any;
  choosen_customer_contacts_values : any = [];
  @ViewChild('fileInputDocument', {static: false}) fileInputDocument:ElementRef;
  selectedAttachedFile: any;
  readonly IS_DOCUMENT = 0;
  // @Input() selectedCandidate : Candidate;
  constructor(
    public bsModalRef: BsModalRef,
    private customerService: CustomerService,
    private vacancyService: VacancyService,
    private contactsCustomerService: ContactsCustomerService,
    private fileService: FileService,
    private mailService: MailService,
    private contactsCandidateService: ContactsCandidateService,
  ) { }

  ngOnInit() {
    this.getCustomers();
    this.getVacancies();
  }

  getCustomers() {
    this.customerService.getData().then(
      customers => {
        this.customers_names = customers.map(customer => customer.name);
        console.log(this.customers_names);
      }
    )
  }

  getVacancies() {
    this.vacancyService.getData().then(
      vacancies => {
        this.vacancies_names = vacancies.map(vacancy => vacancy.name);
      }
    )
  }

  getCustomerIdByName(customerName: string, successCalback) {
    this.customerService.getData().then(
      customers => {
        successCalback(customers.find(customer => customer.name == customerName).id);
      }
    )
  }

  getCustomerContacts(id: string) {
    this.contactsCustomerService.getCustomerContacts(id).then(
        contacts => {
          console.log(contacts);
      });
  }

  getAttachedDocuments() {
    this.fileService.getCandidateDocuments(this.selectedCandidateId).then(
      candidateDocuments => {
        this.attached_documents = candidateDocuments.map(candidateDocument => candidateDocument.file_name);
      }
    )
  }

  onSelect(event: TypeaheadMatch): void {
    this.getCustomerIdByName(event.item, (id) => {
      this.contactsCustomerService.getCustomerContacts(id).then(
        customerContacts => {
          customerContacts.forEach(customerContact => this.customer_contacts_values.push({value: customerContact.value,name: customerContact.contact_type}))
          
          this.customer_contacts_names = customerContacts.map(customerContact => customerContact.contact_type);
          this.customer_contacts_names.forEach(contact_name => this.contactsCandidateService.getCandidateContactTypeName(contact_name).then(
            contact => this.transformed_customer_contacts_names.push({name: contact.name, id: contact.id})
          ));
          this.getAttachedDocuments();
        });
    });
  }

  onSelectCommunication(event: TypeaheadMatch): void {
    this.choosen_customer_contacts_values = this.customer_contacts_values.filter(customerContactValue => customerContactValue.name == event.item.id);
  }

  onFormSubmit(sendCustomer: NgForm) {
    this.mailService.sendMail(
      this.selected_customer,
      this.selected_vacancy,
      this.selected_contact_name,
      this.selected_contact_value,
      this.selected_attached_document,
      this.customer_letter_body
      );
    this.bsModalRef.hide();
  }

  addDocument(fileInput: any) {
    const file: File = fileInput.files[0];
    const reader = new FileReader();
    reader.addEventListener('load', (event: any) => {
      this.fileInputDocument.nativeElement.value = null;
      this.selectedAttachedFile = new FileSnippet(event.target.result, file);
      this.fileService.uploadFile(this.selectedAttachedFile.file, this.selectedCandidateId, this.IS_DOCUMENT).subscribe(
        (res) => {
          this.getAttachedDocuments();
          console.log(res);
        },
        (err) => {
          console.log(err);
        });      
    });

    reader.readAsDataURL(file);
  }

}