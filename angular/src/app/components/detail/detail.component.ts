import { Component, OnInit } from '@angular/core';


import {CandidateService} from '../../services/candidate.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Candidate } from '../../models/candidate';
import { switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  candidate: Candidate;

  constructor(
    private candidateService: CandidateService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router

  ) { }

  ngOnInit() {
    this.getCandidate();
  }

  getCandidate() {
//    this.route.params.pipe(switchMap((params: Params) => this.candidateService.getDetail(params['id'])));
    this.route.params.pipe(switchMap((params: Params) => this.candidateService.getDetail(params['id'])))
      .subscribe(candidate => this.candidate = candidate);
  }

  goBack(): void {
    this.location.back();
  }

}
