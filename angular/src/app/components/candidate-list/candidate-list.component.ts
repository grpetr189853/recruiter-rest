import { Component, OnInit, Input, OnChanges, SimpleChange, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import { Candidate } from 'src/app/models/candidate';
import { CandidateService } from 'src/app/services/candidate.service';
import { EventBusService } from 'src/app/services/shared/event-bus.service';
import { EventData } from 'src/app/services/shared/event.class';
// import { StatusesCandidatesService } from 'src/app/services/statuses-candidates.service';

@Component({
  selector: 'app-candidate-list',
  templateUrl: './candidate-list.component.html',
  styleUrls: ['./candidate-list.component.css']
})
export class CandidateListComponent implements OnInit, OnChanges {
  candidates: Candidate[] = [];
  @Input() selected_statuses : any;
  @Input() searchString: any;
  selectedCandidate: Candidate;
  /*****Candidate Statuses*****/
  readonly STATUS_ALL = 0;
  readonly STATUS_NEW = 1;
  readonly STATUS_CONSIDERED = 2;
  readonly STATUS_TODAY = 3;
  readonly STATUS_OFFER = 4;
  readonly STATUS_SELECTED = 5;
  readonly STATUS_BLACKLISTED = 6;
  constructor(
    private candidateService: CandidateService,
    private eventBusService: EventBusService,
    private cdr: ChangeDetectorRef,
  ) { }
  
  ngOnInit() {
  this.getCandidatesExceptBlackListed();
  // console.log(this.selected_statuses);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.getCandidatesWithStatuses(this.selected_statuses);
    this.cdr.detectChanges();
    console.log(this.selected_statuses);
    console.log(changes);
  }

  getCandidatesWithStatuses(selectedStatus: number) {
    this.candidates = [];
    // if status == STATUS_ALL - select all candidates except BLACK_LISTED
    if(selectedStatus == this.STATUS_ALL){
       this.getCandidatesExceptBlackListed();
    } else {
      this.candidateService.getCandidatesWithStatuses().then(
        candidates => {
          candidates.forEach(candidate => {
            candidate.relatedStatuses.forEach(relatedStatus => {
              if(relatedStatus.status == selectedStatus) {
                this.candidates.push(candidate);
              }
            })
          })
          if(this.candidates[0]){
            this.selectedCandidate = this.candidates[0];
            this.eventBusService.emit(new EventData('selectedCandidate',this.candidates[0]));    
          } else {
            this.eventBusService.emit(new EventData('selectedCandidate',undefined));
          }
        }
      )
      // this.eventBusService.emit(new EventData('selectedCandidate',this.candidates[0]));
    }
    // this.eventBusService.emit(new EventData('selectedCandidate',this.candidates[0]));
  }

  getCandidatesExceptBlackListed() {
    this.candidateService.getCandidatesWithStatuses().then(candidates => {

      candidates.forEach(candidate => {
        if(!candidate.relatedStatuses.length){
          this.candidates.push(candidate);
        } else {
          candidate.relatedStatuses.forEach(relatedStatus => {
            if(relatedStatus.status != this.STATUS_BLACKLISTED) {
              this.candidates.push(candidate);
            }
          })  
        }
      })
      this.selectedCandidate = this.candidates[0];
      this.eventBusService.emit(new EventData('selectedCandidate',this.candidates[0]));
    }); 

  }

  async getCandidates() {
    this.candidates = undefined;
    this.candidateService.getData().then(candidates => {
      this.candidates = candidates;
      // this.selectedCandidate = candidates[0];
      // this.getCandidateCity(this.selectedCandidate.location.toString());
      // this.getCandidateContacts(this.selectedCandidate.id);
      // this.getCandidateSkills(this.selectedCandidate.id);
    });
    return true;
  }

  onSelect(candidate: Candidate) {
    this.selectedCandidate = candidate;
    this.eventBusService.emit(new EventData('selectedCandidate',candidate));
  }

}
