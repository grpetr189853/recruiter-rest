import { Component, OnInit, OnChanges, SimpleChanges, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { EventBusService } from 'src/app/services/shared/event-bus.service';
import { Candidate } from 'src/app/models/candidate';
import { CandidateService } from 'src/app/services/candidate.service';
import { CitiesService } from 'src/app/services/cities.service';
import { ContactsCandidateService } from 'src/app/services/contacts-candidate.service';
import { SkillsCandidateService } from 'src/app/services/skills-candidate.service';
import { SkillsService } from 'src/app/services/skills.service';
import { ImageService } from 'src/app/services/image.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { City } from 'src/app/models/city';
import { SendCustomerModalComponent } from '../send-customer-modal/send-customer-modal.component';
import { SendCandidateModalComponent } from '../send-candidate-modal/send-candidate-modal.component';
import { ConnectCustomerModalComponent } from '../connect-customer-modal/connect-customer-modal.component';
import { FileService } from 'src/app/services/file.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {CKEditorComponent} from "@ckeditor/ckeditor5-angular";
class ImageSnippet {
  pending: boolean = false;
  status: string = 'init';
  constructor(public src: string, public file: File) {}
}
class FileSnippet {
  pending: boolean = false;
  status: string = 'init';
  constructor(public src: string, public file: File) {}
}
@Component({
  selector: 'app-candidate-info',
  templateUrl: './candidate-info.component.html',
  styleUrls: ['./candidate-info.component.css']
})
export class CandidateInfoComponent implements OnInit, OnChanges {
  @ViewChild('photoInput', {static: false}) photoInput:ElementRef;
  @ViewChild('fileInput', {static: false}) fileInput:ElementRef;
  selectedCandidate: Candidate;
  city: City;
  candidateContacts: any = [];
  contactType: any;
  candidateSkills: any = [];
  candidates: Candidate[];
  selectedPhoto: ImageSnippet;
  /*****Confirm popup data*****/
  modalRef: BsModalRef;
  message: string;
  /*****Send to customer popup data*****/
  bsModalRef: BsModalRef; 
  candidateDocuments: any;
  selectedCandidateDocument: any;
  deletedCandidateDocument: any;
  uploadedFile: any;
  readonly IS_DOCUMENT = 0; 
  public Editor = ClassicEditor;
  constructor(
    private eventBusService: EventBusService,
    private candidateService: CandidateService,
    private citiesService: CitiesService,
    private contactsCandidateService: ContactsCandidateService,
    private skillsCandidateService: SkillsCandidateService,
    private skillsService: SkillsService,
    private imageService: ImageService,
    private modalService: BsModalService,
    private fileService: FileService,
  ) { }

  ngOnInit() {
    this.eventBusService.on('selectedCandidate',(data: Candidate) => {
      console.log('fired');
      if(data) {
        this.selectedCandidate = data;
        this.getCandidateCity(this.selectedCandidate.location.toString());
        this.getCandidateContacts(this.selectedCandidate.id);
        this.getCandidateSkills(this.selectedCandidate.id); 
        this.getCandidateDocuments(this.selectedCandidate.id);  
      } else {
        this.selectedCandidate = undefined;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log("changes", changes.selectedCandidate);
    // console.log("changes", changes);
  }

  getCandidateCity(id: string) {
    this.citiesService.getDetail(id).then( city => this.city = city);
  }

  getCandidateContacts(id: string) {
    this.contactsCandidateService.getCandidateContacts(id).then(
      contacts => {
        this.candidateContacts = [];
        contacts.forEach( contact => {
          this.contactType = undefined;
          this.contactsCandidateService.getCandidateContactTypeName(contact.contact_type.toString())
            .then(contact_type => {
              this.candidateContacts.push({contact_value: contact.value, contact_type_name: contact_type.name});
            });
        });
      });
    // });
  }

  getCandidateSkills(id: string) {
    this.candidateSkills = [];
    this.skillsCandidateService.getAllCandidatesSkills(id).then(
      skills => skills.relatedSkills.forEach(skill => {
        this.skillsService.getDetail(skill.skill_id).then(
          skillCandidate =>  this.candidateSkills.push(skillCandidate.name)
        );
      })
    );
  }
  
  getCandidates() {
    this.candidateService.getData().then(candidates => {
      this.candidates = candidates;
      this.selectedCandidate = candidates[0];
      this.getCandidateCity(this.selectedCandidate.location.toString());
      this.getCandidateContacts(this.selectedCandidate.id);
      this.getCandidateSkills(this.selectedCandidate.id);
    });
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-md'});
  }
 
  confirm(): void {
    this.candidateService.delete(this.selectedCandidate.id).then(item => {
      this.getCandidates();
    });
    this.modalRef.hide();
  }
 
  decline(): void {
    this.modalRef.hide();
  }

  openSendCustomerModal() {
    const initialState = {
      title: 'Отправить заказчику'
    };
    this.bsModalRef = this.modalService.show(SendCustomerModalComponent, {initialState});
    this.bsModalRef.content.selectedCandidateId = this.selectedCandidate.id;
  }

  openSendCandidateModal() {
    const initialState = {
      selectedCandidateId: this.selectedCandidate.id,
      selectedCandidate: this.selectedCandidate,
      title: 'Отправить кандидату'
    };
    this.bsModalRef = this.modalService.show(SendCandidateModalComponent, {initialState});
    this.bsModalRef.content.selectedCandidateId = this.selectedCandidate.id;
    this.bsModalRef.content.selectedCandidate = this.selectedCandidate;
  }

  openConnectCustomerModal() {
    const initialState = {
      selectedCandidateId: this.selectedCandidate.id,
      title: 'Связаться с заказчиком'
    };
    this.bsModalRef = this.modalService.show(ConnectCustomerModalComponent, {initialState});
    this.bsModalRef.content.selectedCandidateId = this.selectedCandidate.id;
  }

  processPhoto(photoInput: any) {
    const file: File = photoInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.photoInput.nativeElement.value = null;
      this.selectedPhoto = new ImageSnippet(event.target.result, file);
      this.selectedPhoto.pending = true;
      this.imageService.uploadPhoto(this.selectedPhoto.file, this.selectedCandidate.id).subscribe(
        (res) => {
          console.log(res);
          this.selectedCandidate.photo = res.json().photo;
        },
        (err) => {
          console.log(err);
        });
      }
    );

    reader.readAsDataURL(file);
  }

  removePhoto() {
    this.imageService.deletePhoto(this.selectedCandidate.id).subscribe(
      (res) => {
        this.selectedPhoto = undefined;
        this.selectedCandidate.photo = undefined;
        console.log(res);
      },
      (err) => {
        console.log(err);
      });
  }
  
  getCandidateDocuments(candidateId: string) {
    this.selectedCandidateDocument = undefined;
    this.fileService.getCandidateDocuments(candidateId).then(
      documentsCandidate => {
        this.candidateDocuments = documentsCandidate;
        if(!this.uploadedFile && this.candidateDocuments[0]){
          this.selectedCandidateDocument = this.candidateDocuments[0].file_name;
        } else if (this.uploadedFile){
          this.selectedCandidateDocument = this.uploadedFile.file.name;
          this.uploadedFile = undefined;
        }

      }
    )
  }

  previewDocument(event, filename: string) {
    event.preventDefault();
    this.selectedCandidateDocument = filename;
  }

  removeCandidateDocumentByName($event, fileName: string) {
    $event.preventDefault();
    this.fileService.getCandidateDocuments(this.selectedCandidate.id)
      .then(documentCandidate =>  {
        this.deletedCandidateDocument = documentCandidate.filter(doc => doc.file_name === fileName ).pop();
        this.fileService.deleteFileByName(this.selectedCandidate.id, this.deletedCandidateDocument.id).subscribe(
          (res) => {
            this.getCandidateDocuments(this.selectedCandidate.id); 
          },
          (err) =>{
            console.log(err);
          }
        );
      }
    );
  }

  processFile(fileInput: any) {
    
    const file: File = fileInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.fileInput.nativeElement.value = null;
      this.uploadedFile = new FileSnippet(event.target.result, file);

      this.uploadedFile.pending = true;

      this.fileService.uploadFile(this.uploadedFile.file, this.selectedCandidate.id, this.IS_DOCUMENT).subscribe(
        (res) => {
          this.selectedCandidateDocument = this.uploadedFile.file.name;
          this.getCandidateDocuments(this.selectedCandidate.id);
        },
        (err) => {
          console.log(err);
        });

    });

    reader.readAsDataURL(file);

  }


}
