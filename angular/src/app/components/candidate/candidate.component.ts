import { Component, OnInit } from '@angular/core';

import { CandidateService } from '../../services/candidate.service';

import {Candidate} from '../../models/candidate';

import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';


import { PagerService } from '../../services/pager.service';

import { map } from 'rxjs/operators';

@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.css']
})
export class CandidateComponent implements OnInit {

  candidates: Candidate[];

  constructor(
    private http: Http, private pagerService: PagerService,
    private candidateService: CandidateService,
  ) { }

  // array of all items to be paged
  private allItems: any[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  private candidateUrl = 'web/candidates?per-page=100000&page-count=44';

  ngOnInit() {
    this.getCandidates();
    this.http.get(this.candidateUrl).pipe(map((response: Response) => response.json()))
      .subscribe(data => {
        // set items to json response
        this.allItems = data;

        // initialize to page 1
        this.setPage(1);
      });
  }

  getCandidates() {
    this.candidateService.getData().then(candidates => this.candidates = candidates);
  }

  setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.allItems.length, page);

    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
