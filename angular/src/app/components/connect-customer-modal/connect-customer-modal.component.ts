import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CustomerService } from 'src/app/services/customer.service';
import { ContactsCustomerService } from 'src/app/services/contacts-customer.service';
import { FileService } from 'src/app/services/file.service';
import { MailService } from 'src/app/services/mail.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { ContactsCandidateService } from 'src/app/services/contacts-candidate.service';
import { NgForm } from '@angular/forms';

class FileSnippet {
  pending: boolean = false;
  status: string = 'init';
  constructor(public src: string, public file: File) {}
}
@Component({
  selector: 'app-connect-customer-modal',
  templateUrl: './connect-customer-modal.component.html',
  styleUrls: ['./connect-customer-modal.component.css']
})
export class ConnectCustomerModalComponent implements OnInit {

  title: string;
  closeBtnName: string;

  customers_names: string[] = [];
  selected_customer: string;
  selected_contact_name: any;
  customer_contacts_names: any;
  attached_documents: any;
  selected_attached_document: any;
  choosen_customer_contacts_values : any = [];
  selected_contact_value: any;
  selectedCandidateId: string;
  transformed_customer_contacts_names: any = [];
  customer_contacts_values: any = [];
  customer_letter_body : any;
  selectedAttachedFile: any;
  @ViewChild('fileInputDocument', {static: false}) fileInputDocument:ElementRef;
  readonly IS_DOCUMENT = 0;
  constructor(
    public bsModalRef: BsModalRef,
    private customerService: CustomerService,
    private contactsCustomerService: ContactsCustomerService,
    private fileService: FileService,
    private mailService: MailService,
    private contactsCandidateService: ContactsCandidateService,
  ) { }

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers() {
    this.customerService.getData().then(
      customers => {
        this.customers_names = customers.map(customer => customer.name);
        console.log(this.customers_names);
      }
    )
  }

  getCustomerIdByName(customerName: string, successCalback) {
    this.customerService.getData().then(
      customers => {
        successCalback(customers.find(customer => customer.name == customerName).id);
      }
    )
  }

  getAttachedDocuments() {
    this.fileService.getCandidateDocuments(this.selectedCandidateId).then(
      candidateDocuments => {
        this.attached_documents = candidateDocuments.map(candidateDocument => candidateDocument.file_name);
      }
    )
  }


  onSelect(event: TypeaheadMatch): void {
    this.getCustomerIdByName(event.item, (id) => {
      this.contactsCustomerService.getCustomerContacts(id).then(
        customerContacts => {
          customerContacts.forEach(customerContact => this.customer_contacts_values.push({value: customerContact.value,name: customerContact.contact_type}))
          
          this.customer_contacts_names = customerContacts.map(customerContact => customerContact.contact_type);
          this.customer_contacts_names.forEach(contact_name => this.contactsCandidateService.getCandidateContactTypeName(contact_name).then(
            contact => this.transformed_customer_contacts_names.push({name: contact.name, id: contact.id})
          ));
          this.getAttachedDocuments();
        });
    });
  }

  onSelectCommunication(event: TypeaheadMatch): void {
    this.choosen_customer_contacts_values = this.customer_contacts_values.filter(customerContactValue => customerContactValue.name == event.item.id);
  }

  onFormSubmit(sendCustomer: NgForm) {
    this.mailService.sendMail(
      this.selected_customer,
      undefined,
      this.selected_contact_name,
      this.selected_contact_value,
      this.selected_attached_document,
      this.customer_letter_body
      );
    this.bsModalRef.hide();
  }

  addDocument(fileInput: any) {
    const file: File = fileInput.files[0];
    const reader = new FileReader();
    reader.addEventListener('load', (event: any) => {
      this.fileInputDocument.nativeElement.value = null;
      this.selectedAttachedFile = new FileSnippet(event.target.result, file);
      this.fileService.uploadFile(this.selectedAttachedFile.file, this.selectedCandidateId, this.IS_DOCUMENT).subscribe(
        (res) => {
          this.getAttachedDocuments();
          console.log(res);
        },
        (err) => {
          console.log(err);
        });      
    });

    reader.readAsDataURL(file);
  }

}
