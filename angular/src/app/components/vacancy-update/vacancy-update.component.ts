import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {CKEditorComponent} from "@ckeditor/ckeditor5-angular";
import { NgForm } from '@angular/forms';
import { VacancyService } from 'src/app/services/vacancy.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { City } from 'src/app/models/city';
import {CitiesService} from "../../services/cities.service";
import { switchMap, map } from 'rxjs/operators';
import { ActivatedRoute, Params } from '@angular/router';
import { Skill } from 'src/app/models/skill';
import { SkillsService } from 'src/app/services/skills.service';
import { SkillsVacancy } from 'src/app/models/skillsVacancy';
import { SkillsVacancyService } from 'src/app/services/skills-vacancy.service';
import { EventData } from 'src/app/services/shared/event.class';
import { EventBusService } from 'src/app/services/shared/event-bus.service';
import { VacancyDocumentsService } from 'src/app/services/vacancy-documents.service';
class FileSnippet {
  pending: boolean = false;
  status: string = 'init';
  constructor(public src: string, public file: File) {}
}
@Component({
  selector: 'app-vacancy-update',
  templateUrl: './vacancy-update.component.html',
  styleUrls: ['./vacancy-update.component.css']
})
export class VacancyUpdateComponent implements OnInit {
  @ViewChild('fileInputDocument', {static: false}) fileInputDocument:ElementRef;
  public Editor = ClassicEditor;
  cities: City[];
  skills: Skill[];
  selectedSkills: Array<any> = Array();
  isSelectedSkill: boolean;
  skillsVacancy: SkillsVacancy[] = [];
  selectedVacancyId: string;
  selectedAttachedFile: any;
  constructor(
    private vacancyService: VacancyService,
    public bsModalRef: BsModalRef,
    private citiesService: CitiesService,
    private route: ActivatedRoute,
    private skillsService: SkillsService,
    private skillsVacancyService: SkillsVacancyService,
    private eventBusService: EventBusService,
    private vacancyDocumentService: VacancyDocumentsService,
  ) { }

  ngOnInit() {
    this.getCities();
    this.getSkills();
    this.getSkillsVacancies(this.selectedVacancyId);
  }

  getCities() {
    this.route.params.pipe(switchMap((params: Params) => this.citiesService.getData()))
      .subscribe( cities => this.cities = cities);
  }

  getSkills() {
    this.route.params.pipe(switchMap((params: Params) => this.skillsService.getData()))
      .subscribe( skills => {
        this.skills = skills;
      });
  }
  getSkillsVacancies(id) {
    this.route.params.pipe(switchMap((params: Params) => this.skillsVacancyService.getData(id)))
      .subscribe( skillsVacancy => {
        this.skillsVacancy = skillsVacancy;
        this.isSelectedSkill = true;
        const vacSkillIds = this.skillsVacancy.map(i => i.skill_id);
        this.skills.forEach(item => {
          if (vacSkillIds.includes(item.id)) {
            this.selectedSkills.push(item);
          }
        });
      });
    console.log(this.selectedSkills);
    console.dir(this.selectedSkills);
  }
  onFormSubmit(vacancyUpdate: NgForm) {
    console.log(vacancyUpdate.form.value);
    const vacancy = {
      id: vacancyUpdate.form.value.vacancyId,
      name: vacancyUpdate.form.value.vacancy_position,
      location: vacancyUpdate.form.value.location,
      salary: vacancyUpdate.form.value.vacancy_salary,
      v_duties: vacancyUpdate.form.value.duties,
      w_conditions: vacancyUpdate.form.value.conditions,
      v_requirements: vacancyUpdate.form.value.requirements,
      v_description: vacancyUpdate.form.value.description,
    };
    (async () => {
      await this.skillsVacancyService.removeAllSkillsVacancy(this.selectedVacancyId);
      await this.selectedSkills.forEach(skill => this.skillsVacancyService.addSkillVacancy({skill_id: skill.id, vacancy_id: this.selectedVacancyId}));    
      this.eventBusService.emit(new EventData('selectedVacancy', vacancy));
    })();
    this.vacancyService.update(vacancy);
  }

  addDocument(fileInput: any) {
    const file: File = fileInput.files[0];
    const reader = new FileReader();
    reader.addEventListener('load', (event: any) => {
      this.fileInputDocument.nativeElement.value = null;
      this.selectedAttachedFile = new FileSnippet(event.target.result, file);
      this.vacancyDocumentService.uploadFile(this.selectedAttachedFile.file, this.selectedVacancyId).subscribe(
        (res) => {
          // this.getAttachedDocuments();
          console.log(res);
        },
        (err) => {
          console.log(err);
        });      
    });

    reader.readAsDataURL(file);
  }

}
