import { Component, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { VacancyService } from 'src/app/services/vacancy.service';
import { Vacancy } from 'src/app/models/vacancy';
import { CitiesService } from 'src/app/services/cities.service';
import { CompanyService } from 'src/app/services/company.service';
import { EventBusService } from 'src/app/services/shared/event-bus.service';
import { EventData } from 'src/app/services/shared/event.class';

@Component({
  selector: 'app-vacancy-list',
  templateUrl: './vacancy-list.component.html',
  styleUrls: ['./vacancy-list.component.css']
})
export class VacancyListComponent implements OnInit, OnChanges {
  vacancies: Vacancy[] = [];
  selectedVacancy: Vacancy;
  constructor(
    private vacancyService: VacancyService,
    private citiesService: CitiesService,
    private companyService: CompanyService,
    private eventBusService: EventBusService,
    ) { }

  ngOnInit() {
    this.getVacancies();
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("vacancy list", changes);
    this.getVacancies();
  }

  getVacancies() {
    this.vacancyService.getData().then(
      vacancies => {
        this.vacancies = vacancies;
        this.vacancies.forEach(vacancy => {
          this.citiesService.getDetail(vacancy.location).then(
            city => {
              vacancy.city_name = city.name;
            }
          )
          this.companyService.getDetail(vacancy.company_id).then(
            company => {
              vacancy.company_name = company.name;
            }
          )
        })    
      }
    )
  }

  onSelect(vacancy: Vacancy) {
    this.selectedVacancy = vacancy;
    this.eventBusService.emit(new EventData('selectedVacancy',vacancy));
  }

}
