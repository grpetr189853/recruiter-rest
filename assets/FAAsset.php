<?php


namespace app\assets;

use yii\web\AssetBundle;

class FAAsset extends  AssetBundle
{
    public $sourcePath = '@bower/font-awesome';
    public $css = [
        'css/all.css',
    ];

}
