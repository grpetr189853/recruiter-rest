var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
        /***/ "./$$_lazy_route_resource lazy recursive": 
        /*!******************************************************!*\
          !*** ./$$_lazy_route_resource lazy namespace object ***!
          \******************************************************/
        /*! no static exports found */
        /***/ (function (module, exports) {
            function webpackEmptyAsyncContext(req) {
                // Here Promise.resolve().then() is used instead of new Promise() to prevent
                // uncaught exception popping up in devtools
                return Promise.resolve().then(function () {
                    var e = new Error("Cannot find module '" + req + "'");
                    e.code = 'MODULE_NOT_FOUND';
                    throw e;
                });
            }
            webpackEmptyAsyncContext.keys = function () { return []; };
            webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
            module.exports = webpackEmptyAsyncContext;
            webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html": 
        /*!**************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
          \**************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"top-menu-wrap\">\n  <div class=\"logo-wrap\">\n    <img src=\"/img/logo.png\" alt=\"\">\n  </div>\n  <div class=\"menu-right\">\n    <div class=\"menu-search\">\n      <a href=\"#\" class=\"\"><i class=\"fas fa-search\"></i></a>\n    </div>\n    <div class=\"menu-items\">\n      <i class=\"fas fa-envelope\"></i>\n      <i class=\"fas fa-calendar\"></i>\n      <a href=\"#\" class=\"\">\n        <i class=\"fas fas fa-sign-out-alt\"></i>\n      </a>\n    </div>\n    <div class=\"menu-auth\">\n      Привет, Brain!\n    </div>\n  </div>\n</div>\n\n<div class=\"container-fluid h100\">\n  <div class=\"main-container__wrap row h-100\">\n    <div class=\"main-nav-left border-right col-2 col-lg-2\" style=\"padding: 20px;\">\n      <div class=\"main-nav-item\">\n        <a [routerLink]=\"['/home']\">Home</a>\n      </div>\n      <div class=\"main-nav-item\">\n        <a [routerLink]=\"['/all-candidates']\">Candidates</a>\n      </div>\n      <div class=\"main-nav-item\">\n        <a [routerLink]=\"['/all-vacancies']\">Vacancies</a>\n      </div>\n      <div class=\"main-nav-item\">\n        <a [routerLink]=\"['/companies']\">Companies</a>\n      </div>\n      <div class=\"main-nav-item\">\n        <a [routerLink]=\"['/admin']\">Administration</a>\n      </div>\n      <div class=\"main-nav-item\">\n        <a [routerLink]=\"['/add-candidate']\">Add Candidate</a>\n      </div>\n      <div class=\"main-nav-item\">\n        <a [routerLink]=\"['/add-vacancy']\">Add Vacancy</a>\n      </div>\n    </div>\n    <div class=\"content col-10 col-lg-10\" style=\"\">\n      <div id=\"content\">\n<!--        <div class=\"container-fluid\">-->\n          <router-outlet></router-outlet>\n<!--        </div>-->\n      </div>\n    </div>\n  </div>\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/add-candidate/add-candidate.component.html": 
        /*!*************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/add-candidate/add-candidate.component.html ***!
          \*************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<form #candidateAddForm=\"ngForm\" (ngSubmit)=\"onFormSubmit(candidateAddForm)\">\n<div class=\"add-candidate container-fluid\">\n  <div class=\"add-candidate__panel-buttons p-3\">\n    <div class=\"row border-bottom\">\n      <div class=\"entity-details__buttons col-11 col-lg-11 pt-4 pl-0 pb-4\">\n        <div id=\"entity_buttons\" class=\"entity-buttons-wrapper text-left\">\n          <button type=\"submit\" id=\"addUserTest\" class=\"btn btn-crm-default\" [disabled]=\"isCandidateSaved\">СОХРАНИТЬ</button>\n          <button type=\"button\" class=\"btn btn-crm-default\" disabled=\"true\">НАЙТИ СООТВЕТСТВИЯ</button>\n          <button type=\"button\" class=\"btn btn-crm-default\" [routerLink]=\"['/all-candidates']\">ЗАКРЫТЬ</button>\n          <button type=\"button\" class=\"btn btn-crm-default\" [disabled]=\"!isCandidateSaved\" (click)=\"openModal(template)\">УДАЛИТЬ</button>\n        </div>\n      </div>\n    </div>\n  </div>\n  <br><br>\n  <ng-template #template>\n    <div class=\"modal-header text-center\">\n      <h5>Вы действительно хотите удалить кандидата?</h5>\n      <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" (click)=\"decline()\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n    </div>\n    <div class=\"modal-body text-center\">\n      <p>{{lastname | uppercase}} {{name | uppercase}} {{fathername | uppercase}}</p>\n      <p>{{company}} {{position}}</p>\n      <button type=\"button\" class=\"btn btn-sm btn-crm btn-crm-primary mr-2\" (click)=\"confirm()\" >Yes</button>\n      <button type=\"button\" class=\"btn btn-sm btn-crm btn-crm-default btn-crm-default_red\" (click)=\"decline()\" >No</button>\n    </div>\n  </ng-template>\n  <div class=\"add-candidate__wrap\">\n    <div class=\"row\">\n      <div class=\"col-6 col-lg-6 border-right\">\n        <div class=\"row\">\n          <div class=\"col-6 col-lg-6\">\n            <div class=\"add-candidate__input_wrap\">\n              <div class=\"form-group\">\n                <input class=\"form-control\" [(ngModel)]=\"lastname\" name=\"lastname\" placeholder=\"Фамилия\" />\n              </div>\n              <div class=\"form-group\">\n                <input class=\"form-control\" [(ngModel)]=\"name\" name=\"name\" placeholder=\"Имя\" />\n              </div>\n              <div class=\"form-group\">\n                <input class=\"form-control\" [(ngModel)]=\"fathername\" name=\"fathername\" placeholder=\"Отчество\" />\n              </div>\n              <div class=\"form-group\">\n                <select class=\"form-control\" name=\"location\"\n                        [(ngModel)]=\"location\" #city=\"ngModel\" required>\n                  <option [ngValue]=\"undefined\" disabled>Выберите город</option>\n                  <option  *ngFor=\"let city of cities\" [ngValue]=\"city.id\">\n                    {{city.name}}\n                  </option>\n                </select>\n              </div>\n\n              <div class=\"form-group\">\n                <input class=\"form-control\" [(ngModel)]=\"company\" name=\"company\" placeholder=\"Текущая компания\" />\n              </div>\n              <div class=\"form-group\">\n                <input class=\"form-control\" [(ngModel)]=\"position\" name=\"position\" placeholder=\"Текущая должность\" />\n              </div>\n              <div class=\"form-group\">\n                <input class=\"form-control\" [(ngModel)]=\"salary\" name=\"salary\" placeholder=\"Зарплата\" />\n              </div>\n              <div class=\"form-group\">\n                <input class=\"form-control\" [(ngModel)]=\"wish\" name=\"wish\" placeholder=\"Пожелания\" />\n              </div>\n              <mat-form-field>\n                <input matInput [matDatepicker]=\"picker1\" [(ngModel)]=\"date_birth\" name=\"date_birth\" placeholder=\"Дата рождения\">\n                <mat-datepicker-toggle matSuffix [for]=\"picker1\"></mat-datepicker-toggle>\n                <mat-datepicker #picker1></mat-datepicker>\n              </mat-form-field>\n\n              <div class=\"candidate-create__contacts_group form-group\" style=\"position: relative;\">\n                <input class=\"form-control\" #contact type=\"text\" [(ngModel)]=\"contacts[0]\" name=\"contact__0\" placeholder=\"Контакт\">\n                <i class=\"fas fa-plus-circle add_contact_input\" (click)=\"addContact(contact)\"\n                   style=\"position: absolute;top: 0; right: 0; padding: .7rem .75rem;\">\n                </i>\n              </div>\n              <div *ngFor=\"let element of contacts; let i = index; trackBy:trackByFn\">\n                <div class=\"\" *ngIf=\"i != 0\">\n                  <div class=\"candidate-create__contacts_group form-group\" style=\"position: relative;\">\n                    <input class=\"form-control\" type=\"text\" [(ngModel)]=\"contacts[i]\" name=\"contact__{{i}}\" placeholder=\"Контакт\">\n                    <i class=\"fas fa-minus-circle remove_contact_input\" (click)=\"removeContact(i)\"\n                      style=\"position: absolute;top: 0; right: 0; padding: .7rem .75rem;\">\n                    </i>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n\n<!--          candidate photo-->\n          <div class=\"col-6 col-lg-6\">\n            <div class=\"\" style=\"display: flex;justify-content: flex-end\">\n\n              <span class=\"empty-img entity-item-round\" *ngIf=\"!selectedPhoto\">\n                <i class=\"fas fa-user avatar\"></i>\n              </span>\n\n              <div *ngIf=\"selectedPhoto\" class=\"img-preview-container\">\n\n                <div class=\"empty-img entity-item-round img-preview{{selectedPhoto.status === 'fail' ? '-error' : ''}}\"\n                     [ngStyle]=\"{'background-image': 'url('+ selectedPhoto.src + ')'}\">\n                </div>\n\n              </div>\n\n<!--              <div *ngIf=\"!selectedPhoto\" class=\"img-preview-container\">-->\n\n<!--                <div class=\"empty-img entity-item-round img-preview\"-->\n<!--                     [ngStyle]=\"{'background-image': 'url('+ '/files/photo/'  + ')'}\">-->\n<!--                </div>-->\n\n<!--              </div>-->\n\n\n              <span class=\"three-dots\">\n              <div class=\"dropdown\">\n                <button class=\"btn btn-default\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\"\n                        aria-haspopup=\"true\" aria-expanded=\"false\">\n                  <span class=\"dot\"></span>\n                  <span class=\"dot\"></span>\n                  <span class=\"dot\"></span>\n                </button>\n\n                <div class=\"profile__dropdown-menu dropdown-menu dropdown-menu-right\" aria-labelledby=\"dropdownMenuButton\" x-placement=\"bottom-end\" style=\"position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-132px, 31px, 0px);\">\n                  <input id=\"addPhoto\" class=\"input-photo\" #photoInput type=\"file\" accept=\"image/*\" (change)=\"processPhoto(photoInput)\"><label for=\"addPhoto\" class=\"dropdown-item\"><i class=\"fas fa-plus\" style=\"color: #BDBDBD;border: 1px solid #BDBDBD; border-radius:50%; font-size: 10px;text-align: center \"></i><span>Добавить</span></label>\n                  <button class=\"dropdown-item\"><i class=\"fas fa-plus\" style=\"color: #BDBDBD;border: 1px solid #BDBDBD; border-radius:50%; font-size: 10px;text-align: center \"></i> Из CV</button>\n                  <button class=\"dropdown-item\" id=\"removePhoto\" (click)=\"removePhoto()\" type=\"button\"><i class=\"fas fa-minus\" style=\"color: #BDBDBD;border: 1px solid #BDBDBD; border-radius:50%; font-size: 10px;text-align: center \"></i> Удалить</button>\n                </div>\n\n              </div>\n            </span>\n            </div>\n          </div>\n<!--          candidate photo-->\n\n          <div class=\"col-12 \">\n            <div class=\"p3 border-bottom\"></div>\n            <div class=\"row\">\n              <div class=\"col-6 col-lg-6 candidate-create__skills-content\">\n                <div class=\"candidate-update__skills-label pt-3\">\n                  <h6>Навыки</h6>\n                </div>\n\n                <mat-form-field>\n                  <mat-select multiple (selectionChange)=\"addSkill($event)\" name=\"selectedSkills\">\n                    <mat-option  *ngFor=\"let skill of skills\" [value]=\"skill\">{{skill.name}}</mat-option>\n                  </mat-select>\n                </mat-form-field>\n\n              </div>\n            </div>\n          </div>\n\n          <div class=\"col-12\">\n            <div class=\"p3 border-bottom\"></div>\n            <div class=\"row\">\n              <div class=\"col-12 col-lg-12 candidate-create__comment-content\">\n                <div class=\"candidate-create__comment-label pt-3\">\n                  <h6>Комментарий</h6>\n                </div>\n                <ckeditor [editor]=\"Editor\" data=\"\" [(ngModel)]=\"notes\" name=\"notes\"></ckeditor>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <!--          candidate resume and documents-->\n      <div class=\"col-md-6\" style=\"padding: 40px;\">\n        <div class=\"row\">\n          <div class=\"col-12\">\n            <div class=\"row\">\n              <div class=\"col-6 pull-left\">\n                <h4>\n                  Резюме\n                </h4>\n              </div>\n              <div class=\"col-6 pull-right text-right\">\n                <input id=\"addResume\" class=\"input-resume\" #fileInputResume type=\"file\" accept=\"application/pdf\" (change)=\"processFile(fileInputResume, IS_RESUME)\" ><label for=\"addResume\" class=\"add-resume-label\"><i class=\"fas fa-plus-circle fa-add-resume\" aria-hidden=\"true\"></i><span>Добавить резюме</span></label>\n                <button class=\"remove-resume-button\" id=\"removeResume\" type=\"button\" (click)=\"removeFile()\" [disabled]=\"!(candidateResume || selectedFile)\"><i class=\"fas fa-minus-circle\"></i>Удалить резюме</button>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-12\" style=\"background: #f5f5f5; height: 80vh;\">\n            <div *ngIf=\"selectedFile\">\n              <ngx-extended-pdf-viewer [src]=\"selectedFile.src\" useBrowserLocale=\"true\" height=\"80vh\"></ngx-extended-pdf-viewer>\n            </div>\n            <div *ngIf=\"candidateResume?.length\">\n              <ngx-extended-pdf-viewer [src]=\"'/files/attachments/' + candidate.id + '/' + candidateResume[0].file_name\" useBrowserLocale=\"true\" height=\"80vh\"></ngx-extended-pdf-viewer>\n            </div>\n          </div>\n          <div class=\"col-12\">\n            <div class=\"row\">\n              <div class=\"col-6 pull-left\">\n                <h4>\n                  Документы\n                </h4>\n              </div>\n              <div class=\"col-6 pull-right text-right\">\n                <input id=\"addDocument\" class=\"input-document\" #fileInputDocument type=\"file\" accept=\"text/plain,application/pdf,application/vnd.oasis.opendocument.text,application/msword\" (change)=\"processFile(fileInputDocument, IS_DOCUMENT)\"><label for=\"addDocument\" class=\"add-document-label\"><i class=\"fas fa-plus-circle fa-add-document\" aria-hidden=\"true\"></i><span>Добавить документ</span></label>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-12\">\n\n            <div class=\"p3 border-bottom\"></div>\n\n            <div class=\"\" *ngFor=\"let doc of candidateDocuments; let i = index;\">\n              <div style=\"display: flex;justify-content: space-around;\">\n                <div style=\"display: flex; margin-right: auto;\">\n                  <div style=\"margin-right: 10px;\">\n                    <i class=\"fas fa-file-image-o\"></i>\n                  </div>\n                  <span>{{doc.file.name}}</span>\n                </div>\n                <div style=\"display: flex;\">\n                  <div style=\"margin-right: 10px;\">\n                    <a href=\"{{'/files/attachments/' + candidate.id + '/' + doc.file.name}}\" download=\"\"><i class=\"fas fa-download\"></i></a>\n                  </div>\n                  <div>\n                    <a href=\"#\" #deleteLink [attr.data-document-id]=\"i\" (click)=\"removeCandidateDocument($event, deleteLink)\"><i class=\"fas fa-trash-alt\"></i></a>\n                  </div>\n                </div>\n\n              </div>\n            </div>\n            <div class=\"p3 border-bottom\"></div>\n\n          </div>\n        </div>\n      </div>\n      <!--          candidate resume and documents-->\n\n    </div>\n  </div>\n</div>\n</form>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/add-vacancy/add-vacancy.component.html": 
        /*!*********************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/add-vacancy/add-vacancy.component.html ***!
          \*********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<form #vacancyAddForm=\"ngForm\" (ngSubmit)=\"onFormSubmit(vacancyAddForm)\">\n    <div class=\"add-vacancy container-fluid\">\n        <div class=\"add-vacancy__panel-buttons pt3\">\n            <div class=\"row border-bottom\">\n                <div class=\"entity-details__buttons col-11 col-lg-11 pt-4 pl-0 pb-4\">\n                    <div id=\"entity-buttons\" class=\"entity-buttons-wrapper text-left\">\n                        <button type=\"submit\" id=\"addVacancyTest\" class=\"btn btn-crm-default\" [disabled]=\"isVacancySaved\">СОХРАНИТЬ</button>\n                        <button type=\"button\" class=\"btn btn-crm-default\" [routerLink]=\"['/all-vacancies']\">ЗАКРЫТЬ</button>\n                        <button type=\"button\" class=\"btn btn-crm-default\" [disabled]=\"!isVacancySaved\" (click)=\"openModal(template)\">УДАЛИТЬ</button>                        \n                    </div>\n                </div>\n            </div>\n        </div>\n        <br><br>\n        <ng-template #template>\n          <div class=\"modal-header text-center\">\n            <h5>Вы действительно хотите удалить вакансию?</h5>\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" (click)=\"decline()\">\n              <span aria-hidden=\"true\">&times;</span>\n            </button>\n          </div>\n          <div class=\"modal-body text-center\">\n            <p>{{name | uppercase}}</p>\n            <p></p>\n            <button type=\"button\" class=\"btn btn-sm btn-crm btn-crm-primary mr-2\" (click)=\"confirm()\" >Yes</button>\n            <button type=\"button\" class=\"btn btn-sm btn-crm btn-crm-default btn-crm-default_red\" (click)=\"decline()\" >No</button>\n          </div>\n        </ng-template>        \n        <div class=\"add-vacancy__wrap\">\n            <div class=\"row\">\n                <div class=\"col-6 col-lg-6 border-right\">\n                    <div class=\"row\">\n                        <div class=\"col-6 col-lg-6\">\n                            <div class=\"add-vacancy__input_wrap\">\n                                <div class=\"form-group\">\n                                    <input class=\"form-control\" [(ngModel)]=\"name\" name=\"name\" placeholder=\"Название вакансии\" />\n                                </div>\n                                <div class=\"form-group\">\n                                  <select class=\"form-control\" name=\"company\"\n                                  [(ngModel)]=\"company\" #vacancyCompany=\"ngModel\" required>\n                                    <option [ngValue]=\"undefined\" disabled>Выберите компанию</option>\n                                    <option  *ngFor=\"let company of companies\" [ngValue]=\"company.id\">\n                                      {{company.name}}\n                                    </option>\n                                  </select>\n                                </div> \n                                <div class=\"form-group\">\n                                  <select class=\"form-control\" name=\"location\"\n                                  [(ngModel)]=\"location\" #city=\"ngModel\" required>\n                                    <option [ngValue]=\"undefined\" disabled>Выберите город</option>\n                                    <option  *ngFor=\"let city of cities\" [ngValue]=\"city.id\">\n                                      {{city.name}}\n                                    </option>\n                                  </select>\n                                </div>  \n                                <div class=\"form-group\">\n                                    <input class=\"form-control\" [(ngModel)]=\"salary\" name=\"salary\" placeholder=\"Зарплата\" />\n                                </div>   \n                                <div class=\"form-group\">\n\n                                  <mat-form-field>\n                                    <mat-select multiple (selectionChange)=\"addSkill($event)\" name=\"selectedSkills\">\n                                      <mat-option  *ngFor=\"let skill of skills\" [value]=\"skill\">{{skill.name}}</mat-option>\n                                    </mat-select>\n                                  </mat-form-field>\n\n                                </div>    \n                                                                                                                                                                                                                                                                                         \n                            </div>\n                        </div>\n                        <div class=\"col-12\">\n                          <div class=\"p3 border-bottom\"></div>\n                          <div class=\"row\">\n                            <div class=\"col-12 col-lg-12 candidate-create__comment-content\">\n                              <div class=\"candidate-create__comment-label pt-3\">\n                                <h6>Обязанности</h6>\n                              </div>\n                              <ckeditor [editor]=\"Editor\" data=\"\" [(ngModel)]=\"v_duties\" name=\"v_duties\"></ckeditor>\n                            </div>\n                          </div>\n                      </div>  \n                      <div class=\"col-12\">\n                          <div class=\"p3 border-bottom\"></div>\n                          <div class=\"row\">\n                            <div class=\"col-12 col-lg-12 candidate-create__comment-content\">\n                              <div class=\"candidate-create__comment-label pt-3\">\n                                <h6>Условия работы</h6>\n                              </div>\n                              <ckeditor [editor]=\"Editor\" data=\"\" [(ngModel)]=\"w_conditions\" name=\"w_conditions\"></ckeditor>\n                            </div>\n                          </div>\n                      </div> \n                      <div class=\"col-12\">\n                          <div class=\"p3 border-bottom\"></div>\n                          <div class=\"row\">\n                            <div class=\"col-12 col-lg-12 candidate-create__comment-content\">\n                              <div class=\"candidate-create__comment-label pt-3\">\n                                <h6>Требования</h6>\n                              </div>\n                              <ckeditor [editor]=\"Editor\" data=\"\" [(ngModel)]=\"v_requirements\" name=\"v_requirements\"></ckeditor>\n                            </div>\n                          </div>\n                      </div> \n                      <div class=\"col-12\">\n                          <div class=\"p3 border-bottom\"></div>\n                          <div class=\"row\">\n                            <div class=\"col-12 col-lg-12 candidate-create__comment-content\">\n                              <div class=\"candidate-create__comment-label pt-3\">\n                                <h6>Описание вакансии</h6>\n                              </div>\n                              <ckeditor [editor]=\"Editor\" data=\"\" [(ngModel)]=\"v_description\" name=\"v_description\"></ckeditor>\n                            </div>\n                          </div>\n                      </div>                        \n                    </div>\n                </div>\n                <div class=\"col-6 col-lg-6\">\n                  <div class=\"col-12\">\n                    <div class=\"row\">\n                      <div class=\"col-6 pull-left\">\n                        <h4>\n                          Документы\n                        </h4>\n                      </div>\n                      <div class=\"col-6 pull-right text-right\">\n                        <input id=\"addDocument\" class=\"input-document\" #fileInputDocument type=\"file\" accept=\"text/plain,application/pdf,application/vnd.oasis.opendocument.text,application/msword\" (change)=\"processFile(fileInputDocument)\"><label for=\"addDocument\" class=\"add-document-label\"><i class=\"fas fa-plus-circle fa-add-document\" aria-hidden=\"true\"></i><span>Добавить документ</span></label>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"col-12\">\n\n                    <div class=\"p3 border-bottom\"></div>\n        \n                    <div class=\"\" *ngFor=\"let doc of vacancyDocuments; let i = index;\">\n                      <div style=\"display: flex;justify-content: space-around;\">\n                        <div style=\"display: flex; margin-right: auto;\">\n                          <div style=\"margin-right: 10px;\">\n                            <i class=\"fas fa-file-image-o\"></i>\n                          </div>\n                          <span>{{doc.file.name}}</span>\n                        </div>\n                        <div style=\"display: flex;\">\n                          <div style=\"margin-right: 10px;\">\n                            <a href=\"{{'/files/attachments/' + vacancy.id + '/' + doc.file.name}}\" download=\"\"><i class=\"fas fa-download\"></i></a>\n                          </div>\n                          <div>\n                            <a href=\"#\" #deleteLink [attr.data-document-id]=\"i\" (click)=\"removeVacancyDocument($event, deleteLink)\"><i class=\"fas fa-trash-alt\"></i></a>\n                          </div>\n                        </div>\n        \n                      </div>\n                    </div>\n                    <div class=\"p3 border-bottom\"></div>\n        \n                  </div>\n\n                </div>\n            </div>\n        </div>\n    </div>  \n</form>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/all-candidates/all-candidates.component.html": 
        /*!***************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/all-candidates/all-candidates.component.html ***!
          \***************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<!--<div class=\"container-fluid\">-->\n  <div class=\"main-menu-top row pt-3 pb-2 border-bottom\">\n    <div class=\"col-12 \">\n      <div class=\"text-left\" style=\"float:left\">\n        <a class=\"\" ><i class=\"fas fa-home\"></i></a>\n\n        <a href=\"#\" (click)=\"selectCandidatesStatuses($event, STATUS_ALL)\">Все</a>\n        <a href=\"#\" (click)=\"selectCandidatesStatuses($event, STATUS_NEW)\">Новые</a>\n        <a href=\"#\" (click)=\"selectCandidatesStatuses($event, STATUS_CONSIDERED)\">На рассмотрении</a>\n        <a href=\"#\" (click)=\"selectCandidatesStatuses($event, STATUS_TODAY)\">Сегодня</a>\n        <a href=\"#\" (click)=\"selectCandidatesStatuses($event, STATUS_OFFER)\">Оффер</a>\n        <a href=\"#\" (click)=\"selectCandidatesStatuses($event, STATUS_SELECTED)\">Отобранные</a>\n        <a href=\"#\" (click)=\"selectCandidatesStatuses($event, STATUS_BLACKLISTED)\">Черный список</a>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-lg-12\">\n    <div class=\"row\">\n      <div class=\"col-lg-3\">\n        <app-search-candidate (search)=\"searchCandidate($event)\"></app-search-candidate>\n        <app-candidate-list [selected_statuses]=\"getStatuses()\" [searchString]=\"searchString\"></app-candidate-list>\n      </div>\n      <div class=\"col-lg-9\">\n        <app-candidate-info></app-candidate-info>\n      </div>  \n    </div>\n  </div>\n<!--</div>-->\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/all-vacancies/all-vacancies.component.html": 
        /*!*************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/all-vacancies/all-vacancies.component.html ***!
          \*************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-menu-top row pt-3 pb-2 border-bottom\"></div>\n<div class=\"col-lg-12\">\n    <div class=\"row\">\n      <div class=\"col-lg-3\">\n\n        <app-vacancy-list></app-vacancy-list>\n      </div>\n      <div class=\"col-lg-9\">\n        <app-vacancy-info></app-vacancy-info>\n      </div>  \n    </div>\n  </div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/candidate-info/candidate-info.component.html": 
        /*!***************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/candidate-info/candidate-info.component.html ***!
          \***************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"\" *ngIf=\"selectedCandidate\">\n    <div class=\"entity-details\">\n      <div class=\"row border-bottom\">\n        <div class=\"entity-details__buttons col-11 col-lg-11\">\n          <div class=\"entity-buttons-wrapper\">\n            <button class=\"btn btn-crm-default\" (click)=\"openSendCustomerModal()\">Отправить заказчику</button>\n            <button class=\"btn btn-crm-default\" (click)=\"openSendCandidateModal()\">Отправить</button>\n            <button class=\"btn btn-crm-default\" (click)=\"openConnectCustomerModal()\">Связаться с заказчиком</button>\n            <button class=\"btn btn-crm-default\" routerLink=\"/update/{{selectedCandidate.id}}\">Редактировать</button>\n            <button class=\"btn btn-crm-default\" (click)=\"openModal(template)\">Удалить</button>\n            <button class=\"btn btn-crm-default\">Черный список</button>\n          </div>\n        </div>\n      </div>\n      <br><br>\n      <ng-template #template>\n        <div class=\"modal-header text-center\">\n          <h5>Вы действительно хотите удалить кандидата?</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" (click)=\"decline()\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body text-center\">\n          <p>{{selectedCandidate.lastname | uppercase}} {{selectedCandidate.name | uppercase}} {{selectedCandidate.fathername | uppercase}}</p>\n          <p>{{selectedCandidate.company}} {{selectedCandidate.position}}</p>\n          <button type=\"button\" class=\"btn btn-sm btn-crm btn-crm-primary mr-2\" (click)=\"confirm()\" >Yes</button>\n          <button type=\"button\" class=\"btn btn-sm btn-crm btn-crm-default btn-crm-default_red\" (click)=\"decline()\" >No</button>\n        </div>\n      </ng-template>\n      <div class=\"profile-item row\">\n        <div class=\"col-9 col-md-9 col-lg-9\">\n          <div class=\"entity-name\">{{selectedCandidate.lastname | uppercase}} {{selectedCandidate.name | uppercase}} {{selectedCandidate.fathername | uppercase}}</div>\n          <div class=\"entity-sub\">{{selectedCandidate.company}} {{selectedCandidate.position}}</div>\n          <div class=\"entity-details\" style=\"min-height: 200px;\">\n            <div class=\"item-row\">\n              <span class=\"item-title\">\n                Зарплата:\n              </span>\n              <span class=\"item-value\">\n                {{selectedCandidate.salary}}\n              </span>\n            </div>\n            <div class=\"item-row\">\n              <span class=\"item-title\">\n                Город:\n              </span>\n              <span *ngIf=\"city\" class=\"item-value\">\n                {{city.name}}\n              </span>\n            </div>\n            <div class=\"item-row\" *ngFor=\"let selectedCandidateContact of candidateContacts\">\n              <span class=\"item-title\">\n                {{selectedCandidateContact.contact_type_name}}:\n              </span>\n              <span class=\"item-value\">\n                {{selectedCandidateContact.contact_value}}\n              </span>\n            </div>\n            <div class=\"item-row\">\n              <span class=\"item-title\">\n                Навыки:\n              </span>\n              <div class=\"item-value\" style=\"display: inline-block\">\n                <span class=\"skill\" *ngFor=\"let skill of candidateSkills\">\n                  {{skill}}\n                </span>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-3 col-md-3 col-lg-3\">\n            <!--            Candidate image-->\n          <div class=\"row\">\n            <div class=\"w-100 text-right\">\n              <img src=\"/files/photo/{{selectedCandidate.photo}}\" *ngIf=\"selectedCandidate.photo\" class=\"side-photo\" alt=\"\">\n              <span class=\"empty-img-big-placeholder entity-item-round-big-placeholder\" *ngIf=\"!selectedCandidate.photo\">\n                <i class=\"fas fa-user avatar\"></i>\n              </span>\n            </div>\n\n            <span class=\"three-dots\">\n              <div class=\"dropdown\">\n                <button class=\"btn btn-default\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\"\n                        aria-haspopup=\"true\" aria-expanded=\"false\">\n                  <span class=\"dot\"></span>\n                  <span class=\"dot\"></span>\n                  <span class=\"dot\"></span>\n                </button>\n\n                <div class=\"profile__dropdown-menu dropdown-menu dropdown-menu-right\" aria-labelledby=\"dropdownMenuButton\" x-placement=\"bottom-end\" style=\"position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-132px, 31px, 0px);\">\n                  <input id=\"addPhoto\" class=\"input-photo\" #photoInput type=\"file\" accept=\"image/*\" (change)=\"processPhoto(photoInput)\"><label for=\"addPhoto\" class=\"dropdown-item\"><i class=\"fas fa-plus\" style=\"color: #BDBDBD;border: 1px solid #BDBDBD; border-radius:50%; font-size: 10px;text-align: center \"></i><span>Добавить</span></label>\n                  <button class=\"dropdown-item\"><i class=\"fas fa-plus\" style=\"color: #BDBDBD;border: 1px solid #BDBDBD; border-radius:50%; font-size: 10px;text-align: center \"></i> Из CV</button>\n                  <button class=\"dropdown-item\" id=\"removePhoto\" (click)=\"removePhoto()\"><i class=\"fas fa-minus\" style=\"color: #BDBDBD;border: 1px solid #BDBDBD; border-radius:50%; font-size: 10px;text-align: center \"></i> Удалить</button>\n                </div>\n\n              </div>\n            </span>\n\n          </div>\n        </div>\n      </div>\n      <hr>\n      <div class=\"entity-stages-wrap\">\n        <ul id=\"w1\" class=\"nav nav-tabs\" role=\"tablist\">\n          <li class=\"stages nav-item\"><a class=\"nav-link active\" href=\"#w1-tab0\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"w1-tab0\" aria-selected=\"true\">Newage</a></li>\n          <li class=\"stages nav-item\"><a class=\"nav-link\" href=\"#w1-tab1\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"w1-tab1\" aria-selected=\"false\">E Team</a></li>\n          <li class=\"stages nav-item\"><a class=\"nav-link\" href=\"#w1-tab2\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"w1-tab2\" aria-selected=\"false\">Our well team</a></li>\n        </ul>\n        <div class=\"tab-content\">\n          <div id=\"w1-tab0\" class=\"stages tab-pane active\">\n            <div class=\"pl-4 pt-2\"><b>Этапы подбора</b><br>programmer</div><div class=\"pl-4 pr-4\"><hr>\n            </div>\n            <div class=\"pl-4 pr-4\"><b>Интервью с HR</b><br><small>2018/04/19</small><hr>\n            </div>\n            <div class=\"pl-4 pr-4\"><b>Взят в работу</b><br><small>2018/04/19</small><hr>\n            </div>\n          </div>\n          <div id=\"w1-tab1\" class=\"stages tab-pane\">\n            <div class=\"pl-4 pt-2\"><b>Этапы подбора</b><br>programmer</div>\n          </div>\n          <div id=\"w1-tab2\" class=\"stages tab-pane\">\n            <div class=\"pl-4 pt-2\"><b>Этапы подбора</b><br>programmer</div>\n          </div>\n        </div>\n      </div>\n      <div class=\"entity-notes\">\n        <br>\n        <ul id=\"w2\" class=\"nav nav-tabs\" role=\"tablist\">\n          <li class=\"nav-item\"><a class=\"nav-link\" href=\"#w2-tab0\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"w2-tab0\" aria-selected=\"false\">Заметки</a></li>\n          <li class=\"nav-item\"><a class=\"nav-link active\" href=\"#w2-tab1\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"w2-tab1\" aria-selected=\"true\">Документы</a></li></ul>\n          <div class=\"tab-content\"><div id=\"w2-tab0\" class=\"tab-pane\">Заметки\n            <ckeditor [editor]=\"Editor\" data=\"\" [(ngModel)]=\"selectedCandidate.notes\" name=\"notes\"></ckeditor>\n          </div>\n          <div id=\"w2-tab1\" class=\"tab-pane active\">\n            <div class=\"\" style=\"display: flex;\">\n              <div class='pt-3 buttons-left'>\n                <a href=\"#\">Распечатать</a>\n                <a href=\"{{'/files/attachments/' + selectedCandidate.id + '/' + selectedCandidateDocument}}\" download=\"\" >Скачать</a>\n                <a href=\"{{'/files/attachments/' + selectedCandidate.id + '/' +  selectedCandidateDocument}}?forcedownload=1\">Сохранить</a>\n                <a href=\"#\" (click)=\"removeCandidateDocumentByName($event,selectedCandidateDocument)\">Удалить</a>\n                <button class=\"btn btn-link btn-file btn-sm\" type=\"button\">\n                  Прикрепить\n                  <input #fileInput\n                         type=\"file\" \n                         accept=\"application/pdf\"\n                         (change)=\"processFile(fileInput)\">\n                </button>\n                \n              </div>\n              <div class=\"buttons-right\">\n                <a class=\"dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                  Документы\n                </a>\n                <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">\n                  <div class=\"document__dropdown-menu border-bottom\" *ngFor=\"let doc of candidateDocuments\" style=\"display: flex; padding: 10px;\">\n                    <a class=\"document__dropdown-item dropdown-item\" href=\"#\" (click)=\"previewDocument($event, doc.file_name)\"><i class=\"fas fa-file-image-o\"></i>{{doc.file_name}}</a>\n                    <a href=\"{{'/files/attachments/' + selectedCandidate.id + '/' +  doc.file_name}}\" download=\"\"><i class=\"fas fa-download\"></i></a>\n                    <a href=\"#\" (click)=\"removeCandidateDocumentByName($event,doc.file_name)\"><i class=\"fas fa-trash-alt\"></i></a>\n                  </div>\n                </div>\n              </div>  \n            </div>\n            <hr>\n          <div class=\"row\">\n            <div class=\"col-lg-12\">\n              <div class=\"col-lg-9\">\n                  <div class=\"\" *ngIf=\"selectedCandidateDocument\">\n                    <ngx-extended-pdf-viewer [src]=\"'/files/attachments/' +  selectedCandidate.id + '/' + selectedCandidateDocument\" useBrowserLocale=\"true\" height=\"80vh\"></ngx-extended-pdf-viewer>\n                  </div>\n                  <div class=\"\" style=\"height: 80vh;background: #f5f5f5;\" *ngIf=\"!selectedCandidateDocument\">\n\n                  </div>\n              </div>\n              <div class=\"col-lg-3\">\n  \n              </div>  \n          </div>\n          </div>\n          ...\n          </div>\n        </div>\n      </div>\n\n    </div>\n\n  </div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/candidate-list/candidate-list.component.html": 
        /*!***************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/candidate-list/candidate-list.component.html ***!
          \***************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"candidates-list\">\n  <ul class=\"candidates\">\n    <li *ngFor=\"let candidate of candidates | searchPipe:searchString:selected_statuses|selectedCandidate\"\n        [class.selected]=\"candidate === selectedCandidate\"\n        (click)=\"onSelect(candidate)\"\n        class=\"entity_list__item\">\n      <div class=\"float-left\" style=\"margin-left: 10px;\">\n        <span class=\"entity-item-round\" *ngIf=\"!candidate.photo\"><i class=\"fas fa-user\"></i></span>\n        <div class=\"\" *ngIf=\"candidate.photo\"><img src=\"/files/photo/{{candidate.photo}}\" alt=\"\" class=\"entity-item-round\"></div>\n      </div>\n      <div class=\"list-item-data\" style=\"margin-left: 50px;\">\n        <div class=\"entity-title\">\n          <a><span>{{candidate.lastname}} {{candidate.name}} {{candidate.fathername}}</span></a></div>\n        <div class=\"entity-sub\">\n          <span>{{candidate.position}}</span>\n        </div>\n        <div class=\"entity-company\">\n          <span>{{candidate.company}}</span>\n        </div>\n        <div class=\"location\">\n          <span>{{candidate.location}}</span>\n        </div>\n        <div class=\"skills\">\n\n        </div>\n      </div>\n\n    </li>\n  </ul>\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/candidate-update/candidate-update.component.html": 
        /*!*******************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/candidate-update/candidate-update.component.html ***!
          \*******************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<ng-container *ngIf=\"candidate\">\n  <form #candidateUpdateForm=\"ngForm\" (ngSubmit)=\"onFormSubmit(candidateUpdateForm)\">\n<div class=\"candidate-update container-fluid\">\n  <div class=\"candidate-update__panel-buttons p-3\">\n    <div class=\"row border-bottom\">\n      <div class=\"entity-details__buttons col-11 col-lg-11 pt-4 pl-0 pb-4\">\n        <div id=\"entity_buttons\" class=\"entity-buttons-wrapper text-left\">\n          <button type=\"submit\" id=\"updateUserTest\" class=\"btn btn-crm-default\">СОХРАНИТЬ</button>\n          <button type=\"button\" class=\"btn btn-crm-default\" [routerLink]=\"['/all-candidates']\">ЗАКРЫТЬ</button>\n          <button type=\"button\" class=\"btn btn-crm-default\" (click)=\"openModal(template)\">УДАЛИТЬ</button>\n        </div>\n      </div>\n    </div>\n  </div>\n  <br><br>\n  <ng-template #template>\n    <div class=\"modal-header text-center\">\n      <h5>Вы действительно хотите удалить кандидата?</h5>\n      <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" (click)=\"decline()\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n    </div>\n    <div class=\"modal-body text-center\">\n      <p>{{candidate.lastname | uppercase}} {{candidate.name | uppercase}} {{candidate.fathername | uppercase}}</p>\n      <p>{{candidate.company}} {{candidate.position}}</p>\n      <button type=\"button\" class=\"btn btn-sm btn-crm btn-crm-primary mr-2\" (click)=\"confirm()\" >Yes</button>\n      <button type=\"button\" class=\"btn btn-sm btn-crm btn-crm-default btn-crm-default_red\" (click)=\"decline()\" >No</button>\n    </div>\n  </ng-template>\n  <div class=\"candidate-update__wrap\">\n    <div class=\"row\">\n      <div class=\"col-6  col-lg-6 border-right\">\n        <div class=\"row\">\n          <div class=\"col-6 col-lg-6\">\n            <div class=\"candidate-update__input_wrap\">\n              <div class=\"form-group\">\n                <input class=\"form-control\" [(ngModel)]=\"candidate.lastname\" name=\"lastname\" placeholder=\"Фамилия\" />\n              </div>\n              <div class=\"form-group\">\n                <input class=\"form-control\" [(ngModel)]=\"candidate.name\" name=\"name\" placeholder=\"Имя\" />\n              </div>\n              <div class=\"form-group\">\n                <input class=\"form-control\" [(ngModel)]=\"candidate.fathername\" name=\"fathername\" placeholder=\"Отчество\" />\n              </div>\n              <div class=\"form-group\">\n                <select class=\"form-control\" name=\"location\"\n                        [(ngModel)]=\"candidate.location\" #city=\"ngModel\">\n                  <option  *ngFor=\"let city of cities\" [value]=\"city.id\">\n                    {{city.name}}\n                  </option>\n                </select>\n              </div>\n              <div class=\"form-group\">\n                <input class=\"form-control\" [(ngModel)]=\"candidate.company\" name=\"company\" placeholder=\"Компания\" />\n              </div>\n              <div class=\"form-group\">\n                <input class=\"form-control\" [(ngModel)]=\"candidate.position\" name=\"position\" placeholder=\"Должность\" />\n              </div>\n              <div class=\"form-group\">\n                <input class=\"form-control\" [(ngModel)]=\"candidate.salary\" name=\"salary\" placeholder=\"Зарплата\" />\n              </div>\n\n              <mat-form-field>\n                <input matInput [matDatepicker]=\"picker1\" [(ngModel)]=\"candidate.date_birth\" name=\"date_birth\" placeholder=\"Дата рождения\">\n                <mat-datepicker-toggle matSuffix [for]=\"picker1\"></mat-datepicker-toggle>\n                <mat-datepicker #picker1></mat-datepicker>\n              </mat-form-field>\n\n            </div>\n          </div>\n\n          <div class=\"col-6 col-lg-6\">\n            <div class=\"\" style=\"display: flex;justify-content: flex-end\">\n\n              <span class=\"empty-img entity-item-round\" *ngIf=\"!selectedPhoto && !candidate.photo || isPhotoDeleted\">\n                <i class=\"fas fa-user avatar\"></i>\n              </span>\n\n              <div *ngIf=\"selectedPhoto\" class=\"img-preview-container\">\n\n                <div class=\"empty-img entity-item-round img-preview{{selectedPhoto.status === 'fail' ? '-error' : ''}}\"\n                     [ngStyle]=\"{'background-image': 'url('+ selectedPhoto.src + ')'}\">\n                </div>\n\n              </div>\n\n              <div *ngIf=\"!selectedPhoto && candidate.photo\" class=\"img-preview-container\">\n\n                <div class=\"empty-img entity-item-round img-preview\"\n                     [ngStyle]=\"{'background-image': 'url('+ '/files/photo/' + candidate.photo + ')'}\">\n                </div>\n\n              </div>\n\n\n              <span class=\"three-dots\">\n              <div class=\"dropdown\">\n                <button class=\"btn btn-default\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\"\n                        aria-haspopup=\"true\" aria-expanded=\"false\">\n                  <span class=\"dot\"></span>\n                  <span class=\"dot\"></span>\n                  <span class=\"dot\"></span>\n                </button>\n\n                <div class=\"profile__dropdown-menu dropdown-menu dropdown-menu-right\" aria-labelledby=\"dropdownMenuButton\" x-placement=\"bottom-end\" style=\"position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-132px, 31px, 0px);\">\n                  <input id=\"addPhoto\" class=\"input-photo\" #photoInput type=\"file\" accept=\"image/*\" (change)=\"processPhoto(photoInput)\"><label for=\"addPhoto\" class=\"dropdown-item\"><i class=\"fas fa-plus\" style=\"color: #BDBDBD;border: 1px solid #BDBDBD; border-radius:50%; font-size: 10px;text-align: center \"></i><span>Добавить</span></label>\n                  <button class=\"dropdown-item\"><i class=\"fas fa-plus\" style=\"color: #BDBDBD;border: 1px solid #BDBDBD; border-radius:50%; font-size: 10px;text-align: center \"></i> Из CV</button>\n                  <button class=\"dropdown-item\" id=\"removePhoto\" (click)=\"removePhoto()\"><i class=\"fas fa-minus\" style=\"color: #BDBDBD;border: 1px solid #BDBDBD; border-radius:50%; font-size: 10px;text-align: center \"></i> Удалить</button>\n                </div>\n\n              </div>\n            </span>\n            </div>\n          </div>\n\n          <div class=\"col-12 \">\n            <div class=\"p3 border-bottom\"></div>\n            <div class=\"row\">\n              <div class=\"col-6 col-lg-6 candidate-create__skills-content\">\n                <div class=\"candidate-update__skills-label pt-3\">\n                  <h6>Навыки</h6>\n                </div>\n\n                <mat-form-field  *ngIf=\"isSelectedSkill\">\n                  <mat-select multiple [(ngModel)]=\"selectedSkills\" [value]=\"selectedSkills\" [compareWith]=\"comparer\" name=\"selectedSkills\">\n                    <mat-option  *ngFor=\"let skill of skills\" [value]=\"skill\">{{skill.name}}</mat-option>\n                  </mat-select>\n                </mat-form-field>\n\n              </div>\n            </div>\n          </div>\n\n          <div class=\"col-12\">\n            <div class=\"p3 border-bottom\"></div>\n            <div class=\"row\">\n              <div class=\"col-12 col-lg-12 candidate-create__comment-content\">\n                <div class=\"candidate-create__comment-label pt-3\">\n                  <h6>Комментарий</h6>\n                </div>\n                <ckeditor [editor]=\"Editor\" data=\"\" [(ngModel)]=\"candidate.notes\" name=\"notes\"></ckeditor>\n              </div>\n            </div>\n          </div>\n\n        </div>\n      </div>\n      <div class=\"col-md-6\" style=\"padding: 40px;\">\n        <div class=\"row\">\n          <div class=\"col-12\">\n            <div class=\"row\">\n              <div class=\"col-6 pull-left\">\n                <h4>\n                  Резюме\n                </h4>\n              </div>\n              <div class=\"col-6 pull-right text-right\">\n                <input id=\"addResume\" class=\"input-resume\" #fileInputResume type=\"file\" accept=\"application/pdf\" (change)=\"processFile(fileInputResume, IS_RESUME)\" [attr.disabled]=\"(selectedFile||candidateResume)? '' : null\"><label for=\"addResume\" class=\"add-resume-label\"><i class=\"fas fa-plus-circle fa-add-resume\" aria-hidden=\"true\"></i><span>Добавить резюме</span></label>\n                <button class=\"remove-resume-button\" id=\"removeResume\" (click)=\"removeFile($event)\" [disabled]=\"!(candidateResume || selectedFile)\"><i class=\"fas fa-minus-circle\"></i>Удалить резюме</button>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-12\" style=\"background: #f5f5f5; height: 80vh;\">\n            <div *ngIf=\"selectedFile\">\n              <ngx-extended-pdf-viewer [src]=\"'/files/attachments/' + candidate.id + '/' + selectedFile.file.name\" useBrowserLocale=\"true\" height=\"80vh\"></ngx-extended-pdf-viewer>\n            </div>\n            <div *ngIf=\"candidateResume?.length\">\n              <ngx-extended-pdf-viewer [src]=\"'/files/attachments/' + candidate.id + '/' +  candidateResume[0].file_name\" useBrowserLocale=\"true\" height=\"80vh\"></ngx-extended-pdf-viewer>\n            </div>\n          </div>\n          <div class=\"col-12\">\n            <div class=\"row\">\n              <div class=\"col-6 pull-left\">\n                <h4>\n                  Документы\n                </h4>\n              </div>\n              <div class=\"col-6 pull-right text-right\">\n                <input id=\"addDocument\" class=\"input-document\" #fileInputDocument type=\"file\" accept=\"text/plain,application/pdf,application/vnd.oasis.opendocument.text,application/msword\" (change)=\"processFile(fileInputDocument, IS_DOCUMENT)\"><label for=\"addDocument\" class=\"add-document-label\"><i class=\"fas fa-plus-circle fa-add-document\" aria-hidden=\"true\"></i><span>Добавить документ</span></label>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-12\">\n\n              <div class=\"p3 border-bottom\"></div>\n\n              <div class=\"\" *ngFor=\"let doc of candidateDocuments\">\n                <div style=\"display: flex;justify-content: space-around;\">\n                  <div style=\"display: flex; margin-right: auto;\">\n                    <div style=\"margin-right: 10px;\">\n                      <i class=\"fas fa-file-image-o\"></i>\n                    </div>\n                    <span>{{doc.file_name}}</span>\n                  </div>\n                  <div style=\"display: flex;\">\n                    <div style=\"margin-right: 10px;\">\n                      <a href=\"{{'/files/attachments/' + doc.file_name}}\" download=\"\"><i class=\"fas fa-download\"></i></a>\n                    </div>\n                    <div>\n                      <a href=\"#\" (click)=\"removeCandidateDocumentByName($event,doc.file_name)\"><i class=\"fas fa-trash-alt\"></i></a>\n                    </div>\n                  </div>\n\n                </div>\n              </div>\n              <div class=\"p3 border-bottom\"></div>\n\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</div>\n  </form>\n</ng-container>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/candidate/candidate.component.html": 
        /*!*****************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/candidate/candidate.component.html ***!
          \*****************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<!--<div class=\"container\">-->\n<!--  <div class=\"row\">-->\n    <!-- items being paged -->\n    <div class=\"item-wrap\" *ngFor=\"let item of pagedItems\">\n      <div class=\"text-center \">\n        <div class=\"card mb-5 item-card \" style=\"\">\n          <img *ngIf=\"item.photo\" src = \"/files/photo/{{item.photo}}\" style=\"width:200px;height:200px;\" class=\"card-img-top\">\n          <span *ngIf=\"!item.photo\" class=\"entity-item-round\"><i class=\"fas fa-user\"></i></span>\n          <!--      <div *ngFor=\"let candidate of candidates\" class=\"col-xs-6 col-md-2\">-->\n          <div class=\"card-body\">\n            <h5 class=\"card-title\"><span><b>{{item.position}}</b></span><br><span>{{item.lastname}}</span><br><span>{{item.name}}</span><br><span>{{item.fathername}}</span></h5>\n            <a routerLink=\"/detail/{{item.id}}\" class=\"btn btn-primary\">\n              Детально о кандидате\n            </a>\n          </div>\n        </div>\n      </div>\n    </div>\n<!--  </div>-->\n  <div class=\"container\">\n    <div class=\"row\">\n      <!-- pager -->\n      <ul *ngIf=\"pager.pages && pager.pages.length\" class=\"pagination\">\n        <li [ngClass]=\"{disabled:pager.currentPage === 1}\">\n          <a (click)=\"setPage(1)\" class=\"page-link\">First</a>\n        </li>\n        <li [ngClass]=\"{disabled:pager.currentPage === 1}\">\n          <a (click)=\"setPage(pager.currentPage - 1)\" class=\"page-link\">Previous</a>\n        </li>\n        <li *ngFor=\"let page of pager.pages\" [ngClass]=\"{active:pager.currentPage === page}\">\n          <a (click)=\"setPage(page)\" class=\"page-link\">{{page}}</a>\n        </li>\n        <li [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n          <a (click)=\"setPage(pager.currentPage + 1)\" class=\"page-link\">Next</a>\n        </li>\n        <li [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n          <a (click)=\"setPage(pager.totalPages)\" class=\"page-link\">Last</a>\n        </li>\n      </ul>\n    </div>\n  </div>\n<!--</div>-->\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/connect-customer-modal/connect-customer-modal.component.html": 
        /*!*******************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/connect-customer-modal/connect-customer-modal.component.html ***!
          \*******************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"modal-dialog\" style=\"margin: auto;\">\n\t<div class=\"modal-content\">\n\t\t<div class=\"modal-header\">\n\t\t\t<h5 id=\"customer-label\" class=\"modal-title\">СВЯЗАТЬСЯ С ЗАКАЗЧИКОМ</h5>\n\t\t\t<button type=\"button\" class=\"close pull-right\"  aria-label=\"Close\" data-dismiss=\"modal\" (click)=\"bsModalRef.hide()\"><span aria-hidden=\"true\">&times;</span></button>\t\t\t\n\t\t</div>\n\t\t<div class=\"modal-body\">\n\t\t\t<div class=\"customer-connect\">\n\t\t\t\t<form action=\"\" id=\"formcustomerconnect\" #connectCustomer=\"ngForm\" (ngSubmit)=\"onFormSubmit(connectCustomer)\">\n\t\t\t\t\t<div class=\"customer-connect__wrap\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-6 col-lg-6\">\n\t\t\t\t\t\t\t\t<div class=\"customer-connect__input-wrap\">\n\t\t\t\t\t\t            <div class=\"customer-connect_customer-label pt-3\">\n\t\t\t\t\t\t              <h6>Заказчик</h6>\n\t\t\t\t\t\t            </div>\n\t\t\t\t\t\t\t\t\t<div class=\"form-group field-customerContact\">\n\t\t\t\t\t\t\t            <input [(ngModel)]=\"selected_customer\"\n\t\t\t\t\t\t\t            [typeahead]=\"customers_names\"\n\t\t\t\t\t\t\t            [typeaheadMinLength]=\"0\"\n\t\t\t\t\t\t\t\t\t\t(typeaheadOnSelect)=\"onSelect($event)\"\n\t\t\t\t\t\t\t\t\t\tname=\"selected_customer\"\n\t\t\t\t\t\t\t            class=\"form-control\">\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t            <div class=\"customer-connect_contact-label pt-3\">\n\t\t\t\t\t\t              <h6>Коммуникация</h6> \n\t\t\t\t\t\t            </div>\t\t\t\t\t\t            \t\t\t\t\t\t            \t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<div class=\"form-group field-customerAddress\">\n\t\t\t\t\t\t\t\t\t\t<input [(ngModel)]=\"selected_contact_name\"\n\t\t\t\t\t\t\t\t\t\t[typeahead]=\"transformed_customer_contacts_names\"\n\t\t\t\t\t\t\t\t\t\t[typeaheadMinLength]=\"0\"\n\t\t\t\t\t\t\t\t\t\t(typeaheadOnSelect)=\"onSelectCommunication($event)\"\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\ttypeaheadOptionField=\"name\"\n\t\t\t\t\t\t\t\t\t\tname=\"selected_contact_name\"\n\t\t\t\t\t\t\t\t\t\tclass=\"form-control\">\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"candidate-send_contact-label pt-3\">\n\t\t\t\t\t\t\t\t\t    <h6>Адрес</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t                <div class=\"form-group field-customerAddress\">\n\t\t\t\t\t                  <input [(ngModel)]=\"selected_contact_value\"\n\t\t\t\t\t                  [typeahead]=\"choosen_customer_contacts_values\"\n\t\t\t\t\t                  [typeaheadMinLength]=\"0\"\n\t\t\t\t\t                  typeaheadOptionField=\"value\"\n\t\t\t\t\t                  name=\"selected_contact_value\"\n\t\t\t\t\t                  class=\"form-control\">\n\t\t\t\t\t                </div>\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<div class=\"customer-connect_contact-label pt-3\">\n\t\t\t\t\t\t\t\t\t    <h6>Прикрепить документ</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t                <div class=\"form-group field-customerDocument\">\n\t\t\t\t\t                  <input [(ngModel)]=\"selected_attached_document\"\n\t\t\t\t\t                  [typeahead]=\"attached_documents\"\n\t\t\t\t\t                  [typeaheadMinLength]=\"0\"\n\t\t\t\t\t                  name=\"selected_attached_document\"\n\t\t\t\t\t                  class=\"form-control\"> \n\t\t\t\t\t                </div>\n\t\t\t\t\t                <div class=\"col-12\">\n\t\t\t\t\t                    <div class=\"row clearfix\">\n\t\t\t\t\t                      <div class=\"pull-right text-right\">\n\t\t\t\t\t                          <input id=\"addDocument\" class=\"input-document\" #fileInputDocument type=\"file\" accept=\"application/pdf\" (change)=\"addDocument(fileInputDocument)\"><label for=\"addDocument\" class=\"add-document-label\"><i class=\"fas fa-plus-circle fa-add-document\" aria-hidden=\"true\"></i><span>Добавить документ</span></label>\n\t\t\t\t\t                      </div>\n\t\t\t\t\t                    </div>\n\t\t\t\t\t                </div>\t\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-6 col-lg-6\">\n\t\t\t\t                <div class=\"\" style=\"display: flex; justify-content: flex-end;\">\n\t\t\t\t                    <div class=\"customer-connect__comment-content\">\n\t\t\t\t                        <div class=\"customer-connect__comment-label\">\n\t\t\t\t                            <h6>Сопроводительное письмо</h6>\n\t\t\t\t                        </div>\n\t\t\t\t                        <div class=\"form-group field-customerLetter\">\n\t\t\t\t                            <textarea id=\"customerLetter\" [(ngModel)]=\"customer_letter_body\" class=\"form-control\" name=\"CreateSendForm[letter]\" rows=\"6\"></textarea>\n\t\t\t\t                        </div>                    \n\t\t\t\t                    </div>\n\t\t\t\t                </div>\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"row pt-3\" style=\"justify-content:center;\">\n\t\t\t\t\t\t    <button type=\"submit\" class=\"btn btn-sm btn-crm btn-crm-primary mr-2\">ОТПРАВИТЬ</button>\n\t\t\t\t\t\t    <button type=\"button\" class=\"btn btn-sm btn-crm btn-crm-default btn-crm-default_red\" data-dismiss=\"modal\" (click)=\"bsModalRef.hide()\">ОТМЕНA</button>\n\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/detail/detail.component.html": 
        /*!***********************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/detail/detail.component.html ***!
          \***********************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-6\" style = \"padding-top: 50px;\">\n      <div class=\"panel panel-default\">\n        <div class=\"panel-body\">\n          <p>\n            <img src = \"/files/photo/{{candidate?.photo}}\" onerror=\"this.src='files/photo/index1.png';\" style=\"width:200px;height:200px;\">\n          </p>\n          <span>{{ candidate?.date_created }}</span><br>\n          <span>{{ candidate?.date_birth }}</span><br>\n          <b>Фамилия:</b><span>{{ candidate?.lastname }}</span><br>\n          <b>Имя:</b><span>{{ candidate?.name }}</span><br>\n          <b>Отчество:</b><span>{{ candidate?.fathername }}</span><br>\n          <b>Компания:</b><span>{{ candidate?.company }}</span><br>\n          <b>Должность:</b><span>{{ candidate?.position }}</span><br>\n          <b>Зарплата:</b><span>{{ candidate?.salary }}</span><br>\n          <p>\n            <a class=\"btn btn-default\" [routerLink]=\"['web/candidates']\">back</a>\n          </p>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-md-6\"></div>\n  </div>\n</div>\n\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/search-candidate/search-candidate.component.html": 
        /*!*******************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/search-candidate/search-candidate.component.html ***!
          \*******************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"candidate-search entity-search border-bottom p-0 pt-4 pb-4\">\n    <input #input id=\"searchCandidate\" class=\"w-100\" type=\"text\" placeholder=\"ПОИСК КАНДИДАТА\" name=\"search\" [(ngModel)]=\"searchString\">\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/send-candidate-modal/send-candidate-modal.component.html": 
        /*!***************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/send-candidate-modal/send-candidate-modal.component.html ***!
          \***************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"modal-dialog\" style=\"margin: auto;\">\n\t<div class=\"modal-content\">\n\t\t<div class=\"modal-header\">\n\t\t\t<h5 id=\"customer-label\" class=\"modal-title\">ОТПРАВИТЬ КАНДИДАТУ</h5>\n\t\t\t<button type=\"button\" class=\"close pull-right\"  aria-label=\"Close\" data-dismiss=\"modal\" (click)=\"bsModalRef.hide()\"><span aria-hidden=\"true\">&times;</span></button>\n\t\t</div>\n\t\t<div class=\"modal-body\">\n\t\t\t<div class=\"candidate-send\">\n\t\t\t\t<form action=\"\" id=\"formcandidate\" #sendCandidate=\"ngForm\" (ngSubmit)=\"onFormSubmit(sendCandidate)\">\n\t\t\t\t\t<div class=\"candidate-send__wrap\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-6 col-lg-6\">\n\t\t\t\t\t\t\t\t<div class=\"candidate-send__input-wrap\">\n\t\t\t\t\t\t\t\t\t<div class=\"candidate-send_contact-label pt-3\">\n\t\t\t\t\t\t\t\t\t\t<h6>Отправить кандидату:</h6>\n\t\t\t\t\t\t\t\t\t\t<h6>{{selectedCandidate.lastname | uppercase}} {{selectedCandidate.name | uppercase}} {{selectedCandidate.fathername | uppercase}}</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"\"></div>\n\t\t\t\t\t\t\t\t\t<div class=\"candidate-send_contact-label pt-3\">\n\t\t\t\t\t\t\t\t\t    <h6>Коммуникация</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t                <div class=\"form-group field-candidateContact\">\n\t\t\t\t\t                  <input [(ngModel)]=\"selected_contact_name\"\n\t\t\t\t\t                  [typeahead]=\"transformed_candidate_contacts_names\"\n\t\t\t\t\t                  [typeaheadMinLength]=\"0\"\n\t\t\t\t\t                  (typeaheadOnSelect)=\"onSelectCommunication($event)\"\n\t\t\t\t\t                  typeaheadOptionField=\"name\"\n\t\t\t\t\t                  name=\"selected_contact_name\"\n\t\t\t\t\t                  class=\"form-control\">\n\t\t\t\t\t                </div>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<div class=\"candidate-send_contact-label pt-3\">\n\t\t\t\t\t\t\t\t\t    <h6>Адрес</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t                <div class=\"form-group field-candidateAddress\">\n\t\t\t\t\t                  <input [(ngModel)]=\"selected_contact_value\"\n\t\t\t\t\t                  [typeahead]=\"choosen_candidate_contacts_values\"\n\t\t\t\t\t                  [typeaheadMinLength]=\"0\"\n\t\t\t\t\t                  typeaheadOptionField=\"value\"\n\t\t\t\t\t                  name=\"selected_contact_value\"\n\t\t\t\t\t                  class=\"form-control\">\n\t\t\t\t\t                </div>\n\t\t\t\t\t\t\t\t\t<div class=\"candidate-send_contact-label pt-3\">\n\t\t\t\t\t\t\t\t\t    <h6>Прикрепить документ</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t                <div class=\"form-group field-candidateDocument\">\n\t\t\t\t\t                  <input [(ngModel)]=\"selected_attached_document\"\n\t\t\t\t\t                  [typeahead]=\"attached_documents\"\n\t\t\t\t\t                  [typeaheadMinLength]=\"0\"\n\t\t\t\t\t                  name=\"selected_attached_document\"\n\t\t\t\t\t                  class=\"form-control\"> \n\t\t\t\t\t                </div>\n\t\t\t\t\t                <div class=\"col-12\">\n\t\t\t\t\t                    <div class=\"row clearfix\">\n\t\t\t\t\t                      <div class=\"pull-right text-right\">\n\t\t\t\t\t                          <input id=\"addDocument\" class=\"input-document\" #fileInputDocument type=\"file\" accept=\"application/pdf\" (change)=\"addDocument(fileInputDocument)\"><label for=\"addDocument\" class=\"add-document-label\"><i class=\"fas fa-plus-circle fa-add-document\" aria-hidden=\"true\"></i><span>Добавить документ</span></label>\n\t\t\t\t\t                      </div>\n\t\t\t\t\t                    </div>\n\t\t\t\t\t                </div>\t\t\t\t\t                \t\t\t\t\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-6 col-lg-6\">\n\t\t\t\t                <div class=\"\" style=\"display: flex; justify-content: flex-end;\">\n\t\t\t\t                    <div class=\"candidate-send__comment-content\">\n\t\t\t\t                        <div class=\"candidate-send__comment-label\">\n\t\t\t\t                            <h6>Сопроводительное письмо</h6>\n\t\t\t\t                        </div>\n\t\t\t\t                        <div class=\"form-group field-candidateLetter\">\n\t\t\t\t                            <textarea id=\"candidateLetter\" [(ngModel)]=\"candidate_letter_body\" class=\"form-control\" name=\"CreateSendForm[letter]\" rows=\"6\"></textarea>\n\t\t\t\t                        </div>                    \n\t\t\t\t                    </div>\n\t\t\t\t                </div>\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"row pt-3\" style=\"justify-content:center;\">\n\t\t\t\t\t\t    <button type=\"submit\" class=\"btn btn-sm btn-crm btn-crm-primary mr-2\">ОТПРАВИТЬ</button>\n\t\t\t\t\t\t    <button type=\"button\" class=\"btn btn-sm btn-crm btn-crm-default btn-crm-default_red\" data-dismiss=\"modal\" (click)=\"bsModalRef.hide()\">ОТМЕНA</button>\n\t\t\t\t\t\t</div>\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t</div>\t\t\n\t</div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/send-customer-modal/send-customer-modal.component.html": 
        /*!*************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/send-customer-modal/send-customer-modal.component.html ***!
          \*************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"modal-dialog\" style=\"margin: auto;\">\n\t<div class=\"modal-content\">\n\t\t<div class=\"modal-header\">\n\t\t\t<h5 id=\"customer-label\" class=\"modal-title\">ОТПРАВИТЬ ЗАКАЗЧИКУ</h5>\n\t\t\t<button type=\"button\" class=\"close pull-right\"  aria-label=\"Close\" data-dismiss=\"modal\" (click)=\"bsModalRef.hide()\"><span aria-hidden=\"true\">&times;</span></button>\n\t\t</div>\n\t\t<div class=\"modal-body\">\n\t\t\t<div class=\"customer-send\">\n\t\t\t\t<form action=\"\" id=\"formcustomer\" #sendCustomer=\"ngForm\" (ngSubmit)=\"onFormSubmit(sendCustomer)\">\n\t\t\t\t\t<div class=\"form-group field-createsendform-approved\">\n\t\t\t\t\t\t<div class=\"custom-control custom-checkbox\">\n\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"createsendform-approved\" class=\"custom-control-input\" name=\"\" value=\"1\" checked=\"\">\n              <label class=\"custom-control-label\" for=\"createsendform-approved\" style=\"font-weight: 500;\">\n              Согласие тимлида\n              </label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n          <div class=\"candidate-create__panel-buttons p-3\">\n              <div class=\"row border-bottom\"></div>\n          </div>\n          <div class=\"row\">\n            <div class=\"col-6 col-lg-6\">\n              <div class=\"customer-send__input_wrap\">\n                <div class=\"customer-send_customer-label\">\n                    <h6>Заказчик</h6>\n                </div>\n                <div class=\"form-group field-customerCustomer\">\n                  <input [(ngModel)]=\"selected_customer\"\n                  [typeahead]=\"customers_names\"\n                  [typeaheadMinLength]=\"0\"\n                  (typeaheadOnSelect)=\"onSelect($event)\"\n                  name=\"selected_customer\"\n                  class=\"form-control\">\n                </div>\t\t                \t\t\t\n                <div class=\"customer-send_vacancy-label pt-3\">\n                    <h6>Вакансия</h6>\n                </div>\t                            \n                <div class=\"form-group field-customerVacancy\">\n                  <input [(ngModel)]=\"selected_vacancy\"\n                  [typeahead]=\"vacancies_names\"\n                  [typeaheadMinLength]=\"0\"\n                  name=\"selected_vacancy\"\n                  class=\"form-control\">\n                </div>\n                <div class=\"customer-send_contact-label pt-3\">\n                    <h6>Коммуникация</h6>\n                </div>\n                <div class=\"form-group field-customerContact\">\n                  <input [(ngModel)]=\"selected_contact_name\"\n                  [typeahead]=\"transformed_customer_contacts_names\"\n                  [typeaheadMinLength]=\"0\"\n                  (typeaheadOnSelect)=\"onSelectCommunication($event)\"\n                  typeaheadOptionField=\"name\"\n                  name=\"selected_contact_name\"\n                  class=\"form-control\">\n                </div>\n                <div class=\"customer-send_contact-label pt-3\">\n                    <h6>Адрес</h6>\n                </div>\t                            \t                            \n                <div class=\"form-group field-customerAddress\">\n                  <input [(ngModel)]=\"selected_contact_value\"\n                  [typeahead]=\"choosen_customer_contacts_values\"\n                  [typeaheadMinLength]=\"0\"\n                  typeaheadOptionField=\"value\"\n                  name=\"selected_contact_value\"\n                  class=\"form-control\">\n                </div>\n                <div class=\"customer-send_contact-label pt-3\">\n                    <h6>Прикрепить документ</h6>\n                </div>\t                            \n                <div class=\"form-group field-customerDocument\">\n                  <input [(ngModel)]=\"selected_attached_document\"\n                  [typeahead]=\"attached_documents\"\n                  [typeaheadMinLength]=\"0\"\n                  name=\"selected_attached_document\"\n                  class=\"form-control\"> \n                </div>\n\n                <div class=\"col-12\">\n                    <div class=\"row clearfix\">\n                      <div class=\"pull-right text-right\">\n                          <!-- <i class=\"fa fa-plus-circle fa-add-resume\" aria-hidden=\"true\" slyle=\"color: #58d5e0;\"></i> -->\n                          <!-- <a href=\"#\" id=\"customerAddDocument\" class=\"add-resume-button ml-2\" style=\"color: #58d5e0;\">Добавить документ</a> -->\n                          <input id=\"addDocument\" class=\"input-document\" #fileInputDocument type=\"file\" accept=\"application/pdf\" (change)=\"addDocument(fileInputDocument)\"><label for=\"addDocument\" class=\"add-document-label\"><i class=\"fas fa-plus-circle fa-add-document\" aria-hidden=\"true\"></i><span>Добавить документ</span></label>\n                      </div>\n                    </div>\n                </div>\t                            \n              </div>\t\n            </div>\n            <div class=\"col-6 col-lg-6\">\n                <div class=\"\" style=\"display: flex; justify-content: flex-end;\">\n                    <div class=\"customer-send__comment-content\">\n                        <div class=\"customer-send__comment-label\">\n                            <h6>Сопроводительное письмо</h6>\n                        </div>\n                        <div class=\"form-group field-customerLetter\">\n                            <textarea id=\"customerLetter\" [(ngModel)]=\"customer_letter_body\" class=\"form-control\" name=\"CreateSendForm[letter]\" rows=\"6\"></textarea>\n                        </div>                    \n                    </div>\n                </div>\n            </div>\t                \t\n          </div>\n\t\t\t\t\t<div class=\"row pt-3\" style=\"justify-content:center;\">\n\t\t\t\t\t    <button type=\"submit\" class=\"btn btn-sm btn-crm btn-crm-primary mr-2\">ОТПРАВИТЬ</button>\n\t\t\t\t\t    <button type=\"button\" class=\"btn btn-sm btn-crm btn-crm-default btn-crm-default_red\" data-dismiss=\"modal\" (click)=\"bsModalRef.hide()\">ОТМЕНA</button>\n\t\t\t\t\t</div>\t                \t\t\t\t\t\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/vacancy-info/vacancy-info.component.html": 
        /*!***********************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/vacancy-info/vacancy-info.component.html ***!
          \***********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"\" *ngIf=\"selectedVacancy\">\n    <div class=\"entity-details\">\n        <div class=\"row border-bottom\">\n            <div class=\"entity-details__buttons col-11 col-lg-11\">\n              <div class=\"entity-buttons-wrapper\">\n                <button class=\"btn btn-crm-default\">Подобрать кандидата</button>\n                <button class=\"btn btn-crm-default\" (click)=\"openVacancyUpdateModal()\">Редактировать</button>\n                <button class=\"btn btn-crm-default\">Распечатать</button>\n              </div>\n            </div>\n        </div>\n        <div class=\"profile-item row\">\n            <div class=\"col-9 col-md-9 col-lg-9\">\n                <div class=\"entity-name\">{{selectedVacancy?.name | uppercase}}</div>\n                <div class=\"entity-details\">\n                    <div class=\"item-row\">\n                        <span class=\"item-title\">\n                            Компания:\n                        </span>\n                        <span class=\"item-value\">\n                            {{company.name}}\n                        </span>    \n                    </div>\n                    <div class=\"item-row\">\n                        <span class=\"item-title\">\n                            Локация:\n                        </span>\n                        <span class=\"item-value\">\n                            {{city.name}}\n                        </span>    \n                    </div>\n                    <div class=\"item-row\">\n                        <span class=\"item-title\">\n                            Зарплата:\n                        </span>\n                        <span class=\"item-value\">\n                            {{selectedVacancy.salary}}\n                        </span>    \n                    </div>\n                    <div class=\"item-row\">\n                        <span class=\"item-title\">\n                            Навыки:\n                        </span>\n                        <span class=\"item-value\">\n                            <span class=\"skill\" *ngFor=\"let skill of vacancySkills\">\n                                {{skill}}\n                            </span>  \n                        </span>    \n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"item-row\">\n            <div class=\"item-title\">\n                Обязанности:\n            </div>\n            <div class=\"item-value\">\n                <span [innerHTML]=\"selectedVacancy.v_duties | safehtml\"></span>\n            </div>\n        </div>\n        <div class=\"item-row\">\n            <div class=\"item-title\">\n                Условия работы:\n            </div>\n            <div class=\"item-value\">\n                <span [innerHTML]=\"selectedVacancy.w_conditions | safehtml\"></span>\n            </div>\n        </div>\n        <div class=\"item-row\">\n            <div class=\"item-title\">\n                Требования:\n            </div>\n            <div class=\"item-value\">\n                <span [innerHTML]=\"selectedVacancy.v_requirements | safehtml\"></span>                \n            </div>\n        </div>\n        <div class=\"item-row\">\n            <div class=\"item-title\">\n                Описания вакансии:\n            </div>\n            <div class=\"item-value\">\n                <span [innerHTML]=\"selectedVacancy.v_description | safehtml\"></span>                \n            </div>\n        </div>\n        <!-- vacancy documents -->\n        <div class=\"\" style=\"display: flex;\">\n            <div class='pt-3 buttons-left'>\n              <a href=\"#\">Распечатать</a>\n              <a href=\"{{'/files/attachments/' + selectedVacancy.id + '/' + selectedVacancyDocument}}\" download=\"\" >Скачать</a>\n              <a href=\"{{'/files/attachments/' + selectedVacancy.id + '/' +  selectedVacancyDocument}}?forcedownload=1\">Сохранить</a>\n              <a href=\"#\" (click)=\"removeVacancyDocumentByName($event,selectedVacancyDocument)\">Удалить</a>\n              <button class=\"btn btn-link btn-file btn-sm\" type=\"button\">\n                Прикрепить\n                <input #fileInput\n                       type=\"file\" \n                       accept=\"application/pdf\"\n                       (change)=\"processFile(fileInput)\">\n              </button>\n              \n            </div>\n            <div class=\"buttons-right\">\n              <a class=\"dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                Документы\n              </a>\n              <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">\n                <div class=\"document__dropdown-menu border-bottom\" *ngFor=\"let doc of vacancyDocuments\" style=\"display: flex; padding: 10px;\">\n                  <a class=\"document__dropdown-item dropdown-item\" href=\"#\" (click)=\"previewDocument($event, doc.file_name)\"><i class=\"fas fa-file-image-o\"></i>{{doc.file_name}}</a>\n                  <a href=\"{{'/files/attachments/' + selectedVacancy.id + '/' +  doc.file_name}}\" download=\"\"><i class=\"fas fa-download\"></i></a>\n                  <a href=\"#\" (click)=\"removeVacancyDocumentByName($event,doc.file_name)\"><i class=\"fas fa-trash-alt\"></i></a>\n                </div>\n              </div>\n            </div>  \n          </div> \n          <hr>\n          <div class=\"row\">\n            <div class=\"col-lg-12\">\n              <div class=\"col-lg-9\">\n                  <div class=\"\" *ngIf=\"selectedVacancyDocument\">\n                    <ngx-extended-pdf-viewer [src]=\"'/files/attachments/' +  selectedVacancy.id + '/' + selectedVacancyDocument\" useBrowserLocale=\"true\" height=\"80vh\"></ngx-extended-pdf-viewer>\n                  </div>\n                  <div class=\"\" style=\"height: 80vh;background: #f5f5f5;\" *ngIf=\"!selectedVacancyDocument\">\n\n                  </div>\n              </div>\n              <div class=\"col-lg-3\">\n  \n              </div>  \n          </div>\n          </div>       \n        <!-- vacancy documents -->\n    </div>\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/vacancy-list/vacancy-list.component.html": 
        /*!***********************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/vacancy-list/vacancy-list.component.html ***!
          \***********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"vacancy-list\">\n    <ul class=\"vacancies\">\n        <li *ngFor=\"let vacancy of vacancies\"\n        [class.selected]=\"vacancy === selectedVacancy\"\n        (click)=\"onSelect(vacancy)\"\n        class=\"entity_list__item\">\n            <div class=\"list-item-data\" style=\"margin-left: 50px;\">\n                <div class=\"entity-title\">\n                <a><span>{{vacancy.name}}</span></a></div>\n                <div class=\"entity-sub\">\n                <span>{{vacancy.company_name}}</span>\n                </div>\n                <div class=\"location\">\n                <span>{{vacancy.city_name}}</span>\n                </div>\n            </div>\n        </li>\n    </ul>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/vacancy-update/vacancy-update.component.html": 
        /*!***************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/vacancy-update/vacancy-update.component.html ***!
          \***************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"modal-dialog\" style=\"margin: auto;\">\n\t<div class=\"modal-content\">\n\t\t<div class=\"modal-header\">\n\t\t\t<h5 id=\"customer-label\" class=\"modal-title\">РЕДАКТИРОВАТЬ ВАКАНСИЮ</h5>\n\t\t\t<button type=\"button\" class=\"close pull-right\"  aria-label=\"Close\" data-dismiss=\"modal\" (click)=\"bsModalRef.hide()\"><span aria-hidden=\"true\">&times;</span></button>\t\t\t\n\t\t</div>\n\t\t<div class=\"modal-body\">\n\t\t\t<div class=\"vacancy-update\">\n\t\t\t\t<form action=\"\" id=\"vacancyupdate\" #vacancyUpdate=\"ngForm\" (ngSubmit)=\"onFormSubmit(vacancyUpdate)\">\n\t\t\t\t\t<div class=\"vacancy-update__wrap\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-12 col-lg-12\">\n\t\t\t\t\t\t\t\t<div class=\"vacancy-update__input-wrap\">\n\t\t\t\t\t\t\t\t\t<input [ngModel]=\"selectedVacancyId\" type=\"hidden\" name=\"vacancyId\">\n\t\t\t\t\t\t\t\t\t<div class=\"vacancy-update_position-label pt-3\">\n\t\t\t\t\t\t\t\t\t\t<h6>Должность</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"form-group field-vacancyPosition\">\n\t\t\t\t\t\t\t            <input [(ngModel)]=\"selectedVacancy.name\"\n\t\t\t\t\t\t\t\t\t\tname=\"vacancy_position\"\n\t\t\t\t\t\t\t            class=\"form-control\">\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<div class=\"vacancy-update_salary-label pt-3\">\n\t\t\t\t\t\t\t\t\t\t<h6>Зарплата</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"form-group field-vacancySalary\">\n\t\t\t\t\t\t\t            <input [(ngModel)]=\"selectedVacancy.salary\"\n\t\t\t\t\t\t\t\t\t\tname=\"vacancy_salary\"\n\t\t\t\t\t\t\t            class=\"form-control\">\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<div class=\"vacancy-update_location-label pt-3\">\n\t\t\t\t\t\t\t\t\t\t<h6>Локация</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"location\"\n\t\t\t\t\t\t\t\t\t[(ngModel)]=\"selectedVacancy.location\" #city=\"ngModel\">\n\t\t\t\t\t\t\t\t\t\t<option  *ngFor=\"let city of cities\" [value]=\"city.id\">\n\t\t\t\t\t\t\t\t\t\t\t{{city.name}}\n\t\t\t\t\t\t\t\t\t\t</option>\n\t\t\t\t\t\t\t\t\t</select>\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<div class=\"vacancy-update_skills-label pt-3\">\n\t\t\t\t\t\t\t\t\t\t<h6>Навыки</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<mat-form-field *ngIf=\"isSelectedSkill\">\n\t\t\t\t\t\t\t\t\t\t<mat-select multiple [(ngModel)]=\"selectedSkills\" [value]=\"selectedSkills\" name=\"selectedSkills\">\n\t\t\t\t\t\t\t\t\t\t  <mat-option  *ngFor=\"let skill of skills\" [value]=\"skill\">{{skill.name}}</mat-option>\n\t\t\t\t\t\t\t\t\t\t</mat-select>\n\t\t\t\t\t\t\t\t\t</mat-form-field>\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<div class=\"vacancy-update_duties-label pt-3\">\n\t\t\t\t\t\t\t\t\t\t<h6>Обязанности</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<ckeditor [editor]=\"Editor\" data=\"\" [(ngModel)]=\"selectedVacancy.v_duties\" name=\"duties\"></ckeditor>\n\t\t\t\t\t\t\t\t\t<div class=\"vacancy-update_requirements pt-3\">\n\t\t\t\t\t\t\t\t\t\t<h6>Требования</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<ckeditor [editor]=\"Editor\" data=\"\" [(ngModel)]=\"selectedVacancy.v_requirements\" name=\"requirements\"></ckeditor>\n\t\t\t\t\t\t\t\t\t<div class=\"vacancy-update_conditions-label pt-3\">\n\t\t\t\t\t\t\t\t\t\t<h6>Условия работы</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<ckeditor [editor]=\"Editor\" data=\"\" [(ngModel)]=\"selectedVacancy.w_conditions\" name=\"conditions\"></ckeditor>\n\t\t\t\t\t\t\t\t\t<div class=\"vacancy-update_conditions-label pt-3\">\n\t\t\t\t\t\t\t\t\t\t<h6>Описание</h6>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<ckeditor [editor]=\"Editor\" data=\"\" [(ngModel)]=\"selectedVacancy.v_description\" name=\"description\"></ckeditor>\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t                <div class=\"col-12\">\n\t\t\t\t\t                    <div class=\"row clearfix\">\n\t\t\t\t\t                      <div class=\"pull-right text-right\">\n\t\t\t\t\t                          <input id=\"addDocument\" class=\"input-document\" #fileInputDocument type=\"file\" accept=\"application/pdf\" (change)=\"addDocument(fileInputDocument)\"><label for=\"addDocument\" class=\"add-document-label\"><i class=\"fas fa-plus-circle fa-add-document\" aria-hidden=\"true\"></i><span>Добавить документ</span></label>\n\t\t\t\t\t                      </div>\n\t\t\t\t\t                    </div>\n\t\t\t\t\t                </div>\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<div class=\"row pt-3\" style=\"justify-content:center;\">\n\t\t\t\t\t\t\t\t\t    <button type=\"submit\" class=\"btn btn-sm btn-crm btn-crm-primary mr-2\">СОХРАНИТЬ</button>\n\t\t\t\t\t\t\t\t\t    <button type=\"button\" class=\"btn btn-sm btn-crm btn-crm-default btn-crm-default_red\" data-dismiss=\"modal\" (click)=\"bsModalRef.hide()\">ОТМЕНA</button>\n\t\t\t\t\t\t\t\t\t</div>                                    \n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/tslib/tslib.es6.js": 
        /*!*****************************************!*\
          !*** ./node_modules/tslib/tslib.es6.js ***!
          \*****************************************/
        /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function () { return __extends; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function () { return __assign; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function () { return __rest; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function () { return __decorate; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function () { return __param; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function () { return __metadata; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function () { return __awaiter; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function () { return __generator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function () { return __exportStar; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function () { return __values; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function () { return __read; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function () { return __spread; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () { return __spreadArrays; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function () { return __await; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () { return __asyncGenerator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () { return __asyncDelegator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function () { return __asyncValues; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () { return __makeTemplateObject; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function () { return __importStar; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function () { return __importDefault; });
            /*! *****************************************************************************
            Copyright (c) Microsoft Corporation. All rights reserved.
            Licensed under the Apache License, Version 2.0 (the "License"); you may not use
            this file except in compliance with the License. You may obtain a copy of the
            License at http://www.apache.org/licenses/LICENSE-2.0
            
            THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
            KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
            WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
            MERCHANTABLITY OR NON-INFRINGEMENT.
            
            See the Apache Version 2.0 License for specific language governing permissions
            and limitations under the License.
            ***************************************************************************** */
            /* global Reflect, Promise */
            var extendStatics = function (d, b) {
                extendStatics = Object.setPrototypeOf ||
                    ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
                    function (d, b) { for (var p in b)
                        if (b.hasOwnProperty(p))
                            d[p] = b[p]; };
                return extendStatics(d, b);
            };
            function __extends(d, b) {
                extendStatics(d, b);
                function __() { this.constructor = d; }
                d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
            }
            var __assign = function () {
                __assign = Object.assign || function __assign(t) {
                    for (var s, i = 1, n = arguments.length; i < n; i++) {
                        s = arguments[i];
                        for (var p in s)
                            if (Object.prototype.hasOwnProperty.call(s, p))
                                t[p] = s[p];
                    }
                    return t;
                };
                return __assign.apply(this, arguments);
            };
            function __rest(s, e) {
                var t = {};
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
                        t[p] = s[p];
                if (s != null && typeof Object.getOwnPropertySymbols === "function")
                    for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                            t[p[i]] = s[p[i]];
                    }
                return t;
            }
            function __decorate(decorators, target, key, desc) {
                var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
                    r = Reflect.decorate(decorators, target, key, desc);
                else
                    for (var i = decorators.length - 1; i >= 0; i--)
                        if (d = decorators[i])
                            r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            }
            function __param(paramIndex, decorator) {
                return function (target, key) { decorator(target, key, paramIndex); };
            }
            function __metadata(metadataKey, metadataValue) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
                    return Reflect.metadata(metadataKey, metadataValue);
            }
            function __awaiter(thisArg, _arguments, P, generator) {
                return new (P || (P = Promise))(function (resolve, reject) {
                    function fulfilled(value) { try {
                        step(generator.next(value));
                    }
                    catch (e) {
                        reject(e);
                    } }
                    function rejected(value) { try {
                        step(generator["throw"](value));
                    }
                    catch (e) {
                        reject(e);
                    } }
                    function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
                    step((generator = generator.apply(thisArg, _arguments || [])).next());
                });
            }
            function __generator(thisArg, body) {
                var _ = { label: 0, sent: function () { if (t[0] & 1)
                        throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
                return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
                function verb(n) { return function (v) { return step([n, v]); }; }
                function step(op) {
                    if (f)
                        throw new TypeError("Generator is already executing.");
                    while (_)
                        try {
                            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                                return t;
                            if (y = 0, t)
                                op = [op[0] & 2, t.value];
                            switch (op[0]) {
                                case 0:
                                case 1:
                                    t = op;
                                    break;
                                case 4:
                                    _.label++;
                                    return { value: op[1], done: false };
                                case 5:
                                    _.label++;
                                    y = op[1];
                                    op = [0];
                                    continue;
                                case 7:
                                    op = _.ops.pop();
                                    _.trys.pop();
                                    continue;
                                default:
                                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                        _ = 0;
                                        continue;
                                    }
                                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                        _.label = op[1];
                                        break;
                                    }
                                    if (op[0] === 6 && _.label < t[1]) {
                                        _.label = t[1];
                                        t = op;
                                        break;
                                    }
                                    if (t && _.label < t[2]) {
                                        _.label = t[2];
                                        _.ops.push(op);
                                        break;
                                    }
                                    if (t[2])
                                        _.ops.pop();
                                    _.trys.pop();
                                    continue;
                            }
                            op = body.call(thisArg, _);
                        }
                        catch (e) {
                            op = [6, e];
                            y = 0;
                        }
                        finally {
                            f = t = 0;
                        }
                    if (op[0] & 5)
                        throw op[1];
                    return { value: op[0] ? op[1] : void 0, done: true };
                }
            }
            function __exportStar(m, exports) {
                for (var p in m)
                    if (!exports.hasOwnProperty(p))
                        exports[p] = m[p];
            }
            function __values(o) {
                var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
                if (m)
                    return m.call(o);
                return {
                    next: function () {
                        if (o && i >= o.length)
                            o = void 0;
                        return { value: o && o[i++], done: !o };
                    }
                };
            }
            function __read(o, n) {
                var m = typeof Symbol === "function" && o[Symbol.iterator];
                if (!m)
                    return o;
                var i = m.call(o), r, ar = [], e;
                try {
                    while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                        ar.push(r.value);
                }
                catch (error) {
                    e = { error: error };
                }
                finally {
                    try {
                        if (r && !r.done && (m = i["return"]))
                            m.call(i);
                    }
                    finally {
                        if (e)
                            throw e.error;
                    }
                }
                return ar;
            }
            function __spread() {
                for (var ar = [], i = 0; i < arguments.length; i++)
                    ar = ar.concat(__read(arguments[i]));
                return ar;
            }
            function __spreadArrays() {
                for (var s = 0, i = 0, il = arguments.length; i < il; i++)
                    s += arguments[i].length;
                for (var r = Array(s), k = 0, i = 0; i < il; i++)
                    for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                        r[k] = a[j];
                return r;
            }
            ;
            function __await(v) {
                return this instanceof __await ? (this.v = v, this) : new __await(v);
            }
            function __asyncGenerator(thisArg, _arguments, generator) {
                if (!Symbol.asyncIterator)
                    throw new TypeError("Symbol.asyncIterator is not defined.");
                var g = generator.apply(thisArg, _arguments || []), i, q = [];
                return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
                function verb(n) { if (g[n])
                    i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
                function resume(n, v) { try {
                    step(g[n](v));
                }
                catch (e) {
                    settle(q[0][3], e);
                } }
                function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
                function fulfill(value) { resume("next", value); }
                function reject(value) { resume("throw", value); }
                function settle(f, v) { if (f(v), q.shift(), q.length)
                    resume(q[0][0], q[0][1]); }
            }
            function __asyncDelegator(o) {
                var i, p;
                return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
                function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
            }
            function __asyncValues(o) {
                if (!Symbol.asyncIterator)
                    throw new TypeError("Symbol.asyncIterator is not defined.");
                var m = o[Symbol.asyncIterator], i;
                return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
                function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
                function settle(resolve, reject, d, v) { Promise.resolve(v).then(function (v) { resolve({ value: v, done: d }); }, reject); }
            }
            function __makeTemplateObject(cooked, raw) {
                if (Object.defineProperty) {
                    Object.defineProperty(cooked, "raw", { value: raw });
                }
                else {
                    cooked.raw = raw;
                }
                return cooked;
            }
            ;
            function __importStar(mod) {
                if (mod && mod.__esModule)
                    return mod;
                var result = {};
                if (mod != null)
                    for (var k in mod)
                        if (Object.hasOwnProperty.call(mod, k))
                            result[k] = mod[k];
                result.default = mod;
                return result;
            }
            function __importDefault(mod) {
                return (mod && mod.__esModule) ? mod : { default: mod };
            }
            /***/ 
        }),
        /***/ "./src/app/angular-material.module.ts": 
        /*!********************************************!*\
          !*** ./src/app/angular-material.module.ts ***!
          \********************************************/
        /*! exports provided: AngularMaterialModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AngularMaterialModule", function () { return AngularMaterialModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
            var AngularMaterialModule = /** @class */ (function () {
                function AngularMaterialModule() {
                }
                return AngularMaterialModule;
            }());
            AngularMaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [
                        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"]
                    ],
                    exports: [
                        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                        _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"]
                    ]
                })
            ], AngularMaterialModule);
            /***/ 
        }),
        /***/ "./src/app/app-routing.module.ts": 
        /*!***************************************!*\
          !*** ./src/app/app-routing.module.ts ***!
          \***************************************/
        /*! exports provided: AppRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () { return AppRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _components_candidate_candidate_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/candidate/candidate.component */ "./src/app/components/candidate/candidate.component.ts");
            /* harmony import */ var _components_detail_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/detail/detail.component */ "./src/app/components/detail/detail.component.ts");
            /* harmony import */ var _components_candidate_update_candidate_update_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/candidate-update/candidate-update.component */ "./src/app/components/candidate-update/candidate-update.component.ts");
            /* harmony import */ var _components_add_candidate_add_candidate_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/add-candidate/add-candidate.component */ "./src/app/components/add-candidate/add-candidate.component.ts");
            /* harmony import */ var _components_candidate_info_candidate_info_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/candidate-info/candidate-info.component */ "./src/app/components/candidate-info/candidate-info.component.ts");
            /* harmony import */ var _components_candidate_list_candidate_list_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/candidate-list/candidate-list.component */ "./src/app/components/candidate-list/candidate-list.component.ts");
            /* harmony import */ var _components_all_vacancies_all_vacancies_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/all-vacancies/all-vacancies.component */ "./src/app/components/all-vacancies/all-vacancies.component.ts");
            /* harmony import */ var _components_vacancy_list_vacancy_list_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/vacancy-list/vacancy-list.component */ "./src/app/components/vacancy-list/vacancy-list.component.ts");
            /* harmony import */ var _components_vacancy_info_vacancy_info_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/vacancy-info/vacancy-info.component */ "./src/app/components/vacancy-info/vacancy-info.component.ts");
            /* harmony import */ var _components_add_vacancy_add_vacancy_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/add-vacancy/add-vacancy.component */ "./src/app/components/add-vacancy/add-vacancy.component.ts");
            var routes = [
                { path: 'web/candidates', component: _components_candidate_candidate_component__WEBPACK_IMPORTED_MODULE_3__["CandidateComponent"] },
                { path: 'detail/:id', component: _components_detail_detail_component__WEBPACK_IMPORTED_MODULE_4__["DetailComponent"] },
                { path: 'update/:id', component: _components_candidate_update_candidate_update_component__WEBPACK_IMPORTED_MODULE_5__["CandidateUpdateComponent"] },
                { path: 'add-candidate', component: _components_add_candidate_add_candidate_component__WEBPACK_IMPORTED_MODULE_6__["AddCandidateComponent"] },
                { path: 'candidate-list', component: _components_candidate_list_candidate_list_component__WEBPACK_IMPORTED_MODULE_8__["CandidateListComponent"] },
                { path: 'candidate-info', component: _components_candidate_info_candidate_info_component__WEBPACK_IMPORTED_MODULE_7__["CandidateInfoComponent"] },
                { path: 'all-vacancies', component: _components_all_vacancies_all_vacancies_component__WEBPACK_IMPORTED_MODULE_9__["AllVacanciesComponent"] },
                { path: 'vacancy-list', component: _components_vacancy_list_vacancy_list_component__WEBPACK_IMPORTED_MODULE_10__["VacancyListComponent"] },
                { path: 'vacancy-info', component: _components_vacancy_info_vacancy_info_component__WEBPACK_IMPORTED_MODULE_11__["VacancyInfoComponent"] },
                { path: 'add-vacancy', component: _components_add_vacancy_add_vacancy_component__WEBPACK_IMPORTED_MODULE_12__["AddVacancyComponent"] },
            ];
            var AppRoutingModule = /** @class */ (function () {
                function AppRoutingModule() {
                }
                return AppRoutingModule;
            }());
            AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], AppRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/app.component.css": 
        /*!***********************************!*\
          !*** ./src/app/app.component.css ***!
          \***********************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("/*@import '~font-awesome/css/font-awesome.css';*/\n/*main-container__wrap*/\n/*!\n *  Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome\n *  License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)\n */\n/* FONT PATH\n * -------------------------- */\n@font-face {\n  font-family: 'FontAwesome';\n  src: url('fontawesome-webfont.eot?v=4.7.0');\n  src: url('fontawesome-webfont.eot?#iefix&v=4.7.0') format('embedded-opentype'), url('fontawesome-webfont.woff2?v=4.7.0') format('woff2'), url('fontawesome-webfont.woff?v=4.7.0') format('woff'), url('fontawesome-webfont.ttf?v=4.7.0') format('truetype'), url('fontawesome-webfont.svg?v=4.7.0#fontawesomeregular') format('svg');\n  font-weight: normal;\n  font-style: normal;\n}\n.fa {\n  display: inline-block;\n  font: normal normal normal 14px/1 FontAwesome;\n  font-size: inherit;\n  text-rendering: auto;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n}\n/* makes the font 33% larger relative to the icon container */\n.fa-lg {\n  font-size: 1.33333333em;\n  line-height: 0.75em;\n  vertical-align: -15%;\n}\n.fa-2x {\n  font-size: 2em;\n}\n.fa-3x {\n  font-size: 3em;\n}\n.fa-4x {\n  font-size: 4em;\n}\n.fa-5x {\n  font-size: 5em;\n}\n.fa-fw {\n  width: 1.28571429em;\n  text-align: center;\n}\n.fa-ul {\n  padding-left: 0;\n  margin-left: 2.14285714em;\n  list-style-type: none;\n}\n.fa-ul > li {\n  position: relative;\n}\n.fa-li {\n  position: absolute;\n  left: -2.14285714em;\n  width: 2.14285714em;\n  top: 0.14285714em;\n  text-align: center;\n}\n.fa-li.fa-lg {\n  left: -1.85714286em;\n}\n.fa-border {\n  padding: .2em .25em .15em;\n  border: solid 0.08em #eeeeee;\n  border-radius: .1em;\n}\n.fa-pull-left {\n  float: left;\n}\n.fa-pull-right {\n  float: right;\n}\n.fa.fa-pull-left {\n  margin-right: .3em;\n}\n.fa.fa-pull-right {\n  margin-left: .3em;\n}\n/* Deprecated as of 4.4.0 */\n.pull-right {\n  float: right;\n}\n.pull-left {\n  float: left;\n}\n.fa.pull-left {\n  margin-right: .3em;\n}\n.fa.pull-right {\n  margin-left: .3em;\n}\n.fa-spin {\n  -webkit-animation: fa-spin 2s infinite linear;\n  animation: fa-spin 2s infinite linear;\n}\n.fa-pulse {\n  -webkit-animation: fa-spin 1s infinite steps(8);\n  animation: fa-spin 1s infinite steps(8);\n}\n@-webkit-keyframes fa-spin {\n  0% {\n    transform: rotate(0deg);\n  }\n  100% {\n    transform: rotate(359deg);\n  }\n}\n@keyframes fa-spin {\n  0% {\n    transform: rotate(0deg);\n  }\n  100% {\n    transform: rotate(359deg);\n  }\n}\n.fa-rotate-90 {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=1)\";\n  transform: rotate(90deg);\n}\n.fa-rotate-180 {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=2)\";\n  transform: rotate(180deg);\n}\n.fa-rotate-270 {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=3)\";\n  transform: rotate(270deg);\n}\n.fa-flip-horizontal {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=0, mirror=1)\";\n  transform: scale(-1, 1);\n}\n.fa-flip-vertical {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1)\";\n  transform: scale(1, -1);\n}\n:root .fa-rotate-90,\n:root .fa-rotate-180,\n:root .fa-rotate-270,\n:root .fa-flip-horizontal,\n:root .fa-flip-vertical {\n  -webkit-filter: none;\n          filter: none;\n}\n.fa-stack {\n  position: relative;\n  display: inline-block;\n  width: 2em;\n  height: 2em;\n  line-height: 2em;\n  vertical-align: middle;\n}\n.fa-stack-1x,\n.fa-stack-2x {\n  position: absolute;\n  left: 0;\n  width: 100%;\n  text-align: center;\n}\n.fa-stack-1x {\n  line-height: inherit;\n}\n.fa-stack-2x {\n  font-size: 2em;\n}\n.fa-inverse {\n  color: #ffffff;\n}\n/* Font Awesome uses the Unicode Private Use Area (PUA) to ensure screen\n   readers do not read off random characters that represent icons */\n.fa-glass:before {\n  content: \"\\f000\";\n}\n.fa-music:before {\n  content: \"\\f001\";\n}\n.fa-search:before {\n  content: \"\\f002\";\n}\n.fa-envelope-o:before {\n  content: \"\\f003\";\n}\n.fa-heart:before {\n  content: \"\\f004\";\n}\n.fa-star:before {\n  content: \"\\f005\";\n}\n.fa-star-o:before {\n  content: \"\\f006\";\n}\n.fa-user:before {\n  content: \"\\f007\";\n}\n.fa-film:before {\n  content: \"\\f008\";\n}\n.fa-th-large:before {\n  content: \"\\f009\";\n}\n.fa-th:before {\n  content: \"\\f00a\";\n}\n.fa-th-list:before {\n  content: \"\\f00b\";\n}\n.fa-check:before {\n  content: \"\\f00c\";\n}\n.fa-remove:before,\n.fa-close:before,\n.fa-times:before {\n  content: \"\\f00d\";\n}\n.fa-search-plus:before {\n  content: \"\\f00e\";\n}\n.fa-search-minus:before {\n  content: \"\\f010\";\n}\n.fa-power-off:before {\n  content: \"\\f011\";\n}\n.fa-signal:before {\n  content: \"\\f012\";\n}\n.fa-gear:before,\n.fa-cog:before {\n  content: \"\\f013\";\n}\n.fa-trash-o:before {\n  content: \"\\f014\";\n}\n.fa-home:before {\n  content: \"\\f015\";\n}\n.fa-file-o:before {\n  content: \"\\f016\";\n}\n.fa-clock-o:before {\n  content: \"\\f017\";\n}\n.fa-road:before {\n  content: \"\\f018\";\n}\n.fa-download:before {\n  content: \"\\f019\";\n}\n.fa-arrow-circle-o-down:before {\n  content: \"\\f01a\";\n}\n.fa-arrow-circle-o-up:before {\n  content: \"\\f01b\";\n}\n.fa-inbox:before {\n  content: \"\\f01c\";\n}\n.fa-play-circle-o:before {\n  content: \"\\f01d\";\n}\n.fa-rotate-right:before,\n.fa-repeat:before {\n  content: \"\\f01e\";\n}\n.fa-refresh:before {\n  content: \"\\f021\";\n}\n.fa-list-alt:before {\n  content: \"\\f022\";\n}\n.fa-lock:before {\n  content: \"\\f023\";\n}\n.fa-flag:before {\n  content: \"\\f024\";\n}\n.fa-headphones:before {\n  content: \"\\f025\";\n}\n.fa-volume-off:before {\n  content: \"\\f026\";\n}\n.fa-volume-down:before {\n  content: \"\\f027\";\n}\n.fa-volume-up:before {\n  content: \"\\f028\";\n}\n.fa-qrcode:before {\n  content: \"\\f029\";\n}\n.fa-barcode:before {\n  content: \"\\f02a\";\n}\n.fa-tag:before {\n  content: \"\\f02b\";\n}\n.fa-tags:before {\n  content: \"\\f02c\";\n}\n.fa-book:before {\n  content: \"\\f02d\";\n}\n.fa-bookmark:before {\n  content: \"\\f02e\";\n}\n.fa-print:before {\n  content: \"\\f02f\";\n}\n.fa-camera:before {\n  content: \"\\f030\";\n}\n.fa-font:before {\n  content: \"\\f031\";\n}\n.fa-bold:before {\n  content: \"\\f032\";\n}\n.fa-italic:before {\n  content: \"\\f033\";\n}\n.fa-text-height:before {\n  content: \"\\f034\";\n}\n.fa-text-width:before {\n  content: \"\\f035\";\n}\n.fa-align-left:before {\n  content: \"\\f036\";\n}\n.fa-align-center:before {\n  content: \"\\f037\";\n}\n.fa-align-right:before {\n  content: \"\\f038\";\n}\n.fa-align-justify:before {\n  content: \"\\f039\";\n}\n.fa-list:before {\n  content: \"\\f03a\";\n}\n.fa-dedent:before,\n.fa-outdent:before {\n  content: \"\\f03b\";\n}\n.fa-indent:before {\n  content: \"\\f03c\";\n}\n.fa-video-camera:before {\n  content: \"\\f03d\";\n}\n.fa-photo:before,\n.fa-image:before,\n.fa-picture-o:before {\n  content: \"\\f03e\";\n}\n.fa-pencil:before {\n  content: \"\\f040\";\n}\n.fa-map-marker:before {\n  content: \"\\f041\";\n}\n.fa-adjust:before {\n  content: \"\\f042\";\n}\n.fa-tint:before {\n  content: \"\\f043\";\n}\n.fa-edit:before,\n.fa-pencil-square-o:before {\n  content: \"\\f044\";\n}\n.fa-share-square-o:before {\n  content: \"\\f045\";\n}\n.fa-check-square-o:before {\n  content: \"\\f046\";\n}\n.fa-arrows:before {\n  content: \"\\f047\";\n}\n.fa-step-backward:before {\n  content: \"\\f048\";\n}\n.fa-fast-backward:before {\n  content: \"\\f049\";\n}\n.fa-backward:before {\n  content: \"\\f04a\";\n}\n.fa-play:before {\n  content: \"\\f04b\";\n}\n.fa-pause:before {\n  content: \"\\f04c\";\n}\n.fa-stop:before {\n  content: \"\\f04d\";\n}\n.fa-forward:before {\n  content: \"\\f04e\";\n}\n.fa-fast-forward:before {\n  content: \"\\f050\";\n}\n.fa-step-forward:before {\n  content: \"\\f051\";\n}\n.fa-eject:before {\n  content: \"\\f052\";\n}\n.fa-chevron-left:before {\n  content: \"\\f053\";\n}\n.fa-chevron-right:before {\n  content: \"\\f054\";\n}\n.fa-plus-circle:before {\n  content: \"\\f055\";\n}\n.fa-minus-circle:before {\n  content: \"\\f056\";\n}\n.fa-times-circle:before {\n  content: \"\\f057\";\n}\n.fa-check-circle:before {\n  content: \"\\f058\";\n}\n.fa-question-circle:before {\n  content: \"\\f059\";\n}\n.fa-info-circle:before {\n  content: \"\\f05a\";\n}\n.fa-crosshairs:before {\n  content: \"\\f05b\";\n}\n.fa-times-circle-o:before {\n  content: \"\\f05c\";\n}\n.fa-check-circle-o:before {\n  content: \"\\f05d\";\n}\n.fa-ban:before {\n  content: \"\\f05e\";\n}\n.fa-arrow-left:before {\n  content: \"\\f060\";\n}\n.fa-arrow-right:before {\n  content: \"\\f061\";\n}\n.fa-arrow-up:before {\n  content: \"\\f062\";\n}\n.fa-arrow-down:before {\n  content: \"\\f063\";\n}\n.fa-mail-forward:before,\n.fa-share:before {\n  content: \"\\f064\";\n}\n.fa-expand:before {\n  content: \"\\f065\";\n}\n.fa-compress:before {\n  content: \"\\f066\";\n}\n.fa-plus:before {\n  content: \"\\f067\";\n}\n.fa-minus:before {\n  content: \"\\f068\";\n}\n.fa-asterisk:before {\n  content: \"\\f069\";\n}\n.fa-exclamation-circle:before {\n  content: \"\\f06a\";\n}\n.fa-gift:before {\n  content: \"\\f06b\";\n}\n.fa-leaf:before {\n  content: \"\\f06c\";\n}\n.fa-fire:before {\n  content: \"\\f06d\";\n}\n.fa-eye:before {\n  content: \"\\f06e\";\n}\n.fa-eye-slash:before {\n  content: \"\\f070\";\n}\n.fa-warning:before,\n.fa-exclamation-triangle:before {\n  content: \"\\f071\";\n}\n.fa-plane:before {\n  content: \"\\f072\";\n}\n.fa-calendar:before {\n  content: \"\\f073\";\n}\n.fa-random:before {\n  content: \"\\f074\";\n}\n.fa-comment:before {\n  content: \"\\f075\";\n}\n.fa-magnet:before {\n  content: \"\\f076\";\n}\n.fa-chevron-up:before {\n  content: \"\\f077\";\n}\n.fa-chevron-down:before {\n  content: \"\\f078\";\n}\n.fa-retweet:before {\n  content: \"\\f079\";\n}\n.fa-shopping-cart:before {\n  content: \"\\f07a\";\n}\n.fa-folder:before {\n  content: \"\\f07b\";\n}\n.fa-folder-open:before {\n  content: \"\\f07c\";\n}\n.fa-arrows-v:before {\n  content: \"\\f07d\";\n}\n.fa-arrows-h:before {\n  content: \"\\f07e\";\n}\n.fa-bar-chart-o:before,\n.fa-bar-chart:before {\n  content: \"\\f080\";\n}\n.fa-twitter-square:before {\n  content: \"\\f081\";\n}\n.fa-facebook-square:before {\n  content: \"\\f082\";\n}\n.fa-camera-retro:before {\n  content: \"\\f083\";\n}\n.fa-key:before {\n  content: \"\\f084\";\n}\n.fa-gears:before,\n.fa-cogs:before {\n  content: \"\\f085\";\n}\n.fa-comments:before {\n  content: \"\\f086\";\n}\n.fa-thumbs-o-up:before {\n  content: \"\\f087\";\n}\n.fa-thumbs-o-down:before {\n  content: \"\\f088\";\n}\n.fa-star-half:before {\n  content: \"\\f089\";\n}\n.fa-heart-o:before {\n  content: \"\\f08a\";\n}\n.fa-sign-out:before {\n  content: \"\\f08b\";\n}\n.fa-linkedin-square:before {\n  content: \"\\f08c\";\n}\n.fa-thumb-tack:before {\n  content: \"\\f08d\";\n}\n.fa-external-link:before {\n  content: \"\\f08e\";\n}\n.fa-sign-in:before {\n  content: \"\\f090\";\n}\n.fa-trophy:before {\n  content: \"\\f091\";\n}\n.fa-github-square:before {\n  content: \"\\f092\";\n}\n.fa-upload:before {\n  content: \"\\f093\";\n}\n.fa-lemon-o:before {\n  content: \"\\f094\";\n}\n.fa-phone:before {\n  content: \"\\f095\";\n}\n.fa-square-o:before {\n  content: \"\\f096\";\n}\n.fa-bookmark-o:before {\n  content: \"\\f097\";\n}\n.fa-phone-square:before {\n  content: \"\\f098\";\n}\n.fa-twitter:before {\n  content: \"\\f099\";\n}\n.fa-facebook-f:before,\n.fa-facebook:before {\n  content: \"\\f09a\";\n}\n.fa-github:before {\n  content: \"\\f09b\";\n}\n.fa-unlock:before {\n  content: \"\\f09c\";\n}\n.fa-credit-card:before {\n  content: \"\\f09d\";\n}\n.fa-feed:before,\n.fa-rss:before {\n  content: \"\\f09e\";\n}\n.fa-hdd-o:before {\n  content: \"\\f0a0\";\n}\n.fa-bullhorn:before {\n  content: \"\\f0a1\";\n}\n.fa-bell:before {\n  content: \"\\f0f3\";\n}\n.fa-certificate:before {\n  content: \"\\f0a3\";\n}\n.fa-hand-o-right:before {\n  content: \"\\f0a4\";\n}\n.fa-hand-o-left:before {\n  content: \"\\f0a5\";\n}\n.fa-hand-o-up:before {\n  content: \"\\f0a6\";\n}\n.fa-hand-o-down:before {\n  content: \"\\f0a7\";\n}\n.fa-arrow-circle-left:before {\n  content: \"\\f0a8\";\n}\n.fa-arrow-circle-right:before {\n  content: \"\\f0a9\";\n}\n.fa-arrow-circle-up:before {\n  content: \"\\f0aa\";\n}\n.fa-arrow-circle-down:before {\n  content: \"\\f0ab\";\n}\n.fa-globe:before {\n  content: \"\\f0ac\";\n}\n.fa-wrench:before {\n  content: \"\\f0ad\";\n}\n.fa-tasks:before {\n  content: \"\\f0ae\";\n}\n.fa-filter:before {\n  content: \"\\f0b0\";\n}\n.fa-briefcase:before {\n  content: \"\\f0b1\";\n}\n.fa-arrows-alt:before {\n  content: \"\\f0b2\";\n}\n.fa-group:before,\n.fa-users:before {\n  content: \"\\f0c0\";\n}\n.fa-chain:before,\n.fa-link:before {\n  content: \"\\f0c1\";\n}\n.fa-cloud:before {\n  content: \"\\f0c2\";\n}\n.fa-flask:before {\n  content: \"\\f0c3\";\n}\n.fa-cut:before,\n.fa-scissors:before {\n  content: \"\\f0c4\";\n}\n.fa-copy:before,\n.fa-files-o:before {\n  content: \"\\f0c5\";\n}\n.fa-paperclip:before {\n  content: \"\\f0c6\";\n}\n.fa-save:before,\n.fa-floppy-o:before {\n  content: \"\\f0c7\";\n}\n.fa-square:before {\n  content: \"\\f0c8\";\n}\n.fa-navicon:before,\n.fa-reorder:before,\n.fa-bars:before {\n  content: \"\\f0c9\";\n}\n.fa-list-ul:before {\n  content: \"\\f0ca\";\n}\n.fa-list-ol:before {\n  content: \"\\f0cb\";\n}\n.fa-strikethrough:before {\n  content: \"\\f0cc\";\n}\n.fa-underline:before {\n  content: \"\\f0cd\";\n}\n.fa-table:before {\n  content: \"\\f0ce\";\n}\n.fa-magic:before {\n  content: \"\\f0d0\";\n}\n.fa-truck:before {\n  content: \"\\f0d1\";\n}\n.fa-pinterest:before {\n  content: \"\\f0d2\";\n}\n.fa-pinterest-square:before {\n  content: \"\\f0d3\";\n}\n.fa-google-plus-square:before {\n  content: \"\\f0d4\";\n}\n.fa-google-plus:before {\n  content: \"\\f0d5\";\n}\n.fa-money:before {\n  content: \"\\f0d6\";\n}\n.fa-caret-down:before {\n  content: \"\\f0d7\";\n}\n.fa-caret-up:before {\n  content: \"\\f0d8\";\n}\n.fa-caret-left:before {\n  content: \"\\f0d9\";\n}\n.fa-caret-right:before {\n  content: \"\\f0da\";\n}\n.fa-columns:before {\n  content: \"\\f0db\";\n}\n.fa-unsorted:before,\n.fa-sort:before {\n  content: \"\\f0dc\";\n}\n.fa-sort-down:before,\n.fa-sort-desc:before {\n  content: \"\\f0dd\";\n}\n.fa-sort-up:before,\n.fa-sort-asc:before {\n  content: \"\\f0de\";\n}\n.fa-envelope:before {\n  content: \"\\f0e0\";\n}\n.fa-linkedin:before {\n  content: \"\\f0e1\";\n}\n.fa-rotate-left:before,\n.fa-undo:before {\n  content: \"\\f0e2\";\n}\n.fa-legal:before,\n.fa-gavel:before {\n  content: \"\\f0e3\";\n}\n.fa-dashboard:before,\n.fa-tachometer:before {\n  content: \"\\f0e4\";\n}\n.fa-comment-o:before {\n  content: \"\\f0e5\";\n}\n.fa-comments-o:before {\n  content: \"\\f0e6\";\n}\n.fa-flash:before,\n.fa-bolt:before {\n  content: \"\\f0e7\";\n}\n.fa-sitemap:before {\n  content: \"\\f0e8\";\n}\n.fa-umbrella:before {\n  content: \"\\f0e9\";\n}\n.fa-paste:before,\n.fa-clipboard:before {\n  content: \"\\f0ea\";\n}\n.fa-lightbulb-o:before {\n  content: \"\\f0eb\";\n}\n.fa-exchange:before {\n  content: \"\\f0ec\";\n}\n.fa-cloud-download:before {\n  content: \"\\f0ed\";\n}\n.fa-cloud-upload:before {\n  content: \"\\f0ee\";\n}\n.fa-user-md:before {\n  content: \"\\f0f0\";\n}\n.fa-stethoscope:before {\n  content: \"\\f0f1\";\n}\n.fa-suitcase:before {\n  content: \"\\f0f2\";\n}\n.fa-bell-o:before {\n  content: \"\\f0a2\";\n}\n.fa-coffee:before {\n  content: \"\\f0f4\";\n}\n.fa-cutlery:before {\n  content: \"\\f0f5\";\n}\n.fa-file-text-o:before {\n  content: \"\\f0f6\";\n}\n.fa-building-o:before {\n  content: \"\\f0f7\";\n}\n.fa-hospital-o:before {\n  content: \"\\f0f8\";\n}\n.fa-ambulance:before {\n  content: \"\\f0f9\";\n}\n.fa-medkit:before {\n  content: \"\\f0fa\";\n}\n.fa-fighter-jet:before {\n  content: \"\\f0fb\";\n}\n.fa-beer:before {\n  content: \"\\f0fc\";\n}\n.fa-h-square:before {\n  content: \"\\f0fd\";\n}\n.fa-plus-square:before {\n  content: \"\\f0fe\";\n}\n.fa-angle-double-left:before {\n  content: \"\\f100\";\n}\n.fa-angle-double-right:before {\n  content: \"\\f101\";\n}\n.fa-angle-double-up:before {\n  content: \"\\f102\";\n}\n.fa-angle-double-down:before {\n  content: \"\\f103\";\n}\n.fa-angle-left:before {\n  content: \"\\f104\";\n}\n.fa-angle-right:before {\n  content: \"\\f105\";\n}\n.fa-angle-up:before {\n  content: \"\\f106\";\n}\n.fa-angle-down:before {\n  content: \"\\f107\";\n}\n.fa-desktop:before {\n  content: \"\\f108\";\n}\n.fa-laptop:before {\n  content: \"\\f109\";\n}\n.fa-tablet:before {\n  content: \"\\f10a\";\n}\n.fa-mobile-phone:before,\n.fa-mobile:before {\n  content: \"\\f10b\";\n}\n.fa-circle-o:before {\n  content: \"\\f10c\";\n}\n.fa-quote-left:before {\n  content: \"\\f10d\";\n}\n.fa-quote-right:before {\n  content: \"\\f10e\";\n}\n.fa-spinner:before {\n  content: \"\\f110\";\n}\n.fa-circle:before {\n  content: \"\\f111\";\n}\n.fa-mail-reply:before,\n.fa-reply:before {\n  content: \"\\f112\";\n}\n.fa-github-alt:before {\n  content: \"\\f113\";\n}\n.fa-folder-o:before {\n  content: \"\\f114\";\n}\n.fa-folder-open-o:before {\n  content: \"\\f115\";\n}\n.fa-smile-o:before {\n  content: \"\\f118\";\n}\n.fa-frown-o:before {\n  content: \"\\f119\";\n}\n.fa-meh-o:before {\n  content: \"\\f11a\";\n}\n.fa-gamepad:before {\n  content: \"\\f11b\";\n}\n.fa-keyboard-o:before {\n  content: \"\\f11c\";\n}\n.fa-flag-o:before {\n  content: \"\\f11d\";\n}\n.fa-flag-checkered:before {\n  content: \"\\f11e\";\n}\n.fa-terminal:before {\n  content: \"\\f120\";\n}\n.fa-code:before {\n  content: \"\\f121\";\n}\n.fa-mail-reply-all:before,\n.fa-reply-all:before {\n  content: \"\\f122\";\n}\n.fa-star-half-empty:before,\n.fa-star-half-full:before,\n.fa-star-half-o:before {\n  content: \"\\f123\";\n}\n.fa-location-arrow:before {\n  content: \"\\f124\";\n}\n.fa-crop:before {\n  content: \"\\f125\";\n}\n.fa-code-fork:before {\n  content: \"\\f126\";\n}\n.fa-unlink:before,\n.fa-chain-broken:before {\n  content: \"\\f127\";\n}\n.fa-question:before {\n  content: \"\\f128\";\n}\n.fa-info:before {\n  content: \"\\f129\";\n}\n.fa-exclamation:before {\n  content: \"\\f12a\";\n}\n.fa-superscript:before {\n  content: \"\\f12b\";\n}\n.fa-subscript:before {\n  content: \"\\f12c\";\n}\n.fa-eraser:before {\n  content: \"\\f12d\";\n}\n.fa-puzzle-piece:before {\n  content: \"\\f12e\";\n}\n.fa-microphone:before {\n  content: \"\\f130\";\n}\n.fa-microphone-slash:before {\n  content: \"\\f131\";\n}\n.fa-shield:before {\n  content: \"\\f132\";\n}\n.fa-calendar-o:before {\n  content: \"\\f133\";\n}\n.fa-fire-extinguisher:before {\n  content: \"\\f134\";\n}\n.fa-rocket:before {\n  content: \"\\f135\";\n}\n.fa-maxcdn:before {\n  content: \"\\f136\";\n}\n.fa-chevron-circle-left:before {\n  content: \"\\f137\";\n}\n.fa-chevron-circle-right:before {\n  content: \"\\f138\";\n}\n.fa-chevron-circle-up:before {\n  content: \"\\f139\";\n}\n.fa-chevron-circle-down:before {\n  content: \"\\f13a\";\n}\n.fa-html5:before {\n  content: \"\\f13b\";\n}\n.fa-css3:before {\n  content: \"\\f13c\";\n}\n.fa-anchor:before {\n  content: \"\\f13d\";\n}\n.fa-unlock-alt:before {\n  content: \"\\f13e\";\n}\n.fa-bullseye:before {\n  content: \"\\f140\";\n}\n.fa-ellipsis-h:before {\n  content: \"\\f141\";\n}\n.fa-ellipsis-v:before {\n  content: \"\\f142\";\n}\n.fa-rss-square:before {\n  content: \"\\f143\";\n}\n.fa-play-circle:before {\n  content: \"\\f144\";\n}\n.fa-ticket:before {\n  content: \"\\f145\";\n}\n.fa-minus-square:before {\n  content: \"\\f146\";\n}\n.fa-minus-square-o:before {\n  content: \"\\f147\";\n}\n.fa-level-up:before {\n  content: \"\\f148\";\n}\n.fa-level-down:before {\n  content: \"\\f149\";\n}\n.fa-check-square:before {\n  content: \"\\f14a\";\n}\n.fa-pencil-square:before {\n  content: \"\\f14b\";\n}\n.fa-external-link-square:before {\n  content: \"\\f14c\";\n}\n.fa-share-square:before {\n  content: \"\\f14d\";\n}\n.fa-compass:before {\n  content: \"\\f14e\";\n}\n.fa-toggle-down:before,\n.fa-caret-square-o-down:before {\n  content: \"\\f150\";\n}\n.fa-toggle-up:before,\n.fa-caret-square-o-up:before {\n  content: \"\\f151\";\n}\n.fa-toggle-right:before,\n.fa-caret-square-o-right:before {\n  content: \"\\f152\";\n}\n.fa-euro:before,\n.fa-eur:before {\n  content: \"\\f153\";\n}\n.fa-gbp:before {\n  content: \"\\f154\";\n}\n.fa-dollar:before,\n.fa-usd:before {\n  content: \"\\f155\";\n}\n.fa-rupee:before,\n.fa-inr:before {\n  content: \"\\f156\";\n}\n.fa-cny:before,\n.fa-rmb:before,\n.fa-yen:before,\n.fa-jpy:before {\n  content: \"\\f157\";\n}\n.fa-ruble:before,\n.fa-rouble:before,\n.fa-rub:before {\n  content: \"\\f158\";\n}\n.fa-won:before,\n.fa-krw:before {\n  content: \"\\f159\";\n}\n.fa-bitcoin:before,\n.fa-btc:before {\n  content: \"\\f15a\";\n}\n.fa-file:before {\n  content: \"\\f15b\";\n}\n.fa-file-text:before {\n  content: \"\\f15c\";\n}\n.fa-sort-alpha-asc:before {\n  content: \"\\f15d\";\n}\n.fa-sort-alpha-desc:before {\n  content: \"\\f15e\";\n}\n.fa-sort-amount-asc:before {\n  content: \"\\f160\";\n}\n.fa-sort-amount-desc:before {\n  content: \"\\f161\";\n}\n.fa-sort-numeric-asc:before {\n  content: \"\\f162\";\n}\n.fa-sort-numeric-desc:before {\n  content: \"\\f163\";\n}\n.fa-thumbs-up:before {\n  content: \"\\f164\";\n}\n.fa-thumbs-down:before {\n  content: \"\\f165\";\n}\n.fa-youtube-square:before {\n  content: \"\\f166\";\n}\n.fa-youtube:before {\n  content: \"\\f167\";\n}\n.fa-xing:before {\n  content: \"\\f168\";\n}\n.fa-xing-square:before {\n  content: \"\\f169\";\n}\n.fa-youtube-play:before {\n  content: \"\\f16a\";\n}\n.fa-dropbox:before {\n  content: \"\\f16b\";\n}\n.fa-stack-overflow:before {\n  content: \"\\f16c\";\n}\n.fa-instagram:before {\n  content: \"\\f16d\";\n}\n.fa-flickr:before {\n  content: \"\\f16e\";\n}\n.fa-adn:before {\n  content: \"\\f170\";\n}\n.fa-bitbucket:before {\n  content: \"\\f171\";\n}\n.fa-bitbucket-square:before {\n  content: \"\\f172\";\n}\n.fa-tumblr:before {\n  content: \"\\f173\";\n}\n.fa-tumblr-square:before {\n  content: \"\\f174\";\n}\n.fa-long-arrow-down:before {\n  content: \"\\f175\";\n}\n.fa-long-arrow-up:before {\n  content: \"\\f176\";\n}\n.fa-long-arrow-left:before {\n  content: \"\\f177\";\n}\n.fa-long-arrow-right:before {\n  content: \"\\f178\";\n}\n.fa-apple:before {\n  content: \"\\f179\";\n}\n.fa-windows:before {\n  content: \"\\f17a\";\n}\n.fa-android:before {\n  content: \"\\f17b\";\n}\n.fa-linux:before {\n  content: \"\\f17c\";\n}\n.fa-dribbble:before {\n  content: \"\\f17d\";\n}\n.fa-skype:before {\n  content: \"\\f17e\";\n}\n.fa-foursquare:before {\n  content: \"\\f180\";\n}\n.fa-trello:before {\n  content: \"\\f181\";\n}\n.fa-female:before {\n  content: \"\\f182\";\n}\n.fa-male:before {\n  content: \"\\f183\";\n}\n.fa-gittip:before,\n.fa-gratipay:before {\n  content: \"\\f184\";\n}\n.fa-sun-o:before {\n  content: \"\\f185\";\n}\n.fa-moon-o:before {\n  content: \"\\f186\";\n}\n.fa-archive:before {\n  content: \"\\f187\";\n}\n.fa-bug:before {\n  content: \"\\f188\";\n}\n.fa-vk:before {\n  content: \"\\f189\";\n}\n.fa-weibo:before {\n  content: \"\\f18a\";\n}\n.fa-renren:before {\n  content: \"\\f18b\";\n}\n.fa-pagelines:before {\n  content: \"\\f18c\";\n}\n.fa-stack-exchange:before {\n  content: \"\\f18d\";\n}\n.fa-arrow-circle-o-right:before {\n  content: \"\\f18e\";\n}\n.fa-arrow-circle-o-left:before {\n  content: \"\\f190\";\n}\n.fa-toggle-left:before,\n.fa-caret-square-o-left:before {\n  content: \"\\f191\";\n}\n.fa-dot-circle-o:before {\n  content: \"\\f192\";\n}\n.fa-wheelchair:before {\n  content: \"\\f193\";\n}\n.fa-vimeo-square:before {\n  content: \"\\f194\";\n}\n.fa-turkish-lira:before,\n.fa-try:before {\n  content: \"\\f195\";\n}\n.fa-plus-square-o:before {\n  content: \"\\f196\";\n}\n.fa-space-shuttle:before {\n  content: \"\\f197\";\n}\n.fa-slack:before {\n  content: \"\\f198\";\n}\n.fa-envelope-square:before {\n  content: \"\\f199\";\n}\n.fa-wordpress:before {\n  content: \"\\f19a\";\n}\n.fa-openid:before {\n  content: \"\\f19b\";\n}\n.fa-institution:before,\n.fa-bank:before,\n.fa-university:before {\n  content: \"\\f19c\";\n}\n.fa-mortar-board:before,\n.fa-graduation-cap:before {\n  content: \"\\f19d\";\n}\n.fa-yahoo:before {\n  content: \"\\f19e\";\n}\n.fa-google:before {\n  content: \"\\f1a0\";\n}\n.fa-reddit:before {\n  content: \"\\f1a1\";\n}\n.fa-reddit-square:before {\n  content: \"\\f1a2\";\n}\n.fa-stumbleupon-circle:before {\n  content: \"\\f1a3\";\n}\n.fa-stumbleupon:before {\n  content: \"\\f1a4\";\n}\n.fa-delicious:before {\n  content: \"\\f1a5\";\n}\n.fa-digg:before {\n  content: \"\\f1a6\";\n}\n.fa-pied-piper-pp:before {\n  content: \"\\f1a7\";\n}\n.fa-pied-piper-alt:before {\n  content: \"\\f1a8\";\n}\n.fa-drupal:before {\n  content: \"\\f1a9\";\n}\n.fa-joomla:before {\n  content: \"\\f1aa\";\n}\n.fa-language:before {\n  content: \"\\f1ab\";\n}\n.fa-fax:before {\n  content: \"\\f1ac\";\n}\n.fa-building:before {\n  content: \"\\f1ad\";\n}\n.fa-child:before {\n  content: \"\\f1ae\";\n}\n.fa-paw:before {\n  content: \"\\f1b0\";\n}\n.fa-spoon:before {\n  content: \"\\f1b1\";\n}\n.fa-cube:before {\n  content: \"\\f1b2\";\n}\n.fa-cubes:before {\n  content: \"\\f1b3\";\n}\n.fa-behance:before {\n  content: \"\\f1b4\";\n}\n.fa-behance-square:before {\n  content: \"\\f1b5\";\n}\n.fa-steam:before {\n  content: \"\\f1b6\";\n}\n.fa-steam-square:before {\n  content: \"\\f1b7\";\n}\n.fa-recycle:before {\n  content: \"\\f1b8\";\n}\n.fa-automobile:before,\n.fa-car:before {\n  content: \"\\f1b9\";\n}\n.fa-cab:before,\n.fa-taxi:before {\n  content: \"\\f1ba\";\n}\n.fa-tree:before {\n  content: \"\\f1bb\";\n}\n.fa-spotify:before {\n  content: \"\\f1bc\";\n}\n.fa-deviantart:before {\n  content: \"\\f1bd\";\n}\n.fa-soundcloud:before {\n  content: \"\\f1be\";\n}\n.fa-database:before {\n  content: \"\\f1c0\";\n}\n.fa-file-pdf-o:before {\n  content: \"\\f1c1\";\n}\n.fa-file-word-o:before {\n  content: \"\\f1c2\";\n}\n.fa-file-excel-o:before {\n  content: \"\\f1c3\";\n}\n.fa-file-powerpoint-o:before {\n  content: \"\\f1c4\";\n}\n.fa-file-photo-o:before,\n.fa-file-picture-o:before,\n.fa-file-image-o:before {\n  content: \"\\f1c5\";\n}\n.fa-file-zip-o:before,\n.fa-file-archive-o:before {\n  content: \"\\f1c6\";\n}\n.fa-file-sound-o:before,\n.fa-file-audio-o:before {\n  content: \"\\f1c7\";\n}\n.fa-file-movie-o:before,\n.fa-file-video-o:before {\n  content: \"\\f1c8\";\n}\n.fa-file-code-o:before {\n  content: \"\\f1c9\";\n}\n.fa-vine:before {\n  content: \"\\f1ca\";\n}\n.fa-codepen:before {\n  content: \"\\f1cb\";\n}\n.fa-jsfiddle:before {\n  content: \"\\f1cc\";\n}\n.fa-life-bouy:before,\n.fa-life-buoy:before,\n.fa-life-saver:before,\n.fa-support:before,\n.fa-life-ring:before {\n  content: \"\\f1cd\";\n}\n.fa-circle-o-notch:before {\n  content: \"\\f1ce\";\n}\n.fa-ra:before,\n.fa-resistance:before,\n.fa-rebel:before {\n  content: \"\\f1d0\";\n}\n.fa-ge:before,\n.fa-empire:before {\n  content: \"\\f1d1\";\n}\n.fa-git-square:before {\n  content: \"\\f1d2\";\n}\n.fa-git:before {\n  content: \"\\f1d3\";\n}\n.fa-y-combinator-square:before,\n.fa-yc-square:before,\n.fa-hacker-news:before {\n  content: \"\\f1d4\";\n}\n.fa-tencent-weibo:before {\n  content: \"\\f1d5\";\n}\n.fa-qq:before {\n  content: \"\\f1d6\";\n}\n.fa-wechat:before,\n.fa-weixin:before {\n  content: \"\\f1d7\";\n}\n.fa-send:before,\n.fa-paper-plane:before {\n  content: \"\\f1d8\";\n}\n.fa-send-o:before,\n.fa-paper-plane-o:before {\n  content: \"\\f1d9\";\n}\n.fa-history:before {\n  content: \"\\f1da\";\n}\n.fa-circle-thin:before {\n  content: \"\\f1db\";\n}\n.fa-header:before {\n  content: \"\\f1dc\";\n}\n.fa-paragraph:before {\n  content: \"\\f1dd\";\n}\n.fa-sliders:before {\n  content: \"\\f1de\";\n}\n.fa-share-alt:before {\n  content: \"\\f1e0\";\n}\n.fa-share-alt-square:before {\n  content: \"\\f1e1\";\n}\n.fa-bomb:before {\n  content: \"\\f1e2\";\n}\n.fa-soccer-ball-o:before,\n.fa-futbol-o:before {\n  content: \"\\f1e3\";\n}\n.fa-tty:before {\n  content: \"\\f1e4\";\n}\n.fa-binoculars:before {\n  content: \"\\f1e5\";\n}\n.fa-plug:before {\n  content: \"\\f1e6\";\n}\n.fa-slideshare:before {\n  content: \"\\f1e7\";\n}\n.fa-twitch:before {\n  content: \"\\f1e8\";\n}\n.fa-yelp:before {\n  content: \"\\f1e9\";\n}\n.fa-newspaper-o:before {\n  content: \"\\f1ea\";\n}\n.fa-wifi:before {\n  content: \"\\f1eb\";\n}\n.fa-calculator:before {\n  content: \"\\f1ec\";\n}\n.fa-paypal:before {\n  content: \"\\f1ed\";\n}\n.fa-google-wallet:before {\n  content: \"\\f1ee\";\n}\n.fa-cc-visa:before {\n  content: \"\\f1f0\";\n}\n.fa-cc-mastercard:before {\n  content: \"\\f1f1\";\n}\n.fa-cc-discover:before {\n  content: \"\\f1f2\";\n}\n.fa-cc-amex:before {\n  content: \"\\f1f3\";\n}\n.fa-cc-paypal:before {\n  content: \"\\f1f4\";\n}\n.fa-cc-stripe:before {\n  content: \"\\f1f5\";\n}\n.fa-bell-slash:before {\n  content: \"\\f1f6\";\n}\n.fa-bell-slash-o:before {\n  content: \"\\f1f7\";\n}\n.fa-trash:before {\n  content: \"\\f1f8\";\n}\n.fa-copyright:before {\n  content: \"\\f1f9\";\n}\n.fa-at:before {\n  content: \"\\f1fa\";\n}\n.fa-eyedropper:before {\n  content: \"\\f1fb\";\n}\n.fa-paint-brush:before {\n  content: \"\\f1fc\";\n}\n.fa-birthday-cake:before {\n  content: \"\\f1fd\";\n}\n.fa-area-chart:before {\n  content: \"\\f1fe\";\n}\n.fa-pie-chart:before {\n  content: \"\\f200\";\n}\n.fa-line-chart:before {\n  content: \"\\f201\";\n}\n.fa-lastfm:before {\n  content: \"\\f202\";\n}\n.fa-lastfm-square:before {\n  content: \"\\f203\";\n}\n.fa-toggle-off:before {\n  content: \"\\f204\";\n}\n.fa-toggle-on:before {\n  content: \"\\f205\";\n}\n.fa-bicycle:before {\n  content: \"\\f206\";\n}\n.fa-bus:before {\n  content: \"\\f207\";\n}\n.fa-ioxhost:before {\n  content: \"\\f208\";\n}\n.fa-angellist:before {\n  content: \"\\f209\";\n}\n.fa-cc:before {\n  content: \"\\f20a\";\n}\n.fa-shekel:before,\n.fa-sheqel:before,\n.fa-ils:before {\n  content: \"\\f20b\";\n}\n.fa-meanpath:before {\n  content: \"\\f20c\";\n}\n.fa-buysellads:before {\n  content: \"\\f20d\";\n}\n.fa-connectdevelop:before {\n  content: \"\\f20e\";\n}\n.fa-dashcube:before {\n  content: \"\\f210\";\n}\n.fa-forumbee:before {\n  content: \"\\f211\";\n}\n.fa-leanpub:before {\n  content: \"\\f212\";\n}\n.fa-sellsy:before {\n  content: \"\\f213\";\n}\n.fa-shirtsinbulk:before {\n  content: \"\\f214\";\n}\n.fa-simplybuilt:before {\n  content: \"\\f215\";\n}\n.fa-skyatlas:before {\n  content: \"\\f216\";\n}\n.fa-cart-plus:before {\n  content: \"\\f217\";\n}\n.fa-cart-arrow-down:before {\n  content: \"\\f218\";\n}\n.fa-diamond:before {\n  content: \"\\f219\";\n}\n.fa-ship:before {\n  content: \"\\f21a\";\n}\n.fa-user-secret:before {\n  content: \"\\f21b\";\n}\n.fa-motorcycle:before {\n  content: \"\\f21c\";\n}\n.fa-street-view:before {\n  content: \"\\f21d\";\n}\n.fa-heartbeat:before {\n  content: \"\\f21e\";\n}\n.fa-venus:before {\n  content: \"\\f221\";\n}\n.fa-mars:before {\n  content: \"\\f222\";\n}\n.fa-mercury:before {\n  content: \"\\f223\";\n}\n.fa-intersex:before,\n.fa-transgender:before {\n  content: \"\\f224\";\n}\n.fa-transgender-alt:before {\n  content: \"\\f225\";\n}\n.fa-venus-double:before {\n  content: \"\\f226\";\n}\n.fa-mars-double:before {\n  content: \"\\f227\";\n}\n.fa-venus-mars:before {\n  content: \"\\f228\";\n}\n.fa-mars-stroke:before {\n  content: \"\\f229\";\n}\n.fa-mars-stroke-v:before {\n  content: \"\\f22a\";\n}\n.fa-mars-stroke-h:before {\n  content: \"\\f22b\";\n}\n.fa-neuter:before {\n  content: \"\\f22c\";\n}\n.fa-genderless:before {\n  content: \"\\f22d\";\n}\n.fa-facebook-official:before {\n  content: \"\\f230\";\n}\n.fa-pinterest-p:before {\n  content: \"\\f231\";\n}\n.fa-whatsapp:before {\n  content: \"\\f232\";\n}\n.fa-server:before {\n  content: \"\\f233\";\n}\n.fa-user-plus:before {\n  content: \"\\f234\";\n}\n.fa-user-times:before {\n  content: \"\\f235\";\n}\n.fa-hotel:before,\n.fa-bed:before {\n  content: \"\\f236\";\n}\n.fa-viacoin:before {\n  content: \"\\f237\";\n}\n.fa-train:before {\n  content: \"\\f238\";\n}\n.fa-subway:before {\n  content: \"\\f239\";\n}\n.fa-medium:before {\n  content: \"\\f23a\";\n}\n.fa-yc:before,\n.fa-y-combinator:before {\n  content: \"\\f23b\";\n}\n.fa-optin-monster:before {\n  content: \"\\f23c\";\n}\n.fa-opencart:before {\n  content: \"\\f23d\";\n}\n.fa-expeditedssl:before {\n  content: \"\\f23e\";\n}\n.fa-battery-4:before,\n.fa-battery:before,\n.fa-battery-full:before {\n  content: \"\\f240\";\n}\n.fa-battery-3:before,\n.fa-battery-three-quarters:before {\n  content: \"\\f241\";\n}\n.fa-battery-2:before,\n.fa-battery-half:before {\n  content: \"\\f242\";\n}\n.fa-battery-1:before,\n.fa-battery-quarter:before {\n  content: \"\\f243\";\n}\n.fa-battery-0:before,\n.fa-battery-empty:before {\n  content: \"\\f244\";\n}\n.fa-mouse-pointer:before {\n  content: \"\\f245\";\n}\n.fa-i-cursor:before {\n  content: \"\\f246\";\n}\n.fa-object-group:before {\n  content: \"\\f247\";\n}\n.fa-object-ungroup:before {\n  content: \"\\f248\";\n}\n.fa-sticky-note:before {\n  content: \"\\f249\";\n}\n.fa-sticky-note-o:before {\n  content: \"\\f24a\";\n}\n.fa-cc-jcb:before {\n  content: \"\\f24b\";\n}\n.fa-cc-diners-club:before {\n  content: \"\\f24c\";\n}\n.fa-clone:before {\n  content: \"\\f24d\";\n}\n.fa-balance-scale:before {\n  content: \"\\f24e\";\n}\n.fa-hourglass-o:before {\n  content: \"\\f250\";\n}\n.fa-hourglass-1:before,\n.fa-hourglass-start:before {\n  content: \"\\f251\";\n}\n.fa-hourglass-2:before,\n.fa-hourglass-half:before {\n  content: \"\\f252\";\n}\n.fa-hourglass-3:before,\n.fa-hourglass-end:before {\n  content: \"\\f253\";\n}\n.fa-hourglass:before {\n  content: \"\\f254\";\n}\n.fa-hand-grab-o:before,\n.fa-hand-rock-o:before {\n  content: \"\\f255\";\n}\n.fa-hand-stop-o:before,\n.fa-hand-paper-o:before {\n  content: \"\\f256\";\n}\n.fa-hand-scissors-o:before {\n  content: \"\\f257\";\n}\n.fa-hand-lizard-o:before {\n  content: \"\\f258\";\n}\n.fa-hand-spock-o:before {\n  content: \"\\f259\";\n}\n.fa-hand-pointer-o:before {\n  content: \"\\f25a\";\n}\n.fa-hand-peace-o:before {\n  content: \"\\f25b\";\n}\n.fa-trademark:before {\n  content: \"\\f25c\";\n}\n.fa-registered:before {\n  content: \"\\f25d\";\n}\n.fa-creative-commons:before {\n  content: \"\\f25e\";\n}\n.fa-gg:before {\n  content: \"\\f260\";\n}\n.fa-gg-circle:before {\n  content: \"\\f261\";\n}\n.fa-tripadvisor:before {\n  content: \"\\f262\";\n}\n.fa-odnoklassniki:before {\n  content: \"\\f263\";\n}\n.fa-odnoklassniki-square:before {\n  content: \"\\f264\";\n}\n.fa-get-pocket:before {\n  content: \"\\f265\";\n}\n.fa-wikipedia-w:before {\n  content: \"\\f266\";\n}\n.fa-safari:before {\n  content: \"\\f267\";\n}\n.fa-chrome:before {\n  content: \"\\f268\";\n}\n.fa-firefox:before {\n  content: \"\\f269\";\n}\n.fa-opera:before {\n  content: \"\\f26a\";\n}\n.fa-internet-explorer:before {\n  content: \"\\f26b\";\n}\n.fa-tv:before,\n.fa-television:before {\n  content: \"\\f26c\";\n}\n.fa-contao:before {\n  content: \"\\f26d\";\n}\n.fa-500px:before {\n  content: \"\\f26e\";\n}\n.fa-amazon:before {\n  content: \"\\f270\";\n}\n.fa-calendar-plus-o:before {\n  content: \"\\f271\";\n}\n.fa-calendar-minus-o:before {\n  content: \"\\f272\";\n}\n.fa-calendar-times-o:before {\n  content: \"\\f273\";\n}\n.fa-calendar-check-o:before {\n  content: \"\\f274\";\n}\n.fa-industry:before {\n  content: \"\\f275\";\n}\n.fa-map-pin:before {\n  content: \"\\f276\";\n}\n.fa-map-signs:before {\n  content: \"\\f277\";\n}\n.fa-map-o:before {\n  content: \"\\f278\";\n}\n.fa-map:before {\n  content: \"\\f279\";\n}\n.fa-commenting:before {\n  content: \"\\f27a\";\n}\n.fa-commenting-o:before {\n  content: \"\\f27b\";\n}\n.fa-houzz:before {\n  content: \"\\f27c\";\n}\n.fa-vimeo:before {\n  content: \"\\f27d\";\n}\n.fa-black-tie:before {\n  content: \"\\f27e\";\n}\n.fa-fonticons:before {\n  content: \"\\f280\";\n}\n.fa-reddit-alien:before {\n  content: \"\\f281\";\n}\n.fa-edge:before {\n  content: \"\\f282\";\n}\n.fa-credit-card-alt:before {\n  content: \"\\f283\";\n}\n.fa-codiepie:before {\n  content: \"\\f284\";\n}\n.fa-modx:before {\n  content: \"\\f285\";\n}\n.fa-fort-awesome:before {\n  content: \"\\f286\";\n}\n.fa-usb:before {\n  content: \"\\f287\";\n}\n.fa-product-hunt:before {\n  content: \"\\f288\";\n}\n.fa-mixcloud:before {\n  content: \"\\f289\";\n}\n.fa-scribd:before {\n  content: \"\\f28a\";\n}\n.fa-pause-circle:before {\n  content: \"\\f28b\";\n}\n.fa-pause-circle-o:before {\n  content: \"\\f28c\";\n}\n.fa-stop-circle:before {\n  content: \"\\f28d\";\n}\n.fa-stop-circle-o:before {\n  content: \"\\f28e\";\n}\n.fa-shopping-bag:before {\n  content: \"\\f290\";\n}\n.fa-shopping-basket:before {\n  content: \"\\f291\";\n}\n.fa-hashtag:before {\n  content: \"\\f292\";\n}\n.fa-bluetooth:before {\n  content: \"\\f293\";\n}\n.fa-bluetooth-b:before {\n  content: \"\\f294\";\n}\n.fa-percent:before {\n  content: \"\\f295\";\n}\n.fa-gitlab:before {\n  content: \"\\f296\";\n}\n.fa-wpbeginner:before {\n  content: \"\\f297\";\n}\n.fa-wpforms:before {\n  content: \"\\f298\";\n}\n.fa-envira:before {\n  content: \"\\f299\";\n}\n.fa-universal-access:before {\n  content: \"\\f29a\";\n}\n.fa-wheelchair-alt:before {\n  content: \"\\f29b\";\n}\n.fa-question-circle-o:before {\n  content: \"\\f29c\";\n}\n.fa-blind:before {\n  content: \"\\f29d\";\n}\n.fa-audio-description:before {\n  content: \"\\f29e\";\n}\n.fa-volume-control-phone:before {\n  content: \"\\f2a0\";\n}\n.fa-braille:before {\n  content: \"\\f2a1\";\n}\n.fa-assistive-listening-systems:before {\n  content: \"\\f2a2\";\n}\n.fa-asl-interpreting:before,\n.fa-american-sign-language-interpreting:before {\n  content: \"\\f2a3\";\n}\n.fa-deafness:before,\n.fa-hard-of-hearing:before,\n.fa-deaf:before {\n  content: \"\\f2a4\";\n}\n.fa-glide:before {\n  content: \"\\f2a5\";\n}\n.fa-glide-g:before {\n  content: \"\\f2a6\";\n}\n.fa-signing:before,\n.fa-sign-language:before {\n  content: \"\\f2a7\";\n}\n.fa-low-vision:before {\n  content: \"\\f2a8\";\n}\n.fa-viadeo:before {\n  content: \"\\f2a9\";\n}\n.fa-viadeo-square:before {\n  content: \"\\f2aa\";\n}\n.fa-snapchat:before {\n  content: \"\\f2ab\";\n}\n.fa-snapchat-ghost:before {\n  content: \"\\f2ac\";\n}\n.fa-snapchat-square:before {\n  content: \"\\f2ad\";\n}\n.fa-pied-piper:before {\n  content: \"\\f2ae\";\n}\n.fa-first-order:before {\n  content: \"\\f2b0\";\n}\n.fa-yoast:before {\n  content: \"\\f2b1\";\n}\n.fa-themeisle:before {\n  content: \"\\f2b2\";\n}\n.fa-google-plus-circle:before,\n.fa-google-plus-official:before {\n  content: \"\\f2b3\";\n}\n.fa-fa:before,\n.fa-font-awesome:before {\n  content: \"\\f2b4\";\n}\n.fa-handshake-o:before {\n  content: \"\\f2b5\";\n}\n.fa-envelope-open:before {\n  content: \"\\f2b6\";\n}\n.fa-envelope-open-o:before {\n  content: \"\\f2b7\";\n}\n.fa-linode:before {\n  content: \"\\f2b8\";\n}\n.fa-address-book:before {\n  content: \"\\f2b9\";\n}\n.fa-address-book-o:before {\n  content: \"\\f2ba\";\n}\n.fa-vcard:before,\n.fa-address-card:before {\n  content: \"\\f2bb\";\n}\n.fa-vcard-o:before,\n.fa-address-card-o:before {\n  content: \"\\f2bc\";\n}\n.fa-user-circle:before {\n  content: \"\\f2bd\";\n}\n.fa-user-circle-o:before {\n  content: \"\\f2be\";\n}\n.fa-user-o:before {\n  content: \"\\f2c0\";\n}\n.fa-id-badge:before {\n  content: \"\\f2c1\";\n}\n.fa-drivers-license:before,\n.fa-id-card:before {\n  content: \"\\f2c2\";\n}\n.fa-drivers-license-o:before,\n.fa-id-card-o:before {\n  content: \"\\f2c3\";\n}\n.fa-quora:before {\n  content: \"\\f2c4\";\n}\n.fa-free-code-camp:before {\n  content: \"\\f2c5\";\n}\n.fa-telegram:before {\n  content: \"\\f2c6\";\n}\n.fa-thermometer-4:before,\n.fa-thermometer:before,\n.fa-thermometer-full:before {\n  content: \"\\f2c7\";\n}\n.fa-thermometer-3:before,\n.fa-thermometer-three-quarters:before {\n  content: \"\\f2c8\";\n}\n.fa-thermometer-2:before,\n.fa-thermometer-half:before {\n  content: \"\\f2c9\";\n}\n.fa-thermometer-1:before,\n.fa-thermometer-quarter:before {\n  content: \"\\f2ca\";\n}\n.fa-thermometer-0:before,\n.fa-thermometer-empty:before {\n  content: \"\\f2cb\";\n}\n.fa-shower:before {\n  content: \"\\f2cc\";\n}\n.fa-bathtub:before,\n.fa-s15:before,\n.fa-bath:before {\n  content: \"\\f2cd\";\n}\n.fa-podcast:before {\n  content: \"\\f2ce\";\n}\n.fa-window-maximize:before {\n  content: \"\\f2d0\";\n}\n.fa-window-minimize:before {\n  content: \"\\f2d1\";\n}\n.fa-window-restore:before {\n  content: \"\\f2d2\";\n}\n.fa-times-rectangle:before,\n.fa-window-close:before {\n  content: \"\\f2d3\";\n}\n.fa-times-rectangle-o:before,\n.fa-window-close-o:before {\n  content: \"\\f2d4\";\n}\n.fa-bandcamp:before {\n  content: \"\\f2d5\";\n}\n.fa-grav:before {\n  content: \"\\f2d6\";\n}\n.fa-etsy:before {\n  content: \"\\f2d7\";\n}\n.fa-imdb:before {\n  content: \"\\f2d8\";\n}\n.fa-ravelry:before {\n  content: \"\\f2d9\";\n}\n.fa-eercast:before {\n  content: \"\\f2da\";\n}\n.fa-microchip:before {\n  content: \"\\f2db\";\n}\n.fa-snowflake-o:before {\n  content: \"\\f2dc\";\n}\n.fa-superpowers:before {\n  content: \"\\f2dd\";\n}\n.fa-wpexplorer:before {\n  content: \"\\f2de\";\n}\n.fa-meetup:before {\n  content: \"\\f2e0\";\n}\n.sr-only {\n  position: absolute;\n  width: 1px;\n  height: 1px;\n  padding: 0;\n  margin: -1px;\n  overflow: hidden;\n  clip: rect(0, 0, 0, 0);\n  border: 0;\n}\n.sr-only-focusable:active,\n.sr-only-focusable:focus {\n  position: static;\n  width: auto;\n  height: auto;\n  margin: 0;\n  overflow: visible;\n  clip: auto;\n}\n.main-container__wrap {\n  display: flex;\n  flex-wrap: wrap;\n}\n/***TOP MENU**/\n.top-menu-wrap{\n  height: 50px;\n  background-color: #26696E;\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  color: white;\n  padding: 0 15px;\n}\n.menu-right {\n  display: flex;\n}\n.main-nav-left {\n  box-shadow: inset 0px 28px 20px -20px rgba(0,0,0,0.28);\n}\n.menu-search i,.menu-items i{\n  padding : 0.6rem;\n}\n.menu-auth {\n  padding : 1rem;\n  line-height: 0.2em;\n}\n/*main nav left*/\n.main-nav-left {\n  background: #26696E;\n  color: rgba(255, 255, 255, 0.7);\n  border-right: 1px solid #26696E !important;\n  min-height: calc(100vh - 100px);\n}\n.main-nav-item {\n  padding: 20px 0 12px;\n}\n.main-nav-item:not(:last-child) {\n  border-bottom: 1px solid #7DA5A8;\n}\n.main-nav-left a {\n  color: rgba(255, 255, 255, 0.7);\n}\n.main-nav-left .main-nav-item:first-child a:before {\n  content: \"\\f015\";\n}\n.main-nav-left .main-nav-item:nth-child(2) a:before {\n  content: \"\\f0c0\";\n}\n.main-nav-left .main-nav-item:nth-child(3) a:before {\n  content: \"\\f0b1\";\n}\n.main-nav-left .main-nav-item:nth-child(4) a:before {\n  content: \"\\f07b\";\n}\n.main-nav-left .main-nav-item:nth-child(5) a:before {\n  content: \"\\f085\";\n}\n.main-nav-left .main-nav-item:nth-child(6) a:before {\n  content: \"\\f055\";\n}\n.main-nav-left .main-nav-item:nth-child(7) a:before {\n  content: \"\\f055\";\n}\n.main-nav-left .main-nav-item a:before {\n  font-family: \"Font Awesome 5 Free\";\n  /*font: normal normal normal 14px/1 FontAwesome;*/\n  font-weight: 900;\n  padding-right: 22px;\n}\n.menu-active {\n  color: #fff;\n  font-weight: bold;\n}\n.main-menu-top {\n  padding: 0 !important;\n  background: #0E5B61;\n  /*box-shadow: 0px 4px 28px rgba(0, 0, 0, 0.25);*/\n  box-shadow: inset 0px 28px 20px -20px rgba(0,0,0,0.28);\n}\n.main-menu-top a, .main-menu-top i {\n  color: rgba(255, 255, 255, 0.8);\n  padding: 1em 28px;\n}\n.candidate-search {\n  padding: 42px 0 31px !important;\n}\n.candidate-search input {\n  background: #F6F6F6;\n  border: 1px solid #E1E1E1;\n  font-size: 14px;\n  padding: 12px;\n  height: 40px;\n}\n.candidate-search input::-webkit-input-placeholder {\n  text-transform: uppercase;\n  color: #757575;\n}\n.candidate-search input::-moz-placeholder {\n  text-transform: uppercase;\n  color: #757575;\n}\n.candidate-search input::-ms-input-placeholder {\n  text-transform: uppercase;\n  color: #757575;\n}\n.candidate-search input::placeholder {\n  text-transform: uppercase;\n  color: #757575;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJub2RlX21vZHVsZXMvZm9udC1hd2Vzb21lL2Nzcy9mb250LWF3ZXNvbWUuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdEQUFnRDtBQUNoRCx1QkFBdUI7QUNEdkI7OztFQUdFO0FBQ0Y7K0JBQytCO0FBQy9CO0VBQ0UsMEJBQTBCO0VBQzFCLDJDQUFvRDtFQUNwRCxvVUFBaVg7RUFDalgsbUJBQW1CO0VBQ25CLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UscUJBQXFCO0VBQ3JCLDZDQUE2QztFQUM3QyxrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLG1DQUFtQztFQUNuQyxrQ0FBa0M7QUFDcEM7QUFDQSw2REFBNkQ7QUFDN0Q7RUFDRSx1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLG9CQUFvQjtBQUN0QjtBQUNBO0VBQ0UsY0FBYztBQUNoQjtBQUNBO0VBQ0UsY0FBYztBQUNoQjtBQUNBO0VBQ0UsY0FBYztBQUNoQjtBQUNBO0VBQ0UsY0FBYztBQUNoQjtBQUNBO0VBQ0UsbUJBQW1CO0VBQ25CLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixxQkFBcUI7QUFDdkI7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsNEJBQTRCO0VBQzVCLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsV0FBVztBQUNiO0FBQ0E7RUFDRSxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsaUJBQWlCO0FBQ25CO0FBQ0EsMkJBQTJCO0FBQzNCO0VBQ0UsWUFBWTtBQUNkO0FBQ0E7RUFDRSxXQUFXO0FBQ2I7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsaUJBQWlCO0FBQ25CO0FBQ0E7RUFDRSw2Q0FBNkM7RUFDN0MscUNBQXFDO0FBQ3ZDO0FBQ0E7RUFDRSwrQ0FBK0M7RUFDL0MsdUNBQXVDO0FBQ3pDO0FBQ0E7RUFDRTtJQUVFLHVCQUF1QjtFQUN6QjtFQUNBO0lBRUUseUJBQXlCO0VBQzNCO0FBQ0Y7QUFDQTtFQUNFO0lBRUUsdUJBQXVCO0VBQ3pCO0VBQ0E7SUFFRSx5QkFBeUI7RUFDM0I7QUFDRjtBQUNBO0VBQ0Usc0VBQXNFO0VBR3RFLHdCQUF3QjtBQUMxQjtBQUNBO0VBQ0Usc0VBQXNFO0VBR3RFLHlCQUF5QjtBQUMzQjtBQUNBO0VBQ0Usc0VBQXNFO0VBR3RFLHlCQUF5QjtBQUMzQjtBQUNBO0VBQ0UsZ0ZBQWdGO0VBR2hGLHVCQUF1QjtBQUN6QjtBQUNBO0VBQ0UsZ0ZBQWdGO0VBR2hGLHVCQUF1QjtBQUN6QjtBQUNBOzs7OztFQUtFLG9CQUFZO1VBQVosWUFBWTtBQUNkO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLFVBQVU7RUFDVixXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLHNCQUFzQjtBQUN4QjtBQUNBOztFQUVFLGtCQUFrQjtFQUNsQixPQUFPO0VBQ1AsV0FBVztFQUNYLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0Usb0JBQW9CO0FBQ3RCO0FBQ0E7RUFDRSxjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxjQUFjO0FBQ2hCO0FBQ0E7bUVBQ21FO0FBQ25FO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7OztFQUdFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOzs7RUFHRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTs7O0VBR0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTs7O0VBR0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOzs7O0VBSUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7OztFQUdFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7OztFQUdFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7OztFQUdFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7Ozs7O0VBS0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTs7O0VBR0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOzs7RUFHRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7OztFQUdFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7OztFQUdFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7OztFQUdFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTs7RUFFRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7OztFQUdFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7OztFQUdFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7O0VBRUUsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixXQUFXO0VBQ1gsVUFBVTtFQUNWLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsc0JBQXNCO0VBQ3RCLFNBQVM7QUFDWDtBQUNBOztFQUVFLGdCQUFnQjtFQUNoQixXQUFXO0VBQ1gsWUFBWTtFQUNaLFNBQVM7RUFDVCxpQkFBaUI7RUFDakIsVUFBVTtBQUNaO0FEN3hFQTtFQUNFLGFBQWE7RUFDYixlQUFlO0FBQ2pCO0FBQ0EsY0FBYztBQUNkO0VBQ0UsWUFBWTtFQUNaLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDhCQUE4QjtFQUM5QixZQUFZO0VBQ1osZUFBZTtBQUNqQjtBQUNBO0VBQ0UsYUFBYTtBQUNmO0FBQ0E7RUFDRSxzREFBc0Q7QUFDeEQ7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsY0FBYztFQUNkLGtCQUFrQjtBQUNwQjtBQUNBLGdCQUFnQjtBQUNoQjtFQUNFLG1CQUFtQjtFQUNuQiwrQkFBK0I7RUFDL0IsMENBQTBDO0VBQzFDLCtCQUErQjtBQUNqQztBQUNBO0VBQ0Usb0JBQW9CO0FBQ3RCO0FBQ0E7RUFDRSxnQ0FBZ0M7QUFDbEM7QUFDQTtFQUNFLCtCQUErQjtBQUNqQztBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBRUE7RUFDRSxnQkFBZ0I7QUFDbEI7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBRUE7RUFDRSxrQ0FBa0M7RUFDbEMsaURBQWlEO0VBQ2pELGdCQUFnQjtFQUNoQixtQkFBbUI7QUFDckI7QUFDQTtFQUNFLFdBQVc7RUFDWCxpQkFBaUI7QUFDbkI7QUFDQTtFQUNFLHFCQUFxQjtFQUNyQixtQkFBbUI7RUFDbkIsZ0RBQWdEO0VBQ2hELHNEQUFzRDtBQUN4RDtBQUNBO0VBQ0UsK0JBQStCO0VBQy9CLGlCQUFpQjtBQUNuQjtBQUNBO0VBQ0UsK0JBQStCO0FBQ2pDO0FBQ0E7RUFDRSxtQkFBbUI7RUFDbkIseUJBQXlCO0VBQ3pCLGVBQWU7RUFDZixhQUFhO0VBQ2IsWUFBWTtBQUNkO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsY0FBYztBQUNoQjtBQUhBO0VBQ0UseUJBQXlCO0VBQ3pCLGNBQWM7QUFDaEI7QUFIQTtFQUNFLHlCQUF5QjtFQUN6QixjQUFjO0FBQ2hCO0FBSEE7RUFDRSx5QkFBeUI7RUFDekIsY0FBYztBQUNoQiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLypAaW1wb3J0ICd+Zm9udC1hd2Vzb21lL2Nzcy9mb250LWF3ZXNvbWUuY3NzJzsqL1xuLyptYWluLWNvbnRhaW5lcl9fd3JhcCovXG5AaW1wb3J0ICcuLi8uLi9ub2RlX21vZHVsZXMvZm9udC1hd2Vzb21lL2Nzcy9mb250LWF3ZXNvbWUuY3NzJztcbi5tYWluLWNvbnRhaW5lcl9fd3JhcCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cbi8qKipUT1AgTUVOVSoqL1xuLnRvcC1tZW51LXdyYXB7XG4gIGhlaWdodDogNTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzI2Njk2RTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDAgMTVweDtcbn1cbi5tZW51LXJpZ2h0IHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5tYWluLW5hdi1sZWZ0IHtcbiAgYm94LXNoYWRvdzogaW5zZXQgMHB4IDI4cHggMjBweCAtMjBweCByZ2JhKDAsMCwwLDAuMjgpO1xufVxuLm1lbnUtc2VhcmNoIGksLm1lbnUtaXRlbXMgaXtcbiAgcGFkZGluZyA6IDAuNnJlbTtcbn1cbi5tZW51LWF1dGgge1xuICBwYWRkaW5nIDogMXJlbTtcbiAgbGluZS1oZWlnaHQ6IDAuMmVtO1xufVxuLyptYWluIG5hdiBsZWZ0Ki9cbi5tYWluLW5hdi1sZWZ0IHtcbiAgYmFja2dyb3VuZDogIzI2Njk2RTtcbiAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43KTtcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgIzI2Njk2RSAhaW1wb3J0YW50O1xuICBtaW4taGVpZ2h0OiBjYWxjKDEwMHZoIC0gMTAwcHgpO1xufVxuLm1haW4tbmF2LWl0ZW0ge1xuICBwYWRkaW5nOiAyMHB4IDAgMTJweDtcbn1cbi5tYWluLW5hdi1pdGVtOm5vdCg6bGFzdC1jaGlsZCkge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzdEQTVBODtcbn1cbi5tYWluLW5hdi1sZWZ0IGEge1xuICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjcpO1xufVxuLm1haW4tbmF2LWxlZnQgLm1haW4tbmF2LWl0ZW06Zmlyc3QtY2hpbGQgYTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAxNVwiO1xufVxuLm1haW4tbmF2LWxlZnQgLm1haW4tbmF2LWl0ZW06bnRoLWNoaWxkKDIpIGE6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwYzBcIjtcbn1cblxuLm1haW4tbmF2LWxlZnQgLm1haW4tbmF2LWl0ZW06bnRoLWNoaWxkKDMpIGE6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwYjFcIjtcbn1cbi5tYWluLW5hdi1sZWZ0IC5tYWluLW5hdi1pdGVtOm50aC1jaGlsZCg0KSBhOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDdiXCI7XG59XG5cbi5tYWluLW5hdi1sZWZ0IC5tYWluLW5hdi1pdGVtOm50aC1jaGlsZCg1KSBhOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDg1XCI7XG59XG5cbi5tYWluLW5hdi1sZWZ0IC5tYWluLW5hdi1pdGVtOm50aC1jaGlsZCg2KSBhOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDU1XCI7XG59XG5cbi5tYWluLW5hdi1sZWZ0IC5tYWluLW5hdi1pdGVtOm50aC1jaGlsZCg3KSBhOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDU1XCI7XG59XG5cbi5tYWluLW5hdi1sZWZ0IC5tYWluLW5hdi1pdGVtIGE6YmVmb3JlIHtcbiAgZm9udC1mYW1pbHk6IFwiRm9udCBBd2Vzb21lIDUgRnJlZVwiO1xuICAvKmZvbnQ6IG5vcm1hbCBub3JtYWwgbm9ybWFsIDE0cHgvMSBGb250QXdlc29tZTsqL1xuICBmb250LXdlaWdodDogOTAwO1xuICBwYWRkaW5nLXJpZ2h0OiAyMnB4O1xufVxuLm1lbnUtYWN0aXZlIHtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLm1haW4tbWVudS10b3Age1xuICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6ICMwRTVCNjE7XG4gIC8qYm94LXNoYWRvdzogMHB4IDRweCAyOHB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7Ki9cbiAgYm94LXNoYWRvdzogaW5zZXQgMHB4IDI4cHggMjBweCAtMjBweCByZ2JhKDAsMCwwLDAuMjgpO1xufVxuLm1haW4tbWVudS10b3AgYSwgLm1haW4tbWVudS10b3AgaSB7XG4gIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOCk7XG4gIHBhZGRpbmc6IDFlbSAyOHB4O1xufVxuLmNhbmRpZGF0ZS1zZWFyY2gge1xuICBwYWRkaW5nOiA0MnB4IDAgMzFweCAhaW1wb3J0YW50O1xufVxuLmNhbmRpZGF0ZS1zZWFyY2ggaW5wdXQge1xuICBiYWNrZ3JvdW5kOiAjRjZGNkY2O1xuICBib3JkZXI6IDFweCBzb2xpZCAjRTFFMUUxO1xuICBmb250LXNpemU6IDE0cHg7XG4gIHBhZGRpbmc6IDEycHg7XG4gIGhlaWdodDogNDBweDtcbn1cbi5jYW5kaWRhdGUtc2VhcmNoIGlucHV0OjpwbGFjZWhvbGRlciB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGNvbG9yOiAjNzU3NTc1O1xufVxuIiwiLyohXG4gKiAgRm9udCBBd2Vzb21lIDQuNy4wIGJ5IEBkYXZlZ2FuZHkgLSBodHRwOi8vZm9udGF3ZXNvbWUuaW8gLSBAZm9udGF3ZXNvbWVcbiAqICBMaWNlbnNlIC0gaHR0cDovL2ZvbnRhd2Vzb21lLmlvL2xpY2Vuc2UgKEZvbnQ6IFNJTCBPRkwgMS4xLCBDU1M6IE1JVCBMaWNlbnNlKVxuICovXG4vKiBGT05UIFBBVEhcbiAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tICovXG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6ICdGb250QXdlc29tZSc7XG4gIHNyYzogdXJsKCcuLi9mb250cy9mb250YXdlc29tZS13ZWJmb250LmVvdD92PTQuNy4wJyk7XG4gIHNyYzogdXJsKCcuLi9mb250cy9mb250YXdlc29tZS13ZWJmb250LmVvdD8jaWVmaXgmdj00LjcuMCcpIGZvcm1hdCgnZW1iZWRkZWQtb3BlbnR5cGUnKSwgdXJsKCcuLi9mb250cy9mb250YXdlc29tZS13ZWJmb250LndvZmYyP3Y9NC43LjAnKSBmb3JtYXQoJ3dvZmYyJyksIHVybCgnLi4vZm9udHMvZm9udGF3ZXNvbWUtd2ViZm9udC53b2ZmP3Y9NC43LjAnKSBmb3JtYXQoJ3dvZmYnKSwgdXJsKCcuLi9mb250cy9mb250YXdlc29tZS13ZWJmb250LnR0Zj92PTQuNy4wJykgZm9ybWF0KCd0cnVldHlwZScpLCB1cmwoJy4uL2ZvbnRzL2ZvbnRhd2Vzb21lLXdlYmZvbnQuc3ZnP3Y9NC43LjAjZm9udGF3ZXNvbWVyZWd1bGFyJykgZm9ybWF0KCdzdmcnKTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xufVxuLmZhIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250OiBub3JtYWwgbm9ybWFsIG5vcm1hbCAxNHB4LzEgRm9udEF3ZXNvbWU7XG4gIGZvbnQtc2l6ZTogaW5oZXJpdDtcbiAgdGV4dC1yZW5kZXJpbmc6IGF1dG87XG4gIC13ZWJraXQtZm9udC1zbW9vdGhpbmc6IGFudGlhbGlhc2VkO1xuICAtbW96LW9zeC1mb250LXNtb290aGluZzogZ3JheXNjYWxlO1xufVxuLyogbWFrZXMgdGhlIGZvbnQgMzMlIGxhcmdlciByZWxhdGl2ZSB0byB0aGUgaWNvbiBjb250YWluZXIgKi9cbi5mYS1sZyB7XG4gIGZvbnQtc2l6ZTogMS4zMzMzMzMzM2VtO1xuICBsaW5lLWhlaWdodDogMC43NWVtO1xuICB2ZXJ0aWNhbC1hbGlnbjogLTE1JTtcbn1cbi5mYS0yeCB7XG4gIGZvbnQtc2l6ZTogMmVtO1xufVxuLmZhLTN4IHtcbiAgZm9udC1zaXplOiAzZW07XG59XG4uZmEtNHgge1xuICBmb250LXNpemU6IDRlbTtcbn1cbi5mYS01eCB7XG4gIGZvbnQtc2l6ZTogNWVtO1xufVxuLmZhLWZ3IHtcbiAgd2lkdGg6IDEuMjg1NzE0MjllbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmZhLXVsIHtcbiAgcGFkZGluZy1sZWZ0OiAwO1xuICBtYXJnaW4tbGVmdDogMi4xNDI4NTcxNGVtO1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG59XG4uZmEtdWwgPiBsaSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5mYS1saSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogLTIuMTQyODU3MTRlbTtcbiAgd2lkdGg6IDIuMTQyODU3MTRlbTtcbiAgdG9wOiAwLjE0Mjg1NzE0ZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5mYS1saS5mYS1sZyB7XG4gIGxlZnQ6IC0xLjg1NzE0Mjg2ZW07XG59XG4uZmEtYm9yZGVyIHtcbiAgcGFkZGluZzogLjJlbSAuMjVlbSAuMTVlbTtcbiAgYm9yZGVyOiBzb2xpZCAwLjA4ZW0gI2VlZWVlZTtcbiAgYm9yZGVyLXJhZGl1czogLjFlbTtcbn1cbi5mYS1wdWxsLWxlZnQge1xuICBmbG9hdDogbGVmdDtcbn1cbi5mYS1wdWxsLXJpZ2h0IHtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuLmZhLmZhLXB1bGwtbGVmdCB7XG4gIG1hcmdpbi1yaWdodDogLjNlbTtcbn1cbi5mYS5mYS1wdWxsLXJpZ2h0IHtcbiAgbWFyZ2luLWxlZnQ6IC4zZW07XG59XG4vKiBEZXByZWNhdGVkIGFzIG9mIDQuNC4wICovXG4ucHVsbC1yaWdodCB7XG4gIGZsb2F0OiByaWdodDtcbn1cbi5wdWxsLWxlZnQge1xuICBmbG9hdDogbGVmdDtcbn1cbi5mYS5wdWxsLWxlZnQge1xuICBtYXJnaW4tcmlnaHQ6IC4zZW07XG59XG4uZmEucHVsbC1yaWdodCB7XG4gIG1hcmdpbi1sZWZ0OiAuM2VtO1xufVxuLmZhLXNwaW4ge1xuICAtd2Via2l0LWFuaW1hdGlvbjogZmEtc3BpbiAycyBpbmZpbml0ZSBsaW5lYXI7XG4gIGFuaW1hdGlvbjogZmEtc3BpbiAycyBpbmZpbml0ZSBsaW5lYXI7XG59XG4uZmEtcHVsc2Uge1xuICAtd2Via2l0LWFuaW1hdGlvbjogZmEtc3BpbiAxcyBpbmZpbml0ZSBzdGVwcyg4KTtcbiAgYW5pbWF0aW9uOiBmYS1zcGluIDFzIGluZmluaXRlIHN0ZXBzKDgpO1xufVxuQC13ZWJraXQta2V5ZnJhbWVzIGZhLXNwaW4ge1xuICAwJSB7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcbiAgfVxuICAxMDAlIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDM1OWRlZyk7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMzU5ZGVnKTtcbiAgfVxufVxuQGtleWZyYW1lcyBmYS1zcGluIHtcbiAgMCUge1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XG4gIH1cbiAgMTAwJSB7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzNTlkZWcpO1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDM1OWRlZyk7XG4gIH1cbn1cbi5mYS1yb3RhdGUtOTAge1xuICAtbXMtZmlsdGVyOiBcInByb2dpZDpEWEltYWdlVHJhbnNmb3JtLk1pY3Jvc29mdC5CYXNpY0ltYWdlKHJvdGF0aW9uPTEpXCI7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG59XG4uZmEtcm90YXRlLTE4MCB7XG4gIC1tcy1maWx0ZXI6IFwicHJvZ2lkOkRYSW1hZ2VUcmFuc2Zvcm0uTWljcm9zb2Z0LkJhc2ljSW1hZ2Uocm90YXRpb249MilcIjtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcbn1cbi5mYS1yb3RhdGUtMjcwIHtcbiAgLW1zLWZpbHRlcjogXCJwcm9naWQ6RFhJbWFnZVRyYW5zZm9ybS5NaWNyb3NvZnQuQmFzaWNJbWFnZShyb3RhdGlvbj0zKVwiO1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDI3MGRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgyNzBkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgyNzBkZWcpO1xufVxuLmZhLWZsaXAtaG9yaXpvbnRhbCB7XG4gIC1tcy1maWx0ZXI6IFwicHJvZ2lkOkRYSW1hZ2VUcmFuc2Zvcm0uTWljcm9zb2Z0LkJhc2ljSW1hZ2Uocm90YXRpb249MCwgbWlycm9yPTEpXCI7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgtMSwgMSk7XG4gIC1tcy10cmFuc2Zvcm06IHNjYWxlKC0xLCAxKTtcbiAgdHJhbnNmb3JtOiBzY2FsZSgtMSwgMSk7XG59XG4uZmEtZmxpcC12ZXJ0aWNhbCB7XG4gIC1tcy1maWx0ZXI6IFwicHJvZ2lkOkRYSW1hZ2VUcmFuc2Zvcm0uTWljcm9zb2Z0LkJhc2ljSW1hZ2Uocm90YXRpb249MiwgbWlycm9yPTEpXCI7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgxLCAtMSk7XG4gIC1tcy10cmFuc2Zvcm06IHNjYWxlKDEsIC0xKTtcbiAgdHJhbnNmb3JtOiBzY2FsZSgxLCAtMSk7XG59XG46cm9vdCAuZmEtcm90YXRlLTkwLFxuOnJvb3QgLmZhLXJvdGF0ZS0xODAsXG46cm9vdCAuZmEtcm90YXRlLTI3MCxcbjpyb290IC5mYS1mbGlwLWhvcml6b250YWwsXG46cm9vdCAuZmEtZmxpcC12ZXJ0aWNhbCB7XG4gIGZpbHRlcjogbm9uZTtcbn1cbi5mYS1zdGFjayB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogMmVtO1xuICBoZWlnaHQ6IDJlbTtcbiAgbGluZS1oZWlnaHQ6IDJlbTtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbn1cbi5mYS1zdGFjay0xeCxcbi5mYS1zdGFjay0yeCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5mYS1zdGFjay0xeCB7XG4gIGxpbmUtaGVpZ2h0OiBpbmhlcml0O1xufVxuLmZhLXN0YWNrLTJ4IHtcbiAgZm9udC1zaXplOiAyZW07XG59XG4uZmEtaW52ZXJzZSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuLyogRm9udCBBd2Vzb21lIHVzZXMgdGhlIFVuaWNvZGUgUHJpdmF0ZSBVc2UgQXJlYSAoUFVBKSB0byBlbnN1cmUgc2NyZWVuXG4gICByZWFkZXJzIGRvIG5vdCByZWFkIG9mZiByYW5kb20gY2hhcmFjdGVycyB0aGF0IHJlcHJlc2VudCBpY29ucyAqL1xuLmZhLWdsYXNzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDAwXCI7XG59XG4uZmEtbXVzaWM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwMDFcIjtcbn1cbi5mYS1zZWFyY2g6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwMDJcIjtcbn1cbi5mYS1lbnZlbG9wZS1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDAzXCI7XG59XG4uZmEtaGVhcnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwMDRcIjtcbn1cbi5mYS1zdGFyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDA1XCI7XG59XG4uZmEtc3Rhci1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDA2XCI7XG59XG4uZmEtdXNlcjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAwN1wiO1xufVxuLmZhLWZpbG06YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwMDhcIjtcbn1cbi5mYS10aC1sYXJnZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAwOVwiO1xufVxuLmZhLXRoOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDBhXCI7XG59XG4uZmEtdGgtbGlzdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAwYlwiO1xufVxuLmZhLWNoZWNrOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDBjXCI7XG59XG4uZmEtcmVtb3ZlOmJlZm9yZSxcbi5mYS1jbG9zZTpiZWZvcmUsXG4uZmEtdGltZXM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwMGRcIjtcbn1cbi5mYS1zZWFyY2gtcGx1czpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAwZVwiO1xufVxuLmZhLXNlYXJjaC1taW51czpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAxMFwiO1xufVxuLmZhLXBvd2VyLW9mZjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAxMVwiO1xufVxuLmZhLXNpZ25hbDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAxMlwiO1xufVxuLmZhLWdlYXI6YmVmb3JlLFxuLmZhLWNvZzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAxM1wiO1xufVxuLmZhLXRyYXNoLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwMTRcIjtcbn1cbi5mYS1ob21lOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDE1XCI7XG59XG4uZmEtZmlsZS1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDE2XCI7XG59XG4uZmEtY2xvY2stbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAxN1wiO1xufVxuLmZhLXJvYWQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwMThcIjtcbn1cbi5mYS1kb3dubG9hZDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAxOVwiO1xufVxuLmZhLWFycm93LWNpcmNsZS1vLWRvd246YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwMWFcIjtcbn1cbi5mYS1hcnJvdy1jaXJjbGUtby11cDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAxYlwiO1xufVxuLmZhLWluYm94OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDFjXCI7XG59XG4uZmEtcGxheS1jaXJjbGUtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAxZFwiO1xufVxuLmZhLXJvdGF0ZS1yaWdodDpiZWZvcmUsXG4uZmEtcmVwZWF0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDFlXCI7XG59XG4uZmEtcmVmcmVzaDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAyMVwiO1xufVxuLmZhLWxpc3QtYWx0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDIyXCI7XG59XG4uZmEtbG9jazpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAyM1wiO1xufVxuLmZhLWZsYWc6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwMjRcIjtcbn1cbi5mYS1oZWFkcGhvbmVzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDI1XCI7XG59XG4uZmEtdm9sdW1lLW9mZjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAyNlwiO1xufVxuLmZhLXZvbHVtZS1kb3duOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDI3XCI7XG59XG4uZmEtdm9sdW1lLXVwOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDI4XCI7XG59XG4uZmEtcXJjb2RlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDI5XCI7XG59XG4uZmEtYmFyY29kZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAyYVwiO1xufVxuLmZhLXRhZzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAyYlwiO1xufVxuLmZhLXRhZ3M6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwMmNcIjtcbn1cbi5mYS1ib29rOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDJkXCI7XG59XG4uZmEtYm9va21hcms6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwMmVcIjtcbn1cbi5mYS1wcmludDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAyZlwiO1xufVxuLmZhLWNhbWVyYTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAzMFwiO1xufVxuLmZhLWZvbnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwMzFcIjtcbn1cbi5mYS1ib2xkOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDMyXCI7XG59XG4uZmEtaXRhbGljOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDMzXCI7XG59XG4uZmEtdGV4dC1oZWlnaHQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwMzRcIjtcbn1cbi5mYS10ZXh0LXdpZHRoOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDM1XCI7XG59XG4uZmEtYWxpZ24tbGVmdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAzNlwiO1xufVxuLmZhLWFsaWduLWNlbnRlcjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAzN1wiO1xufVxuLmZhLWFsaWduLXJpZ2h0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDM4XCI7XG59XG4uZmEtYWxpZ24tanVzdGlmeTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAzOVwiO1xufVxuLmZhLWxpc3Q6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwM2FcIjtcbn1cbi5mYS1kZWRlbnQ6YmVmb3JlLFxuLmZhLW91dGRlbnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwM2JcIjtcbn1cbi5mYS1pbmRlbnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwM2NcIjtcbn1cbi5mYS12aWRlby1jYW1lcmE6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwM2RcIjtcbn1cbi5mYS1waG90bzpiZWZvcmUsXG4uZmEtaW1hZ2U6YmVmb3JlLFxuLmZhLXBpY3R1cmUtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjAzZVwiO1xufVxuLmZhLXBlbmNpbDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA0MFwiO1xufVxuLmZhLW1hcC1tYXJrZXI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNDFcIjtcbn1cbi5mYS1hZGp1c3Q6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNDJcIjtcbn1cbi5mYS10aW50OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDQzXCI7XG59XG4uZmEtZWRpdDpiZWZvcmUsXG4uZmEtcGVuY2lsLXNxdWFyZS1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDQ0XCI7XG59XG4uZmEtc2hhcmUtc3F1YXJlLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNDVcIjtcbn1cbi5mYS1jaGVjay1zcXVhcmUtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA0NlwiO1xufVxuLmZhLWFycm93czpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA0N1wiO1xufVxuLmZhLXN0ZXAtYmFja3dhcmQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNDhcIjtcbn1cbi5mYS1mYXN0LWJhY2t3YXJkOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDQ5XCI7XG59XG4uZmEtYmFja3dhcmQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNGFcIjtcbn1cbi5mYS1wbGF5OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDRiXCI7XG59XG4uZmEtcGF1c2U6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNGNcIjtcbn1cbi5mYS1zdG9wOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDRkXCI7XG59XG4uZmEtZm9yd2FyZDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA0ZVwiO1xufVxuLmZhLWZhc3QtZm9yd2FyZDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA1MFwiO1xufVxuLmZhLXN0ZXAtZm9yd2FyZDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA1MVwiO1xufVxuLmZhLWVqZWN0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDUyXCI7XG59XG4uZmEtY2hldnJvbi1sZWZ0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDUzXCI7XG59XG4uZmEtY2hldnJvbi1yaWdodDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA1NFwiO1xufVxuLmZhLXBsdXMtY2lyY2xlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDU1XCI7XG59XG4uZmEtbWludXMtY2lyY2xlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDU2XCI7XG59XG4uZmEtdGltZXMtY2lyY2xlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDU3XCI7XG59XG4uZmEtY2hlY2stY2lyY2xlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDU4XCI7XG59XG4uZmEtcXVlc3Rpb24tY2lyY2xlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDU5XCI7XG59XG4uZmEtaW5mby1jaXJjbGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNWFcIjtcbn1cbi5mYS1jcm9zc2hhaXJzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDViXCI7XG59XG4uZmEtdGltZXMtY2lyY2xlLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNWNcIjtcbn1cbi5mYS1jaGVjay1jaXJjbGUtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA1ZFwiO1xufVxuLmZhLWJhbjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA1ZVwiO1xufVxuLmZhLWFycm93LWxlZnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNjBcIjtcbn1cbi5mYS1hcnJvdy1yaWdodDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA2MVwiO1xufVxuLmZhLWFycm93LXVwOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDYyXCI7XG59XG4uZmEtYXJyb3ctZG93bjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA2M1wiO1xufVxuLmZhLW1haWwtZm9yd2FyZDpiZWZvcmUsXG4uZmEtc2hhcmU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNjRcIjtcbn1cbi5mYS1leHBhbmQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNjVcIjtcbn1cbi5mYS1jb21wcmVzczpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA2NlwiO1xufVxuLmZhLXBsdXM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNjdcIjtcbn1cbi5mYS1taW51czpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA2OFwiO1xufVxuLmZhLWFzdGVyaXNrOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDY5XCI7XG59XG4uZmEtZXhjbGFtYXRpb24tY2lyY2xlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDZhXCI7XG59XG4uZmEtZ2lmdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA2YlwiO1xufVxuLmZhLWxlYWY6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNmNcIjtcbn1cbi5mYS1maXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDZkXCI7XG59XG4uZmEtZXllOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDZlXCI7XG59XG4uZmEtZXllLXNsYXNoOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDcwXCI7XG59XG4uZmEtd2FybmluZzpiZWZvcmUsXG4uZmEtZXhjbGFtYXRpb24tdHJpYW5nbGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNzFcIjtcbn1cbi5mYS1wbGFuZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA3MlwiO1xufVxuLmZhLWNhbGVuZGFyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDczXCI7XG59XG4uZmEtcmFuZG9tOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDc0XCI7XG59XG4uZmEtY29tbWVudDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA3NVwiO1xufVxuLmZhLW1hZ25ldDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA3NlwiO1xufVxuLmZhLWNoZXZyb24tdXA6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNzdcIjtcbn1cbi5mYS1jaGV2cm9uLWRvd246YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNzhcIjtcbn1cbi5mYS1yZXR3ZWV0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDc5XCI7XG59XG4uZmEtc2hvcHBpbmctY2FydDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA3YVwiO1xufVxuLmZhLWZvbGRlcjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA3YlwiO1xufVxuLmZhLWZvbGRlci1vcGVuOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDdjXCI7XG59XG4uZmEtYXJyb3dzLXY6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwN2RcIjtcbn1cbi5mYS1hcnJvd3MtaDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA3ZVwiO1xufVxuLmZhLWJhci1jaGFydC1vOmJlZm9yZSxcbi5mYS1iYXItY2hhcnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwODBcIjtcbn1cbi5mYS10d2l0dGVyLXNxdWFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA4MVwiO1xufVxuLmZhLWZhY2Vib29rLXNxdWFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA4MlwiO1xufVxuLmZhLWNhbWVyYS1yZXRybzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA4M1wiO1xufVxuLmZhLWtleTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA4NFwiO1xufVxuLmZhLWdlYXJzOmJlZm9yZSxcbi5mYS1jb2dzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDg1XCI7XG59XG4uZmEtY29tbWVudHM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwODZcIjtcbn1cbi5mYS10aHVtYnMtby11cDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA4N1wiO1xufVxuLmZhLXRodW1icy1vLWRvd246YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwODhcIjtcbn1cbi5mYS1zdGFyLWhhbGY6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwODlcIjtcbn1cbi5mYS1oZWFydC1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDhhXCI7XG59XG4uZmEtc2lnbi1vdXQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwOGJcIjtcbn1cbi5mYS1saW5rZWRpbi1zcXVhcmU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwOGNcIjtcbn1cbi5mYS10aHVtYi10YWNrOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDhkXCI7XG59XG4uZmEtZXh0ZXJuYWwtbGluazpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA4ZVwiO1xufVxuLmZhLXNpZ24taW46YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwOTBcIjtcbn1cbi5mYS10cm9waHk6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwOTFcIjtcbn1cbi5mYS1naXRodWItc3F1YXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDkyXCI7XG59XG4uZmEtdXBsb2FkOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDkzXCI7XG59XG4uZmEtbGVtb24tbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA5NFwiO1xufVxuLmZhLXBob25lOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDk1XCI7XG59XG4uZmEtc3F1YXJlLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwOTZcIjtcbn1cbi5mYS1ib29rbWFyay1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDk3XCI7XG59XG4uZmEtcGhvbmUtc3F1YXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDk4XCI7XG59XG4uZmEtdHdpdHRlcjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA5OVwiO1xufVxuLmZhLWZhY2Vib29rLWY6YmVmb3JlLFxuLmZhLWZhY2Vib29rOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDlhXCI7XG59XG4uZmEtZ2l0aHViOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDliXCI7XG59XG4uZmEtdW5sb2NrOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDljXCI7XG59XG4uZmEtY3JlZGl0LWNhcmQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwOWRcIjtcbn1cbi5mYS1mZWVkOmJlZm9yZSxcbi5mYS1yc3M6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwOWVcIjtcbn1cbi5mYS1oZGQtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBhMFwiO1xufVxuLmZhLWJ1bGxob3JuOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGExXCI7XG59XG4uZmEtYmVsbDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBmM1wiO1xufVxuLmZhLWNlcnRpZmljYXRlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGEzXCI7XG59XG4uZmEtaGFuZC1vLXJpZ2h0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGE0XCI7XG59XG4uZmEtaGFuZC1vLWxlZnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwYTVcIjtcbn1cbi5mYS1oYW5kLW8tdXA6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwYTZcIjtcbn1cbi5mYS1oYW5kLW8tZG93bjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBhN1wiO1xufVxuLmZhLWFycm93LWNpcmNsZS1sZWZ0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGE4XCI7XG59XG4uZmEtYXJyb3ctY2lyY2xlLXJpZ2h0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGE5XCI7XG59XG4uZmEtYXJyb3ctY2lyY2xlLXVwOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGFhXCI7XG59XG4uZmEtYXJyb3ctY2lyY2xlLWRvd246YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwYWJcIjtcbn1cbi5mYS1nbG9iZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBhY1wiO1xufVxuLmZhLXdyZW5jaDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBhZFwiO1xufVxuLmZhLXRhc2tzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGFlXCI7XG59XG4uZmEtZmlsdGVyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGIwXCI7XG59XG4uZmEtYnJpZWZjYXNlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGIxXCI7XG59XG4uZmEtYXJyb3dzLWFsdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBiMlwiO1xufVxuLmZhLWdyb3VwOmJlZm9yZSxcbi5mYS11c2VyczpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBjMFwiO1xufVxuLmZhLWNoYWluOmJlZm9yZSxcbi5mYS1saW5rOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGMxXCI7XG59XG4uZmEtY2xvdWQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwYzJcIjtcbn1cbi5mYS1mbGFzazpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBjM1wiO1xufVxuLmZhLWN1dDpiZWZvcmUsXG4uZmEtc2Npc3NvcnM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwYzRcIjtcbn1cbi5mYS1jb3B5OmJlZm9yZSxcbi5mYS1maWxlcy1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGM1XCI7XG59XG4uZmEtcGFwZXJjbGlwOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGM2XCI7XG59XG4uZmEtc2F2ZTpiZWZvcmUsXG4uZmEtZmxvcHB5LW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwYzdcIjtcbn1cbi5mYS1zcXVhcmU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwYzhcIjtcbn1cbi5mYS1uYXZpY29uOmJlZm9yZSxcbi5mYS1yZW9yZGVyOmJlZm9yZSxcbi5mYS1iYXJzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGM5XCI7XG59XG4uZmEtbGlzdC11bDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBjYVwiO1xufVxuLmZhLWxpc3Qtb2w6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwY2JcIjtcbn1cbi5mYS1zdHJpa2V0aHJvdWdoOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGNjXCI7XG59XG4uZmEtdW5kZXJsaW5lOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGNkXCI7XG59XG4uZmEtdGFibGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwY2VcIjtcbn1cbi5mYS1tYWdpYzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBkMFwiO1xufVxuLmZhLXRydWNrOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGQxXCI7XG59XG4uZmEtcGludGVyZXN0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGQyXCI7XG59XG4uZmEtcGludGVyZXN0LXNxdWFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBkM1wiO1xufVxuLmZhLWdvb2dsZS1wbHVzLXNxdWFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBkNFwiO1xufVxuLmZhLWdvb2dsZS1wbHVzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGQ1XCI7XG59XG4uZmEtbW9uZXk6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwZDZcIjtcbn1cbi5mYS1jYXJldC1kb3duOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGQ3XCI7XG59XG4uZmEtY2FyZXQtdXA6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwZDhcIjtcbn1cbi5mYS1jYXJldC1sZWZ0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGQ5XCI7XG59XG4uZmEtY2FyZXQtcmlnaHQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwZGFcIjtcbn1cbi5mYS1jb2x1bW5zOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGRiXCI7XG59XG4uZmEtdW5zb3J0ZWQ6YmVmb3JlLFxuLmZhLXNvcnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwZGNcIjtcbn1cbi5mYS1zb3J0LWRvd246YmVmb3JlLFxuLmZhLXNvcnQtZGVzYzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBkZFwiO1xufVxuLmZhLXNvcnQtdXA6YmVmb3JlLFxuLmZhLXNvcnQtYXNjOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGRlXCI7XG59XG4uZmEtZW52ZWxvcGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwZTBcIjtcbn1cbi5mYS1saW5rZWRpbjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBlMVwiO1xufVxuLmZhLXJvdGF0ZS1sZWZ0OmJlZm9yZSxcbi5mYS11bmRvOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGUyXCI7XG59XG4uZmEtbGVnYWw6YmVmb3JlLFxuLmZhLWdhdmVsOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGUzXCI7XG59XG4uZmEtZGFzaGJvYXJkOmJlZm9yZSxcbi5mYS10YWNob21ldGVyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGU0XCI7XG59XG4uZmEtY29tbWVudC1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGU1XCI7XG59XG4uZmEtY29tbWVudHMtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBlNlwiO1xufVxuLmZhLWZsYXNoOmJlZm9yZSxcbi5mYS1ib2x0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGU3XCI7XG59XG4uZmEtc2l0ZW1hcDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBlOFwiO1xufVxuLmZhLXVtYnJlbGxhOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGU5XCI7XG59XG4uZmEtcGFzdGU6YmVmb3JlLFxuLmZhLWNsaXBib2FyZDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBlYVwiO1xufVxuLmZhLWxpZ2h0YnVsYi1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGViXCI7XG59XG4uZmEtZXhjaGFuZ2U6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwZWNcIjtcbn1cbi5mYS1jbG91ZC1kb3dubG9hZDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBlZFwiO1xufVxuLmZhLWNsb3VkLXVwbG9hZDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBlZVwiO1xufVxuLmZhLXVzZXItbWQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwZjBcIjtcbn1cbi5mYS1zdGV0aG9zY29wZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBmMVwiO1xufVxuLmZhLXN1aXRjYXNlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGYyXCI7XG59XG4uZmEtYmVsbC1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGEyXCI7XG59XG4uZmEtY29mZmVlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGY0XCI7XG59XG4uZmEtY3V0bGVyeTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBmNVwiO1xufVxuLmZhLWZpbGUtdGV4dC1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGY2XCI7XG59XG4uZmEtYnVpbGRpbmctbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBmN1wiO1xufVxuLmZhLWhvc3BpdGFsLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwZjhcIjtcbn1cbi5mYS1hbWJ1bGFuY2U6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwZjlcIjtcbn1cbi5mYS1tZWRraXQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwZmFcIjtcbn1cbi5mYS1maWdodGVyLWpldDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBmYlwiO1xufVxuLmZhLWJlZXI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwZmNcIjtcbn1cbi5mYS1oLXNxdWFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjBmZFwiO1xufVxuLmZhLXBsdXMtc3F1YXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMGZlXCI7XG59XG4uZmEtYW5nbGUtZG91YmxlLWxlZnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMDBcIjtcbn1cbi5mYS1hbmdsZS1kb3VibGUtcmlnaHQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMDFcIjtcbn1cbi5mYS1hbmdsZS1kb3VibGUtdXA6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMDJcIjtcbn1cbi5mYS1hbmdsZS1kb3VibGUtZG93bjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjEwM1wiO1xufVxuLmZhLWFuZ2xlLWxlZnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMDRcIjtcbn1cbi5mYS1hbmdsZS1yaWdodDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjEwNVwiO1xufVxuLmZhLWFuZ2xlLXVwOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTA2XCI7XG59XG4uZmEtYW5nbGUtZG93bjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjEwN1wiO1xufVxuLmZhLWRlc2t0b3A6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMDhcIjtcbn1cbi5mYS1sYXB0b3A6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMDlcIjtcbn1cbi5mYS10YWJsZXQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMGFcIjtcbn1cbi5mYS1tb2JpbGUtcGhvbmU6YmVmb3JlLFxuLmZhLW1vYmlsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjEwYlwiO1xufVxuLmZhLWNpcmNsZS1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTBjXCI7XG59XG4uZmEtcXVvdGUtbGVmdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjEwZFwiO1xufVxuLmZhLXF1b3RlLXJpZ2h0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTBlXCI7XG59XG4uZmEtc3Bpbm5lcjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjExMFwiO1xufVxuLmZhLWNpcmNsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjExMVwiO1xufVxuLmZhLW1haWwtcmVwbHk6YmVmb3JlLFxuLmZhLXJlcGx5OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTEyXCI7XG59XG4uZmEtZ2l0aHViLWFsdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjExM1wiO1xufVxuLmZhLWZvbGRlci1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTE0XCI7XG59XG4uZmEtZm9sZGVyLW9wZW4tbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjExNVwiO1xufVxuLmZhLXNtaWxlLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMThcIjtcbn1cbi5mYS1mcm93bi1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTE5XCI7XG59XG4uZmEtbWVoLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMWFcIjtcbn1cbi5mYS1nYW1lcGFkOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTFiXCI7XG59XG4uZmEta2V5Ym9hcmQtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjExY1wiO1xufVxuLmZhLWZsYWctbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjExZFwiO1xufVxuLmZhLWZsYWctY2hlY2tlcmVkOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTFlXCI7XG59XG4uZmEtdGVybWluYWw6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMjBcIjtcbn1cbi5mYS1jb2RlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTIxXCI7XG59XG4uZmEtbWFpbC1yZXBseS1hbGw6YmVmb3JlLFxuLmZhLXJlcGx5LWFsbDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjEyMlwiO1xufVxuLmZhLXN0YXItaGFsZi1lbXB0eTpiZWZvcmUsXG4uZmEtc3Rhci1oYWxmLWZ1bGw6YmVmb3JlLFxuLmZhLXN0YXItaGFsZi1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTIzXCI7XG59XG4uZmEtbG9jYXRpb24tYXJyb3c6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMjRcIjtcbn1cbi5mYS1jcm9wOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTI1XCI7XG59XG4uZmEtY29kZS1mb3JrOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTI2XCI7XG59XG4uZmEtdW5saW5rOmJlZm9yZSxcbi5mYS1jaGFpbi1icm9rZW46YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMjdcIjtcbn1cbi5mYS1xdWVzdGlvbjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjEyOFwiO1xufVxuLmZhLWluZm86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMjlcIjtcbn1cbi5mYS1leGNsYW1hdGlvbjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjEyYVwiO1xufVxuLmZhLXN1cGVyc2NyaXB0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTJiXCI7XG59XG4uZmEtc3Vic2NyaXB0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTJjXCI7XG59XG4uZmEtZXJhc2VyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTJkXCI7XG59XG4uZmEtcHV6emxlLXBpZWNlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTJlXCI7XG59XG4uZmEtbWljcm9waG9uZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjEzMFwiO1xufVxuLmZhLW1pY3JvcGhvbmUtc2xhc2g6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMzFcIjtcbn1cbi5mYS1zaGllbGQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMzJcIjtcbn1cbi5mYS1jYWxlbmRhci1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTMzXCI7XG59XG4uZmEtZmlyZS1leHRpbmd1aXNoZXI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMzRcIjtcbn1cbi5mYS1yb2NrZXQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMzVcIjtcbn1cbi5mYS1tYXhjZG46YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMzZcIjtcbn1cbi5mYS1jaGV2cm9uLWNpcmNsZS1sZWZ0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTM3XCI7XG59XG4uZmEtY2hldnJvbi1jaXJjbGUtcmlnaHQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxMzhcIjtcbn1cbi5mYS1jaGV2cm9uLWNpcmNsZS11cDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjEzOVwiO1xufVxuLmZhLWNoZXZyb24tY2lyY2xlLWRvd246YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxM2FcIjtcbn1cbi5mYS1odG1sNTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjEzYlwiO1xufVxuLmZhLWNzczM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxM2NcIjtcbn1cbi5mYS1hbmNob3I6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxM2RcIjtcbn1cbi5mYS11bmxvY2stYWx0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTNlXCI7XG59XG4uZmEtYnVsbHNleWU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNDBcIjtcbn1cbi5mYS1lbGxpcHNpcy1oOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTQxXCI7XG59XG4uZmEtZWxsaXBzaXMtdjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE0MlwiO1xufVxuLmZhLXJzcy1zcXVhcmU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNDNcIjtcbn1cbi5mYS1wbGF5LWNpcmNsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE0NFwiO1xufVxuLmZhLXRpY2tldDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE0NVwiO1xufVxuLmZhLW1pbnVzLXNxdWFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE0NlwiO1xufVxuLmZhLW1pbnVzLXNxdWFyZS1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTQ3XCI7XG59XG4uZmEtbGV2ZWwtdXA6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNDhcIjtcbn1cbi5mYS1sZXZlbC1kb3duOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTQ5XCI7XG59XG4uZmEtY2hlY2stc3F1YXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTRhXCI7XG59XG4uZmEtcGVuY2lsLXNxdWFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE0YlwiO1xufVxuLmZhLWV4dGVybmFsLWxpbmstc3F1YXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTRjXCI7XG59XG4uZmEtc2hhcmUtc3F1YXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTRkXCI7XG59XG4uZmEtY29tcGFzczpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE0ZVwiO1xufVxuLmZhLXRvZ2dsZS1kb3duOmJlZm9yZSxcbi5mYS1jYXJldC1zcXVhcmUtby1kb3duOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTUwXCI7XG59XG4uZmEtdG9nZ2xlLXVwOmJlZm9yZSxcbi5mYS1jYXJldC1zcXVhcmUtby11cDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE1MVwiO1xufVxuLmZhLXRvZ2dsZS1yaWdodDpiZWZvcmUsXG4uZmEtY2FyZXQtc3F1YXJlLW8tcmlnaHQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNTJcIjtcbn1cbi5mYS1ldXJvOmJlZm9yZSxcbi5mYS1ldXI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNTNcIjtcbn1cbi5mYS1nYnA6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNTRcIjtcbn1cbi5mYS1kb2xsYXI6YmVmb3JlLFxuLmZhLXVzZDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE1NVwiO1xufVxuLmZhLXJ1cGVlOmJlZm9yZSxcbi5mYS1pbnI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNTZcIjtcbn1cbi5mYS1jbnk6YmVmb3JlLFxuLmZhLXJtYjpiZWZvcmUsXG4uZmEteWVuOmJlZm9yZSxcbi5mYS1qcHk6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNTdcIjtcbn1cbi5mYS1ydWJsZTpiZWZvcmUsXG4uZmEtcm91YmxlOmJlZm9yZSxcbi5mYS1ydWI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNThcIjtcbn1cbi5mYS13b246YmVmb3JlLFxuLmZhLWtydzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE1OVwiO1xufVxuLmZhLWJpdGNvaW46YmVmb3JlLFxuLmZhLWJ0YzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE1YVwiO1xufVxuLmZhLWZpbGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNWJcIjtcbn1cbi5mYS1maWxlLXRleHQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNWNcIjtcbn1cbi5mYS1zb3J0LWFscGhhLWFzYzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE1ZFwiO1xufVxuLmZhLXNvcnQtYWxwaGEtZGVzYzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE1ZVwiO1xufVxuLmZhLXNvcnQtYW1vdW50LWFzYzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE2MFwiO1xufVxuLmZhLXNvcnQtYW1vdW50LWRlc2M6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNjFcIjtcbn1cbi5mYS1zb3J0LW51bWVyaWMtYXNjOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTYyXCI7XG59XG4uZmEtc29ydC1udW1lcmljLWRlc2M6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNjNcIjtcbn1cbi5mYS10aHVtYnMtdXA6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNjRcIjtcbn1cbi5mYS10aHVtYnMtZG93bjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE2NVwiO1xufVxuLmZhLXlvdXR1YmUtc3F1YXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTY2XCI7XG59XG4uZmEteW91dHViZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE2N1wiO1xufVxuLmZhLXhpbmc6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNjhcIjtcbn1cbi5mYS14aW5nLXNxdWFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE2OVwiO1xufVxuLmZhLXlvdXR1YmUtcGxheTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE2YVwiO1xufVxuLmZhLWRyb3Bib3g6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNmJcIjtcbn1cbi5mYS1zdGFjay1vdmVyZmxvdzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE2Y1wiO1xufVxuLmZhLWluc3RhZ3JhbTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE2ZFwiO1xufVxuLmZhLWZsaWNrcjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE2ZVwiO1xufVxuLmZhLWFkbjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE3MFwiO1xufVxuLmZhLWJpdGJ1Y2tldDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE3MVwiO1xufVxuLmZhLWJpdGJ1Y2tldC1zcXVhcmU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNzJcIjtcbn1cbi5mYS10dW1ibHI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNzNcIjtcbn1cbi5mYS10dW1ibHItc3F1YXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTc0XCI7XG59XG4uZmEtbG9uZy1hcnJvdy1kb3duOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTc1XCI7XG59XG4uZmEtbG9uZy1hcnJvdy11cDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE3NlwiO1xufVxuLmZhLWxvbmctYXJyb3ctbGVmdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE3N1wiO1xufVxuLmZhLWxvbmctYXJyb3ctcmlnaHQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxNzhcIjtcbn1cbi5mYS1hcHBsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE3OVwiO1xufVxuLmZhLXdpbmRvd3M6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxN2FcIjtcbn1cbi5mYS1hbmRyb2lkOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTdiXCI7XG59XG4uZmEtbGludXg6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxN2NcIjtcbn1cbi5mYS1kcmliYmJsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE3ZFwiO1xufVxuLmZhLXNreXBlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTdlXCI7XG59XG4uZmEtZm91cnNxdWFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE4MFwiO1xufVxuLmZhLXRyZWxsbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE4MVwiO1xufVxuLmZhLWZlbWFsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE4MlwiO1xufVxuLmZhLW1hbGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxODNcIjtcbn1cbi5mYS1naXR0aXA6YmVmb3JlLFxuLmZhLWdyYXRpcGF5OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTg0XCI7XG59XG4uZmEtc3VuLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxODVcIjtcbn1cbi5mYS1tb29uLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxODZcIjtcbn1cbi5mYS1hcmNoaXZlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTg3XCI7XG59XG4uZmEtYnVnOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTg4XCI7XG59XG4uZmEtdms6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxODlcIjtcbn1cbi5mYS13ZWlibzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE4YVwiO1xufVxuLmZhLXJlbnJlbjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE4YlwiO1xufVxuLmZhLXBhZ2VsaW5lczpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE4Y1wiO1xufVxuLmZhLXN0YWNrLWV4Y2hhbmdlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMThkXCI7XG59XG4uZmEtYXJyb3ctY2lyY2xlLW8tcmlnaHQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxOGVcIjtcbn1cbi5mYS1hcnJvdy1jaXJjbGUtby1sZWZ0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTkwXCI7XG59XG4uZmEtdG9nZ2xlLWxlZnQ6YmVmb3JlLFxuLmZhLWNhcmV0LXNxdWFyZS1vLWxlZnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxOTFcIjtcbn1cbi5mYS1kb3QtY2lyY2xlLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxOTJcIjtcbn1cbi5mYS13aGVlbGNoYWlyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTkzXCI7XG59XG4uZmEtdmltZW8tc3F1YXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTk0XCI7XG59XG4uZmEtdHVya2lzaC1saXJhOmJlZm9yZSxcbi5mYS10cnk6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxOTVcIjtcbn1cbi5mYS1wbHVzLXNxdWFyZS1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTk2XCI7XG59XG4uZmEtc3BhY2Utc2h1dHRsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjE5N1wiO1xufVxuLmZhLXNsYWNrOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTk4XCI7XG59XG4uZmEtZW52ZWxvcGUtc3F1YXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTk5XCI7XG59XG4uZmEtd29yZHByZXNzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTlhXCI7XG59XG4uZmEtb3BlbmlkOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTliXCI7XG59XG4uZmEtaW5zdGl0dXRpb246YmVmb3JlLFxuLmZhLWJhbms6YmVmb3JlLFxuLmZhLXVuaXZlcnNpdHk6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxOWNcIjtcbn1cbi5mYS1tb3J0YXItYm9hcmQ6YmVmb3JlLFxuLmZhLWdyYWR1YXRpb24tY2FwOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMTlkXCI7XG59XG4uZmEteWFob286YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxOWVcIjtcbn1cbi5mYS1nb29nbGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYTBcIjtcbn1cbi5mYS1yZWRkaXQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYTFcIjtcbn1cbi5mYS1yZWRkaXQtc3F1YXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWEyXCI7XG59XG4uZmEtc3R1bWJsZXVwb24tY2lyY2xlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWEzXCI7XG59XG4uZmEtc3R1bWJsZXVwb246YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYTRcIjtcbn1cbi5mYS1kZWxpY2lvdXM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYTVcIjtcbn1cbi5mYS1kaWdnOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWE2XCI7XG59XG4uZmEtcGllZC1waXBlci1wcDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFhN1wiO1xufVxuLmZhLXBpZWQtcGlwZXItYWx0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWE4XCI7XG59XG4uZmEtZHJ1cGFsOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWE5XCI7XG59XG4uZmEtam9vbWxhOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWFhXCI7XG59XG4uZmEtbGFuZ3VhZ2U6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYWJcIjtcbn1cbi5mYS1mYXg6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYWNcIjtcbn1cbi5mYS1idWlsZGluZzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFhZFwiO1xufVxuLmZhLWNoaWxkOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWFlXCI7XG59XG4uZmEtcGF3OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWIwXCI7XG59XG4uZmEtc3Bvb246YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYjFcIjtcbn1cbi5mYS1jdWJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWIyXCI7XG59XG4uZmEtY3ViZXM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYjNcIjtcbn1cbi5mYS1iZWhhbmNlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWI0XCI7XG59XG4uZmEtYmVoYW5jZS1zcXVhcmU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYjVcIjtcbn1cbi5mYS1zdGVhbTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFiNlwiO1xufVxuLmZhLXN0ZWFtLXNxdWFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFiN1wiO1xufVxuLmZhLXJlY3ljbGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYjhcIjtcbn1cbi5mYS1hdXRvbW9iaWxlOmJlZm9yZSxcbi5mYS1jYXI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYjlcIjtcbn1cbi5mYS1jYWI6YmVmb3JlLFxuLmZhLXRheGk6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYmFcIjtcbn1cbi5mYS10cmVlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWJiXCI7XG59XG4uZmEtc3BvdGlmeTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFiY1wiO1xufVxuLmZhLWRldmlhbnRhcnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYmRcIjtcbn1cbi5mYS1zb3VuZGNsb3VkOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWJlXCI7XG59XG4uZmEtZGF0YWJhc2U6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYzBcIjtcbn1cbi5mYS1maWxlLXBkZi1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWMxXCI7XG59XG4uZmEtZmlsZS13b3JkLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYzJcIjtcbn1cbi5mYS1maWxlLWV4Y2VsLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYzNcIjtcbn1cbi5mYS1maWxlLXBvd2VycG9pbnQtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFjNFwiO1xufVxuLmZhLWZpbGUtcGhvdG8tbzpiZWZvcmUsXG4uZmEtZmlsZS1waWN0dXJlLW86YmVmb3JlLFxuLmZhLWZpbGUtaW1hZ2UtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFjNVwiO1xufVxuLmZhLWZpbGUtemlwLW86YmVmb3JlLFxuLmZhLWZpbGUtYXJjaGl2ZS1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWM2XCI7XG59XG4uZmEtZmlsZS1zb3VuZC1vOmJlZm9yZSxcbi5mYS1maWxlLWF1ZGlvLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxYzdcIjtcbn1cbi5mYS1maWxlLW1vdmllLW86YmVmb3JlLFxuLmZhLWZpbGUtdmlkZW8tbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFjOFwiO1xufVxuLmZhLWZpbGUtY29kZS1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWM5XCI7XG59XG4uZmEtdmluZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFjYVwiO1xufVxuLmZhLWNvZGVwZW46YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxY2JcIjtcbn1cbi5mYS1qc2ZpZGRsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFjY1wiO1xufVxuLmZhLWxpZmUtYm91eTpiZWZvcmUsXG4uZmEtbGlmZS1idW95OmJlZm9yZSxcbi5mYS1saWZlLXNhdmVyOmJlZm9yZSxcbi5mYS1zdXBwb3J0OmJlZm9yZSxcbi5mYS1saWZlLXJpbmc6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxY2RcIjtcbn1cbi5mYS1jaXJjbGUtby1ub3RjaDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFjZVwiO1xufVxuLmZhLXJhOmJlZm9yZSxcbi5mYS1yZXNpc3RhbmNlOmJlZm9yZSxcbi5mYS1yZWJlbDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFkMFwiO1xufVxuLmZhLWdlOmJlZm9yZSxcbi5mYS1lbXBpcmU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZDFcIjtcbn1cbi5mYS1naXQtc3F1YXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWQyXCI7XG59XG4uZmEtZ2l0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWQzXCI7XG59XG4uZmEteS1jb21iaW5hdG9yLXNxdWFyZTpiZWZvcmUsXG4uZmEteWMtc3F1YXJlOmJlZm9yZSxcbi5mYS1oYWNrZXItbmV3czpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFkNFwiO1xufVxuLmZhLXRlbmNlbnQtd2VpYm86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZDVcIjtcbn1cbi5mYS1xcTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFkNlwiO1xufVxuLmZhLXdlY2hhdDpiZWZvcmUsXG4uZmEtd2VpeGluOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWQ3XCI7XG59XG4uZmEtc2VuZDpiZWZvcmUsXG4uZmEtcGFwZXItcGxhbmU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZDhcIjtcbn1cbi5mYS1zZW5kLW86YmVmb3JlLFxuLmZhLXBhcGVyLXBsYW5lLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZDlcIjtcbn1cbi5mYS1oaXN0b3J5OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWRhXCI7XG59XG4uZmEtY2lyY2xlLXRoaW46YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZGJcIjtcbn1cbi5mYS1oZWFkZXI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZGNcIjtcbn1cbi5mYS1wYXJhZ3JhcGg6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZGRcIjtcbn1cbi5mYS1zbGlkZXJzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWRlXCI7XG59XG4uZmEtc2hhcmUtYWx0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWUwXCI7XG59XG4uZmEtc2hhcmUtYWx0LXNxdWFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFlMVwiO1xufVxuLmZhLWJvbWI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZTJcIjtcbn1cbi5mYS1zb2NjZXItYmFsbC1vOmJlZm9yZSxcbi5mYS1mdXRib2wtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFlM1wiO1xufVxuLmZhLXR0eTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFlNFwiO1xufVxuLmZhLWJpbm9jdWxhcnM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZTVcIjtcbn1cbi5mYS1wbHVnOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWU2XCI7XG59XG4uZmEtc2xpZGVzaGFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFlN1wiO1xufVxuLmZhLXR3aXRjaDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFlOFwiO1xufVxuLmZhLXllbHA6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZTlcIjtcbn1cbi5mYS1uZXdzcGFwZXItbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFlYVwiO1xufVxuLmZhLXdpZmk6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZWJcIjtcbn1cbi5mYS1jYWxjdWxhdG9yOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWVjXCI7XG59XG4uZmEtcGF5cGFsOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWVkXCI7XG59XG4uZmEtZ29vZ2xlLXdhbGxldDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFlZVwiO1xufVxuLmZhLWNjLXZpc2E6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZjBcIjtcbn1cbi5mYS1jYy1tYXN0ZXJjYXJkOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWYxXCI7XG59XG4uZmEtY2MtZGlzY292ZXI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZjJcIjtcbn1cbi5mYS1jYy1hbWV4OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWYzXCI7XG59XG4uZmEtY2MtcGF5cGFsOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWY0XCI7XG59XG4uZmEtY2Mtc3RyaXBlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWY1XCI7XG59XG4uZmEtYmVsbC1zbGFzaDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFmNlwiO1xufVxuLmZhLWJlbGwtc2xhc2gtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFmN1wiO1xufVxuLmZhLXRyYXNoOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWY4XCI7XG59XG4uZmEtY29weXJpZ2h0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWY5XCI7XG59XG4uZmEtYXQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZmFcIjtcbn1cbi5mYS1leWVkcm9wcGVyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWZiXCI7XG59XG4uZmEtcGFpbnQtYnJ1c2g6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYxZmNcIjtcbn1cbi5mYS1iaXJ0aGRheS1jYWtlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMWZkXCI7XG59XG4uZmEtYXJlYS1jaGFydDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjFmZVwiO1xufVxuLmZhLXBpZS1jaGFydDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIwMFwiO1xufVxuLmZhLWxpbmUtY2hhcnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMDFcIjtcbn1cbi5mYS1sYXN0Zm06YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMDJcIjtcbn1cbi5mYS1sYXN0Zm0tc3F1YXJlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjAzXCI7XG59XG4uZmEtdG9nZ2xlLW9mZjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIwNFwiO1xufVxuLmZhLXRvZ2dsZS1vbjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIwNVwiO1xufVxuLmZhLWJpY3ljbGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMDZcIjtcbn1cbi5mYS1idXM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMDdcIjtcbn1cbi5mYS1pb3hob3N0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjA4XCI7XG59XG4uZmEtYW5nZWxsaXN0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjA5XCI7XG59XG4uZmEtY2M6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMGFcIjtcbn1cbi5mYS1zaGVrZWw6YmVmb3JlLFxuLmZhLXNoZXFlbDpiZWZvcmUsXG4uZmEtaWxzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjBiXCI7XG59XG4uZmEtbWVhbnBhdGg6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMGNcIjtcbn1cbi5mYS1idXlzZWxsYWRzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjBkXCI7XG59XG4uZmEtY29ubmVjdGRldmVsb3A6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMGVcIjtcbn1cbi5mYS1kYXNoY3ViZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIxMFwiO1xufVxuLmZhLWZvcnVtYmVlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjExXCI7XG59XG4uZmEtbGVhbnB1YjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIxMlwiO1xufVxuLmZhLXNlbGxzeTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIxM1wiO1xufVxuLmZhLXNoaXJ0c2luYnVsazpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIxNFwiO1xufVxuLmZhLXNpbXBseWJ1aWx0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjE1XCI7XG59XG4uZmEtc2t5YXRsYXM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMTZcIjtcbn1cbi5mYS1jYXJ0LXBsdXM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMTdcIjtcbn1cbi5mYS1jYXJ0LWFycm93LWRvd246YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMThcIjtcbn1cbi5mYS1kaWFtb25kOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjE5XCI7XG59XG4uZmEtc2hpcDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIxYVwiO1xufVxuLmZhLXVzZXItc2VjcmV0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjFiXCI7XG59XG4uZmEtbW90b3JjeWNsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIxY1wiO1xufVxuLmZhLXN0cmVldC12aWV3OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjFkXCI7XG59XG4uZmEtaGVhcnRiZWF0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjFlXCI7XG59XG4uZmEtdmVudXM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMjFcIjtcbn1cbi5mYS1tYXJzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjIyXCI7XG59XG4uZmEtbWVyY3VyeTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIyM1wiO1xufVxuLmZhLWludGVyc2V4OmJlZm9yZSxcbi5mYS10cmFuc2dlbmRlcjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIyNFwiO1xufVxuLmZhLXRyYW5zZ2VuZGVyLWFsdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIyNVwiO1xufVxuLmZhLXZlbnVzLWRvdWJsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIyNlwiO1xufVxuLmZhLW1hcnMtZG91YmxlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjI3XCI7XG59XG4uZmEtdmVudXMtbWFyczpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIyOFwiO1xufVxuLmZhLW1hcnMtc3Ryb2tlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjI5XCI7XG59XG4uZmEtbWFycy1zdHJva2UtdjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIyYVwiO1xufVxuLmZhLW1hcnMtc3Ryb2tlLWg6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMmJcIjtcbn1cbi5mYS1uZXV0ZXI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMmNcIjtcbn1cbi5mYS1nZW5kZXJsZXNzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjJkXCI7XG59XG4uZmEtZmFjZWJvb2stb2ZmaWNpYWw6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMzBcIjtcbn1cbi5mYS1waW50ZXJlc3QtcDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIzMVwiO1xufVxuLmZhLXdoYXRzYXBwOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjMyXCI7XG59XG4uZmEtc2VydmVyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjMzXCI7XG59XG4uZmEtdXNlci1wbHVzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjM0XCI7XG59XG4uZmEtdXNlci10aW1lczpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIzNVwiO1xufVxuLmZhLWhvdGVsOmJlZm9yZSxcbi5mYS1iZWQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMzZcIjtcbn1cbi5mYS12aWFjb2luOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjM3XCI7XG59XG4uZmEtdHJhaW46YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMzhcIjtcbn1cbi5mYS1zdWJ3YXk6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyMzlcIjtcbn1cbi5mYS1tZWRpdW06YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyM2FcIjtcbn1cbi5mYS15YzpiZWZvcmUsXG4uZmEteS1jb21iaW5hdG9yOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjNiXCI7XG59XG4uZmEtb3B0aW4tbW9uc3RlcjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjIzY1wiO1xufVxuLmZhLW9wZW5jYXJ0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjNkXCI7XG59XG4uZmEtZXhwZWRpdGVkc3NsOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjNlXCI7XG59XG4uZmEtYmF0dGVyeS00OmJlZm9yZSxcbi5mYS1iYXR0ZXJ5OmJlZm9yZSxcbi5mYS1iYXR0ZXJ5LWZ1bGw6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNDBcIjtcbn1cbi5mYS1iYXR0ZXJ5LTM6YmVmb3JlLFxuLmZhLWJhdHRlcnktdGhyZWUtcXVhcnRlcnM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNDFcIjtcbn1cbi5mYS1iYXR0ZXJ5LTI6YmVmb3JlLFxuLmZhLWJhdHRlcnktaGFsZjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI0MlwiO1xufVxuLmZhLWJhdHRlcnktMTpiZWZvcmUsXG4uZmEtYmF0dGVyeS1xdWFydGVyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjQzXCI7XG59XG4uZmEtYmF0dGVyeS0wOmJlZm9yZSxcbi5mYS1iYXR0ZXJ5LWVtcHR5OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjQ0XCI7XG59XG4uZmEtbW91c2UtcG9pbnRlcjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI0NVwiO1xufVxuLmZhLWktY3Vyc29yOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjQ2XCI7XG59XG4uZmEtb2JqZWN0LWdyb3VwOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjQ3XCI7XG59XG4uZmEtb2JqZWN0LXVuZ3JvdXA6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNDhcIjtcbn1cbi5mYS1zdGlja3ktbm90ZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI0OVwiO1xufVxuLmZhLXN0aWNreS1ub3RlLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNGFcIjtcbn1cbi5mYS1jYy1qY2I6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNGJcIjtcbn1cbi5mYS1jYy1kaW5lcnMtY2x1YjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI0Y1wiO1xufVxuLmZhLWNsb25lOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjRkXCI7XG59XG4uZmEtYmFsYW5jZS1zY2FsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI0ZVwiO1xufVxuLmZhLWhvdXJnbGFzcy1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjUwXCI7XG59XG4uZmEtaG91cmdsYXNzLTE6YmVmb3JlLFxuLmZhLWhvdXJnbGFzcy1zdGFydDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI1MVwiO1xufVxuLmZhLWhvdXJnbGFzcy0yOmJlZm9yZSxcbi5mYS1ob3VyZ2xhc3MtaGFsZjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI1MlwiO1xufVxuLmZhLWhvdXJnbGFzcy0zOmJlZm9yZSxcbi5mYS1ob3VyZ2xhc3MtZW5kOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjUzXCI7XG59XG4uZmEtaG91cmdsYXNzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjU0XCI7XG59XG4uZmEtaGFuZC1ncmFiLW86YmVmb3JlLFxuLmZhLWhhbmQtcm9jay1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjU1XCI7XG59XG4uZmEtaGFuZC1zdG9wLW86YmVmb3JlLFxuLmZhLWhhbmQtcGFwZXItbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI1NlwiO1xufVxuLmZhLWhhbmQtc2Npc3NvcnMtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI1N1wiO1xufVxuLmZhLWhhbmQtbGl6YXJkLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNThcIjtcbn1cbi5mYS1oYW5kLXNwb2NrLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNTlcIjtcbn1cbi5mYS1oYW5kLXBvaW50ZXItbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI1YVwiO1xufVxuLmZhLWhhbmQtcGVhY2UtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI1YlwiO1xufVxuLmZhLXRyYWRlbWFyazpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI1Y1wiO1xufVxuLmZhLXJlZ2lzdGVyZWQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNWRcIjtcbn1cbi5mYS1jcmVhdGl2ZS1jb21tb25zOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjVlXCI7XG59XG4uZmEtZ2c6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNjBcIjtcbn1cbi5mYS1nZy1jaXJjbGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNjFcIjtcbn1cbi5mYS10cmlwYWR2aXNvcjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI2MlwiO1xufVxuLmZhLW9kbm9rbGFzc25pa2k6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNjNcIjtcbn1cbi5mYS1vZG5va2xhc3NuaWtpLXNxdWFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI2NFwiO1xufVxuLmZhLWdldC1wb2NrZXQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNjVcIjtcbn1cbi5mYS13aWtpcGVkaWEtdzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI2NlwiO1xufVxuLmZhLXNhZmFyaTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI2N1wiO1xufVxuLmZhLWNocm9tZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI2OFwiO1xufVxuLmZhLWZpcmVmb3g6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNjlcIjtcbn1cbi5mYS1vcGVyYTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI2YVwiO1xufVxuLmZhLWludGVybmV0LWV4cGxvcmVyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjZiXCI7XG59XG4uZmEtdHY6YmVmb3JlLFxuLmZhLXRlbGV2aXNpb246YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNmNcIjtcbn1cbi5mYS1jb250YW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNmRcIjtcbn1cbi5mYS01MDBweDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI2ZVwiO1xufVxuLmZhLWFtYXpvbjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI3MFwiO1xufVxuLmZhLWNhbGVuZGFyLXBsdXMtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI3MVwiO1xufVxuLmZhLWNhbGVuZGFyLW1pbnVzLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyNzJcIjtcbn1cbi5mYS1jYWxlbmRhci10aW1lcy1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjczXCI7XG59XG4uZmEtY2FsZW5kYXItY2hlY2stbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI3NFwiO1xufVxuLmZhLWluZHVzdHJ5OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjc1XCI7XG59XG4uZmEtbWFwLXBpbjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI3NlwiO1xufVxuLmZhLW1hcC1zaWduczpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI3N1wiO1xufVxuLmZhLW1hcC1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjc4XCI7XG59XG4uZmEtbWFwOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjc5XCI7XG59XG4uZmEtY29tbWVudGluZzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI3YVwiO1xufVxuLmZhLWNvbW1lbnRpbmctbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI3YlwiO1xufVxuLmZhLWhvdXp6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjdjXCI7XG59XG4uZmEtdmltZW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyN2RcIjtcbn1cbi5mYS1ibGFjay10aWU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyN2VcIjtcbn1cbi5mYS1mb250aWNvbnM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyODBcIjtcbn1cbi5mYS1yZWRkaXQtYWxpZW46YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyODFcIjtcbn1cbi5mYS1lZGdlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjgyXCI7XG59XG4uZmEtY3JlZGl0LWNhcmQtYWx0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjgzXCI7XG59XG4uZmEtY29kaWVwaWU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyODRcIjtcbn1cbi5mYS1tb2R4OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjg1XCI7XG59XG4uZmEtZm9ydC1hd2Vzb21lOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjg2XCI7XG59XG4uZmEtdXNiOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjg3XCI7XG59XG4uZmEtcHJvZHVjdC1odW50OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjg4XCI7XG59XG4uZmEtbWl4Y2xvdWQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyODlcIjtcbn1cbi5mYS1zY3JpYmQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyOGFcIjtcbn1cbi5mYS1wYXVzZS1jaXJjbGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyOGJcIjtcbn1cbi5mYS1wYXVzZS1jaXJjbGUtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI4Y1wiO1xufVxuLmZhLXN0b3AtY2lyY2xlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjhkXCI7XG59XG4uZmEtc3RvcC1jaXJjbGUtbzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI4ZVwiO1xufVxuLmZhLXNob3BwaW5nLWJhZzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI5MFwiO1xufVxuLmZhLXNob3BwaW5nLWJhc2tldDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI5MVwiO1xufVxuLmZhLWhhc2h0YWc6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyOTJcIjtcbn1cbi5mYS1ibHVldG9vdGg6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyOTNcIjtcbn1cbi5mYS1ibHVldG9vdGgtYjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI5NFwiO1xufVxuLmZhLXBlcmNlbnQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyOTVcIjtcbn1cbi5mYS1naXRsYWI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyOTZcIjtcbn1cbi5mYS13cGJlZ2lubmVyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjk3XCI7XG59XG4uZmEtd3Bmb3JtczpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI5OFwiO1xufVxuLmZhLWVudmlyYTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI5OVwiO1xufVxuLmZhLXVuaXZlcnNhbC1hY2Nlc3M6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyOWFcIjtcbn1cbi5mYS13aGVlbGNoYWlyLWFsdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI5YlwiO1xufVxuLmZhLXF1ZXN0aW9uLWNpcmNsZS1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMjljXCI7XG59XG4uZmEtYmxpbmQ6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyOWRcIjtcbn1cbi5mYS1hdWRpby1kZXNjcmlwdGlvbjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjI5ZVwiO1xufVxuLmZhLXZvbHVtZS1jb250cm9sLXBob25lOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmEwXCI7XG59XG4uZmEtYnJhaWxsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJhMVwiO1xufVxuLmZhLWFzc2lzdGl2ZS1saXN0ZW5pbmctc3lzdGVtczpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJhMlwiO1xufVxuLmZhLWFzbC1pbnRlcnByZXRpbmc6YmVmb3JlLFxuLmZhLWFtZXJpY2FuLXNpZ24tbGFuZ3VhZ2UtaW50ZXJwcmV0aW5nOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmEzXCI7XG59XG4uZmEtZGVhZm5lc3M6YmVmb3JlLFxuLmZhLWhhcmQtb2YtaGVhcmluZzpiZWZvcmUsXG4uZmEtZGVhZjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJhNFwiO1xufVxuLmZhLWdsaWRlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmE1XCI7XG59XG4uZmEtZ2xpZGUtZzpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJhNlwiO1xufVxuLmZhLXNpZ25pbmc6YmVmb3JlLFxuLmZhLXNpZ24tbGFuZ3VhZ2U6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyYTdcIjtcbn1cbi5mYS1sb3ctdmlzaW9uOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmE4XCI7XG59XG4uZmEtdmlhZGVvOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmE5XCI7XG59XG4uZmEtdmlhZGVvLXNxdWFyZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJhYVwiO1xufVxuLmZhLXNuYXBjaGF0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmFiXCI7XG59XG4uZmEtc25hcGNoYXQtZ2hvc3Q6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyYWNcIjtcbn1cbi5mYS1zbmFwY2hhdC1zcXVhcmU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyYWRcIjtcbn1cbi5mYS1waWVkLXBpcGVyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmFlXCI7XG59XG4uZmEtZmlyc3Qtb3JkZXI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyYjBcIjtcbn1cbi5mYS15b2FzdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJiMVwiO1xufVxuLmZhLXRoZW1laXNsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJiMlwiO1xufVxuLmZhLWdvb2dsZS1wbHVzLWNpcmNsZTpiZWZvcmUsXG4uZmEtZ29vZ2xlLXBsdXMtb2ZmaWNpYWw6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyYjNcIjtcbn1cbi5mYS1mYTpiZWZvcmUsXG4uZmEtZm9udC1hd2Vzb21lOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmI0XCI7XG59XG4uZmEtaGFuZHNoYWtlLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyYjVcIjtcbn1cbi5mYS1lbnZlbG9wZS1vcGVuOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmI2XCI7XG59XG4uZmEtZW52ZWxvcGUtb3Blbi1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmI3XCI7XG59XG4uZmEtbGlub2RlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmI4XCI7XG59XG4uZmEtYWRkcmVzcy1ib29rOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmI5XCI7XG59XG4uZmEtYWRkcmVzcy1ib29rLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyYmFcIjtcbn1cbi5mYS12Y2FyZDpiZWZvcmUsXG4uZmEtYWRkcmVzcy1jYXJkOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmJiXCI7XG59XG4uZmEtdmNhcmQtbzpiZWZvcmUsXG4uZmEtYWRkcmVzcy1jYXJkLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyYmNcIjtcbn1cbi5mYS11c2VyLWNpcmNsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJiZFwiO1xufVxuLmZhLXVzZXItY2lyY2xlLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyYmVcIjtcbn1cbi5mYS11c2VyLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyYzBcIjtcbn1cbi5mYS1pZC1iYWRnZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJjMVwiO1xufVxuLmZhLWRyaXZlcnMtbGljZW5zZTpiZWZvcmUsXG4uZmEtaWQtY2FyZDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJjMlwiO1xufVxuLmZhLWRyaXZlcnMtbGljZW5zZS1vOmJlZm9yZSxcbi5mYS1pZC1jYXJkLW86YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyYzNcIjtcbn1cbi5mYS1xdW9yYTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJjNFwiO1xufVxuLmZhLWZyZWUtY29kZS1jYW1wOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmM1XCI7XG59XG4uZmEtdGVsZWdyYW06YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyYzZcIjtcbn1cbi5mYS10aGVybW9tZXRlci00OmJlZm9yZSxcbi5mYS10aGVybW9tZXRlcjpiZWZvcmUsXG4uZmEtdGhlcm1vbWV0ZXItZnVsbDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJjN1wiO1xufVxuLmZhLXRoZXJtb21ldGVyLTM6YmVmb3JlLFxuLmZhLXRoZXJtb21ldGVyLXRocmVlLXF1YXJ0ZXJzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmM4XCI7XG59XG4uZmEtdGhlcm1vbWV0ZXItMjpiZWZvcmUsXG4uZmEtdGhlcm1vbWV0ZXItaGFsZjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJjOVwiO1xufVxuLmZhLXRoZXJtb21ldGVyLTE6YmVmb3JlLFxuLmZhLXRoZXJtb21ldGVyLXF1YXJ0ZXI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyY2FcIjtcbn1cbi5mYS10aGVybW9tZXRlci0wOmJlZm9yZSxcbi5mYS10aGVybW9tZXRlci1lbXB0eTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJjYlwiO1xufVxuLmZhLXNob3dlcjpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJjY1wiO1xufVxuLmZhLWJhdGh0dWI6YmVmb3JlLFxuLmZhLXMxNTpiZWZvcmUsXG4uZmEtYmF0aDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJjZFwiO1xufVxuLmZhLXBvZGNhc3Q6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyY2VcIjtcbn1cbi5mYS13aW5kb3ctbWF4aW1pemU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyZDBcIjtcbn1cbi5mYS13aW5kb3ctbWluaW1pemU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyZDFcIjtcbn1cbi5mYS13aW5kb3ctcmVzdG9yZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJkMlwiO1xufVxuLmZhLXRpbWVzLXJlY3RhbmdsZTpiZWZvcmUsXG4uZmEtd2luZG93LWNsb3NlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmQzXCI7XG59XG4uZmEtdGltZXMtcmVjdGFuZ2xlLW86YmVmb3JlLFxuLmZhLXdpbmRvdy1jbG9zZS1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmQ0XCI7XG59XG4uZmEtYmFuZGNhbXA6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyZDVcIjtcbn1cbi5mYS1ncmF2OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmQ2XCI7XG59XG4uZmEtZXRzeTpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJkN1wiO1xufVxuLmZhLWltZGI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyZDhcIjtcbn1cbi5mYS1yYXZlbHJ5OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmQ5XCI7XG59XG4uZmEtZWVyY2FzdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJkYVwiO1xufVxuLmZhLW1pY3JvY2hpcDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjJkYlwiO1xufVxuLmZhLXNub3dmbGFrZS1vOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmRjXCI7XG59XG4uZmEtc3VwZXJwb3dlcnM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYyZGRcIjtcbn1cbi5mYS13cGV4cGxvcmVyOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmRlXCI7XG59XG4uZmEtbWVldHVwOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMmUwXCI7XG59XG4uc3Itb25seSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDFweDtcbiAgaGVpZ2h0OiAxcHg7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogLTFweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgY2xpcDogcmVjdCgwLCAwLCAwLCAwKTtcbiAgYm9yZGVyOiAwO1xufVxuLnNyLW9ubHktZm9jdXNhYmxlOmFjdGl2ZSxcbi5zci1vbmx5LWZvY3VzYWJsZTpmb2N1cyB7XG4gIHBvc2l0aW9uOiBzdGF0aWM7XG4gIHdpZHRoOiBhdXRvO1xuICBoZWlnaHQ6IGF1dG87XG4gIG1hcmdpbjogMDtcbiAgb3ZlcmZsb3c6IHZpc2libGU7XG4gIGNsaXA6IGF1dG87XG59XG4iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/app.component.ts": 
        /*!**********************************!*\
          !*** ./src/app/app.component.ts ***!
          \**********************************/
        /*! exports provided: AppComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function () { return AppComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _services_candidate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/candidate.service */ "./src/app/services/candidate.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            var AppComponent = /** @class */ (function () {
                function AppComponent(router) {
                    this.router = router;
                }
                ;
                return AppComponent;
            }());
            AppComponent.ctorParameters = function () { return [
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
            ]; };
            AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-root',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
                    providers: [_services_candidate_service__WEBPACK_IMPORTED_MODULE_2__["CandidateService"]],
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
                })
            ], AppComponent);
            /***/ 
        }),
        /***/ "./src/app/app.module.ts": 
        /*!*******************************!*\
          !*** ./src/app/app.module.ts ***!
          \*******************************/
        /*! exports provided: AppModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function () { return AppModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
            /* harmony import */ var _components_candidate_candidate_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/candidate/candidate.component */ "./src/app/components/candidate/candidate.component.ts");
            /* harmony import */ var angular_font_awesome__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-font-awesome */ "./node_modules/angular-font-awesome/dist/angular-font-awesome.js");
            /* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            /* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./index */ "./src/app/index.ts");
            /* harmony import */ var _components_detail_detail_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/detail/detail.component */ "./src/app/components/detail/detail.component.ts");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _components_all_candidates_all_candidates_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/all-candidates/all-candidates.component */ "./src/app/components/all-candidates/all-candidates.component.ts");
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
            /* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
            /* harmony import */ var _angular_material_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./angular-material.module */ "./src/app/angular-material.module.ts");
            /* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
            /* harmony import */ var _date_adapters_date_adapter__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./date_adapters/date.adapter */ "./src/app/date_adapters/date.adapter.ts");
            /* harmony import */ var _components_candidate_update_candidate_update_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/candidate-update/candidate-update.component */ "./src/app/components/candidate-update/candidate-update.component.ts");
            /* harmony import */ var _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ckeditor/ckeditor5-angular */ "./node_modules/@ckeditor/ckeditor5-angular/fesm2015/ckeditor-ckeditor5-angular.js");
            /* harmony import */ var ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ngx-extended-pdf-viewer */ "./node_modules/ngx-extended-pdf-viewer/fesm2015/ngx-extended-pdf-viewer.js");
            /* harmony import */ var _components_add_candidate_add_candidate_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/add-candidate/add-candidate.component */ "./src/app/components/add-candidate/add-candidate.component.ts");
            /* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
            /* harmony import */ var ngx_bootstrap_typeahead__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ngx-bootstrap/typeahead */ "./node_modules/ngx-bootstrap/typeahead/fesm2015/ngx-bootstrap-typeahead.js");
            /* harmony import */ var _components_send_customer_modal_send_customer_modal_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/send-customer-modal/send-customer-modal.component */ "./src/app/components/send-customer-modal/send-customer-modal.component.ts");
            /* harmony import */ var _components_send_candidate_modal_send_candidate_modal_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./components/send-candidate-modal/send-candidate-modal.component */ "./src/app/components/send-candidate-modal/send-candidate-modal.component.ts");
            /* harmony import */ var _components_connect_customer_modal_connect_customer_modal_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./components/connect-customer-modal/connect-customer-modal.component */ "./src/app/components/connect-customer-modal/connect-customer-modal.component.ts");
            /* harmony import */ var _components_candidate_list_candidate_list_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./components/candidate-list/candidate-list.component */ "./src/app/components/candidate-list/candidate-list.component.ts");
            /* harmony import */ var _components_candidate_info_candidate_info_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./components/candidate-info/candidate-info.component */ "./src/app/components/candidate-info/candidate-info.component.ts");
            /* harmony import */ var _components_search_candidate_search_candidate_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./components/search-candidate/search-candidate.component */ "./src/app/components/search-candidate/search-candidate.component.ts");
            /* harmony import */ var _pipes_search_pipe_pipe__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./pipes/search-pipe.pipe */ "./src/app/pipes/search-pipe.pipe.ts");
            /* harmony import */ var _pipes_selected_candidate_pipe__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./pipes/selected-candidate.pipe */ "./src/app/pipes/selected-candidate.pipe.ts");
            /* harmony import */ var _components_all_vacancies_all_vacancies_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./components/all-vacancies/all-vacancies.component */ "./src/app/components/all-vacancies/all-vacancies.component.ts");
            /* harmony import */ var _components_vacancy_list_vacancy_list_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./components/vacancy-list/vacancy-list.component */ "./src/app/components/vacancy-list/vacancy-list.component.ts");
            /* harmony import */ var _components_vacancy_info_vacancy_info_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./components/vacancy-info/vacancy-info.component */ "./src/app/components/vacancy-info/vacancy-info.component.ts");
            /* harmony import */ var _components_vacancy_update_vacancy_update_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./components/vacancy-update/vacancy-update.component */ "./src/app/components/vacancy-update/vacancy-update.component.ts");
            /* harmony import */ var _pipes_safehtml_pipe__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./pipes/safehtml.pipe */ "./src/app/pipes/safehtml.pipe.ts");
            /* harmony import */ var _components_add_vacancy_add_vacancy_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./components/add-vacancy/add-vacancy.component */ "./src/app/components/add-vacancy/add-vacancy.component.ts");
            var routes = [
                // basic routes
                { path: '', redirectTo: 'home', pathMatch: 'full' },
                { path: 'home', component: _components_candidate_candidate_component__WEBPACK_IMPORTED_MODULE_4__["CandidateComponent"] },
                { path: 'all-candidates', component: _components_all_candidates_all_candidates_component__WEBPACK_IMPORTED_MODULE_12__["AllCandidatesComponent"] },
            ];
            var AppModule = /** @class */ (function () {
                function AppModule() {
                }
                return AppModule;
            }());
            AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
                    declarations: [
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                        _components_candidate_candidate_component__WEBPACK_IMPORTED_MODULE_4__["CandidateComponent"],
                        _components_detail_detail_component__WEBPACK_IMPORTED_MODULE_9__["DetailComponent"],
                        _components_all_candidates_all_candidates_component__WEBPACK_IMPORTED_MODULE_12__["AllCandidatesComponent"],
                        _components_candidate_update_candidate_update_component__WEBPACK_IMPORTED_MODULE_18__["CandidateUpdateComponent"],
                        _components_add_candidate_add_candidate_component__WEBPACK_IMPORTED_MODULE_21__["AddCandidateComponent"],
                        _components_send_customer_modal_send_customer_modal_component__WEBPACK_IMPORTED_MODULE_24__["SendCustomerModalComponent"],
                        _components_send_candidate_modal_send_candidate_modal_component__WEBPACK_IMPORTED_MODULE_25__["SendCandidateModalComponent"],
                        _components_connect_customer_modal_connect_customer_modal_component__WEBPACK_IMPORTED_MODULE_26__["ConnectCustomerModalComponent"],
                        _components_candidate_list_candidate_list_component__WEBPACK_IMPORTED_MODULE_27__["CandidateListComponent"],
                        _components_candidate_info_candidate_info_component__WEBPACK_IMPORTED_MODULE_28__["CandidateInfoComponent"],
                        _components_search_candidate_search_candidate_component__WEBPACK_IMPORTED_MODULE_29__["SearchCandidateComponent"],
                        _pipes_search_pipe_pipe__WEBPACK_IMPORTED_MODULE_30__["SearchPipePipe"],
                        _pipes_selected_candidate_pipe__WEBPACK_IMPORTED_MODULE_31__["SelectedCandidatePipe"],
                        _components_all_vacancies_all_vacancies_component__WEBPACK_IMPORTED_MODULE_32__["AllVacanciesComponent"],
                        _components_vacancy_list_vacancy_list_component__WEBPACK_IMPORTED_MODULE_33__["VacancyListComponent"],
                        _components_vacancy_info_vacancy_info_component__WEBPACK_IMPORTED_MODULE_34__["VacancyInfoComponent"],
                        _components_vacancy_update_vacancy_update_component__WEBPACK_IMPORTED_MODULE_35__["VacancyUpdateComponent"],
                        _pipes_safehtml_pipe__WEBPACK_IMPORTED_MODULE_36__["SafehtmlPipe"],
                        _components_add_vacancy_add_vacancy_component__WEBPACK_IMPORTED_MODULE_37__["AddVacancyComponent"],
                    ],
                    imports: [
                        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                        _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
                        _angular_http__WEBPACK_IMPORTED_MODULE_7__["HttpModule"],
                        _angular_router__WEBPACK_IMPORTED_MODULE_11__["RouterModule"].forRoot(routes),
                        angular_font_awesome__WEBPACK_IMPORTED_MODULE_5__["AngularFontAwesomeModule"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_13__["FormsModule"],
                        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_14__["BrowserAnimationsModule"],
                        _angular_material_module__WEBPACK_IMPORTED_MODULE_15__["AngularMaterialModule"],
                        _ckeditor_ckeditor5_angular__WEBPACK_IMPORTED_MODULE_19__["CKEditorModule"],
                        ngx_extended_pdf_viewer__WEBPACK_IMPORTED_MODULE_20__["NgxExtendedPdfViewerModule"],
                        ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_22__["ModalModule"].forRoot(),
                        ngx_bootstrap_typeahead__WEBPACK_IMPORTED_MODULE_23__["TypeaheadModule"].forRoot(),
                    ],
                    entryComponents: [
                        _components_send_customer_modal_send_customer_modal_component__WEBPACK_IMPORTED_MODULE_24__["SendCustomerModalComponent"],
                        _components_send_candidate_modal_send_candidate_modal_component__WEBPACK_IMPORTED_MODULE_25__["SendCandidateModalComponent"],
                        _components_connect_customer_modal_connect_customer_modal_component__WEBPACK_IMPORTED_MODULE_26__["ConnectCustomerModalComponent"],
                        _components_vacancy_update_vacancy_update_component__WEBPACK_IMPORTED_MODULE_35__["VacancyUpdateComponent"],
                    ],
                    providers: [
                        _index__WEBPACK_IMPORTED_MODULE_8__["PagerService"],
                        { provide: _angular_common__WEBPACK_IMPORTED_MODULE_10__["APP_BASE_HREF"], useValue: '/' },
                        {
                            provide: _angular_material__WEBPACK_IMPORTED_MODULE_16__["DateAdapter"], useClass: _date_adapters_date_adapter__WEBPACK_IMPORTED_MODULE_17__["AppDateAdapter"]
                        },
                        {
                            provide: _angular_material__WEBPACK_IMPORTED_MODULE_16__["MAT_DATE_FORMATS"], useValue: _date_adapters_date_adapter__WEBPACK_IMPORTED_MODULE_17__["APP_DATE_FORMATS"]
                        },
                    ],
                    bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
                })
            ], AppModule);
            /***/ 
        }),
        /***/ "./src/app/components/add-candidate/add-candidate.component.css": 
        /*!**********************************************************************!*\
          !*** ./src/app/components/add-candidate/add-candidate.component.css ***!
          \**********************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".main-nav-item:not(:last-child) {\n  border-bottom: 1px solid #7DA5A8;\n}\n.border-right {\n  border-right: 1px solid\n  #dee2e6 !important;\n}\n.form-control {\n  border-radius: 0px;\n}\ninput,select {\n  background:\n    #f5f5f5 !important;\n}\nmat-form-field {\n  min-width: 100%;\n}\n/*******BUTTONS********/\n.entity-details__buttons {\n  padding-bottom: 1.5rem !important;\n  padding-top: 1.5rem !important;\n  padding-left: 0 !important;\n  text-align: center;\n}\n.entity-buttons-wrapper .btn {\n  height: 40px;\n  /*padding: 12px 9px;*/\n  margin-right: 12px;\n  font-weight: 500 !important;\n  font-size: 14px !important;\n  text-transform: uppercase;\n  color:\n    rgba(0, 0, 0, 0.87);\n}\n.btn-crm {\n  border-radius: 0!important;\n  height: 30px;\n  min-width: 120px;\n}\n.btn-crm-primary{\n  font-size: 11px!important;\n  font-weight: 700!important;\n  border-radius: 0!important;\n  border: 1px solid #01747f !important;\n  height: 30px;\n  background-color: #01919D!important;\n  color: white!important;\n}\n.btn-crm-default{\n  font-size: 11px!important;\n  font-weight: 700!important;\n  border-radius: 0!important;\n  border: 1px solid #aeaeae !important;\n  height: 30px;\n}\n.btn-crm-default:hover {\n  background: #f5f5f5;\n  outline: 0;\n  box-shadow: 0 0 0 0.15rem rgba(255, 177, 6, 0.25);\n}\n.btn-crm-default_red {\n  border-color: red !important;\n  color: red !important;\n}\n/*******PHOTO WIDGET*******/\n.three-dots {\n  position: absolute;\n  top: 10px;\n  right: 0;\n}\n.entity-item-round {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  text-align: center;\n  margin-top: 5px;\n  margin-left: -200px;\n  width: 65px;\n  height: 65px;\n  background-color:\n    #f5f5f5;\n  border-radius: 50% !important;\n  border: 1px solid\n  #eaeaea;\n  z-index: 99;\n}\n.entity-item-round i {\n  line-height: 65px;\n  font-size: 40px;\n  color:\n    #C4C4C4;\n}\n.empty-img {\n  margin-right: 50px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-top: 10px;\n  width: 150px;\n  height: 150px;\n  background-color:\n    #f5f5f5;\n  border-radius: 50% !important;\n}\n.dropdown {\n  position: relative;\n}\n.btn-default {\n  border-radius: 50% !important;\n  border: 0px !important;\n}\n.dot {\n  height: 5px;\n  width: 5px;\n  background-color:\n    #bbb;\n  border-radius: 50%;\n  display: flex;\n  margin: 1px;\n}\n.profile__dropdown-menu i {\n  width: 22px;\n  height: 22px;\n  margin-right: 16px;\n}\n.profile__dropdown-menu i::before {\n  line-height: 22px;\n  font-size: 12px;\n}\n.input-photo {\n  width: 0.1px;\n  height: 0.1px;\n  opacity: 0;\n  overflow: hidden;\n  position: absolute;\n  z-index: -1;\n}\n.input-photo + label {\n  max-width: 100%;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  cursor: pointer;\n  display: inline-block;\n  padding-left:1.45rem;\n  margin-bottom: 0;\n}\n/******IMAGE PREVIEW******/\n.image-upload-container {\n  cursor: pointer;\n}\n.img-preview-container {\n  position: relative;\n}\n.img-preview {\n  /*background: center center no-repeat;*/\n  background-size: cover;\n  border-radius: 50% !important;\n  background-position: center;\n}\n/******UPLOAD RESUME AND DOCUMENTS******/\n.fa-add-resume:before {\n  content: \"\\f055\";\n  line-height: 1;\n  padding: 1.5px;\n  color: #58d5e0;\n}\n.add-resume-button,.remove-resume-button {\n  color: #58d5e0;\n}\n.add-resume-label {\n  display: block;\n  width: 100%;\n  padding: .25rem;\n  clear: both;\n  font-weight: 400;\n  color: #58d5e0;\n  text-align: inherit;\n  white-space: nowrap;\n  background-color: transparent;\n  border: 0;\n}\n.add-resume-label span {\n  margin-left: 10px;\n  color: #58d5e0;\n}\n.input-resume {\n  display: none;\n}\n.remove-resume-button {\n  outline: none;\n  border: none;\n  background-color: #fff;\n  margin-bottom: .5rem;\n}\n.remove-resume-button i{\n  margin-right: 19px;\n}\n.fa-add-document:before {\n  content: \"\\f055\";\n  line-height: 1;\n  padding: 1.5px;\n  color: #58d5e0;\n}\n.add-document-button,.remove-document-button {\n  color: #58d5e0;\n}\n.add-document-label {\n  display: block;\n  width: 100%;\n  padding: .25rem;\n  clear: both;\n  font-weight: 400;\n  color: #58d5e0;\n  text-align: inherit;\n  white-space: nowrap;\n  background-color: transparent;\n  border: 0;\n}\n.add-document-label span {\n  margin-left: 10px;\n  color: #58d5e0;\n}\n.input-document {\n  display: none;\n}\n/*******MODALS - DELETE CANDIDATE ******/\n.modal-body .btn {\n  font-size: 14px !important;\n}\n.modal-header{\n  background: #01919D;\n  color: white;\n  padding: 0.5rem 1rem!important;\n  align-items: center!important;\n}\n.modal-header h5 {\n  font-size: 14px;\n  margin: 0px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZGQtY2FuZGlkYXRlL2FkZC1jYW5kaWRhdGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdDQUFnQztBQUNsQztBQUNBO0VBQ0U7b0JBQ2tCO0FBQ3BCO0FBQ0E7RUFDRSxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFO3NCQUNvQjtBQUN0QjtBQUNBO0VBQ0UsZUFBZTtBQUNqQjtBQUNBLHVCQUF1QjtBQUN2QjtFQUNFLGlDQUFpQztFQUNqQyw4QkFBOEI7RUFDOUIsMEJBQTBCO0VBQzFCLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsWUFBWTtFQUNaLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsMkJBQTJCO0VBQzNCLDBCQUEwQjtFQUMxQix5QkFBeUI7RUFDekI7dUJBQ3FCO0FBQ3ZCO0FBQ0E7RUFDRSwwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLDBCQUEwQjtFQUMxQiwwQkFBMEI7RUFDMUIsb0NBQW9DO0VBQ3BDLFlBQVk7RUFDWixtQ0FBbUM7RUFDbkMsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsMEJBQTBCO0VBQzFCLDBCQUEwQjtFQUMxQixvQ0FBb0M7RUFDcEMsWUFBWTtBQUNkO0FBQ0E7RUFDRSxtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLGlEQUFpRDtBQUNuRDtBQUNBO0VBQ0UsNEJBQTRCO0VBQzVCLHFCQUFxQjtBQUN2QjtBQUNBLDJCQUEyQjtBQUMzQjtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsUUFBUTtBQUNWO0FBQ0E7RUFDRSxhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsWUFBWTtFQUNaO1dBQ1M7RUFDVCw2QkFBNkI7RUFDN0I7U0FDTztFQUNQLFdBQVc7QUFDYjtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZjtXQUNTO0FBQ1g7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLGFBQWE7RUFDYjtXQUNTO0VBQ1QsNkJBQTZCO0FBQy9CO0FBQ0E7RUFDRSxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLDZCQUE2QjtFQUM3QixzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLFdBQVc7RUFDWCxVQUFVO0VBQ1Y7UUFDTTtFQUNOLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsV0FBVztBQUNiO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7QUFDakI7QUFDQTtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsV0FBVztBQUNiO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLG9CQUFvQjtFQUNwQixnQkFBZ0I7QUFDbEI7QUFDQSwwQkFBMEI7QUFDMUI7RUFDRSxlQUFlO0FBQ2pCO0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLHVDQUF1QztFQUN2QyxzQkFBc0I7RUFDdEIsNkJBQTZCO0VBQzdCLDJCQUEyQjtBQUM3QjtBQUNBLHdDQUF3QztBQUN4QztFQUNFLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsY0FBYztFQUNkLGNBQWM7QUFDaEI7QUFDQTtFQUNFLGNBQWM7QUFDaEI7QUFDQTtFQUNFLGNBQWM7RUFDZCxXQUFXO0VBQ1gsZUFBZTtFQUNmLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsNkJBQTZCO0VBQzdCLFNBQVM7QUFDWDtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLGNBQWM7QUFDaEI7QUFDQTtFQUNFLGFBQWE7QUFDZjtBQUNBO0VBQ0UsYUFBYTtFQUNiLFlBQVk7RUFDWixzQkFBc0I7RUFDdEIsb0JBQW9CO0FBQ3RCO0FBQ0E7RUFDRSxrQkFBa0I7QUFDcEI7QUFFQTtFQUNFLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsY0FBYztFQUNkLGNBQWM7QUFDaEI7QUFDQTtFQUNFLGNBQWM7QUFDaEI7QUFDQTtFQUNFLGNBQWM7RUFDZCxXQUFXO0VBQ1gsZUFBZTtFQUNmLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsNkJBQTZCO0VBQzdCLFNBQVM7QUFDWDtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLGNBQWM7QUFDaEI7QUFDQTtFQUNFLGFBQWE7QUFDZjtBQUNBLHdDQUF3QztBQUN4QztFQUNFLDBCQUEwQjtBQUM1QjtBQUNBO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWiw4QkFBOEI7RUFDOUIsNkJBQTZCO0FBQy9CO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsV0FBVztBQUNiIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hZGQtY2FuZGlkYXRlL2FkZC1jYW5kaWRhdGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluLW5hdi1pdGVtOm5vdCg6bGFzdC1jaGlsZCkge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzdEQTVBODtcbn1cbi5ib3JkZXItcmlnaHQge1xuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZFxuICAjZGVlMmU2ICFpbXBvcnRhbnQ7XG59XG4uZm9ybS1jb250cm9sIHtcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xufVxuaW5wdXQsc2VsZWN0IHtcbiAgYmFja2dyb3VuZDpcbiAgICAjZjVmNWY1ICFpbXBvcnRhbnQ7XG59XG5tYXQtZm9ybS1maWVsZCB7XG4gIG1pbi13aWR0aDogMTAwJTtcbn1cbi8qKioqKioqQlVUVE9OUyoqKioqKioqL1xuLmVudGl0eS1kZXRhaWxzX19idXR0b25zIHtcbiAgcGFkZGluZy1ib3R0b206IDEuNXJlbSAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXRvcDogMS41cmVtICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctbGVmdDogMCAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uZW50aXR5LWJ1dHRvbnMtd3JhcHBlciAuYnRuIHtcbiAgaGVpZ2h0OiA0MHB4O1xuICAvKnBhZGRpbmc6IDEycHggOXB4OyovXG4gIG1hcmdpbi1yaWdodDogMTJweDtcbiAgZm9udC13ZWlnaHQ6IDUwMCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgY29sb3I6XG4gICAgcmdiYSgwLCAwLCAwLCAwLjg3KTtcbn1cbi5idG4tY3JtIHtcbiAgYm9yZGVyLXJhZGl1czogMCFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMzBweDtcbiAgbWluLXdpZHRoOiAxMjBweDtcbn1cbi5idG4tY3JtLXByaW1hcnl7XG4gIGZvbnQtc2l6ZTogMTFweCFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA3MDAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiAwIWltcG9ydGFudDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAxNzQ3ZiAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDMwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMTkxOUQhaW1wb3J0YW50O1xuICBjb2xvcjogd2hpdGUhaW1wb3J0YW50O1xufVxuLmJ0bi1jcm0tZGVmYXVsdHtcbiAgZm9udC1zaXplOiAxMXB4IWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDcwMCFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDAhaW1wb3J0YW50O1xuICBib3JkZXI6IDFweCBzb2xpZCAjYWVhZWFlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMzBweDtcbn1cbi5idG4tY3JtLWRlZmF1bHQ6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjZjVmNWY1O1xuICBvdXRsaW5lOiAwO1xuICBib3gtc2hhZG93OiAwIDAgMCAwLjE1cmVtIHJnYmEoMjU1LCAxNzcsIDYsIDAuMjUpO1xufVxuLmJ0bi1jcm0tZGVmYXVsdF9yZWQge1xuICBib3JkZXItY29sb3I6IHJlZCAhaW1wb3J0YW50O1xuICBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XG59XG4vKioqKioqKlBIT1RPIFdJREdFVCoqKioqKiovXG4udGhyZWUtZG90cyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAxMHB4O1xuICByaWdodDogMDtcbn1cbi5lbnRpdHktaXRlbS1yb3VuZCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWxlZnQ6IC0yMDBweDtcbiAgd2lkdGg6IDY1cHg7XG4gIGhlaWdodDogNjVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjpcbiAgICAjZjVmNWY1O1xuICBib3JkZXItcmFkaXVzOiA1MCUgIWltcG9ydGFudDtcbiAgYm9yZGVyOiAxcHggc29saWRcbiAgI2VhZWFlYTtcbiAgei1pbmRleDogOTk7XG59XG4uZW50aXR5LWl0ZW0tcm91bmQgaSB7XG4gIGxpbmUtaGVpZ2h0OiA2NXB4O1xuICBmb250LXNpemU6IDQwcHg7XG4gIGNvbG9yOlxuICAgICNDNEM0QzQ7XG59XG4uZW1wdHktaW1nIHtcbiAgbWFyZ2luLXJpZ2h0OiA1MHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgd2lkdGg6IDE1MHB4O1xuICBoZWlnaHQ6IDE1MHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOlxuICAgICNmNWY1ZjU7XG4gIGJvcmRlci1yYWRpdXM6IDUwJSAhaW1wb3J0YW50O1xufVxuLmRyb3Bkb3duIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmJ0bi1kZWZhdWx0IHtcbiAgYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogMHB4ICFpbXBvcnRhbnQ7XG59XG4uZG90IHtcbiAgaGVpZ2h0OiA1cHg7XG4gIHdpZHRoOiA1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6XG4gICAgI2JiYjtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBtYXJnaW46IDFweDtcbn1cbi5wcm9maWxlX19kcm9wZG93bi1tZW51IGkge1xuICB3aWR0aDogMjJweDtcbiAgaGVpZ2h0OiAyMnB4O1xuICBtYXJnaW4tcmlnaHQ6IDE2cHg7XG59XG4ucHJvZmlsZV9fZHJvcGRvd24tbWVudSBpOjpiZWZvcmUge1xuICBsaW5lLWhlaWdodDogMjJweDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuLmlucHV0LXBob3RvIHtcbiAgd2lkdGg6IDAuMXB4O1xuICBoZWlnaHQ6IDAuMXB4O1xuICBvcGFjaXR5OiAwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IC0xO1xufVxuLmlucHV0LXBob3RvICsgbGFiZWwge1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZy1sZWZ0OjEuNDVyZW07XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG4vKioqKioqSU1BR0UgUFJFVklFVyoqKioqKi9cbi5pbWFnZS11cGxvYWQtY29udGFpbmVyIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4uaW1nLXByZXZpZXctY29udGFpbmVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmltZy1wcmV2aWV3IHtcbiAgLypiYWNrZ3JvdW5kOiBjZW50ZXIgY2VudGVyIG5vLXJlcGVhdDsqL1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBib3JkZXItcmFkaXVzOiA1MCUgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xufVxuLyoqKioqKlVQTE9BRCBSRVNVTUUgQU5EIERPQ1VNRU5UUyoqKioqKi9cbi5mYS1hZGQtcmVzdW1lOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDU1XCI7XG4gIGxpbmUtaGVpZ2h0OiAxO1xuICBwYWRkaW5nOiAxLjVweDtcbiAgY29sb3I6ICM1OGQ1ZTA7XG59XG4uYWRkLXJlc3VtZS1idXR0b24sLnJlbW92ZS1yZXN1bWUtYnV0dG9uIHtcbiAgY29sb3I6ICM1OGQ1ZTA7XG59XG4uYWRkLXJlc3VtZS1sYWJlbCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogLjI1cmVtO1xuICBjbGVhcjogYm90aDtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgY29sb3I6ICM1OGQ1ZTA7XG4gIHRleHQtYWxpZ246IGluaGVyaXQ7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBib3JkZXI6IDA7XG59XG4uYWRkLXJlc3VtZS1sYWJlbCBzcGFuIHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIGNvbG9yOiAjNThkNWUwO1xufVxuLmlucHV0LXJlc3VtZSB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG4ucmVtb3ZlLXJlc3VtZS1idXR0b24ge1xuICBvdXRsaW5lOiBub25lO1xuICBib3JkZXI6IG5vbmU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gIG1hcmdpbi1ib3R0b206IC41cmVtO1xufVxuLnJlbW92ZS1yZXN1bWUtYnV0dG9uIGl7XG4gIG1hcmdpbi1yaWdodDogMTlweDtcbn1cblxuLmZhLWFkZC1kb2N1bWVudDpiZWZvcmUge1xuICBjb250ZW50OiBcIlxcZjA1NVwiO1xuICBsaW5lLWhlaWdodDogMTtcbiAgcGFkZGluZzogMS41cHg7XG4gIGNvbG9yOiAjNThkNWUwO1xufVxuLmFkZC1kb2N1bWVudC1idXR0b24sLnJlbW92ZS1kb2N1bWVudC1idXR0b24ge1xuICBjb2xvcjogIzU4ZDVlMDtcbn1cbi5hZGQtZG9jdW1lbnQtbGFiZWwge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IC4yNXJlbTtcbiAgY2xlYXI6IGJvdGg7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGNvbG9yOiAjNThkNWUwO1xuICB0ZXh0LWFsaWduOiBpbmhlcml0O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgYm9yZGVyOiAwO1xufVxuLmFkZC1kb2N1bWVudC1sYWJlbCBzcGFuIHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIGNvbG9yOiAjNThkNWUwO1xufVxuLmlucHV0LWRvY3VtZW50IHtcbiAgZGlzcGxheTogbm9uZTtcbn1cbi8qKioqKioqTU9EQUxTIC0gREVMRVRFIENBTkRJREFURSAqKioqKiovXG4ubW9kYWwtYm9keSAuYnRuIHtcbiAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XG59XG4ubW9kYWwtaGVhZGVye1xuICBiYWNrZ3JvdW5kOiAjMDE5MTlEO1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDAuNXJlbSAxcmVtIWltcG9ydGFudDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlciFpbXBvcnRhbnQ7XG59XG4ubW9kYWwtaGVhZGVyIGg1IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW46IDBweDtcbn1cbiJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/components/add-candidate/add-candidate.component.ts": 
        /*!*********************************************************************!*\
          !*** ./src/app/components/add-candidate/add-candidate.component.ts ***!
          \*********************************************************************/
        /*! exports provided: AddCandidateComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddCandidateComponent", function () { return AddCandidateComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _services_candidate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/candidate.service */ "./src/app/services/candidate.service.ts");
            /* harmony import */ var _models_candidate__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models/candidate */ "./src/app/models/candidate.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _services_cities_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/cities.service */ "./src/app/services/cities.service.ts");
            /* harmony import */ var _services_skills_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/skills.service */ "./src/app/services/skills.service.ts");
            /* harmony import */ var _services_skills_candidate_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/skills-candidate.service */ "./src/app/services/skills-candidate.service.ts");
            /* harmony import */ var _services_image_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/image.service */ "./src/app/services/image.service.ts");
            /* harmony import */ var _services_file_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/file.service */ "./src/app/services/file.service.ts");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            /* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
            /* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/ __webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_11__);
            /* harmony import */ var _services_contacts_candidate_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../services/contacts-candidate.service */ "./src/app/services/contacts-candidate.service.ts");
            /* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
            var ImageSnippet = /** @class */ (function () {
                function ImageSnippet(src, file) {
                    this.src = src;
                    this.file = file;
                    this.pending = false;
                    this.status = 'init';
                }
                return ImageSnippet;
            }());
            var FileSnippet = /** @class */ (function () {
                function FileSnippet(src, file) {
                    this.src = src;
                    this.file = file;
                    this.pending = false;
                    this.status = 'init';
                }
                return FileSnippet;
            }());
            var AddCandidateComponent = /** @class */ (function () {
                function AddCandidateComponent(candidateService, route, citiesService, skillsService, skillsCandidateService, contactsCandidateService, imageService, fileService, modalService) {
                    this.candidateService = candidateService;
                    this.route = route;
                    this.citiesService = citiesService;
                    this.skillsService = skillsService;
                    this.skillsCandidateService = skillsCandidateService;
                    this.contactsCandidateService = contactsCandidateService;
                    this.imageService = imageService;
                    this.fileService = fileService;
                    this.modalService = modalService;
                    /*DOCUMENT TYPE*/
                    this.IS_RESUME = 1;
                    this.IS_DOCUMENT = 0;
                    /*CONTAcT TYPE*/
                    this.CONTACT_TYPE_PHONE = '1';
                    this.CONTACT_TYPE_EMAIL = '2';
                    this.CONTACT_TYPE_SKYPE = '3';
                    this.CONTACT_TYPE_LINKEDIN = '4';
                    this.CONTACT_TYPE_GITHUB = '5';
                    this.CONTACT_TYPE_BITBUCKET = '6';
                    this.contacts = [];
                    this.notes = '';
                    this.selectedSkills = [];
                    this.selectedSkillsCandidate = [];
                    this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_11__;
                    this.candidateResume = undefined;
                    this.candidateDocuments = [];
                }
                AddCandidateComponent.prototype.ngOnInit = function () {
                    this.getCities();
                    this.getSkills();
                };
                AddCandidateComponent.prototype.onFormSubmit = function (candidateAddForm) {
                    var _this = this;
                    console.log(candidateAddForm);
                    this.candidate = new _models_candidate__WEBPACK_IMPORTED_MODULE_3__["Candidate"]();
                    this.candidate.lastname = this.lastname;
                    this.candidate.name = this.name;
                    this.candidate.fathername = this.fathername;
                    this.candidate.location = this.location;
                    this.candidate.company = this.company;
                    this.candidate.position = this.position;
                    this.candidate.salary = this.salary;
                    this.candidate.wish = this.wish;
                    this.candidate.date_birth = this.date_birth;
                    this.candidate.notes = this.notes;
                    console.log(this.candidate);
                    this.candidateService.create(this.candidate).then(function (candidate) {
                        _this.isCandidateSaved = true;
                        _this.id = candidate.id;
                        _this.uniqueSkillCandidate(_this.selectedSkills).map(function (item) {
                            _this.selectedSkillsCandidate.push({ skill_id: item.skill_id, candidate_id: candidate.id });
                        });
                        _this.selectedSkillsCandidate.forEach(function (item) { return _this.skillsCandidateService.addSkillCandidate(item); });
                        _this.contacts.forEach(function (item) {
                            if (item.match(/^\+380\d{9}$/)) {
                                _this.contact_type = _this.CONTACT_TYPE_PHONE;
                            }
                            else if (item.includes('@')) {
                                _this.contact_type = _this.CONTACT_TYPE_EMAIL;
                            }
                            else if (item.match('^[a-zA-Z][a-zA-Z0-9-_\\.]{1,20}$')) {
                                _this.contact_type = _this.CONTACT_TYPE_SKYPE;
                            }
                            else if (item.includes('linkedin')) {
                                _this.contact_type = _this.CONTACT_TYPE_LINKEDIN;
                            }
                            else if (item.includes('github')) {
                                _this.contact_type = _this.CONTACT_TYPE_GITHUB;
                            }
                            else if (item.includes('bitbucket')) {
                                _this.contact_type = _this.CONTACT_TYPE_BITBUCKET;
                            }
                            else {
                                _this.contact_type = _this.CONTACT_TYPE_PHONE;
                            }
                            _this.contactsCandidateService.addContactCandidate({
                                candidate_id: candidate.id,
                                contact_type: _this.contact_type,
                                value: item,
                            });
                        });
                        if (_this.selectedPhoto) {
                            _this.imageService.uploadPhoto(_this.selectedPhoto.file, candidate.id).subscribe(function (res) {
                                console.log(res);
                            }, function (err) {
                            });
                        }
                        if (_this.selectedFile) {
                            _this.fileService.uploadFile(_this.selectedFile.file, candidate.id, _this.IS_RESUME).subscribe(function (res) {
                            }, function (err) {
                            });
                        }
                        if (_this.selectedFileDocument) {
                            _this.fileService.uploadFile(_this.selectedFileDocument.file, candidate.id, _this.IS_DOCUMENT).subscribe(function (res) {
                            }, function (err) {
                            });
                        }
                    });
                };
                AddCandidateComponent.prototype.getCities = function () {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["switchMap"])(function (params) { return _this.citiesService.getData(); }))
                        .subscribe(function (cities) { return _this.cities = cities; });
                };
                AddCandidateComponent.prototype.getSkills = function () {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["switchMap"])(function (params) { return _this.skillsService.getData(); }))
                        .subscribe(function (skills) {
                        _this.skills = skills;
                    });
                };
                AddCandidateComponent.prototype.addSkill = function (event) {
                    var _this = this;
                    event.value.map(function (item) { return _this.selectedSkills.push({ skill_id: item.id }); });
                };
                // Unique elements of array
                AddCandidateComponent.prototype.uniqueSkillCandidate = function (arr) {
                    var e_1, _a;
                    var result = [];
                    var _loop_1 = function (el) {
                        if (!result.some(function (e) {
                            return e.skill_id === el.skill_id && e.candidate_id === el.candidate_id;
                        })) {
                            result.push(el);
                        }
                    };
                    try {
                        for (var arr_1 = __values(arr), arr_1_1 = arr_1.next(); !arr_1_1.done; arr_1_1 = arr_1.next()) {
                            var el = arr_1_1.value;
                            _loop_1(el);
                        }
                    }
                    catch (e_1_1) { e_1 = { error: e_1_1 }; }
                    finally {
                        try {
                            if (arr_1_1 && !arr_1_1.done && (_a = arr_1.return)) _a.call(arr_1);
                        }
                        finally { if (e_1) throw e_1.error; }
                    }
                    return result;
                };
                AddCandidateComponent.prototype.addContact = function (contact) {
                    var contactValue = contact.value;
                    this.contacts.push('');
                };
                AddCandidateComponent.prototype.removeContact = function (i) {
                    this.contacts.splice(i, 1);
                };
                AddCandidateComponent.prototype.trackByFn = function (index, item) {
                    return index;
                };
                AddCandidateComponent.prototype.processPhoto = function (photoInput) {
                    var _this = this;
                    var file = photoInput.files[0];
                    var reader = new FileReader();
                    reader.addEventListener('load', function (event) {
                        _this.photoInput.nativeElement.value = null;
                        _this.selectedPhoto = new ImageSnippet(event.target.result, file);
                        _this.selectedPhoto.pending = true;
                    });
                    reader.readAsDataURL(file);
                };
                AddCandidateComponent.prototype.processFile = function (fileInput, fileType) {
                    var _this = this;
                    var file = fileInput.files[0];
                    var reader = new FileReader();
                    reader.addEventListener('load', function (event) {
                        _this.fileInputResume.nativeElement.value = null;
                        _this.fileInputDocument.nativeElement.value = null;
                        if (fileType === _this.IS_RESUME) {
                            _this.selectedFile = new FileSnippet(event.target.result, file);
                            _this.fileType = _this.IS_RESUME;
                        }
                        else if (fileType === _this.IS_DOCUMENT) {
                            _this.selectedFileDocument = new FileSnippet(event.target.result, file);
                            _this.fileType = _this.IS_DOCUMENT;
                            _this.candidateDocuments.push(_this.selectedFileDocument);
                        }
                    });
                    reader.readAsDataURL(file);
                };
                AddCandidateComponent.prototype.removePhoto = function () {
                    this.selectedPhoto = undefined;
                };
                AddCandidateComponent.prototype.removeFile = function () {
                    this.selectedFile = undefined;
                };
                AddCandidateComponent.prototype.removeCandidateDocument = function (event, el) {
                    event.preventDefault();
                    var deletedDocumentId = el.getAttribute('data-document-id');
                    this.candidateDocuments.splice(deletedDocumentId, 1);
                };
                AddCandidateComponent.prototype.openModal = function (template) {
                    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
                };
                AddCandidateComponent.prototype.confirm = function () {
                    var _this = this;
                    this.candidateService.delete(this.id).then(function (item) {
                        _this.id = undefined;
                        _this.candidate = undefined;
                        _this.lastname = undefined;
                        _this.name = undefined;
                        _this.fathername = undefined;
                        _this.location = undefined;
                        _this.company = undefined;
                        _this.position = undefined;
                        _this.salary = undefined;
                        _this.wish = undefined;
                        _this.skills = [];
                        _this.contacts = [];
                        _this.selectedSkills = [];
                        _this.selectedSkillsCandidate = [];
                        _this.date_birth = undefined;
                        _this.notes = '';
                        _this.selectedPhoto = undefined;
                        _this.selectedFile = undefined;
                        _this.selectedFileDocument = undefined;
                        _this.candidateDocuments = [];
                        _this.isCandidateSaved = false;
                        _this.getSkills();
                        _this.fileInputResume.nativeElement.value = null;
                        _this.fileInputDocument.nativeElement.value = null;
                        _this.photoInput.nativeElement.value = null;
                    });
                    this.modalRef.hide();
                };
                AddCandidateComponent.prototype.decline = function () {
                    this.modalRef.hide();
                };
                return AddCandidateComponent;
            }());
            AddCandidateComponent.ctorParameters = function () { return [
                { type: _services_candidate_service__WEBPACK_IMPORTED_MODULE_2__["CandidateService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
                { type: _services_cities_service__WEBPACK_IMPORTED_MODULE_5__["CitiesService"] },
                { type: _services_skills_service__WEBPACK_IMPORTED_MODULE_6__["SkillsService"] },
                { type: _services_skills_candidate_service__WEBPACK_IMPORTED_MODULE_7__["SkillsCandidateService"] },
                { type: _services_contacts_candidate_service__WEBPACK_IMPORTED_MODULE_12__["ContactsCandidateService"] },
                { type: _services_image_service__WEBPACK_IMPORTED_MODULE_8__["ImageService"] },
                { type: _services_file_service__WEBPACK_IMPORTED_MODULE_9__["FileService"] },
                { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_13__["BsModalService"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInputResume', { static: false })
            ], AddCandidateComponent.prototype, "fileInputResume", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInputDocument', { static: false })
            ], AddCandidateComponent.prototype, "fileInputDocument", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('photoInput', { static: false })
            ], AddCandidateComponent.prototype, "photoInput", void 0);
            AddCandidateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-add-candidate',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./add-candidate.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/add-candidate/add-candidate.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./add-candidate.component.css */ "./src/app/components/add-candidate/add-candidate.component.css")).default]
                })
            ], AddCandidateComponent);
            /***/ 
        }),
        /***/ "./src/app/components/add-vacancy/add-vacancy.component.css": 
        /*!******************************************************************!*\
          !*** ./src/app/components/add-vacancy/add-vacancy.component.css ***!
          \******************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".main-nav-item:not(:last-child) {\n    border-bottom: 1px solid #7DA5A8;\n  }\n  .border-right {\n    border-right: 1px solid\n    #dee2e6 !important;\n  }\n  .form-control {\n    border-radius: 0px;\n  }\n  input,select {\n    background:\n      #f5f5f5 !important;\n  }\n  mat-form-field {\n    min-width: 100%;\n  }\n  /*******BUTTONS********/\n  .entity-details__buttons {\n    padding-bottom: 1.5rem !important;\n    padding-top: 1.5rem !important;\n    padding-left: 0 !important;\n    text-align: center;\n  }\n  .entity-buttons-wrapper .btn {\n    height: 40px;\n    /*padding: 12px 9px;*/\n    margin-right: 12px;\n    font-weight: 500 !important;\n    font-size: 14px !important;\n    text-transform: uppercase;\n    color:\n      rgba(0, 0, 0, 0.87);\n  }\n  .btn-crm {\n    border-radius: 0!important;\n    height: 30px;\n    min-width: 120px;\n  }\n  .btn-crm-primary{\n    font-size: 11px!important;\n    font-weight: 700!important;\n    border-radius: 0!important;\n    border: 1px solid #01747f !important;\n    height: 30px;\n    background-color: #01919D!important;\n    color: white!important;\n  }\n  .btn-crm-default{\n    font-size: 11px!important;\n    font-weight: 700!important;\n    border-radius: 0!important;\n    border: 1px solid #aeaeae !important;\n    height: 30px;\n  }\n  .btn-crm-default:hover {\n    background: #f5f5f5;\n    outline: 0;\n    box-shadow: 0 0 0 0.15rem rgba(255, 177, 6, 0.25);\n  }\n  .btn-crm-default_red {\n    border-color: red !important;\n    color: red !important;\n  }\n  /******UPLOAD RESUME AND DOCUMENTS******/\n  .fa-add-resume:before {\n    content: \"\\f055\";\n    line-height: 1;\n    padding: 1.5px;\n    color: #58d5e0;\n  }\n  .add-resume-button,.remove-resume-button {\n    color: #58d5e0;\n  }\n  .add-resume-label {\n    display: block;\n    width: 100%;\n    padding: .25rem;\n    clear: both;\n    font-weight: 400;\n    color: #58d5e0;\n    text-align: inherit;\n    white-space: nowrap;\n    background-color: transparent;\n    border: 0;\n  }\n  .add-resume-label span {\n    margin-left: 10px;\n    color: #58d5e0;\n  }\n  .input-resume {\n    display: none;\n  }\n  .remove-resume-button {\n    outline: none;\n    border: none;\n    background-color: #fff;\n    margin-bottom: .5rem;\n  }\n  .remove-resume-button i{\n    margin-right: 19px;\n  }\n  .fa-add-document:before {\n    content: \"\\f055\";\n    line-height: 1;\n    padding: 1.5px;\n    color: #58d5e0;\n  }\n  .add-document-button,.remove-document-button {\n    color: #58d5e0;\n  }\n  .add-document-label {\n    display: block;\n    width: 100%;\n    padding: .25rem;\n    clear: both;\n    font-weight: 400;\n    color: #58d5e0;\n    text-align: inherit;\n    white-space: nowrap;\n    background-color: transparent;\n    border: 0;\n  }\n  .add-document-label span {\n    margin-left: 10px;\n    color: #58d5e0;\n  }\n  .input-document {\n    display: none;\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZGQtdmFjYW5jeS9hZGQtdmFjYW5jeS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksZ0NBQWdDO0VBQ2xDO0VBQ0E7SUFDRTtzQkFDa0I7RUFDcEI7RUFDQTtJQUNFLGtCQUFrQjtFQUNwQjtFQUNBO0lBQ0U7d0JBQ29CO0VBQ3RCO0VBQ0E7SUFDRSxlQUFlO0VBQ2pCO0VBRUYsdUJBQXVCO0VBQ3ZCO0lBQ0ksaUNBQWlDO0lBQ2pDLDhCQUE4QjtJQUM5QiwwQkFBMEI7SUFDMUIsa0JBQWtCO0VBQ3BCO0VBQ0E7SUFDRSxZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCLGtCQUFrQjtJQUNsQiwyQkFBMkI7SUFDM0IsMEJBQTBCO0lBQzFCLHlCQUF5QjtJQUN6Qjt5QkFDcUI7RUFDdkI7RUFDQTtJQUNFLDBCQUEwQjtJQUMxQixZQUFZO0lBQ1osZ0JBQWdCO0VBQ2xCO0VBQ0E7SUFDRSx5QkFBeUI7SUFDekIsMEJBQTBCO0lBQzFCLDBCQUEwQjtJQUMxQixvQ0FBb0M7SUFDcEMsWUFBWTtJQUNaLG1DQUFtQztJQUNuQyxzQkFBc0I7RUFDeEI7RUFDQTtJQUNFLHlCQUF5QjtJQUN6QiwwQkFBMEI7SUFDMUIsMEJBQTBCO0lBQzFCLG9DQUFvQztJQUNwQyxZQUFZO0VBQ2Q7RUFDQTtJQUNFLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsaURBQWlEO0VBQ25EO0VBQ0E7SUFDRSw0QkFBNEI7SUFDNUIscUJBQXFCO0VBQ3ZCO0VBQ0Ysd0NBQXdDO0VBQ3hDO0lBQ0ksZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxjQUFjO0lBQ2QsY0FBYztFQUNoQjtFQUNBO0lBQ0UsY0FBYztFQUNoQjtFQUNBO0lBQ0UsY0FBYztJQUNkLFdBQVc7SUFDWCxlQUFlO0lBQ2YsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQiw2QkFBNkI7SUFDN0IsU0FBUztFQUNYO0VBQ0E7SUFDRSxpQkFBaUI7SUFDakIsY0FBYztFQUNoQjtFQUNBO0lBQ0UsYUFBYTtFQUNmO0VBQ0E7SUFDRSxhQUFhO0lBQ2IsWUFBWTtJQUNaLHNCQUFzQjtJQUN0QixvQkFBb0I7RUFDdEI7RUFDQTtJQUNFLGtCQUFrQjtFQUNwQjtFQUVBO0lBQ0UsZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxjQUFjO0lBQ2QsY0FBYztFQUNoQjtFQUNBO0lBQ0UsY0FBYztFQUNoQjtFQUNBO0lBQ0UsY0FBYztJQUNkLFdBQVc7SUFDWCxlQUFlO0lBQ2YsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQiw2QkFBNkI7SUFDN0IsU0FBUztFQUNYO0VBQ0E7SUFDRSxpQkFBaUI7SUFDakIsY0FBYztFQUNoQjtFQUNBO0lBQ0UsYUFBYTtFQUNmIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hZGQtdmFjYW5jeS9hZGQtdmFjYW5jeS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1haW4tbmF2LWl0ZW06bm90KDpsYXN0LWNoaWxkKSB7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICM3REE1QTg7XG4gIH1cbiAgLmJvcmRlci1yaWdodCB7XG4gICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWRcbiAgICAjZGVlMmU2ICFpbXBvcnRhbnQ7XG4gIH1cbiAgLmZvcm0tY29udHJvbCB7XG4gICAgYm9yZGVyLXJhZGl1czogMHB4O1xuICB9XG4gIGlucHV0LHNlbGVjdCB7XG4gICAgYmFja2dyb3VuZDpcbiAgICAgICNmNWY1ZjUgIWltcG9ydGFudDtcbiAgfVxuICBtYXQtZm9ybS1maWVsZCB7XG4gICAgbWluLXdpZHRoOiAxMDAlO1xuICB9XG5cbi8qKioqKioqQlVUVE9OUyoqKioqKioqL1xuLmVudGl0eS1kZXRhaWxzX19idXR0b25zIHtcbiAgICBwYWRkaW5nLWJvdHRvbTogMS41cmVtICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZy10b3A6IDEuNXJlbSAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmctbGVmdDogMCAhaW1wb3J0YW50O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICAuZW50aXR5LWJ1dHRvbnMtd3JhcHBlciAuYnRuIHtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgLypwYWRkaW5nOiAxMnB4IDlweDsqL1xuICAgIG1hcmdpbi1yaWdodDogMTJweDtcbiAgICBmb250LXdlaWdodDogNTAwICFpbXBvcnRhbnQ7XG4gICAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjpcbiAgICAgIHJnYmEoMCwgMCwgMCwgMC44Nyk7XG4gIH1cbiAgLmJ0bi1jcm0ge1xuICAgIGJvcmRlci1yYWRpdXM6IDAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMzBweDtcbiAgICBtaW4td2lkdGg6IDEyMHB4O1xuICB9XG4gIC5idG4tY3JtLXByaW1hcnl7XG4gICAgZm9udC1zaXplOiAxMXB4IWltcG9ydGFudDtcbiAgICBmb250LXdlaWdodDogNzAwIWltcG9ydGFudDtcbiAgICBib3JkZXItcmFkaXVzOiAwIWltcG9ydGFudDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMDE3NDdmICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAzMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMTkxOUQhaW1wb3J0YW50O1xuICAgIGNvbG9yOiB3aGl0ZSFpbXBvcnRhbnQ7XG4gIH1cbiAgLmJ0bi1jcm0tZGVmYXVsdHtcbiAgICBmb250LXNpemU6IDExcHghaW1wb3J0YW50O1xuICAgIGZvbnQtd2VpZ2h0OiA3MDAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1yYWRpdXM6IDAhaW1wb3J0YW50O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNhZWFlYWUgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDMwcHg7XG4gIH1cbiAgLmJ0bi1jcm0tZGVmYXVsdDpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogI2Y1ZjVmNTtcbiAgICBvdXRsaW5lOiAwO1xuICAgIGJveC1zaGFkb3c6IDAgMCAwIDAuMTVyZW0gcmdiYSgyNTUsIDE3NywgNiwgMC4yNSk7XG4gIH1cbiAgLmJ0bi1jcm0tZGVmYXVsdF9yZWQge1xuICAgIGJvcmRlci1jb2xvcjogcmVkICFpbXBvcnRhbnQ7XG4gICAgY29sb3I6IHJlZCAhaW1wb3J0YW50O1xuICB9XG4vKioqKioqVVBMT0FEIFJFU1VNRSBBTkQgRE9DVU1FTlRTKioqKioqL1xuLmZhLWFkZC1yZXN1bWU6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlxcZjA1NVwiO1xuICAgIGxpbmUtaGVpZ2h0OiAxO1xuICAgIHBhZGRpbmc6IDEuNXB4O1xuICAgIGNvbG9yOiAjNThkNWUwO1xuICB9XG4gIC5hZGQtcmVzdW1lLWJ1dHRvbiwucmVtb3ZlLXJlc3VtZS1idXR0b24ge1xuICAgIGNvbG9yOiAjNThkNWUwO1xuICB9XG4gIC5hZGQtcmVzdW1lLWxhYmVsIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAuMjVyZW07XG4gICAgY2xlYXI6IGJvdGg7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBjb2xvcjogIzU4ZDVlMDtcbiAgICB0ZXh0LWFsaWduOiBpbmhlcml0O1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyOiAwO1xuICB9XG4gIC5hZGQtcmVzdW1lLWxhYmVsIHNwYW4ge1xuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgIGNvbG9yOiAjNThkNWUwO1xuICB9XG4gIC5pbnB1dC1yZXN1bWUge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbiAgLnJlbW92ZS1yZXN1bWUtYnV0dG9uIHtcbiAgICBvdXRsaW5lOiBub25lO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICAgIG1hcmdpbi1ib3R0b206IC41cmVtO1xuICB9XG4gIC5yZW1vdmUtcmVzdW1lLWJ1dHRvbiBpe1xuICAgIG1hcmdpbi1yaWdodDogMTlweDtcbiAgfVxuICBcbiAgLmZhLWFkZC1kb2N1bWVudDpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXFxmMDU1XCI7XG4gICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgcGFkZGluZzogMS41cHg7XG4gICAgY29sb3I6ICM1OGQ1ZTA7XG4gIH1cbiAgLmFkZC1kb2N1bWVudC1idXR0b24sLnJlbW92ZS1kb2N1bWVudC1idXR0b24ge1xuICAgIGNvbG9yOiAjNThkNWUwO1xuICB9XG4gIC5hZGQtZG9jdW1lbnQtbGFiZWwge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IC4yNXJlbTtcbiAgICBjbGVhcjogYm90aDtcbiAgICBmb250LXdlaWdodDogNDAwO1xuICAgIGNvbG9yOiAjNThkNWUwO1xuICAgIHRleHQtYWxpZ246IGluaGVyaXQ7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXI6IDA7XG4gIH1cbiAgLmFkZC1kb2N1bWVudC1sYWJlbCBzcGFuIHtcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICBjb2xvcjogIzU4ZDVlMDtcbiAgfVxuICAuaW5wdXQtZG9jdW1lbnQge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/components/add-vacancy/add-vacancy.component.ts": 
        /*!*****************************************************************!*\
          !*** ./src/app/components/add-vacancy/add-vacancy.component.ts ***!
          \*****************************************************************/
        /*! exports provided: AddVacancyComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddVacancyComponent", function () { return AddVacancyComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
            /* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/ __webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2__);
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var src_app_services_cities_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/cities.service */ "./src/app/services/cities.service.ts");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            /* harmony import */ var src_app_services_skills_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/skills.service */ "./src/app/services/skills.service.ts");
            /* harmony import */ var src_app_services_company_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/company.service */ "./src/app/services/company.service.ts");
            /* harmony import */ var src_app_models_vacancy__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/models/vacancy */ "./src/app/models/vacancy.ts");
            /* harmony import */ var src_app_services_skills_vacancy_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/skills-vacancy.service */ "./src/app/services/skills-vacancy.service.ts");
            /* harmony import */ var src_app_services_vacancy_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/vacancy.service */ "./src/app/services/vacancy.service.ts");
            /* harmony import */ var src_app_services_vacancy_documents_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/services/vacancy-documents.service */ "./src/app/services/vacancy-documents.service.ts");
            /* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
            var FileSnippet = /** @class */ (function () {
                function FileSnippet(src, file) {
                    this.src = src;
                    this.file = file;
                    this.pending = false;
                    this.status = 'init';
                }
                return FileSnippet;
            }());
            var AddVacancyComponent = /** @class */ (function () {
                function AddVacancyComponent(route, citiesService, skillsService, companyService, vacancyService, skillsVacancyService, vacancyDocumentService, modalService) {
                    this.route = route;
                    this.citiesService = citiesService;
                    this.skillsService = skillsService;
                    this.companyService = companyService;
                    this.vacancyService = vacancyService;
                    this.skillsVacancyService = skillsVacancyService;
                    this.vacancyDocumentService = vacancyDocumentService;
                    this.modalService = modalService;
                    this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2__;
                    this.v_duties = '';
                    this.w_conditions = '';
                    this.v_requirements = '';
                    this.v_description = '';
                    this.selectedSkills = [];
                    this.selectedSkillsVacancy = [];
                    this.vacancyDocuments = [];
                }
                AddVacancyComponent.prototype.ngOnInit = function () {
                    this.getCities();
                    this.getSkills();
                    this.getCompanies();
                };
                AddVacancyComponent.prototype.onFormSubmit = function (vacancyAddForm) {
                    var _this = this;
                    console.log(vacancyAddForm);
                    this.vacancy = new src_app_models_vacancy__WEBPACK_IMPORTED_MODULE_8__["Vacancy"]();
                    this.vacancy.name = vacancyAddForm.value.name;
                    this.vacancy.company_id = vacancyAddForm.value.company;
                    this.vacancy.location = vacancyAddForm.value.location;
                    this.vacancy.salary = vacancyAddForm.value.salary;
                    this.vacancy.v_duties = vacancyAddForm.value.v_duties;
                    this.vacancy.w_conditions = vacancyAddForm.value.w_conditions;
                    this.vacancy.v_requirements = vacancyAddForm.value.v_requirements;
                    this.vacancy.v_description = vacancyAddForm.value.v_description;
                    this.vacancyService.create(this.vacancy).then(function (vacancy) {
                        _this.isVacancySaved = true;
                        _this.selectedVacancyId = vacancy.id;
                        //Add selected skills
                        _this.uniqueSkillVacancy(_this.selectedSkills).map(function (item) {
                            _this.selectedSkillsVacancy.push({ skill_id: item.skill_id, vacancy_id: vacancy.id });
                        });
                        _this.selectedSkillsVacancy.forEach(function (item) { return _this.skillsVacancyService.addSkillVacancy(item); });
                        if (_this.selectedFileDocument) {
                            _this.vacancyDocumentService.uploadFile(_this.selectedFileDocument.file, vacancy.id).subscribe(function (res) {
                            }, function (err) {
                            });
                        }
                    });
                };
                AddVacancyComponent.prototype.getCities = function () {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (params) { return _this.citiesService.getData(); }))
                        .subscribe(function (cities) { return _this.cities = cities; });
                };
                AddVacancyComponent.prototype.getSkills = function () {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (params) { return _this.skillsService.getData(); }))
                        .subscribe(function (skills) {
                        _this.skills = skills;
                    });
                };
                AddVacancyComponent.prototype.getCompanies = function () {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (params) { return _this.companyService.getAllCompanies(); }))
                        .subscribe(function (companies) {
                        _this.companies = companies;
                    });
                };
                // Unique elements of array
                AddVacancyComponent.prototype.uniqueSkillVacancy = function (arr) {
                    var e_2, _a;
                    var result = [];
                    var _loop_2 = function (el) {
                        if (!result.some(function (e) {
                            return e.skill_id === el.skill_id && e.candidate_id === el.candidate_id;
                        })) {
                            result.push(el);
                        }
                    };
                    try {
                        for (var arr_2 = __values(arr), arr_2_1 = arr_2.next(); !arr_2_1.done; arr_2_1 = arr_2.next()) {
                            var el = arr_2_1.value;
                            _loop_2(el);
                        }
                    }
                    catch (e_2_1) { e_2 = { error: e_2_1 }; }
                    finally {
                        try {
                            if (arr_2_1 && !arr_2_1.done && (_a = arr_2.return)) _a.call(arr_2);
                        }
                        finally { if (e_2) throw e_2.error; }
                    }
                    return result;
                };
                AddVacancyComponent.prototype.addSkill = function (event) {
                    var _this = this;
                    event.value.map(function (item) { return _this.selectedSkills.push({ skill_id: item.id }); });
                };
                AddVacancyComponent.prototype.processFile = function (fileInput) {
                    var _this = this;
                    var file = fileInput.files[0];
                    var reader = new FileReader();
                    reader.addEventListener('load', function (event) {
                        _this.fileInputDocument.nativeElement.value = null;
                        _this.selectedFileDocument = new FileSnippet(event.target.result, file);
                        _this.vacancyDocuments.push(_this.selectedFileDocument);
                    });
                    reader.readAsDataURL(file);
                };
                AddVacancyComponent.prototype.removeVacancyDocument = function (event, el) {
                    event.preventDefault();
                    var deletedDocumentId = el.getAttribute('data-document-id');
                    this.vacancyDocuments.splice(deletedDocumentId, 1);
                };
                AddVacancyComponent.prototype.openModal = function (template) {
                    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
                };
                AddVacancyComponent.prototype.decline = function () {
                    this.modalRef.hide();
                };
                AddVacancyComponent.prototype.confirm = function () {
                    var _this = this;
                    this.vacancyService.delete(this.selectedVacancyId).then(function (item) {
                        _this.name = undefined;
                        _this.company = undefined;
                        _this.location = undefined;
                        _this.salary = undefined;
                        _this.v_duties = '';
                        _this.w_conditions = '';
                        _this.v_requirements = '';
                        _this.v_description = '';
                        _this.skills = [];
                        _this.isVacancySaved = false;
                        _this.getSkills();
                        _this.fileInputDocument.nativeElement.value = null;
                        _this.selectedFileDocument = undefined;
                        _this.vacancyDocuments = [];
                        /*
                        this.vacancy.id = undefined;
                        this.vacancy = undefined;
                        this.vacancy.name = undefined;
                        this.vacancy.location = undefined;
                        this.vacancy.company_id = undefined;
                        this.vacancy.salary = undefined;
                        // this.vacancy.skills = [];
                        this.selectedSkills = [];
                        this.selectedSkillsVacancy = [];
                        this.selectedFileDocument = undefined;
                        this.vacancyDocuments = [];
                        this.isVacancySaved = false;
                        this.getSkills();
                        this.fileInputDocument.nativeElement.value = null;
                        */
                    });
                    this.modalRef.hide();
                };
                return AddVacancyComponent;
            }());
            AddVacancyComponent.ctorParameters = function () { return [
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
                { type: src_app_services_cities_service__WEBPACK_IMPORTED_MODULE_4__["CitiesService"] },
                { type: src_app_services_skills_service__WEBPACK_IMPORTED_MODULE_6__["SkillsService"] },
                { type: src_app_services_company_service__WEBPACK_IMPORTED_MODULE_7__["CompanyService"] },
                { type: src_app_services_vacancy_service__WEBPACK_IMPORTED_MODULE_10__["VacancyService"] },
                { type: src_app_services_skills_vacancy_service__WEBPACK_IMPORTED_MODULE_9__["SkillsVacancyService"] },
                { type: src_app_services_vacancy_documents_service__WEBPACK_IMPORTED_MODULE_11__["VacancyDocumentsService"] },
                { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_12__["BsModalService"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInputDocument', { static: false })
            ], AddVacancyComponent.prototype, "fileInputDocument", void 0);
            AddVacancyComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-add-vacancy',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./add-vacancy.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/add-vacancy/add-vacancy.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./add-vacancy.component.css */ "./src/app/components/add-vacancy/add-vacancy.component.css")).default]
                })
            ], AddVacancyComponent);
            /***/ 
        }),
        /***/ "./src/app/components/all-candidates/all-candidates.component.css": 
        /*!************************************************************************!*\
          !*** ./src/app/components/all-candidates/all-candidates.component.css ***!
          \************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".selected {\n  background-color: #CFD8DC !important;\n  color: white;\n}\n.candidates {\n  margin: 0 0 2em 0;\n  list-style-type: none;\n  padding: 0;\n}\n.candidates li {\n  cursor: pointer;\n  position: relative;\n  left: 0;\n  margin: .5em;\n  padding: .3em 0;\n  height: 1.6em;\n  border-radius: 4px;\n}\n.candidates li.selected:hover {\n  background-color: #BBD8DC !important;\n  color: white;\n}\n.candidates li:hover {\n  color: #607D8B;\n  background-color: #DDD;\n  left: .1em;\n}\n.candidates .text {\n  position: relative;\n  top: -3px;\n}\n.candidates .badge {\n  display: inline-block;\n  font-size: small;\n  color: white;\n  padding: 0.8em 0.7em 0 0.7em;\n  background-color: #607D8B;\n  line-height: 1em;\n  position: relative;\n  left: -1px;\n  top: -4px;\n  height: 1.8em;\n  margin-right: .8em;\n  border-radius: 4px 0 0 4px;\n}\n/*******Main menu top*******/\n.main-menu-top {\n  padding: 0 !important;\n  background: #0E5B61;\n  /*box-shadow: 0px 4px 28px rgba(0, 0, 0, 0.25);*/\n  box-shadow: inset 0px 28px 20px -20px rgba(0,0,0,0.28);\n}\n.main-menu-top a, .main-menu-top i {\n  color: rgba(255, 255, 255, 0.8);\n  padding: 1em 28px;\n}\n.candidate-search {\n  padding: 42px 0 31px !important;\n}\n#searchCandidate {\n  width: 35rem;\n}\n.candidate-search input {\n  background: #F6F6F6;\n  border: 1px solid #E1E1E1;\n  font-size: 14px;\n  padding: 12px;\n  height: 40px;\n}\n.candidate-search input::-webkit-input-placeholder {\n  text-transform: uppercase;\n  color: #757575;\n}\n.candidate-search input::-moz-placeholder {\n  text-transform: uppercase;\n  color: #757575;\n}\n.candidate-search input::-ms-input-placeholder {\n  text-transform: uppercase;\n  color: #757575;\n}\n.candidate-search input::placeholder {\n  text-transform: uppercase;\n  color: #757575;\n}\n.entity_list__item {\n  display:flex;\n  background: #fff;\n  min-height: 100px;\n  /*padding-bottom: 5rem !important;*/\n  /*padding-top: 5rem !important;*/\n  border-bottom: 1px solid #dee2e6 !important;\n  margin: 0 !important;\n}\n.candidates-list {\n  overflow-y: scroll;\n  padding: 0;\n  overflow-x: hidden;\n  max-height: 875px;\n}\n.entity_list__item a {\n  color: black !important;\n  text-decoration: none;\n  background-color: transparent;\n}\n.entity_list__item .entity-title {\n  font-weight: 500;\n}\n.entity_list__item .entity-sub {\n  font-size: 14px;\n  color:\n    #616161;\n  font-weight: normal;\n}\n.entity-item-round {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  text-align: center;\n  margin-top: 5px;\n  margin-left: 20px;\n  width: 65px;\n  height: 65px;\n  background-color:\n    #f5f5f5;\n  border-radius: 50% !important;\n  border: 1px solid\n  #eaeaea;\n}\n.entity-item-round i {\n  line-height: 65px;\n  font-size: 40px;\n  color:\n    #C4C4C4;\n}\n.border-bottom {\n  border-bottom: 1px solid #dee2e6 !important;\n}\n/*******BUTTONS********/\n.entity-details__buttons {\n  padding-bottom: 1.5rem !important;\n  padding-top: 1.5rem !important;\n  padding-left: 0 !important;\n  text-align: center;\n}\n.entity-buttons-wrapper .btn {\n  height: 40px;\n  /*padding: 12px 9px;*/\n  margin-right: 12px;\n  font-weight: 500 !important;\n  font-size: 14px !important;\n  text-transform: uppercase;\n  color:\n    rgba(0, 0, 0, 0.87);\n}\n.btn-crm {\n  border-radius: 0!important;\n  height: 30px;\n  min-width: 120px;\n}\n.btn-crm-primary{\n  font-size: 11px!important;\n  font-weight: 700!important;\n  border-radius: 0!important;\n  border: 1px solid #01747f !important;\n  height: 30px;\n  background-color: #01919D!important;\n  color: white!important;\n}\n.btn-crm-default{\n  font-size: 11px!important;\n  font-weight: 700!important;\n  border-radius: 0!important;\n  border: 1px solid #aeaeae !important;\n  height: 30px;\n}\n.btn-crm-default:hover {\n  background: #f5f5f5;\n  outline: 0;\n  box-shadow: 0 0 0 0.15rem rgba(255, 177, 6, 0.25);\n}\n.btn-crm-default_red {\n  border-color: red !important;\n  color: red !important;\n}\n/********Main panel*******/\n.entity-name {\n  font-size: 20px;\n  color: #151319;\n  padding-top: 24px;\n}\n.entity-sub {\n  /*padding-top: 12px;*/\n  font-size: 18px;\n  color: #151319;\n}\n.entity-details .item-title {\n  font-size: 16px;\n  line-height: 30px;\n  color: #6A6A6A;\n}\n.entity-details .item-value{\n  font-weight: normal;\n  font-size: 16px;\n  line-height: 30px;\n  color: #151319;\n}\n.stages.nav-item a {\n  font-weight: normal;\n  font-size: 20px;\n  color: #6A6A6A;\n}\n.stages.nav-item a.active {\n  font-weight: bold;\n  font-size: 20px;\n  color: #26696E;\n}\n.stages .pl-4 {\n  font-weight: normal;\n  font-size: 16px;\n  color: #4B4C51\n}\n.stages b {\n  font-weight: bold;\n  font-size: 18px;\n  line-height: normal;\n  color: #4B4C51;\n}\n.stages.tab-pane{\n  background: #edf7f8;\n}\n.entity-stages-wrap .tab-pane {\n  min-height: 250px;\n  border-bottom: 1px solid #dee2e6;\n}\n.entity-stages-wrap .tab-content {\n  border-left: 1px solid #dee2e6;\n  border-right: 1px solid #dee2e6;\n\n}\n.nav-tabs .nav-link.active {\n  border-bottom-color: #edf7f8;\n}\n.entity-details .skill {\n  display: inline;\n  margin-right: 10px;\n  background: rgba(225, 225, 225, 0.6);\n  border: 1px solid #C4C4C4;\n  font-size: 10px;\n  text-transform: uppercase;\n  color: #151319;\n}\nspan.skill {\n  margin: 1px 2px;\n  padding: 1px 5px;\n  border: 1px solid #aeaeae;\n  font-size: 12px;\n  background: #f5f5f5;\n  display: inline-block;\n  font-weight: 500;\n}\n.side-photo {\n  width: 150px;\n  height: 150px;\n  border-radius: 50%;\n  margin-right: 50px;\n  margin-top: 10px;\n}\n.entity-item-round-big-placeholder {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  text-align: center;\n  margin-top: 5px;\n  width: 65px;\n  height: 65px;\n  background-color:\n    #f5f5f5;\n  border-radius: 50% !important;\n  border: 1px solid\n  #eaeaea;\n  z-index: 99;\n}\n.entity-item-round-big-placeholder i {\n  line-height: 65px;\n  font-size: 40px;\n  color:\n    #C4C4C4;\n}\n.empty-img-big-placeholder {\n  margin-right: 50px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-top: 10px;\n  width: 150px;\n  height: 150px;\n  background-color:\n    #f5f5f5;\n  border-radius: 50% !important;\n}\n.three-dots {\n  position: absolute;\n  top: 10px;\n  right: 0;\n}\n.dropdown {\n  position: relative;\n}\n.btn-default {\n  border-radius: 50% !important;\n  border: 0px !important;\n}\n.dot {\n  height: 5px;\n  width: 5px;\n  background-color:\n    #bbb;\n  border-radius: 50%;\n  display: flex;\n  margin: 1px;\n}\n.profile__dropdown-menu i {\n  width: 22px;\n  height: 22px;\n  margin-right: 16px;\n}\n.profile__dropdown-menu i::before {\n  line-height: 22px;\n  font-size: 12px;\n}\n.input-photo {\n  width: 0.1px;\n  height: 0.1px;\n  opacity: 0;\n  overflow: hidden;\n  position: absolute;\n  z-index: -1;\n}\n.input-photo + label {\n  max-width: 100%;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  cursor: pointer;\n  display: inline-block;\n  padding-left:1.45rem;\n  margin-bottom: 0;\n}\n/*******MODALS - DELETE CANDIDATE ******/\n.modal-body .btn {\n  font-size: 14px !important;\n}\n.modal-header{\n  background: #01919D;\n  color: white;\n  padding: 0.5rem 1rem!important;\n  align-items: center!important;\n}\n.modal-header h5 {\n  font-size: 14px;\n  margin: 0px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hbGwtY2FuZGlkYXRlcy9hbGwtY2FuZGlkYXRlcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usb0NBQW9DO0VBQ3BDLFlBQVk7QUFDZDtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLHFCQUFxQjtFQUNyQixVQUFVO0FBQ1o7QUFDQTtFQUNFLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsT0FBTztFQUNQLFlBQVk7RUFDWixlQUFlO0VBQ2YsYUFBYTtFQUNiLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0Usb0NBQW9DO0VBQ3BDLFlBQVk7QUFDZDtBQUNBO0VBQ0UsY0FBYztFQUNkLHNCQUFzQjtFQUN0QixVQUFVO0FBQ1o7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0FBQ1g7QUFDQTtFQUNFLHFCQUFxQjtFQUNyQixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLDRCQUE0QjtFQUM1Qix5QkFBeUI7RUFDekIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsU0FBUztFQUNULGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsMEJBQTBCO0FBQzVCO0FBQ0EsNEJBQTRCO0FBQzVCO0VBQ0UscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQixnREFBZ0Q7RUFDaEQsc0RBQXNEO0FBQ3hEO0FBQ0E7RUFDRSwrQkFBK0I7RUFDL0IsaUJBQWlCO0FBQ25CO0FBQ0E7RUFDRSwrQkFBK0I7QUFDakM7QUFDQTtFQUNFLFlBQVk7QUFDZDtBQUNBO0VBQ0UsbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6QixlQUFlO0VBQ2YsYUFBYTtFQUNiLFlBQVk7QUFDZDtBQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLGNBQWM7QUFDaEI7QUFIQTtFQUNFLHlCQUF5QjtFQUN6QixjQUFjO0FBQ2hCO0FBSEE7RUFDRSx5QkFBeUI7RUFDekIsY0FBYztBQUNoQjtBQUhBO0VBQ0UseUJBQXlCO0VBQ3pCLGNBQWM7QUFDaEI7QUFDQTtFQUNFLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG1DQUFtQztFQUNuQyxnQ0FBZ0M7RUFDaEMsMkNBQTJDO0VBQzNDLG9CQUFvQjtBQUN0QjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixrQkFBa0I7RUFDbEIsaUJBQWlCO0FBQ25CO0FBQ0E7RUFDRSx1QkFBdUI7RUFDdkIscUJBQXFCO0VBQ3JCLDZCQUE2QjtBQUMvQjtBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxlQUFlO0VBQ2Y7V0FDUztFQUNULG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsV0FBVztFQUNYLFlBQVk7RUFDWjtXQUNTO0VBQ1QsNkJBQTZCO0VBQzdCO1NBQ087QUFDVDtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZjtXQUNTO0FBQ1g7QUFDQTtFQUNFLDJDQUEyQztBQUM3QztBQUNBLHVCQUF1QjtBQUN2QjtFQUNFLGlDQUFpQztFQUNqQyw4QkFBOEI7RUFDOUIsMEJBQTBCO0VBQzFCLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsWUFBWTtFQUNaLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsMkJBQTJCO0VBQzNCLDBCQUEwQjtFQUMxQix5QkFBeUI7RUFDekI7dUJBQ3FCO0FBQ3ZCO0FBQ0E7RUFDRSwwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLDBCQUEwQjtFQUMxQiwwQkFBMEI7RUFDMUIsb0NBQW9DO0VBQ3BDLFlBQVk7RUFDWixtQ0FBbUM7RUFDbkMsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsMEJBQTBCO0VBQzFCLDBCQUEwQjtFQUMxQixvQ0FBb0M7RUFDcEMsWUFBWTtBQUNkO0FBQ0E7RUFDRSxtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLGlEQUFpRDtBQUNuRDtBQUNBO0VBQ0UsNEJBQTRCO0VBQzVCLHFCQUFxQjtBQUN2QjtBQUNBLDBCQUEwQjtBQUUxQjtFQUNFLGVBQWU7RUFDZixjQUFjO0VBQ2QsaUJBQWlCO0FBQ25CO0FBQ0E7RUFDRSxxQkFBcUI7RUFDckIsZUFBZTtFQUNmLGNBQWM7QUFDaEI7QUFDQTtFQUNFLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsY0FBYztBQUNoQjtBQUVBO0VBQ0UsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsY0FBYztBQUNoQjtBQUNBO0VBQ0UsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGNBQWM7QUFDaEI7QUFDQTtFQUNFLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2Y7QUFDRjtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsY0FBYztBQUNoQjtBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsZ0NBQWdDO0FBQ2xDO0FBQ0E7RUFDRSw4QkFBOEI7RUFDOUIsK0JBQStCOztBQUVqQztBQUNBO0VBQ0UsNEJBQTRCO0FBQzlCO0FBQ0E7RUFDRSxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLG9DQUFvQztFQUNwQyx5QkFBeUI7RUFDekIsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHlCQUF5QjtFQUN6QixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQixnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGFBQWE7RUFDYix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsV0FBVztFQUNYLFlBQVk7RUFDWjtXQUNTO0VBQ1QsNkJBQTZCO0VBQzdCO1NBQ087RUFDUCxXQUFXO0FBQ2I7QUFDQTtFQUNFLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2Y7V0FDUztBQUNYO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixhQUFhO0VBQ2I7V0FDUztFQUNULDZCQUE2QjtBQUMvQjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxRQUFRO0FBQ1Y7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsNkJBQTZCO0VBQzdCLHNCQUFzQjtBQUN4QjtBQUNBO0VBQ0UsV0FBVztFQUNYLFVBQVU7RUFDVjtRQUNNO0VBQ04sa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixXQUFXO0FBQ2I7QUFDQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsZUFBZTtBQUNqQjtBQUNBO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixXQUFXO0FBQ2I7QUFDQTtFQUNFLGVBQWU7RUFDZix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsb0JBQW9CO0VBQ3BCLGdCQUFnQjtBQUNsQjtBQUNBLHdDQUF3QztBQUN4QztFQUNFLDBCQUEwQjtBQUM1QjtBQUNBO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWiw4QkFBOEI7RUFDOUIsNkJBQTZCO0FBQy9CO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsV0FBVztBQUNiIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hbGwtY2FuZGlkYXRlcy9hbGwtY2FuZGlkYXRlcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNlbGVjdGVkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0NGRDhEQyAhaW1wb3J0YW50O1xuICBjb2xvcjogd2hpdGU7XG59XG4uY2FuZGlkYXRlcyB7XG4gIG1hcmdpbjogMCAwIDJlbSAwO1xuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG4gIHBhZGRpbmc6IDA7XG59XG4uY2FuZGlkYXRlcyBsaSB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsZWZ0OiAwO1xuICBtYXJnaW46IC41ZW07XG4gIHBhZGRpbmc6IC4zZW0gMDtcbiAgaGVpZ2h0OiAxLjZlbTtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xufVxuLmNhbmRpZGF0ZXMgbGkuc2VsZWN0ZWQ6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjQkJEOERDICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5jYW5kaWRhdGVzIGxpOmhvdmVyIHtcbiAgY29sb3I6ICM2MDdEOEI7XG4gIGJhY2tncm91bmQtY29sb3I6ICNEREQ7XG4gIGxlZnQ6IC4xZW07XG59XG4uY2FuZGlkYXRlcyAudGV4dCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdG9wOiAtM3B4O1xufVxuLmNhbmRpZGF0ZXMgLmJhZGdlIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250LXNpemU6IHNtYWxsO1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDAuOGVtIDAuN2VtIDAgMC43ZW07XG4gIGJhY2tncm91bmQtY29sb3I6ICM2MDdEOEI7XG4gIGxpbmUtaGVpZ2h0OiAxZW07XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbGVmdDogLTFweDtcbiAgdG9wOiAtNHB4O1xuICBoZWlnaHQ6IDEuOGVtO1xuICBtYXJnaW4tcmlnaHQ6IC44ZW07XG4gIGJvcmRlci1yYWRpdXM6IDRweCAwIDAgNHB4O1xufVxuLyoqKioqKipNYWluIG1lbnUgdG9wKioqKioqKi9cbi5tYWluLW1lbnUtdG9wIHtcbiAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kOiAjMEU1QjYxO1xuICAvKmJveC1zaGFkb3c6IDBweCA0cHggMjhweCByZ2JhKDAsIDAsIDAsIDAuMjUpOyovXG4gIGJveC1zaGFkb3c6IGluc2V0IDBweCAyOHB4IDIwcHggLTIwcHggcmdiYSgwLDAsMCwwLjI4KTtcbn1cbi5tYWluLW1lbnUtdG9wIGEsIC5tYWluLW1lbnUtdG9wIGkge1xuICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjgpO1xuICBwYWRkaW5nOiAxZW0gMjhweDtcbn1cbi5jYW5kaWRhdGUtc2VhcmNoIHtcbiAgcGFkZGluZzogNDJweCAwIDMxcHggIWltcG9ydGFudDtcbn1cbiNzZWFyY2hDYW5kaWRhdGUge1xuICB3aWR0aDogMzVyZW07XG59XG4uY2FuZGlkYXRlLXNlYXJjaCBpbnB1dCB7XG4gIGJhY2tncm91bmQ6ICNGNkY2RjY7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNFMUUxRTE7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgcGFkZGluZzogMTJweDtcbiAgaGVpZ2h0OiA0MHB4O1xufVxuLmNhbmRpZGF0ZS1zZWFyY2ggaW5wdXQ6OnBsYWNlaG9sZGVyIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgY29sb3I6ICM3NTc1NzU7XG59XG4uZW50aXR5X2xpc3RfX2l0ZW0ge1xuICBkaXNwbGF5OmZsZXg7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIG1pbi1oZWlnaHQ6IDEwMHB4O1xuICAvKnBhZGRpbmctYm90dG9tOiA1cmVtICFpbXBvcnRhbnQ7Ki9cbiAgLypwYWRkaW5nLXRvcDogNXJlbSAhaW1wb3J0YW50OyovXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGVlMmU2ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMCAhaW1wb3J0YW50O1xufVxuLmNhbmRpZGF0ZXMtbGlzdCB7XG4gIG92ZXJmbG93LXk6IHNjcm9sbDtcbiAgcGFkZGluZzogMDtcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xuICBtYXgtaGVpZ2h0OiA4NzVweDtcbn1cbi5lbnRpdHlfbGlzdF9faXRlbSBhIHtcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG4uZW50aXR5X2xpc3RfX2l0ZW0gLmVudGl0eS10aXRsZSB7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG4uZW50aXR5X2xpc3RfX2l0ZW0gLmVudGl0eS1zdWIge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOlxuICAgICM2MTYxNjE7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG4uZW50aXR5LWl0ZW0tcm91bmQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICB3aWR0aDogNjVweDtcbiAgaGVpZ2h0OiA2NXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOlxuICAgICNmNWY1ZjU7XG4gIGJvcmRlci1yYWRpdXM6IDUwJSAhaW1wb3J0YW50O1xuICBib3JkZXI6IDFweCBzb2xpZFxuICAjZWFlYWVhO1xufVxuLmVudGl0eS1pdGVtLXJvdW5kIGkge1xuICBsaW5lLWhlaWdodDogNjVweDtcbiAgZm9udC1zaXplOiA0MHB4O1xuICBjb2xvcjpcbiAgICAjQzRDNEM0O1xufVxuLmJvcmRlci1ib3R0b20ge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RlZTJlNiAhaW1wb3J0YW50O1xufVxuLyoqKioqKipCVVRUT05TKioqKioqKiovXG4uZW50aXR5LWRldGFpbHNfX2J1dHRvbnMge1xuICBwYWRkaW5nLWJvdHRvbTogMS41cmVtICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiAxLjVyZW0gIWltcG9ydGFudDtcbiAgcGFkZGluZy1sZWZ0OiAwICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5lbnRpdHktYnV0dG9ucy13cmFwcGVyIC5idG4ge1xuICBoZWlnaHQ6IDQwcHg7XG4gIC8qcGFkZGluZzogMTJweCA5cHg7Ki9cbiAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xuICBmb250LXdlaWdodDogNTAwICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBjb2xvcjpcbiAgICByZ2JhKDAsIDAsIDAsIDAuODcpO1xufVxuLmJ0bi1jcm0ge1xuICBib3JkZXItcmFkaXVzOiAwIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBtaW4td2lkdGg6IDEyMHB4O1xufVxuLmJ0bi1jcm0tcHJpbWFyeXtcbiAgZm9udC1zaXplOiAxMXB4IWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDcwMCFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDAhaW1wb3J0YW50O1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDE3NDdmICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMzBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAxOTE5RCFpbXBvcnRhbnQ7XG4gIGNvbG9yOiB3aGl0ZSFpbXBvcnRhbnQ7XG59XG4uYnRuLWNybS1kZWZhdWx0e1xuICBmb250LXNpemU6IDExcHghaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogNzAwIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogMCFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNhZWFlYWUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAzMHB4O1xufVxuLmJ0bi1jcm0tZGVmYXVsdDpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICNmNWY1ZjU7XG4gIG91dGxpbmU6IDA7XG4gIGJveC1zaGFkb3c6IDAgMCAwIDAuMTVyZW0gcmdiYSgyNTUsIDE3NywgNiwgMC4yNSk7XG59XG4uYnRuLWNybS1kZWZhdWx0X3JlZCB7XG4gIGJvcmRlci1jb2xvcjogcmVkICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiByZWQgIWltcG9ydGFudDtcbn1cbi8qKioqKioqKk1haW4gcGFuZWwqKioqKioqL1xuXG4uZW50aXR5LW5hbWUge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGNvbG9yOiAjMTUxMzE5O1xuICBwYWRkaW5nLXRvcDogMjRweDtcbn1cbi5lbnRpdHktc3ViIHtcbiAgLypwYWRkaW5nLXRvcDogMTJweDsqL1xuICBmb250LXNpemU6IDE4cHg7XG4gIGNvbG9yOiAjMTUxMzE5O1xufVxuLmVudGl0eS1kZXRhaWxzIC5pdGVtLXRpdGxlIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBsaW5lLWhlaWdodDogMzBweDtcbiAgY29sb3I6ICM2QTZBNkE7XG59XG5cbi5lbnRpdHktZGV0YWlscyAuaXRlbS12YWx1ZXtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBsaW5lLWhlaWdodDogMzBweDtcbiAgY29sb3I6ICMxNTEzMTk7XG59XG4uc3RhZ2VzLm5hdi1pdGVtIGEge1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBmb250LXNpemU6IDIwcHg7XG4gIGNvbG9yOiAjNkE2QTZBO1xufVxuLnN0YWdlcy5uYXYtaXRlbSBhLmFjdGl2ZSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDIwcHg7XG4gIGNvbG9yOiAjMjY2OTZFO1xufVxuLnN0YWdlcyAucGwtNCB7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgY29sb3I6ICM0QjRDNTFcbn1cbi5zdGFnZXMgYiB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XG4gIGNvbG9yOiAjNEI0QzUxO1xufVxuXG4uc3RhZ2VzLnRhYi1wYW5le1xuICBiYWNrZ3JvdW5kOiAjZWRmN2Y4O1xufVxuLmVudGl0eS1zdGFnZXMtd3JhcCAudGFiLXBhbmUge1xuICBtaW4taGVpZ2h0OiAyNTBweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZWUyZTY7XG59XG4uZW50aXR5LXN0YWdlcy13cmFwIC50YWItY29udGVudCB7XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2RlZTJlNjtcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2RlZTJlNjtcblxufVxuLm5hdi10YWJzIC5uYXYtbGluay5hY3RpdmUge1xuICBib3JkZXItYm90dG9tLWNvbG9yOiAjZWRmN2Y4O1xufVxuLmVudGl0eS1kZXRhaWxzIC5za2lsbCB7XG4gIGRpc3BsYXk6IGlubGluZTtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICBiYWNrZ3JvdW5kOiByZ2JhKDIyNSwgMjI1LCAyMjUsIDAuNik7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNDNEM0QzQ7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgY29sb3I6ICMxNTEzMTk7XG59XG5zcGFuLnNraWxsIHtcbiAgbWFyZ2luOiAxcHggMnB4O1xuICBwYWRkaW5nOiAxcHggNXB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjYWVhZWFlO1xuICBmb250LXNpemU6IDEycHg7XG4gIGJhY2tncm91bmQ6ICNmNWY1ZjU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cbi5zaWRlLXBob3RvIHtcbiAgd2lkdGg6IDE1MHB4O1xuICBoZWlnaHQ6IDE1MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIG1hcmdpbi1yaWdodDogNTBweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cbi5lbnRpdHktaXRlbS1yb3VuZC1iaWctcGxhY2Vob2xkZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIHdpZHRoOiA2NXB4O1xuICBoZWlnaHQ6IDY1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6XG4gICAgI2Y1ZjVmNTtcbiAgYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogMXB4IHNvbGlkXG4gICNlYWVhZWE7XG4gIHotaW5kZXg6IDk5O1xufVxuLmVudGl0eS1pdGVtLXJvdW5kLWJpZy1wbGFjZWhvbGRlciBpIHtcbiAgbGluZS1oZWlnaHQ6IDY1cHg7XG4gIGZvbnQtc2l6ZTogNDBweDtcbiAgY29sb3I6XG4gICAgI0M0QzRDNDtcbn1cbi5lbXB0eS1pbWctYmlnLXBsYWNlaG9sZGVyIHtcbiAgbWFyZ2luLXJpZ2h0OiA1MHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgd2lkdGg6IDE1MHB4O1xuICBoZWlnaHQ6IDE1MHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOlxuICAgICNmNWY1ZjU7XG4gIGJvcmRlci1yYWRpdXM6IDUwJSAhaW1wb3J0YW50O1xufVxuLnRocmVlLWRvdHMge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTBweDtcbiAgcmlnaHQ6IDA7XG59XG4uZHJvcGRvd24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uYnRuLWRlZmF1bHQge1xuICBib3JkZXItcmFkaXVzOiA1MCUgIWltcG9ydGFudDtcbiAgYm9yZGVyOiAwcHggIWltcG9ydGFudDtcbn1cbi5kb3Qge1xuICBoZWlnaHQ6IDVweDtcbiAgd2lkdGg6IDVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjpcbiAgICAjYmJiO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIG1hcmdpbjogMXB4O1xufVxuLnByb2ZpbGVfX2Ryb3Bkb3duLW1lbnUgaSB7XG4gIHdpZHRoOiAyMnB4O1xuICBoZWlnaHQ6IDIycHg7XG4gIG1hcmdpbi1yaWdodDogMTZweDtcbn1cbi5wcm9maWxlX19kcm9wZG93bi1tZW51IGk6OmJlZm9yZSB7XG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICBmb250LXNpemU6IDEycHg7XG59XG4uaW5wdXQtcGhvdG8ge1xuICB3aWR0aDogMC4xcHg7XG4gIGhlaWdodDogMC4xcHg7XG4gIG9wYWNpdHk6IDA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogLTE7XG59XG4uaW5wdXQtcGhvdG8gKyBsYWJlbCB7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwYWRkaW5nLWxlZnQ6MS40NXJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi8qKioqKioqTU9EQUxTIC0gREVMRVRFIENBTkRJREFURSAqKioqKiovXG4ubW9kYWwtYm9keSAuYnRuIHtcbiAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XG59XG4ubW9kYWwtaGVhZGVye1xuICBiYWNrZ3JvdW5kOiAjMDE5MTlEO1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDAuNXJlbSAxcmVtIWltcG9ydGFudDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlciFpbXBvcnRhbnQ7XG59XG4ubW9kYWwtaGVhZGVyIGg1IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW46IDBweDtcbn1cbiJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/components/all-candidates/all-candidates.component.ts": 
        /*!***********************************************************************!*\
          !*** ./src/app/components/all-candidates/all-candidates.component.ts ***!
          \***********************************************************************/
        /*! exports provided: AllCandidatesComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllCandidatesComponent", function () { return AllCandidatesComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _services_candidate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/candidate.service */ "./src/app/services/candidate.service.ts");
            /* harmony import */ var _services_cities_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/cities.service */ "./src/app/services/cities.service.ts");
            /* harmony import */ var _services_contacts_candidate_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/contacts-candidate.service */ "./src/app/services/contacts-candidate.service.ts");
            /* harmony import */ var _services_skills_candidate_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/skills-candidate.service */ "./src/app/services/skills-candidate.service.ts");
            /* harmony import */ var _services_skills_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/skills.service */ "./src/app/services/skills.service.ts");
            /* harmony import */ var src_app_services_image_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/image.service */ "./src/app/services/image.service.ts");
            /* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
            var AllCandidatesComponent = /** @class */ (function () {
                function AllCandidatesComponent(candidateService, citiesService, contactsCandidateService, skillsCandidateService, skillsService, imageService, modalService) {
                    this.candidateService = candidateService;
                    this.citiesService = citiesService;
                    this.contactsCandidateService = contactsCandidateService;
                    this.skillsCandidateService = skillsCandidateService;
                    this.skillsService = skillsService;
                    this.imageService = imageService;
                    this.modalService = modalService;
                    this.candidateContacts = [];
                    this.candidateSkills = [];
                    /*****Candidate Statuses*****/
                    this.STATUS_ALL = 0;
                    this.STATUS_NEW = 1;
                    this.STATUS_CONSIDERED = 2;
                    this.STATUS_TODAY = 3;
                    this.STATUS_OFFER = 4;
                    this.STATUS_SELECTED = 5;
                    this.STATUS_BLACKLISTED = 6;
                    this.candidateUrl = 'web/candidates?per-page=100000&page-count=44';
                }
                AllCandidatesComponent.prototype.ngOnInit = function () {
                    // this.getCandidates();
                };
                AllCandidatesComponent.prototype.selectCandidatesStatuses = function (event, tabStatus) {
                    event.preventDefault();
                    this.tabStatus = tabStatus;
                };
                AllCandidatesComponent.prototype.getStatuses = function () {
                    return this.tabStatus;
                };
                AllCandidatesComponent.prototype.searchCandidate = function (event) {
                    this.searchString = event;
                };
                return AllCandidatesComponent;
            }());
            AllCandidatesComponent.ctorParameters = function () { return [
                { type: _services_candidate_service__WEBPACK_IMPORTED_MODULE_2__["CandidateService"] },
                { type: _services_cities_service__WEBPACK_IMPORTED_MODULE_3__["CitiesService"] },
                { type: _services_contacts_candidate_service__WEBPACK_IMPORTED_MODULE_4__["ContactsCandidateService"] },
                { type: _services_skills_candidate_service__WEBPACK_IMPORTED_MODULE_5__["SkillsCandidateService"] },
                { type: _services_skills_service__WEBPACK_IMPORTED_MODULE_6__["SkillsService"] },
                { type: src_app_services_image_service__WEBPACK_IMPORTED_MODULE_7__["ImageService"] },
                { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__["BsModalService"] }
            ]; };
            AllCandidatesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-all-candidates',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./all-candidates.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/all-candidates/all-candidates.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./all-candidates.component.css */ "./src/app/components/all-candidates/all-candidates.component.css")).default]
                })
            ], AllCandidatesComponent);
            /***/ 
        }),
        /***/ "./src/app/components/all-vacancies/all-vacancies.component.css": 
        /*!**********************************************************************!*\
          !*** ./src/app/components/all-vacancies/all-vacancies.component.css ***!
          \**********************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWxsLXZhY2FuY2llcy9hbGwtdmFjYW5jaWVzLmNvbXBvbmVudC5jc3MifQ== */");
            /***/ 
        }),
        /***/ "./src/app/components/all-vacancies/all-vacancies.component.ts": 
        /*!*********************************************************************!*\
          !*** ./src/app/components/all-vacancies/all-vacancies.component.ts ***!
          \*********************************************************************/
        /*! exports provided: AllVacanciesComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllVacanciesComponent", function () { return AllVacanciesComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var AllVacanciesComponent = /** @class */ (function () {
                function AllVacanciesComponent() {
                }
                AllVacanciesComponent.prototype.ngOnInit = function () {
                };
                return AllVacanciesComponent;
            }());
            AllVacanciesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-all-vacancies',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./all-vacancies.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/all-vacancies/all-vacancies.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./all-vacancies.component.css */ "./src/app/components/all-vacancies/all-vacancies.component.css")).default]
                })
            ], AllVacanciesComponent);
            /***/ 
        }),
        /***/ "./src/app/components/candidate-info/candidate-info.component.css": 
        /*!************************************************************************!*\
          !*** ./src/app/components/candidate-info/candidate-info.component.css ***!
          \************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("/********Main panel*******/\n\n.entity-name {\n    font-size: 20px;\n    color: #151319;\n    padding-top: 24px;\n  }\n\n.entity-sub {\n    font-size: 18px;\n    color: #151319;\n  }\n\n.entity-details .item-title {\n    font-size: 16px;\n    line-height: 30px;\n    color: #6A6A6A;\n  }\n\n.entity-details .item-value{\n    font-weight: normal;\n    font-size: 16px;\n    line-height: 30px;\n    color: #151319;\n  }\n\n.stages.nav-item a {\n    font-weight: normal;\n    font-size: 20px;\n    color: #6A6A6A;\n  }\n\n.stages.nav-item a.active {\n    font-weight: bold;\n    font-size: 20px;\n    color: #26696E;\n  }\n\n.stages .pl-4 {\n    font-weight: normal;\n    font-size: 16px;\n    color: #4B4C51\n  }\n\n.stages b {\n    font-weight: bold;\n    font-size: 18px;\n    line-height: normal;\n    color: #4B4C51;\n  }\n\n.stages.tab-pane{\n    background: #edf7f8;\n  }\n\n.entity-stages-wrap .tab-pane {\n    min-height: 250px;\n    border-bottom: 1px solid #dee2e6;\n  }\n\n.entity-stages-wrap .tab-content {\n    border-left: 1px solid #dee2e6;\n    border-right: 1px solid #dee2e6;\n  \n  }\n\n.nav-tabs .nav-link.active {\n    border-bottom-color: #edf7f8;\n  }\n\n.entity-details .skill {\n    display: inline;\n    margin-right: 10px;\n    background: rgba(225, 225, 225, 0.6);\n    border: 1px solid #C4C4C4;\n    font-size: 10px;\n    text-transform: uppercase;\n    color: #151319;\n  }\n\nspan.skill {\n    margin: 1px 2px;\n    padding: 1px 5px;\n    border: 1px solid #aeaeae;\n    font-size: 12px;\n    background: #f5f5f5;\n    display: inline-block;\n    font-weight: 500;\n  }\n\n.side-photo {\n    width: 150px;\n    height: 150px;\n    border-radius: 50%;\n    margin-right: 50px;\n    margin-top: 10px;\n  }\n\n.entity-item-round-big-placeholder {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    text-align: center;\n    margin-top: 5px;\n    width: 65px;\n    height: 65px;\n    background-color:\n      #f5f5f5;\n    border-radius: 50% !important;\n    border: 1px solid\n    #eaeaea;\n    z-index: 99;\n  }\n\n.entity-item-round-big-placeholder i {\n    line-height: 65px;\n    font-size: 40px;\n    color:\n      #C4C4C4;\n  }\n\n.empty-img-big-placeholder {\n    margin-right: 50px;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    margin-top: 10px;\n    width: 150px;\n    height: 150px;\n    background-color:\n      #f5f5f5;\n    border-radius: 50% !important;\n  }\n\n.three-dots {\n    position: absolute;\n    top: 10px;\n    right: 0;\n  }\n\n.dropdown {\n    position: relative;\n  }\n\n.btn-default {\n    border-radius: 50% !important;\n    border: 0px !important;\n  }\n\n.dot {\n    height: 5px;\n    width: 5px;\n    background-color:\n      #bbb;\n    border-radius: 50%;\n    display: flex;\n    margin: 1px;\n  }\n\n.profile__dropdown-menu i {\n    width: 22px;\n    height: 22px;\n    margin-right: 16px;\n  }\n\n.profile__dropdown-menu i::before {\n    line-height: 22px;\n    font-size: 12px;\n  }\n\n.input-photo {\n    width: 0.1px;\n  }\n\n/********Main panel*******/\n\n.entity-name {\n    font-size: 20px;\n    color: #151319;\n    padding-top: 24px;\n  }\n\n.entity-sub {\n    font-size: 18px;\n    color: #151319;\n  }\n\n.entity-details .item-title {\n    font-size: 16px;\n    line-height: 30px;\n    color: #6A6A6A;\n  }\n\n.entity-details .item-value{\n    font-weight: normal;\n    font-size: 16px;\n    line-height: 30px;\n    color: #151319;\n  }\n\n.stages.nav-item a {\n    font-weight: normal;\n    font-size: 20px;\n    color: #6A6A6A;\n  }\n\n.stages.nav-item a.active {\n    font-weight: bold;\n    font-size: 20px;\n    color: #26696E;\n  }\n\n.stages .pl-4 {\n    font-weight: normal;\n    font-size: 16px;\n    color: #4B4C51\n  }\n\n.stages b {\n    font-weight: bold;\n    font-size: 18px;\n    line-height: normal;\n    color: #4B4C51;\n  }\n\n.stages.tab-pane{\n    background: #edf7f8;\n  }\n\n.entity-stages-wrap .tab-pane {\n    min-height: 250px;\n    border-bottom: 1px solid #dee2e6;\n  }\n\n.entity-stages-wrap .tab-content {\n    border-left: 1px solid #dee2e6;\n    border-right: 1px solid #dee2e6;\n  \n  }\n\n.nav-tabs .nav-link.active {\n    border-bottom-color: #edf7f8;\n  }\n\n.entity-details .skill {\n    display: inline;\n    margin-right: 10px;\n    background: rgba(225, 225, 225, 0.6);\n    border: 1px solid #C4C4C4;\n    font-size: 10px;\n    text-transform: uppercase;\n    color: #151319;\n  }\n\nspan.skill {\n    margin: 1px 2px;\n    padding: 1px 5px;\n    border: 1px solid #aeaeae;\n    font-size: 12px;\n    background: #f5f5f5;\n    display: inline-block;\n    font-weight: 500;\n  }\n\n.side-photo {\n    width: 150px;\n    height: 150px;\n    border-radius: 50%;\n    margin-right: 50px;\n    margin-top: 10px;\n  }\n\n.entity-item-round-big-placeholder {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    text-align: center;\n    margin-top: 5px;\n    width: 65px;\n    height: 65px;\n    background-color:\n      #f5f5f5;\n    border-radius: 50% !important;\n    border: 1px solid\n    #eaeaea;\n    z-index: 99;\n  }\n\n.entity-item-round-big-placeholder i {\n    line-height: 65px;\n    font-size: 40px;\n    color:\n      #C4C4C4;\n  }\n\n.empty-img-big-placeholder {\n    margin-right: 50px;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    margin-top: 10px;\n    width: 150px;\n    height: 150px;\n    background-color:\n      #f5f5f5;\n    border-radius: 50% !important;\n  }\n\n.three-dots {\n    position: absolute;\n    top: 10px;\n    right: 0;\n  }\n\n.dropdown {\n    position: relative;\n  }\n\n.btn-default {\n    border-radius: 50% !important;\n    border: 0px !important;\n  }\n\n.dot {\n    height: 5px;\n    width: 5px;\n    background-color:\n      #bbb;\n    border-radius: 50%;\n    display: flex;\n    margin: 1px;\n  }\n\n.profile__dropdown-menu i {\n    width: 22px;\n    height: 22px;\n    margin-right: 16px;\n  }\n\n.profile__dropdown-menu i::before {\n    line-height: 22px;\n    font-size: 12px;\n  }\n\n.input-photo {\n    width: 0.1px;\n    height: 0.1px;\n    opacity: 0;\n    overflow: hidden;\n    position: absolute;\n    z-index: -1;\n  }\n\n.input-photo + label {\n    max-width: 100%;\n    text-overflow: ellipsis;\n    white-space: nowrap;\n    cursor: pointer;\n    display: inline-block;\n    padding-left:1.45rem;\n    margin-bottom: 0;\n  }\n\n/*******MODALS - DELETE CANDIDATE ******/\n\n.modal-body .btn {\n    font-size: 14px !important;\n  }\n\n.modal-header{\n    background: #01919D;\n    color: white;\n    padding: 0.5rem 1rem!important;\n    align-items: center!important;\n  }\n\n.modal-header h5 {\n    font-size: 14px;\n    margin: 0px;\n  }\n\n/*******BUTTONS********/\n\n.entity-details__buttons {\n  padding-bottom: 1.5rem !important;\n  padding-top: 1.5rem !important;\n  padding-left: 0 !important;\n  text-align: center;\n}\n\n.entity-buttons-wrapper .btn {\n  height: 40px;\n\n  margin-right: 12px;\n  font-weight: 500 !important;\n  font-size: 14px !important;\n  text-transform: uppercase;\n  color:\n    rgba(0, 0, 0, 0.87);\n}\n\n.btn-crm {\n  border-radius: 0!important;\n  height: 30px;\n  min-width: 120px;\n}\n\n.btn-crm-primary{\n  font-size: 11px!important;\n  font-weight: 700!important;\n  border-radius: 0!important;\n  border: 1px solid #01747f !important;\n  height: 30px;\n  background-color: #01919D!important;\n  color: white!important;\n}\n\n.btn-crm-default{\n  font-size: 11px!important;\n  font-weight: 700!important;\n  border-radius: 0!important;\n  border: 1px solid #aeaeae !important;\n  height: 30px;\n}\n\n.btn-crm-default:hover {\n  background: #f5f5f5;\n  outline: 0;\n  box-shadow: 0 0 0 0.15rem rgba(255, 177, 6, 0.25);\n}\n\n.btn-crm-default_red {\n  border-color: red !important;\n  color: red !important;\n}\n\n/****PDF PREVIEW POSITIONING****/\n\n.buttons-left, .buttons-right {\n  display: inline-block;\n  width: 100%;\n  vertical-align: bottom;\n}\n\n.buttons-left a {\n  margin-right: 10px;\n}\n\n.buttons-right {\n  text-align: right;\n  padding-top: 13px;  \n}\n\n.document__dropdown-menu i {\n  width: 22px;\n  height: 22px;\n  margin-right: 0;\n}\n\n.document__dropdown-menu i::before {\n  line-height: 22px;\n  font-size: 12px;\n}\n\n.document__dropdown-item {\n  padding: 0;\n  margin-right: 10px;\n}\n\n#w2-tab1 .btn-file {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 1rem;\n  padding: 0;\n  min-width: 82px;\n  position: relative;\n  overflow: hidden;\n}\n\n#w2-tab1 input[type=\"file\"] {\n  position: absolute;\n  opacity: 0;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jYW5kaWRhdGUtaW5mby9jYW5kaWRhdGUtaW5mby5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDBCQUEwQjs7QUFFMUI7SUFDSSxlQUFlO0lBQ2YsY0FBYztJQUNkLGlCQUFpQjtFQUNuQjs7QUFDQTtJQUNFLGVBQWU7SUFDZixjQUFjO0VBQ2hCOztBQUNBO0lBQ0UsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0VBQ2hCOztBQUVBO0lBQ0UsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztFQUNoQjs7QUFDQTtJQUNFLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsY0FBYztFQUNoQjs7QUFDQTtJQUNFLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsY0FBYztFQUNoQjs7QUFDQTtJQUNFLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Y7RUFDRjs7QUFDQTtJQUNFLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLGNBQWM7RUFDaEI7O0FBRUE7SUFDRSxtQkFBbUI7RUFDckI7O0FBQ0E7SUFDRSxpQkFBaUI7SUFDakIsZ0NBQWdDO0VBQ2xDOztBQUNBO0lBQ0UsOEJBQThCO0lBQzlCLCtCQUErQjs7RUFFakM7O0FBQ0E7SUFDRSw0QkFBNEI7RUFDOUI7O0FBQ0E7SUFDRSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG9DQUFvQztJQUNwQyx5QkFBeUI7SUFDekIsZUFBZTtJQUNmLHlCQUF5QjtJQUN6QixjQUFjO0VBQ2hCOztBQUNBO0lBQ0UsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQix5QkFBeUI7SUFDekIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixxQkFBcUI7SUFDckIsZ0JBQWdCO0VBQ2xCOztBQUNBO0lBQ0UsWUFBWTtJQUNaLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtFQUNsQjs7QUFDQTtJQUNFLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWjthQUNTO0lBQ1QsNkJBQTZCO0lBQzdCO1dBQ087SUFDUCxXQUFXO0VBQ2I7O0FBQ0E7SUFDRSxpQkFBaUI7SUFDakIsZUFBZTtJQUNmO2FBQ1M7RUFDWDs7QUFDQTtJQUNFLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsWUFBWTtJQUNaLGFBQWE7SUFDYjthQUNTO0lBQ1QsNkJBQTZCO0VBQy9COztBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxRQUFRO0VBQ1Y7O0FBQ0E7SUFDRSxrQkFBa0I7RUFDcEI7O0FBQ0E7SUFDRSw2QkFBNkI7SUFDN0Isc0JBQXNCO0VBQ3hCOztBQUNBO0lBQ0UsV0FBVztJQUNYLFVBQVU7SUFDVjtVQUNNO0lBQ04sa0JBQWtCO0lBQ2xCLGFBQWE7SUFDYixXQUFXO0VBQ2I7O0FBQ0E7SUFDRSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGtCQUFrQjtFQUNwQjs7QUFDQTtJQUNFLGlCQUFpQjtJQUNqQixlQUFlO0VBQ2pCOztBQUNBO0lBQ0UsWUFBWTtFQUNkOztBQUNBLDBCQUEwQjs7QUFFMUI7SUFDRSxlQUFlO0lBQ2YsY0FBYztJQUNkLGlCQUFpQjtFQUNuQjs7QUFDQTtJQUNFLGVBQWU7SUFDZixjQUFjO0VBQ2hCOztBQUNBO0lBQ0UsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0VBQ2hCOztBQUVBO0lBQ0UsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsY0FBYztFQUNoQjs7QUFDQTtJQUNFLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsY0FBYztFQUNoQjs7QUFDQTtJQUNFLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsY0FBYztFQUNoQjs7QUFDQTtJQUNFLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Y7RUFDRjs7QUFDQTtJQUNFLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLGNBQWM7RUFDaEI7O0FBRUE7SUFDRSxtQkFBbUI7RUFDckI7O0FBQ0E7SUFDRSxpQkFBaUI7SUFDakIsZ0NBQWdDO0VBQ2xDOztBQUNBO0lBQ0UsOEJBQThCO0lBQzlCLCtCQUErQjs7RUFFakM7O0FBQ0E7SUFDRSw0QkFBNEI7RUFDOUI7O0FBQ0E7SUFDRSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG9DQUFvQztJQUNwQyx5QkFBeUI7SUFDekIsZUFBZTtJQUNmLHlCQUF5QjtJQUN6QixjQUFjO0VBQ2hCOztBQUNBO0lBQ0UsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQix5QkFBeUI7SUFDekIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixxQkFBcUI7SUFDckIsZ0JBQWdCO0VBQ2xCOztBQUNBO0lBQ0UsWUFBWTtJQUNaLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtFQUNsQjs7QUFDQTtJQUNFLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsV0FBVztJQUNYLFlBQVk7SUFDWjthQUNTO0lBQ1QsNkJBQTZCO0lBQzdCO1dBQ087SUFDUCxXQUFXO0VBQ2I7O0FBQ0E7SUFDRSxpQkFBaUI7SUFDakIsZUFBZTtJQUNmO2FBQ1M7RUFDWDs7QUFDQTtJQUNFLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsWUFBWTtJQUNaLGFBQWE7SUFDYjthQUNTO0lBQ1QsNkJBQTZCO0VBQy9COztBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxRQUFRO0VBQ1Y7O0FBQ0E7SUFDRSxrQkFBa0I7RUFDcEI7O0FBQ0E7SUFDRSw2QkFBNkI7SUFDN0Isc0JBQXNCO0VBQ3hCOztBQUNBO0lBQ0UsV0FBVztJQUNYLFVBQVU7SUFDVjtVQUNNO0lBQ04sa0JBQWtCO0lBQ2xCLGFBQWE7SUFDYixXQUFXO0VBQ2I7O0FBQ0E7SUFDRSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGtCQUFrQjtFQUNwQjs7QUFDQTtJQUNFLGlCQUFpQjtJQUNqQixlQUFlO0VBQ2pCOztBQUNBO0lBQ0UsWUFBWTtJQUNaLGFBQWE7SUFDYixVQUFVO0lBQ1YsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixXQUFXO0VBQ2I7O0FBQ0E7SUFDRSxlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLG9CQUFvQjtJQUNwQixnQkFBZ0I7RUFDbEI7O0FBQ0Esd0NBQXdDOztBQUV4QztJQUNFLDBCQUEwQjtFQUM1Qjs7QUFDQTtJQUNFLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osOEJBQThCO0lBQzlCLDZCQUE2QjtFQUMvQjs7QUFDQTtJQUNFLGVBQWU7SUFDZixXQUFXO0VBQ2I7O0FBQ0EsdUJBQXVCOztBQUN6QjtFQUNFLGlDQUFpQztFQUNqQyw4QkFBOEI7RUFDOUIsMEJBQTBCO0VBQzFCLGtCQUFrQjtBQUNwQjs7QUFDQTtFQUNFLFlBQVk7O0VBRVosa0JBQWtCO0VBQ2xCLDJCQUEyQjtFQUMzQiwwQkFBMEI7RUFDMUIseUJBQXlCO0VBQ3pCO3VCQUNxQjtBQUN2Qjs7QUFDQTtFQUNFLDBCQUEwQjtFQUMxQixZQUFZO0VBQ1osZ0JBQWdCO0FBQ2xCOztBQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLDBCQUEwQjtFQUMxQiwwQkFBMEI7RUFDMUIsb0NBQW9DO0VBQ3BDLFlBQVk7RUFDWixtQ0FBbUM7RUFDbkMsc0JBQXNCO0FBQ3hCOztBQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLDBCQUEwQjtFQUMxQiwwQkFBMEI7RUFDMUIsb0NBQW9DO0VBQ3BDLFlBQVk7QUFDZDs7QUFDQTtFQUNFLG1CQUFtQjtFQUNuQixVQUFVO0VBQ1YsaURBQWlEO0FBQ25EOztBQUNBO0VBQ0UsNEJBQTRCO0VBQzVCLHFCQUFxQjtBQUN2Qjs7QUFDQSxnQ0FBZ0M7O0FBQ2hDO0VBQ0UscUJBQXFCO0VBQ3JCLFdBQVc7RUFDWCxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxrQkFBa0I7QUFDcEI7O0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsaUJBQWlCO0FBQ25COztBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixlQUFlO0FBQ2pCOztBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7QUFDakI7O0FBQ0E7RUFDRSxVQUFVO0VBQ1Ysa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0UsaURBQWlEO0VBQ2pELGVBQWU7RUFDZixVQUFVO0VBQ1YsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixnQkFBZ0I7QUFDbEI7O0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLE1BQU07RUFDTixRQUFRO0VBQ1IsU0FBUztFQUNULE9BQU87RUFDUCxXQUFXO0VBQ1gsWUFBWTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jYW5kaWRhdGUtaW5mby9jYW5kaWRhdGUtaW5mby5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqTWFpbiBwYW5lbCoqKioqKiovXG5cbi5lbnRpdHktbmFtZSB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGNvbG9yOiAjMTUxMzE5O1xuICAgIHBhZGRpbmctdG9wOiAyNHB4O1xuICB9XG4gIC5lbnRpdHktc3ViIHtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgY29sb3I6ICMxNTEzMTk7XG4gIH1cbiAgLmVudGl0eS1kZXRhaWxzIC5pdGVtLXRpdGxlIHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgY29sb3I6ICM2QTZBNkE7XG4gIH1cbiAgXG4gIC5lbnRpdHktZGV0YWlscyAuaXRlbS12YWx1ZXtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBsaW5lLWhlaWdodDogMzBweDtcbiAgICBjb2xvcjogIzE1MTMxOTtcbiAgfVxuICAuc3RhZ2VzLm5hdi1pdGVtIGEge1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGNvbG9yOiAjNkE2QTZBO1xuICB9XG4gIC5zdGFnZXMubmF2LWl0ZW0gYS5hY3RpdmUge1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBjb2xvcjogIzI2Njk2RTtcbiAgfVxuICAuc3RhZ2VzIC5wbC00IHtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBjb2xvcjogIzRCNEM1MVxuICB9XG4gIC5zdGFnZXMgYiB7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XG4gICAgY29sb3I6ICM0QjRDNTE7XG4gIH1cbiAgXG4gIC5zdGFnZXMudGFiLXBhbmV7XG4gICAgYmFja2dyb3VuZDogI2VkZjdmODtcbiAgfVxuICAuZW50aXR5LXN0YWdlcy13cmFwIC50YWItcGFuZSB7XG4gICAgbWluLWhlaWdodDogMjUwcHg7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZWUyZTY7XG4gIH1cbiAgLmVudGl0eS1zdGFnZXMtd3JhcCAudGFiLWNvbnRlbnQge1xuICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2RlZTJlNjtcbiAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjZGVlMmU2O1xuICBcbiAgfVxuICAubmF2LXRhYnMgLm5hdi1saW5rLmFjdGl2ZSB7XG4gICAgYm9yZGVyLWJvdHRvbS1jb2xvcjogI2VkZjdmODtcbiAgfVxuICAuZW50aXR5LWRldGFpbHMgLnNraWxsIHtcbiAgICBkaXNwbGF5OiBpbmxpbmU7XG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMjI1LCAyMjUsIDIyNSwgMC42KTtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjQzRDNEM0O1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGNvbG9yOiAjMTUxMzE5O1xuICB9XG4gIHNwYW4uc2tpbGwge1xuICAgIG1hcmdpbjogMXB4IDJweDtcbiAgICBwYWRkaW5nOiAxcHggNXB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNhZWFlYWU7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGJhY2tncm91bmQ6ICNmNWY1ZjU7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIH1cbiAgLnNpZGUtcGhvdG8ge1xuICAgIHdpZHRoOiAxNTBweDtcbiAgICBoZWlnaHQ6IDE1MHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBtYXJnaW4tcmlnaHQ6IDUwcHg7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbiAgfVxuICAuZW50aXR5LWl0ZW0tcm91bmQtYmlnLXBsYWNlaG9sZGVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICB3aWR0aDogNjVweDtcbiAgICBoZWlnaHQ6IDY1cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjpcbiAgICAgICNmNWY1ZjU7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiAxcHggc29saWRcbiAgICAjZWFlYWVhO1xuICAgIHotaW5kZXg6IDk5O1xuICB9XG4gIC5lbnRpdHktaXRlbS1yb3VuZC1iaWctcGxhY2Vob2xkZXIgaSB7XG4gICAgbGluZS1oZWlnaHQ6IDY1cHg7XG4gICAgZm9udC1zaXplOiA0MHB4O1xuICAgIGNvbG9yOlxuICAgICAgI0M0QzRDNDtcbiAgfVxuICAuZW1wdHktaW1nLWJpZy1wbGFjZWhvbGRlciB7XG4gICAgbWFyZ2luLXJpZ2h0OiA1MHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIHdpZHRoOiAxNTBweDtcbiAgICBoZWlnaHQ6IDE1MHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6XG4gICAgICAjZjVmNWY1O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJSAhaW1wb3J0YW50O1xuICB9XG4gIC50aHJlZS1kb3RzIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAxMHB4O1xuICAgIHJpZ2h0OiAwO1xuICB9XG4gIC5kcm9wZG93biB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB9XG4gIC5idG4tZGVmYXVsdCB7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiAwcHggIWltcG9ydGFudDtcbiAgfVxuICAuZG90IHtcbiAgICBoZWlnaHQ6IDVweDtcbiAgICB3aWR0aDogNXB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6XG4gICAgICAjYmJiO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG1hcmdpbjogMXB4O1xuICB9XG4gIC5wcm9maWxlX19kcm9wZG93bi1tZW51IGkge1xuICAgIHdpZHRoOiAyMnB4O1xuICAgIGhlaWdodDogMjJweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDE2cHg7XG4gIH1cbiAgLnByb2ZpbGVfX2Ryb3Bkb3duLW1lbnUgaTo6YmVmb3JlIHtcbiAgICBsaW5lLWhlaWdodDogMjJweDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gIH1cbiAgLmlucHV0LXBob3RvIHtcbiAgICB3aWR0aDogMC4xcHg7XG4gIH1cbiAgLyoqKioqKioqTWFpbiBwYW5lbCoqKioqKiovXG4gIFxuICAuZW50aXR5LW5hbWUge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBjb2xvcjogIzE1MTMxOTtcbiAgICBwYWRkaW5nLXRvcDogMjRweDtcbiAgfVxuICAuZW50aXR5LXN1YiB7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGNvbG9yOiAjMTUxMzE5O1xuICB9XG4gIC5lbnRpdHktZGV0YWlscyAuaXRlbS10aXRsZSB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xuICAgIGNvbG9yOiAjNkE2QTZBO1xuICB9XG4gIFxuICAuZW50aXR5LWRldGFpbHMgLml0ZW0tdmFsdWV7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgY29sb3I6ICMxNTEzMTk7XG4gIH1cbiAgLnN0YWdlcy5uYXYtaXRlbSBhIHtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBjb2xvcjogIzZBNkE2QTtcbiAgfVxuICAuc3RhZ2VzLm5hdi1pdGVtIGEuYWN0aXZlIHtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgY29sb3I6ICMyNjY5NkU7XG4gIH1cbiAgLnN0YWdlcyAucGwtNCB7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgY29sb3I6ICM0QjRDNTFcbiAgfVxuICAuc3RhZ2VzIGIge1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICAgIGNvbG9yOiAjNEI0QzUxO1xuICB9XG4gIFxuICAuc3RhZ2VzLnRhYi1wYW5le1xuICAgIGJhY2tncm91bmQ6ICNlZGY3Zjg7XG4gIH1cbiAgLmVudGl0eS1zdGFnZXMtd3JhcCAudGFiLXBhbmUge1xuICAgIG1pbi1oZWlnaHQ6IDI1MHB4O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGVlMmU2O1xuICB9XG4gIC5lbnRpdHktc3RhZ2VzLXdyYXAgLnRhYi1jb250ZW50IHtcbiAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkICNkZWUyZTY7XG4gICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2RlZTJlNjtcbiAgXG4gIH1cbiAgLm5hdi10YWJzIC5uYXYtbGluay5hY3RpdmUge1xuICAgIGJvcmRlci1ib3R0b20tY29sb3I6ICNlZGY3Zjg7XG4gIH1cbiAgLmVudGl0eS1kZXRhaWxzIC5za2lsbCB7XG4gICAgZGlzcGxheTogaW5saW5lO1xuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDIyNSwgMjI1LCAyMjUsIDAuNik7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjogIzE1MTMxOTtcbiAgfVxuICBzcGFuLnNraWxsIHtcbiAgICBtYXJnaW46IDFweCAycHg7XG4gICAgcGFkZGluZzogMXB4IDVweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYWVhZWFlO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBiYWNrZ3JvdW5kOiAjZjVmNWY1O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBmb250LXdlaWdodDogNTAwO1xuICB9XG4gIC5zaWRlLXBob3RvIHtcbiAgICB3aWR0aDogMTUwcHg7XG4gICAgaGVpZ2h0OiAxNTBweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgbWFyZ2luLXJpZ2h0OiA1MHB4O1xuICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gIH1cbiAgLmVudGl0eS1pdGVtLXJvdW5kLWJpZy1wbGFjZWhvbGRlciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgd2lkdGg6IDY1cHg7XG4gICAgaGVpZ2h0OiA2NXB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6XG4gICAgICAjZjVmNWY1O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJSAhaW1wb3J0YW50O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkXG4gICAgI2VhZWFlYTtcbiAgICB6LWluZGV4OiA5OTtcbiAgfVxuICAuZW50aXR5LWl0ZW0tcm91bmQtYmlnLXBsYWNlaG9sZGVyIGkge1xuICAgIGxpbmUtaGVpZ2h0OiA2NXB4O1xuICAgIGZvbnQtc2l6ZTogNDBweDtcbiAgICBjb2xvcjpcbiAgICAgICNDNEM0QzQ7XG4gIH1cbiAgLmVtcHR5LWltZy1iaWctcGxhY2Vob2xkZXIge1xuICAgIG1hcmdpbi1yaWdodDogNTBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICB3aWR0aDogMTUwcHg7XG4gICAgaGVpZ2h0OiAxNTBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOlxuICAgICAgI2Y1ZjVmNTtcbiAgICBib3JkZXItcmFkaXVzOiA1MCUgIWltcG9ydGFudDtcbiAgfVxuICAudGhyZWUtZG90cyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMTBweDtcbiAgICByaWdodDogMDtcbiAgfVxuICAuZHJvcGRvd24ge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxuICAuYnRuLWRlZmF1bHQge1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJSAhaW1wb3J0YW50O1xuICAgIGJvcmRlcjogMHB4ICFpbXBvcnRhbnQ7XG4gIH1cbiAgLmRvdCB7XG4gICAgaGVpZ2h0OiA1cHg7XG4gICAgd2lkdGg6IDVweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOlxuICAgICAgI2JiYjtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW46IDFweDtcbiAgfVxuICAucHJvZmlsZV9fZHJvcGRvd24tbWVudSBpIHtcbiAgICB3aWR0aDogMjJweDtcbiAgICBoZWlnaHQ6IDIycHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAxNnB4O1xuICB9XG4gIC5wcm9maWxlX19kcm9wZG93bi1tZW51IGk6OmJlZm9yZSB7XG4gICAgbGluZS1oZWlnaHQ6IDIycHg7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICB9XG4gIC5pbnB1dC1waG90byB7XG4gICAgd2lkdGg6IDAuMXB4O1xuICAgIGhlaWdodDogMC4xcHg7XG4gICAgb3BhY2l0eTogMDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB6LWluZGV4OiAtMTtcbiAgfVxuICAuaW5wdXQtcGhvdG8gKyBsYWJlbCB7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBwYWRkaW5nLWxlZnQ6MS40NXJlbTtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICB9XG4gIC8qKioqKioqTU9EQUxTIC0gREVMRVRFIENBTkRJREFURSAqKioqKiovXG4gIFxuICAubW9kYWwtYm9keSAuYnRuIHtcbiAgICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbiAgfVxuICAubW9kYWwtaGVhZGVye1xuICAgIGJhY2tncm91bmQ6ICMwMTkxOUQ7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHBhZGRpbmc6IDAuNXJlbSAxcmVtIWltcG9ydGFudDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyIWltcG9ydGFudDtcbiAgfVxuICAubW9kYWwtaGVhZGVyIGg1IHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbWFyZ2luOiAwcHg7XG4gIH1cbiAgLyoqKioqKipCVVRUT05TKioqKioqKiovXG4uZW50aXR5LWRldGFpbHNfX2J1dHRvbnMge1xuICBwYWRkaW5nLWJvdHRvbTogMS41cmVtICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiAxLjVyZW0gIWltcG9ydGFudDtcbiAgcGFkZGluZy1sZWZ0OiAwICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5lbnRpdHktYnV0dG9ucy13cmFwcGVyIC5idG4ge1xuICBoZWlnaHQ6IDQwcHg7XG5cbiAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xuICBmb250LXdlaWdodDogNTAwICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBjb2xvcjpcbiAgICByZ2JhKDAsIDAsIDAsIDAuODcpO1xufVxuLmJ0bi1jcm0ge1xuICBib3JkZXItcmFkaXVzOiAwIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBtaW4td2lkdGg6IDEyMHB4O1xufVxuLmJ0bi1jcm0tcHJpbWFyeXtcbiAgZm9udC1zaXplOiAxMXB4IWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDcwMCFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDAhaW1wb3J0YW50O1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDE3NDdmICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMzBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAxOTE5RCFpbXBvcnRhbnQ7XG4gIGNvbG9yOiB3aGl0ZSFpbXBvcnRhbnQ7XG59XG4uYnRuLWNybS1kZWZhdWx0e1xuICBmb250LXNpemU6IDExcHghaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogNzAwIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogMCFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNhZWFlYWUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAzMHB4O1xufVxuLmJ0bi1jcm0tZGVmYXVsdDpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICNmNWY1ZjU7XG4gIG91dGxpbmU6IDA7XG4gIGJveC1zaGFkb3c6IDAgMCAwIDAuMTVyZW0gcmdiYSgyNTUsIDE3NywgNiwgMC4yNSk7XG59XG4uYnRuLWNybS1kZWZhdWx0X3JlZCB7XG4gIGJvcmRlci1jb2xvcjogcmVkICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiByZWQgIWltcG9ydGFudDtcbn1cbi8qKioqUERGIFBSRVZJRVcgUE9TSVRJT05JTkcqKioqL1xuLmJ1dHRvbnMtbGVmdCwgLmJ1dHRvbnMtcmlnaHQge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICB2ZXJ0aWNhbC1hbGlnbjogYm90dG9tO1xufVxuLmJ1dHRvbnMtbGVmdCBhIHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuLmJ1dHRvbnMtcmlnaHQge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgcGFkZGluZy10b3A6IDEzcHg7ICBcbn1cbi5kb2N1bWVudF9fZHJvcGRvd24tbWVudSBpIHtcbiAgd2lkdGg6IDIycHg7XG4gIGhlaWdodDogMjJweDtcbiAgbWFyZ2luLXJpZ2h0OiAwO1xufVxuLmRvY3VtZW50X19kcm9wZG93bi1tZW51IGk6OmJlZm9yZSB7XG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICBmb250LXNpemU6IDEycHg7XG59XG4uZG9jdW1lbnRfX2Ryb3Bkb3duLWl0ZW0ge1xuICBwYWRkaW5nOiAwO1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbiN3Mi10YWIxIC5idG4tZmlsZSB7XG4gIGZvbnQtZmFtaWx5OiBSb2JvdG8sIFwiSGVsdmV0aWNhIE5ldWVcIiwgc2Fucy1zZXJpZjtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBwYWRkaW5nOiAwO1xuICBtaW4td2lkdGg6IDgycHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbiN3Mi10YWIxIGlucHV0W3R5cGU9XCJmaWxlXCJdIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBvcGFjaXR5OiAwO1xuICB0b3A6IDA7XG4gIHJpZ2h0OiAwO1xuICBib3R0b206IDA7XG4gIGxlZnQ6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/components/candidate-info/candidate-info.component.ts": 
        /*!***********************************************************************!*\
          !*** ./src/app/components/candidate-info/candidate-info.component.ts ***!
          \***********************************************************************/
        /*! exports provided: CandidateInfoComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CandidateInfoComponent", function () { return CandidateInfoComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_services_shared_event_bus_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/shared/event-bus.service */ "./src/app/services/shared/event-bus.service.ts");
            /* harmony import */ var src_app_services_candidate_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/candidate.service */ "./src/app/services/candidate.service.ts");
            /* harmony import */ var src_app_services_cities_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/cities.service */ "./src/app/services/cities.service.ts");
            /* harmony import */ var src_app_services_contacts_candidate_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/contacts-candidate.service */ "./src/app/services/contacts-candidate.service.ts");
            /* harmony import */ var src_app_services_skills_candidate_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/skills-candidate.service */ "./src/app/services/skills-candidate.service.ts");
            /* harmony import */ var src_app_services_skills_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/skills.service */ "./src/app/services/skills.service.ts");
            /* harmony import */ var src_app_services_image_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/image.service */ "./src/app/services/image.service.ts");
            /* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
            /* harmony import */ var _send_customer_modal_send_customer_modal_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../send-customer-modal/send-customer-modal.component */ "./src/app/components/send-customer-modal/send-customer-modal.component.ts");
            /* harmony import */ var _send_candidate_modal_send_candidate_modal_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../send-candidate-modal/send-candidate-modal.component */ "./src/app/components/send-candidate-modal/send-candidate-modal.component.ts");
            /* harmony import */ var _connect_customer_modal_connect_customer_modal_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../connect-customer-modal/connect-customer-modal.component */ "./src/app/components/connect-customer-modal/connect-customer-modal.component.ts");
            /* harmony import */ var src_app_services_file_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! src/app/services/file.service */ "./src/app/services/file.service.ts");
            /* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
            /* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/ __webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_14__);
            var ImageSnippet = /** @class */ (function () {
                function ImageSnippet(src, file) {
                    this.src = src;
                    this.file = file;
                    this.pending = false;
                    this.status = 'init';
                }
                return ImageSnippet;
            }());
            var FileSnippet = /** @class */ (function () {
                function FileSnippet(src, file) {
                    this.src = src;
                    this.file = file;
                    this.pending = false;
                    this.status = 'init';
                }
                return FileSnippet;
            }());
            var CandidateInfoComponent = /** @class */ (function () {
                function CandidateInfoComponent(eventBusService, candidateService, citiesService, contactsCandidateService, skillsCandidateService, skillsService, imageService, modalService, fileService) {
                    this.eventBusService = eventBusService;
                    this.candidateService = candidateService;
                    this.citiesService = citiesService;
                    this.contactsCandidateService = contactsCandidateService;
                    this.skillsCandidateService = skillsCandidateService;
                    this.skillsService = skillsService;
                    this.imageService = imageService;
                    this.modalService = modalService;
                    this.fileService = fileService;
                    this.candidateContacts = [];
                    this.candidateSkills = [];
                    this.IS_DOCUMENT = 0;
                    this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_14__;
                }
                CandidateInfoComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.eventBusService.on('selectedCandidate', function (data) {
                        console.log('fired');
                        if (data) {
                            _this.selectedCandidate = data;
                            _this.getCandidateCity(_this.selectedCandidate.location.toString());
                            _this.getCandidateContacts(_this.selectedCandidate.id);
                            _this.getCandidateSkills(_this.selectedCandidate.id);
                            _this.getCandidateDocuments(_this.selectedCandidate.id);
                        }
                        else {
                            _this.selectedCandidate = undefined;
                        }
                    });
                };
                CandidateInfoComponent.prototype.ngOnChanges = function (changes) {
                    // console.log("changes", changes.selectedCandidate);
                    // console.log("changes", changes);
                };
                CandidateInfoComponent.prototype.getCandidateCity = function (id) {
                    var _this = this;
                    this.citiesService.getDetail(id).then(function (city) { return _this.city = city; });
                };
                CandidateInfoComponent.prototype.getCandidateContacts = function (id) {
                    var _this = this;
                    this.contactsCandidateService.getCandidateContacts(id).then(function (contacts) {
                        _this.candidateContacts = [];
                        contacts.forEach(function (contact) {
                            _this.contactType = undefined;
                            _this.contactsCandidateService.getCandidateContactTypeName(contact.contact_type.toString())
                                .then(function (contact_type) {
                                _this.candidateContacts.push({ contact_value: contact.value, contact_type_name: contact_type.name });
                            });
                        });
                    });
                    // });
                };
                CandidateInfoComponent.prototype.getCandidateSkills = function (id) {
                    var _this = this;
                    this.candidateSkills = [];
                    this.skillsCandidateService.getAllCandidatesSkills(id).then(function (skills) { return skills.relatedSkills.forEach(function (skill) {
                        _this.skillsService.getDetail(skill.skill_id).then(function (skillCandidate) { return _this.candidateSkills.push(skillCandidate.name); });
                    }); });
                };
                CandidateInfoComponent.prototype.getCandidates = function () {
                    var _this = this;
                    this.candidateService.getData().then(function (candidates) {
                        _this.candidates = candidates;
                        _this.selectedCandidate = candidates[0];
                        _this.getCandidateCity(_this.selectedCandidate.location.toString());
                        _this.getCandidateContacts(_this.selectedCandidate.id);
                        _this.getCandidateSkills(_this.selectedCandidate.id);
                    });
                };
                CandidateInfoComponent.prototype.openModal = function (template) {
                    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
                };
                CandidateInfoComponent.prototype.confirm = function () {
                    var _this = this;
                    this.candidateService.delete(this.selectedCandidate.id).then(function (item) {
                        _this.getCandidates();
                    });
                    this.modalRef.hide();
                };
                CandidateInfoComponent.prototype.decline = function () {
                    this.modalRef.hide();
                };
                CandidateInfoComponent.prototype.openSendCustomerModal = function () {
                    var initialState = {
                        title: 'Отправить заказчику'
                    };
                    this.bsModalRef = this.modalService.show(_send_customer_modal_send_customer_modal_component__WEBPACK_IMPORTED_MODULE_10__["SendCustomerModalComponent"], { initialState: initialState });
                    this.bsModalRef.content.selectedCandidateId = this.selectedCandidate.id;
                };
                CandidateInfoComponent.prototype.openSendCandidateModal = function () {
                    var initialState = {
                        selectedCandidateId: this.selectedCandidate.id,
                        selectedCandidate: this.selectedCandidate,
                        title: 'Отправить кандидату'
                    };
                    this.bsModalRef = this.modalService.show(_send_candidate_modal_send_candidate_modal_component__WEBPACK_IMPORTED_MODULE_11__["SendCandidateModalComponent"], { initialState: initialState });
                    this.bsModalRef.content.selectedCandidateId = this.selectedCandidate.id;
                    this.bsModalRef.content.selectedCandidate = this.selectedCandidate;
                };
                CandidateInfoComponent.prototype.openConnectCustomerModal = function () {
                    var initialState = {
                        selectedCandidateId: this.selectedCandidate.id,
                        title: 'Связаться с заказчиком'
                    };
                    this.bsModalRef = this.modalService.show(_connect_customer_modal_connect_customer_modal_component__WEBPACK_IMPORTED_MODULE_12__["ConnectCustomerModalComponent"], { initialState: initialState });
                    this.bsModalRef.content.selectedCandidateId = this.selectedCandidate.id;
                };
                CandidateInfoComponent.prototype.processPhoto = function (photoInput) {
                    var _this = this;
                    var file = photoInput.files[0];
                    var reader = new FileReader();
                    reader.addEventListener('load', function (event) {
                        _this.photoInput.nativeElement.value = null;
                        _this.selectedPhoto = new ImageSnippet(event.target.result, file);
                        _this.selectedPhoto.pending = true;
                        _this.imageService.uploadPhoto(_this.selectedPhoto.file, _this.selectedCandidate.id).subscribe(function (res) {
                            console.log(res);
                            _this.selectedCandidate.photo = res.json().photo;
                        }, function (err) {
                            console.log(err);
                        });
                    });
                    reader.readAsDataURL(file);
                };
                CandidateInfoComponent.prototype.removePhoto = function () {
                    var _this = this;
                    this.imageService.deletePhoto(this.selectedCandidate.id).subscribe(function (res) {
                        _this.selectedPhoto = undefined;
                        _this.selectedCandidate.photo = undefined;
                        console.log(res);
                    }, function (err) {
                        console.log(err);
                    });
                };
                CandidateInfoComponent.prototype.getCandidateDocuments = function (candidateId) {
                    var _this = this;
                    this.selectedCandidateDocument = undefined;
                    this.fileService.getCandidateDocuments(candidateId).then(function (documentsCandidate) {
                        _this.candidateDocuments = documentsCandidate;
                        if (!_this.uploadedFile && _this.candidateDocuments[0]) {
                            _this.selectedCandidateDocument = _this.candidateDocuments[0].file_name;
                        }
                        else if (_this.uploadedFile) {
                            _this.selectedCandidateDocument = _this.uploadedFile.file.name;
                            _this.uploadedFile = undefined;
                        }
                    });
                };
                CandidateInfoComponent.prototype.previewDocument = function (event, filename) {
                    event.preventDefault();
                    this.selectedCandidateDocument = filename;
                };
                CandidateInfoComponent.prototype.removeCandidateDocumentByName = function ($event, fileName) {
                    var _this = this;
                    $event.preventDefault();
                    this.fileService.getCandidateDocuments(this.selectedCandidate.id)
                        .then(function (documentCandidate) {
                        _this.deletedCandidateDocument = documentCandidate.filter(function (doc) { return doc.file_name === fileName; }).pop();
                        _this.fileService.deleteFileByName(_this.selectedCandidate.id, _this.deletedCandidateDocument.id).subscribe(function (res) {
                            _this.getCandidateDocuments(_this.selectedCandidate.id);
                        }, function (err) {
                            console.log(err);
                        });
                    });
                };
                CandidateInfoComponent.prototype.processFile = function (fileInput) {
                    var _this = this;
                    var file = fileInput.files[0];
                    var reader = new FileReader();
                    reader.addEventListener('load', function (event) {
                        _this.fileInput.nativeElement.value = null;
                        _this.uploadedFile = new FileSnippet(event.target.result, file);
                        _this.uploadedFile.pending = true;
                        _this.fileService.uploadFile(_this.uploadedFile.file, _this.selectedCandidate.id, _this.IS_DOCUMENT).subscribe(function (res) {
                            _this.selectedCandidateDocument = _this.uploadedFile.file.name;
                            _this.getCandidateDocuments(_this.selectedCandidate.id);
                        }, function (err) {
                            console.log(err);
                        });
                    });
                    reader.readAsDataURL(file);
                };
                return CandidateInfoComponent;
            }());
            CandidateInfoComponent.ctorParameters = function () { return [
                { type: src_app_services_shared_event_bus_service__WEBPACK_IMPORTED_MODULE_2__["EventBusService"] },
                { type: src_app_services_candidate_service__WEBPACK_IMPORTED_MODULE_3__["CandidateService"] },
                { type: src_app_services_cities_service__WEBPACK_IMPORTED_MODULE_4__["CitiesService"] },
                { type: src_app_services_contacts_candidate_service__WEBPACK_IMPORTED_MODULE_5__["ContactsCandidateService"] },
                { type: src_app_services_skills_candidate_service__WEBPACK_IMPORTED_MODULE_6__["SkillsCandidateService"] },
                { type: src_app_services_skills_service__WEBPACK_IMPORTED_MODULE_7__["SkillsService"] },
                { type: src_app_services_image_service__WEBPACK_IMPORTED_MODULE_8__["ImageService"] },
                { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_9__["BsModalService"] },
                { type: src_app_services_file_service__WEBPACK_IMPORTED_MODULE_13__["FileService"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('photoInput', { static: false })
            ], CandidateInfoComponent.prototype, "photoInput", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput', { static: false })
            ], CandidateInfoComponent.prototype, "fileInput", void 0);
            CandidateInfoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-candidate-info',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./candidate-info.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/candidate-info/candidate-info.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./candidate-info.component.css */ "./src/app/components/candidate-info/candidate-info.component.css")).default]
                })
            ], CandidateInfoComponent);
            /***/ 
        }),
        /***/ "./src/app/components/candidate-list/candidate-list.component.css": 
        /*!************************************************************************!*\
          !*** ./src/app/components/candidate-list/candidate-list.component.css ***!
          \************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".selected {\n    background-color: #CFD8DC !important;\n    color: white;\n  }\n  .candidates {\n    margin: 0 0 2em 0;\n    list-style-type: none;\n    padding: 0;\n  }\n  .candidates li {\n    cursor: pointer;\n    position: relative;\n    left: 0;\n    margin: .5em;\n    padding: .3em 0;\n    height: 1.6em;\n    border-radius: 4px;\n  }\n  .candidates li.selected:hover {\n    background-color: #BBD8DC !important;\n    color: white;\n  }\n  .candidates li:hover {\n    color: #607D8B;\n    background-color: #DDD;\n    left: .1em;\n  }\n  .candidates .text {\n    position: relative;\n    top: -3px;\n  }\n  .candidates .badge {\n    display: inline-block;\n    font-size: small;\n    color: white;\n    padding: 0.8em 0.7em 0 0.7em;\n    background-color: #607D8B;\n    line-height: 1em;\n    position: relative;\n    left: -1px;\n    top: -4px;\n    height: 1.8em;\n    margin-right: .8em;\n    border-radius: 4px 0 0 4px;\n  }\n  .entity_list__item {\n    display:flex;\n    background: #fff;\n    min-height: 100px;\n    /*padding-bottom: 5rem !important;*/\n    /*padding-top: 5rem !important;*/\n    border-bottom: 1px solid #dee2e6 !important;\n    margin: 0 !important;\n  }\n  .candidates-list {\n    overflow-y: scroll;\n    padding: 0;\n    overflow-x: hidden;\n    max-height: 250vh;\n  }\n  .entity_list__item a {\n    color: black !important;\n    text-decoration: none;\n    background-color: transparent;\n  }\n  .entity_list__item .entity-title {\n    font-weight: 500;\n  }\n  .entity_list__item .entity-sub {\n    font-size: 14px;\n    color:\n      #616161;\n    font-weight: normal;\n  }\n  .entity-item-round {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    text-align: center;\n    margin-top: 5px;\n    margin-left: 20px;\n    width: 65px;\n    height: 65px;\n    background-color:\n      #f5f5f5;\n    border-radius: 50% !important;\n    border: 1px solid\n    #eaeaea;\n  }\n  .entity-item-round i {\n    line-height: 65px;\n    font-size: 40px;\n    color:\n      #C4C4C4;\n  }\n  .border-bottom {\n    border-bottom: 1px solid #dee2e6 !important;\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jYW5kaWRhdGUtbGlzdC9jYW5kaWRhdGUtbGlzdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksb0NBQW9DO0lBQ3BDLFlBQVk7RUFDZDtFQUNBO0lBQ0UsaUJBQWlCO0lBQ2pCLHFCQUFxQjtJQUNyQixVQUFVO0VBQ1o7RUFDQTtJQUNFLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsT0FBTztJQUNQLFlBQVk7SUFDWixlQUFlO0lBQ2YsYUFBYTtJQUNiLGtCQUFrQjtFQUNwQjtFQUNBO0lBQ0Usb0NBQW9DO0lBQ3BDLFlBQVk7RUFDZDtFQUNBO0lBQ0UsY0FBYztJQUNkLHNCQUFzQjtJQUN0QixVQUFVO0VBQ1o7RUFDQTtJQUNFLGtCQUFrQjtJQUNsQixTQUFTO0VBQ1g7RUFDQTtJQUNFLHFCQUFxQjtJQUNyQixnQkFBZ0I7SUFDaEIsWUFBWTtJQUNaLDRCQUE0QjtJQUM1Qix5QkFBeUI7SUFDekIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsU0FBUztJQUNULGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsMEJBQTBCO0VBQzVCO0VBQ0E7SUFDRSxZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixtQ0FBbUM7SUFDbkMsZ0NBQWdDO0lBQ2hDLDJDQUEyQztJQUMzQyxvQkFBb0I7RUFDdEI7RUFDQTtJQUNFLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLGlCQUFpQjtFQUNuQjtFQUNBO0lBQ0UsdUJBQXVCO0lBQ3ZCLHFCQUFxQjtJQUNyQiw2QkFBNkI7RUFDL0I7RUFDQTtJQUNFLGdCQUFnQjtFQUNsQjtFQUNBO0lBQ0UsZUFBZTtJQUNmO2FBQ1M7SUFDVCxtQkFBbUI7RUFDckI7RUFDQTtJQUNFLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLFdBQVc7SUFDWCxZQUFZO0lBQ1o7YUFDUztJQUNULDZCQUE2QjtJQUM3QjtXQUNPO0VBQ1Q7RUFDQTtJQUNFLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2Y7YUFDUztFQUNYO0VBQ0E7SUFDRSwyQ0FBMkM7RUFDN0MiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NhbmRpZGF0ZS1saXN0L2NhbmRpZGF0ZS1saXN0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2VsZWN0ZWQge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNDRkQ4REMgIWltcG9ydGFudDtcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cbiAgLmNhbmRpZGF0ZXMge1xuICAgIG1hcmdpbjogMCAwIDJlbSAwO1xuICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG4gIC5jYW5kaWRhdGVzIGxpIHtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGxlZnQ6IDA7XG4gICAgbWFyZ2luOiAuNWVtO1xuICAgIHBhZGRpbmc6IC4zZW0gMDtcbiAgICBoZWlnaHQ6IDEuNmVtO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgfVxuICAuY2FuZGlkYXRlcyBsaS5zZWxlY3RlZDpob3ZlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0JCRDhEQyAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxuICAuY2FuZGlkYXRlcyBsaTpob3ZlciB7XG4gICAgY29sb3I6ICM2MDdEOEI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0RERDtcbiAgICBsZWZ0OiAuMWVtO1xuICB9XG4gIC5jYW5kaWRhdGVzIC50ZXh0IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdG9wOiAtM3B4O1xuICB9XG4gIC5jYW5kaWRhdGVzIC5iYWRnZSB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIGZvbnQtc2l6ZTogc21hbGw7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHBhZGRpbmc6IDAuOGVtIDAuN2VtIDAgMC43ZW07XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzYwN0Q4QjtcbiAgICBsaW5lLWhlaWdodDogMWVtO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBsZWZ0OiAtMXB4O1xuICAgIHRvcDogLTRweDtcbiAgICBoZWlnaHQ6IDEuOGVtO1xuICAgIG1hcmdpbi1yaWdodDogLjhlbTtcbiAgICBib3JkZXItcmFkaXVzOiA0cHggMCAwIDRweDtcbiAgfVxuICAuZW50aXR5X2xpc3RfX2l0ZW0ge1xuICAgIGRpc3BsYXk6ZmxleDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIG1pbi1oZWlnaHQ6IDEwMHB4O1xuICAgIC8qcGFkZGluZy1ib3R0b206IDVyZW0gIWltcG9ydGFudDsqL1xuICAgIC8qcGFkZGluZy10b3A6IDVyZW0gIWltcG9ydGFudDsqL1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGVlMmU2ICFpbXBvcnRhbnQ7XG4gICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG4gIH1cbiAgLmNhbmRpZGF0ZXMtbGlzdCB7XG4gICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xuICAgIHBhZGRpbmc6IDA7XG4gICAgb3ZlcmZsb3cteDogaGlkZGVuO1xuICAgIG1heC1oZWlnaHQ6IDI1MHZoO1xuICB9XG4gIC5lbnRpdHlfbGlzdF9faXRlbSBhIHtcbiAgICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIH1cbiAgLmVudGl0eV9saXN0X19pdGVtIC5lbnRpdHktdGl0bGUge1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIH1cbiAgLmVudGl0eV9saXN0X19pdGVtIC5lbnRpdHktc3ViIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6XG4gICAgICAjNjE2MTYxO1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIH1cbiAgLmVudGl0eS1pdGVtLXJvdW5kIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcbiAgICB3aWR0aDogNjVweDtcbiAgICBoZWlnaHQ6IDY1cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjpcbiAgICAgICNmNWY1ZjU7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiAxcHggc29saWRcbiAgICAjZWFlYWVhO1xuICB9XG4gIC5lbnRpdHktaXRlbS1yb3VuZCBpIHtcbiAgICBsaW5lLWhlaWdodDogNjVweDtcbiAgICBmb250LXNpemU6IDQwcHg7XG4gICAgY29sb3I6XG4gICAgICAjQzRDNEM0O1xuICB9XG4gIC5ib3JkZXItYm90dG9tIHtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RlZTJlNiAhaW1wb3J0YW50O1xuICB9Il19 */");
            /***/ 
        }),
        /***/ "./src/app/components/candidate-list/candidate-list.component.ts": 
        /*!***********************************************************************!*\
          !*** ./src/app/components/candidate-list/candidate-list.component.ts ***!
          \***********************************************************************/
        /*! exports provided: CandidateListComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CandidateListComponent", function () { return CandidateListComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_services_candidate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/candidate.service */ "./src/app/services/candidate.service.ts");
            /* harmony import */ var src_app_services_shared_event_bus_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/shared/event-bus.service */ "./src/app/services/shared/event-bus.service.ts");
            /* harmony import */ var src_app_services_shared_event_class__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/shared/event.class */ "./src/app/services/shared/event.class.ts");
            // import { StatusesCandidatesService } from 'src/app/services/statuses-candidates.service';
            var CandidateListComponent = /** @class */ (function () {
                function CandidateListComponent(candidateService, eventBusService, cdr) {
                    this.candidateService = candidateService;
                    this.eventBusService = eventBusService;
                    this.cdr = cdr;
                    this.candidates = [];
                    /*****Candidate Statuses*****/
                    this.STATUS_ALL = 0;
                    this.STATUS_NEW = 1;
                    this.STATUS_CONSIDERED = 2;
                    this.STATUS_TODAY = 3;
                    this.STATUS_OFFER = 4;
                    this.STATUS_SELECTED = 5;
                    this.STATUS_BLACKLISTED = 6;
                }
                CandidateListComponent.prototype.ngOnInit = function () {
                    this.getCandidatesExceptBlackListed();
                    // console.log(this.selected_statuses);
                };
                CandidateListComponent.prototype.ngOnChanges = function (changes) {
                    this.getCandidatesWithStatuses(this.selected_statuses);
                    this.cdr.detectChanges();
                    console.log(this.selected_statuses);
                    console.log(changes);
                };
                CandidateListComponent.prototype.getCandidatesWithStatuses = function (selectedStatus) {
                    var _this = this;
                    this.candidates = [];
                    // if status == STATUS_ALL - select all candidates except BLACK_LISTED
                    if (selectedStatus == this.STATUS_ALL) {
                        this.getCandidatesExceptBlackListed();
                    }
                    else {
                        this.candidateService.getCandidatesWithStatuses().then(function (candidates) {
                            candidates.forEach(function (candidate) {
                                candidate.relatedStatuses.forEach(function (relatedStatus) {
                                    if (relatedStatus.status == selectedStatus) {
                                        _this.candidates.push(candidate);
                                    }
                                });
                            });
                            if (_this.candidates[0]) {
                                _this.selectedCandidate = _this.candidates[0];
                                _this.eventBusService.emit(new src_app_services_shared_event_class__WEBPACK_IMPORTED_MODULE_4__["EventData"]('selectedCandidate', _this.candidates[0]));
                            }
                            else {
                                _this.eventBusService.emit(new src_app_services_shared_event_class__WEBPACK_IMPORTED_MODULE_4__["EventData"]('selectedCandidate', undefined));
                            }
                        });
                        // this.eventBusService.emit(new EventData('selectedCandidate',this.candidates[0]));
                    }
                    // this.eventBusService.emit(new EventData('selectedCandidate',this.candidates[0]));
                };
                CandidateListComponent.prototype.getCandidatesExceptBlackListed = function () {
                    var _this = this;
                    this.candidateService.getCandidatesWithStatuses().then(function (candidates) {
                        candidates.forEach(function (candidate) {
                            if (!candidate.relatedStatuses.length) {
                                _this.candidates.push(candidate);
                            }
                            else {
                                candidate.relatedStatuses.forEach(function (relatedStatus) {
                                    if (relatedStatus.status != _this.STATUS_BLACKLISTED) {
                                        _this.candidates.push(candidate);
                                    }
                                });
                            }
                        });
                        _this.selectedCandidate = _this.candidates[0];
                        _this.eventBusService.emit(new src_app_services_shared_event_class__WEBPACK_IMPORTED_MODULE_4__["EventData"]('selectedCandidate', _this.candidates[0]));
                    });
                };
                CandidateListComponent.prototype.getCandidates = function () {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            this.candidates = undefined;
                            this.candidateService.getData().then(function (candidates) {
                                _this.candidates = candidates;
                                // this.selectedCandidate = candidates[0];
                                // this.getCandidateCity(this.selectedCandidate.location.toString());
                                // this.getCandidateContacts(this.selectedCandidate.id);
                                // this.getCandidateSkills(this.selectedCandidate.id);
                            });
                            return [2 /*return*/, true];
                        });
                    });
                };
                CandidateListComponent.prototype.onSelect = function (candidate) {
                    this.selectedCandidate = candidate;
                    this.eventBusService.emit(new src_app_services_shared_event_class__WEBPACK_IMPORTED_MODULE_4__["EventData"]('selectedCandidate', candidate));
                };
                return CandidateListComponent;
            }());
            CandidateListComponent.ctorParameters = function () { return [
                { type: src_app_services_candidate_service__WEBPACK_IMPORTED_MODULE_2__["CandidateService"] },
                { type: src_app_services_shared_event_bus_service__WEBPACK_IMPORTED_MODULE_3__["EventBusService"] },
                { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], CandidateListComponent.prototype, "selected_statuses", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
            ], CandidateListComponent.prototype, "searchString", void 0);
            CandidateListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-candidate-list',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./candidate-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/candidate-list/candidate-list.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./candidate-list.component.css */ "./src/app/components/candidate-list/candidate-list.component.css")).default]
                })
            ], CandidateListComponent);
            /***/ 
        }),
        /***/ "./src/app/components/candidate-update/candidate-update.component.css": 
        /*!****************************************************************************!*\
          !*** ./src/app/components/candidate-update/candidate-update.component.css ***!
          \****************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".main-nav-item:not(:last-child) {\n  border-bottom: 1px solid #7DA5A8;\n}\n.border-right {\n  border-right: 1px solid\n  #dee2e6 !important;\n}\n.form-control {\n  border-radius: 0px;\n}\ninput,select {\n  background:\n    #f5f5f5 !important;\n}\nmat-form-field {\n  min-width: 100%;\n}\n/*******BUTTONS********/\n.entity-details__buttons {\n  padding-bottom: 1.5rem !important;\n  padding-top: 1.5rem !important;\n  padding-left: 0 !important;\n  text-align: center;\n}\n.entity-buttons-wrapper .btn {\n  height: 40px;\n  /*padding: 12px 9px;*/\n  margin-right: 12px;\n  font-weight: 500 !important;\n  font-size: 14px !important;\n  text-transform: uppercase;\n  color:\n    rgba(0, 0, 0, 0.87);\n}\n.btn-crm {\n  border-radius: 0!important;\n  height: 30px;\n  min-width: 120px;\n}\n.btn-crm-primary{\n  font-size: 11px!important;\n  font-weight: 700!important;\n  border-radius: 0!important;\n  border: 1px solid #01747f !important;\n  height: 30px;\n  background-color: #01919D!important;\n  color: white!important;\n}\n.btn-crm-default{\n  font-size: 11px!important;\n  font-weight: 700!important;\n  border-radius: 0!important;\n  border: 1px solid #aeaeae !important;\n  height: 30px;\n}\n.btn-crm-default:hover {\n  background: #f5f5f5;\n  outline: 0;\n  box-shadow: 0 0 0 0.15rem rgba(255, 177, 6, 0.25);\n}\n.btn-crm-default_red {\n  border-color: red !important;\n  color: red !important;\n}\n/*******PHOTO WIDGET*******/\n.three-dots {\n  position: absolute;\n  top: 10px;\n  right: 0;\n}\n.entity-item-round {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  text-align: center;\n  margin-top: 5px;\n  margin-left: 20px;\n  width: 65px;\n  height: 65px;\n  background-color:\n    #f5f5f5;\n  border-radius: 50% !important;\n  border: 1px solid\n  #eaeaea;\n}\n.entity-item-round i {\n  line-height: 65px;\n  font-size: 40px;\n  color:\n    #C4C4C4;\n}\n.empty-img {\n  margin-right: 50px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-top: 10px;\n  width: 150px;\n  height: 150px;\n  background-color:\n    #f5f5f5;\n  border-radius: 50% !important;\n}\n.dropdown {\n  position: relative;\n}\n.btn-default {\n  border-radius: 50% !important;\n  border: 0px !important;\n}\n.dot {\n  height: 5px;\n  width: 5px;\n  background-color:\n    #bbb;\n  border-radius: 50%;\n  display: flex;\n  margin: 1px;\n}\n.profile__dropdown-menu i {\n  width: 22px;\n  height: 22px;\n  margin-right: 16px;\n}\n.profile__dropdown-menu i::before {\n  line-height: 22px;\n  font-size: 12px;\n}\n.input-photo {\n  width: 0.1px;\n  height: 0.1px;\n  opacity: 0;\n  overflow: hidden;\n  position: absolute;\n  z-index: -1;\n}\n.input-photo + label {\n  max-width: 100%;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  cursor: pointer;\n  display: inline-block;\n  padding-left:1.45rem;\n  margin-bottom: 0;\n}\n/******IMAGE PREVIEW******/\n.image-upload-container {\n  cursor: pointer;\n}\n.img-preview-container {\n  position: relative;\n}\n.img-preview {\n  /*background: center center no-repeat;*/\n  background-size: cover;\n  border-radius: 50% !important;\n  background-position: center;\n}\n/******UPLOAD RESUME AND DOCUMENTS******/\n.fa-add-resume:before {\n  content: \"\\f055\";\n  line-height: 1;\n  padding: 1.5px;\n  color: #58d5e0;\n}\n.add-resume-button,.remove-resume-button {\n  color: #58d5e0;\n}\n.add-resume-label {\n  display: block;\n  width: 100%;\n  padding: .25rem;\n  clear: both;\n  font-weight: 400;\n  color: #58d5e0;\n  text-align: inherit;\n  white-space: nowrap;\n  background-color: transparent;\n  border: 0;\n}\n.add-resume-label span {\n  margin-left: 10px;\n  color: #58d5e0;\n}\n.input-resume {\n  display: none;\n}\n.remove-resume-button {\n  outline: none;\n  border: none;\n  background-color: #fff;\n  margin-bottom: .5rem;\n}\n.remove-resume-button i{\n  margin-right: 19px;\n}\n.fa-add-document:before {\n  content: \"\\f055\";\n  line-height: 1;\n  padding: 1.5px;\n  color: #58d5e0;\n}\n.add-document-button,.remove-document-button {\n  color: #58d5e0;\n}\n.add-document-label {\n  display: block;\n  width: 100%;\n  padding: .25rem;\n  clear: both;\n  font-weight: 400;\n  color: #58d5e0;\n  text-align: inherit;\n  white-space: nowrap;\n  background-color: transparent;\n  border: 0;\n}\n.add-document-label span {\n  margin-left: 10px;\n  color: #58d5e0;\n}\n.input-document {\n  display: none;\n}\n/*******MODALS - DELETE CANDIDATE ******/\n.modal-body .btn {\n  font-size: 14px !important;\n}\n.modal-header{\n  background: #01919D;\n  color: white;\n  padding: 0.5rem 1rem!important;\n  align-items: center!important;\n}\n.modal-header h5 {\n  font-size: 14px;\n  margin: 0px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jYW5kaWRhdGUtdXBkYXRlL2NhbmRpZGF0ZS11cGRhdGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdDQUFnQztBQUNsQztBQUNBO0VBQ0U7b0JBQ2tCO0FBQ3BCO0FBQ0E7RUFDRSxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFO3NCQUNvQjtBQUN0QjtBQUNBO0VBQ0UsZUFBZTtBQUNqQjtBQUNBLHVCQUF1QjtBQUN2QjtFQUNFLGlDQUFpQztFQUNqQyw4QkFBOEI7RUFDOUIsMEJBQTBCO0VBQzFCLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsWUFBWTtFQUNaLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsMkJBQTJCO0VBQzNCLDBCQUEwQjtFQUMxQix5QkFBeUI7RUFDekI7dUJBQ3FCO0FBQ3ZCO0FBQ0E7RUFDRSwwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLDBCQUEwQjtFQUMxQiwwQkFBMEI7RUFDMUIsb0NBQW9DO0VBQ3BDLFlBQVk7RUFDWixtQ0FBbUM7RUFDbkMsc0JBQXNCO0FBQ3hCO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsMEJBQTBCO0VBQzFCLDBCQUEwQjtFQUMxQixvQ0FBb0M7RUFDcEMsWUFBWTtBQUNkO0FBQ0E7RUFDRSxtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLGlEQUFpRDtBQUNuRDtBQUNBO0VBQ0UsNEJBQTRCO0VBQzVCLHFCQUFxQjtBQUN2QjtBQUNBLDJCQUEyQjtBQUMzQjtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsUUFBUTtBQUNWO0FBQ0E7RUFDRSxhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsWUFBWTtFQUNaO1dBQ1M7RUFDVCw2QkFBNkI7RUFDN0I7U0FDTztBQUNUO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsZUFBZTtFQUNmO1dBQ1M7QUFDWDtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osYUFBYTtFQUNiO1dBQ1M7RUFDVCw2QkFBNkI7QUFDL0I7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsNkJBQTZCO0VBQzdCLHNCQUFzQjtBQUN4QjtBQUNBO0VBQ0UsV0FBVztFQUNYLFVBQVU7RUFDVjtRQUNNO0VBQ04sa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixXQUFXO0FBQ2I7QUFDQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsZUFBZTtBQUNqQjtBQUNBO0VBQ0UsWUFBWTtFQUNaLGFBQWE7RUFDYixVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixXQUFXO0FBQ2I7QUFDQTtFQUNFLGVBQWU7RUFDZix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsb0JBQW9CO0VBQ3BCLGdCQUFnQjtBQUNsQjtBQUNBLDBCQUEwQjtBQUMxQjtFQUNFLGVBQWU7QUFDakI7QUFFQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsdUNBQXVDO0VBQ3ZDLHNCQUFzQjtFQUN0Qiw2QkFBNkI7RUFDN0IsMkJBQTJCO0FBQzdCO0FBQ0Esd0NBQXdDO0FBQ3hDO0VBQ0UsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxjQUFjO0VBQ2QsY0FBYztBQUNoQjtBQUNBO0VBQ0UsY0FBYztBQUNoQjtBQUNBO0VBQ0UsY0FBYztFQUNkLFdBQVc7RUFDWCxlQUFlO0VBQ2YsV0FBVztFQUNYLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQiw2QkFBNkI7RUFDN0IsU0FBUztBQUNYO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsY0FBYztBQUNoQjtBQUNBO0VBQ0UsYUFBYTtBQUNmO0FBQ0E7RUFDRSxhQUFhO0VBQ2IsWUFBWTtFQUNaLHNCQUFzQjtFQUN0QixvQkFBb0I7QUFDdEI7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxjQUFjO0VBQ2QsY0FBYztBQUNoQjtBQUNBO0VBQ0UsY0FBYztBQUNoQjtBQUNBO0VBQ0UsY0FBYztFQUNkLFdBQVc7RUFDWCxlQUFlO0VBQ2YsV0FBVztFQUNYLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQiw2QkFBNkI7RUFDN0IsU0FBUztBQUNYO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsY0FBYztBQUNoQjtBQUNBO0VBQ0UsYUFBYTtBQUNmO0FBQ0Esd0NBQXdDO0FBQ3hDO0VBQ0UsMEJBQTBCO0FBQzVCO0FBQ0E7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLDhCQUE4QjtFQUM5Qiw2QkFBNkI7QUFDL0I7QUFDQTtFQUNFLGVBQWU7RUFDZixXQUFXO0FBQ2IiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NhbmRpZGF0ZS11cGRhdGUvY2FuZGlkYXRlLXVwZGF0ZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1haW4tbmF2LWl0ZW06bm90KDpsYXN0LWNoaWxkKSB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjN0RBNUE4O1xufVxuLmJvcmRlci1yaWdodCB7XG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkXG4gICNkZWUyZTYgIWltcG9ydGFudDtcbn1cbi5mb3JtLWNvbnRyb2wge1xuICBib3JkZXItcmFkaXVzOiAwcHg7XG59XG5pbnB1dCxzZWxlY3Qge1xuICBiYWNrZ3JvdW5kOlxuICAgICNmNWY1ZjUgIWltcG9ydGFudDtcbn1cbm1hdC1mb3JtLWZpZWxkIHtcbiAgbWluLXdpZHRoOiAxMDAlO1xufVxuLyoqKioqKipCVVRUT05TKioqKioqKiovXG4uZW50aXR5LWRldGFpbHNfX2J1dHRvbnMge1xuICBwYWRkaW5nLWJvdHRvbTogMS41cmVtICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiAxLjVyZW0gIWltcG9ydGFudDtcbiAgcGFkZGluZy1sZWZ0OiAwICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5lbnRpdHktYnV0dG9ucy13cmFwcGVyIC5idG4ge1xuICBoZWlnaHQ6IDQwcHg7XG4gIC8qcGFkZGluZzogMTJweCA5cHg7Ki9cbiAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xuICBmb250LXdlaWdodDogNTAwICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBjb2xvcjpcbiAgICByZ2JhKDAsIDAsIDAsIDAuODcpO1xufVxuLmJ0bi1jcm0ge1xuICBib3JkZXItcmFkaXVzOiAwIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBtaW4td2lkdGg6IDEyMHB4O1xufVxuLmJ0bi1jcm0tcHJpbWFyeXtcbiAgZm9udC1zaXplOiAxMXB4IWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDcwMCFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDAhaW1wb3J0YW50O1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDE3NDdmICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMzBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAxOTE5RCFpbXBvcnRhbnQ7XG4gIGNvbG9yOiB3aGl0ZSFpbXBvcnRhbnQ7XG59XG4uYnRuLWNybS1kZWZhdWx0e1xuICBmb250LXNpemU6IDExcHghaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogNzAwIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogMCFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNhZWFlYWUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAzMHB4O1xufVxuLmJ0bi1jcm0tZGVmYXVsdDpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICNmNWY1ZjU7XG4gIG91dGxpbmU6IDA7XG4gIGJveC1zaGFkb3c6IDAgMCAwIDAuMTVyZW0gcmdiYSgyNTUsIDE3NywgNiwgMC4yNSk7XG59XG4uYnRuLWNybS1kZWZhdWx0X3JlZCB7XG4gIGJvcmRlci1jb2xvcjogcmVkICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiByZWQgIWltcG9ydGFudDtcbn1cbi8qKioqKioqUEhPVE8gV0lER0VUKioqKioqKi9cbi50aHJlZS1kb3RzIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDEwcHg7XG4gIHJpZ2h0OiAwO1xufVxuLmVudGl0eS1pdGVtLXJvdW5kIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBtYXJnaW4tbGVmdDogMjBweDtcbiAgd2lkdGg6IDY1cHg7XG4gIGhlaWdodDogNjVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjpcbiAgICAjZjVmNWY1O1xuICBib3JkZXItcmFkaXVzOiA1MCUgIWltcG9ydGFudDtcbiAgYm9yZGVyOiAxcHggc29saWRcbiAgI2VhZWFlYTtcbn1cbi5lbnRpdHktaXRlbS1yb3VuZCBpIHtcbiAgbGluZS1oZWlnaHQ6IDY1cHg7XG4gIGZvbnQtc2l6ZTogNDBweDtcbiAgY29sb3I6XG4gICAgI0M0QzRDNDtcbn1cbi5lbXB0eS1pbWcge1xuICBtYXJnaW4tcmlnaHQ6IDUwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICB3aWR0aDogMTUwcHg7XG4gIGhlaWdodDogMTUwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6XG4gICAgI2Y1ZjVmNTtcbiAgYm9yZGVyLXJhZGl1czogNTAlICFpbXBvcnRhbnQ7XG59XG4uZHJvcGRvd24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uYnRuLWRlZmF1bHQge1xuICBib3JkZXItcmFkaXVzOiA1MCUgIWltcG9ydGFudDtcbiAgYm9yZGVyOiAwcHggIWltcG9ydGFudDtcbn1cbi5kb3Qge1xuICBoZWlnaHQ6IDVweDtcbiAgd2lkdGg6IDVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjpcbiAgICAjYmJiO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIG1hcmdpbjogMXB4O1xufVxuLnByb2ZpbGVfX2Ryb3Bkb3duLW1lbnUgaSB7XG4gIHdpZHRoOiAyMnB4O1xuICBoZWlnaHQ6IDIycHg7XG4gIG1hcmdpbi1yaWdodDogMTZweDtcbn1cbi5wcm9maWxlX19kcm9wZG93bi1tZW51IGk6OmJlZm9yZSB7XG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICBmb250LXNpemU6IDEycHg7XG59XG4uaW5wdXQtcGhvdG8ge1xuICB3aWR0aDogMC4xcHg7XG4gIGhlaWdodDogMC4xcHg7XG4gIG9wYWNpdHk6IDA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogLTE7XG59XG4uaW5wdXQtcGhvdG8gKyBsYWJlbCB7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwYWRkaW5nLWxlZnQ6MS40NXJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi8qKioqKipJTUFHRSBQUkVWSUVXKioqKioqL1xuLmltYWdlLXVwbG9hZC1jb250YWluZXIge1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5pbWctcHJldmlldy1jb250YWluZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uaW1nLXByZXZpZXcge1xuICAvKmJhY2tncm91bmQ6IGNlbnRlciBjZW50ZXIgbm8tcmVwZWF0OyovXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDUwJSAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG59XG4vKioqKioqVVBMT0FEIFJFU1VNRSBBTkQgRE9DVU1FTlRTKioqKioqL1xuLmZhLWFkZC1yZXN1bWU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcXGYwNTVcIjtcbiAgbGluZS1oZWlnaHQ6IDE7XG4gIHBhZGRpbmc6IDEuNXB4O1xuICBjb2xvcjogIzU4ZDVlMDtcbn1cbi5hZGQtcmVzdW1lLWJ1dHRvbiwucmVtb3ZlLXJlc3VtZS1idXR0b24ge1xuICBjb2xvcjogIzU4ZDVlMDtcbn1cbi5hZGQtcmVzdW1lLWxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAuMjVyZW07XG4gIGNsZWFyOiBib3RoO1xuICBmb250LXdlaWdodDogNDAwO1xuICBjb2xvcjogIzU4ZDVlMDtcbiAgdGV4dC1hbGlnbjogaW5oZXJpdDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGJvcmRlcjogMDtcbn1cbi5hZGQtcmVzdW1lLWxhYmVsIHNwYW4ge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgY29sb3I6ICM1OGQ1ZTA7XG59XG4uaW5wdXQtcmVzdW1lIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cbi5yZW1vdmUtcmVzdW1lLWJ1dHRvbiB7XG4gIG91dGxpbmU6IG5vbmU7XG4gIGJvcmRlcjogbm9uZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgbWFyZ2luLWJvdHRvbTogLjVyZW07XG59XG4ucmVtb3ZlLXJlc3VtZS1idXR0b24gaXtcbiAgbWFyZ2luLXJpZ2h0OiAxOXB4O1xufVxuXG4uZmEtYWRkLWRvY3VtZW50OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXFxmMDU1XCI7XG4gIGxpbmUtaGVpZ2h0OiAxO1xuICBwYWRkaW5nOiAxLjVweDtcbiAgY29sb3I6ICM1OGQ1ZTA7XG59XG4uYWRkLWRvY3VtZW50LWJ1dHRvbiwucmVtb3ZlLWRvY3VtZW50LWJ1dHRvbiB7XG4gIGNvbG9yOiAjNThkNWUwO1xufVxuLmFkZC1kb2N1bWVudC1sYWJlbCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogLjI1cmVtO1xuICBjbGVhcjogYm90aDtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgY29sb3I6ICM1OGQ1ZTA7XG4gIHRleHQtYWxpZ246IGluaGVyaXQ7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBib3JkZXI6IDA7XG59XG4uYWRkLWRvY3VtZW50LWxhYmVsIHNwYW4ge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbiAgY29sb3I6ICM1OGQ1ZTA7XG59XG4uaW5wdXQtZG9jdW1lbnQge1xuICBkaXNwbGF5OiBub25lO1xufVxuLyoqKioqKipNT0RBTFMgLSBERUxFVEUgQ0FORElEQVRFICoqKioqKi9cbi5tb2RhbC1ib2R5IC5idG4ge1xuICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbn1cbi5tb2RhbC1oZWFkZXJ7XG4gIGJhY2tncm91bmQ6ICMwMTkxOUQ7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMC41cmVtIDFyZW0haW1wb3J0YW50O1xuICBhbGlnbi1pdGVtczogY2VudGVyIWltcG9ydGFudDtcbn1cbi5tb2RhbC1oZWFkZXIgaDUge1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbjogMHB4O1xufVxuIl19 */");
            /***/ 
        }),
        /***/ "./src/app/components/candidate-update/candidate-update.component.ts": 
        /*!***************************************************************************!*\
          !*** ./src/app/components/candidate-update/candidate-update.component.ts ***!
          \***************************************************************************/
        /*! exports provided: CandidateUpdateComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CandidateUpdateComponent", function () { return CandidateUpdateComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _services_candidate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/candidate.service */ "./src/app/services/candidate.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            /* harmony import */ var _services_cities_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/cities.service */ "./src/app/services/cities.service.ts");
            /* harmony import */ var _services_skills_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/skills.service */ "./src/app/services/skills.service.ts");
            /* harmony import */ var _services_skills_candidate_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/skills-candidate.service */ "./src/app/services/skills-candidate.service.ts");
            /* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
            /* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/ __webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8__);
            /* harmony import */ var _services_image_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/image.service */ "./src/app/services/image.service.ts");
            /* harmony import */ var _services_file_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/file.service */ "./src/app/services/file.service.ts");
            /* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
            var ImageSnippet = /** @class */ (function () {
                function ImageSnippet(src, file) {
                    this.src = src;
                    this.file = file;
                    this.pending = false;
                    this.status = 'init';
                }
                return ImageSnippet;
            }());
            var FileSnippet = /** @class */ (function () {
                function FileSnippet(src, file) {
                    this.src = src;
                    this.file = file;
                    this.pending = false;
                    this.status = 'init';
                }
                return FileSnippet;
            }());
            var CandidateUpdateComponent = /** @class */ (function () {
                function CandidateUpdateComponent(candidateService, route, citiesService, skillsService, skillsCandidateService, imageService, fileService, modalService, router) {
                    this.candidateService = candidateService;
                    this.route = route;
                    this.citiesService = citiesService;
                    this.skillsService = skillsService;
                    this.skillsCandidateService = skillsCandidateService;
                    this.imageService = imageService;
                    this.fileService = fileService;
                    this.modalService = modalService;
                    this.router = router;
                    this.IS_RESUME = 1;
                    this.IS_DOCUMENT = 0;
                    this.selectedSkills = Array();
                    this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_8__;
                    this.isPhotoDeleted = false;
                    this.candidateResume = undefined;
                }
                CandidateUpdateComponent.prototype.ngOnInit = function () {
                    this.getCandidates();
                    this.getCities();
                    this.getSkills();
                    this.getCandidateResume();
                    this.getCandidateDocuments();
                };
                CandidateUpdateComponent.prototype.getCities = function () {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (params) { return _this.citiesService.getData(); }))
                        .subscribe(function (cities) { return _this.cities = cities; });
                };
                CandidateUpdateComponent.prototype.getSkills = function () {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (params) { return _this.skillsService.getData(); }))
                        .subscribe(function (skills) {
                        _this.skills = skills;
                    });
                };
                CandidateUpdateComponent.prototype.getCandidates = function () {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (params) { return _this.candidateService.getDetail(params['id']); }))
                        .subscribe(function (candidate) {
                        _this.candidate = candidate;
                        _this.getSkillsCandidates(_this.candidate.id);
                    });
                };
                CandidateUpdateComponent.prototype.getSkillsCandidates = function (id) {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (params) { return _this.skillsCandidateService.getData(id); }))
                        .subscribe(function (skillsCandidate) {
                        _this.skillsCandidate = skillsCandidate;
                        _this.isSelectedSkill = true;
                        var candSkillIds = _this.skillsCandidate.map(function (i) { return i.skill_id; });
                        _this.skills.forEach(function (item) {
                            if (candSkillIds.includes(item.id)) {
                                _this.selectedSkills.push(item);
                            }
                        });
                    });
                    console.log(this.selectedSkills);
                    console.dir(this.selectedSkills);
                };
                CandidateUpdateComponent.prototype.getCandidateResume = function () {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (params) { return _this.fileService.getCandidateResume(params['id']); }))
                        .subscribe(function (documentCandidate) {
                        if (Object.keys(documentCandidate).length !== 0) {
                            _this.candidateResume = documentCandidate.filter(function (doc) { return doc.is_resume === _this.IS_RESUME; });
                        }
                        else {
                            _this.candidateResume = undefined;
                        }
                    });
                };
                CandidateUpdateComponent.prototype.getCandidateDocuments = function () {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (params) { return _this.fileService.getCandidateDocuments(params['id']); }))
                        .subscribe(function (documentCandidate) {
                        _this.candidateDocuments = documentCandidate.filter(function (doc) { return doc.is_resume === _this.IS_DOCUMENT; });
                    });
                };
                CandidateUpdateComponent.prototype.comparer = function (o1, o2) {
                    // if possible compare by object's name property - and not by reference.
                    return o1 && o2 ? o1.name === o2.name : o2 === o2;
                };
                CandidateUpdateComponent.prototype.onFormSubmit = function (candidateUpdateForm) {
                    var _this = this;
                    // this.skillsCandidateService.removeAllSkillsCandidate(this.candidate.id)
                    // .then((value: any) => this.selectedSkills.forEach(skill => this.skillsCandidateService.addSkillCandidate({skill_id: skill.id, candidate_id: this.candidate.id})));
                    (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, this.skillsCandidateService.removeAllSkillsCandidate(this.candidate.id)];
                                case 1:
                                    _a.sent();
                                    return [4 /*yield*/, this.selectedSkills.forEach(function (skill) { return _this.skillsCandidateService.addSkillCandidate({ skill_id: skill.id, candidate_id: _this.candidate.id }); })];
                                case 2:
                                    _a.sent();
                                    return [2 /*return*/];
                            }
                        });
                    }); })();
                    this.candidateService.update(this.candidate);
                };
                CandidateUpdateComponent.prototype.onSuccess = function () {
                    this.selectedPhoto.pending = false;
                    this.selectedPhoto.status = 'ok';
                };
                CandidateUpdateComponent.prototype.onError = function () {
                    this.selectedPhoto.pending = false;
                    this.selectedPhoto.status = 'fail';
                    this.selectedPhoto.src = '';
                };
                CandidateUpdateComponent.prototype.onFileSuccess = function () {
                    this.selectedFile.pending = false;
                    this.selectedFile.status = 'ok';
                };
                CandidateUpdateComponent.prototype.onFileError = function () {
                    this.selectedFile.pending = false;
                    this.selectedFile.status = 'fail';
                    this.selectedFile.src = '';
                };
                CandidateUpdateComponent.prototype.processPhoto = function (photoInput) {
                    var _this = this;
                    var file = photoInput.files[0];
                    var reader = new FileReader();
                    reader.addEventListener('load', function (event) {
                        _this.photoInput.nativeElement.value = null;
                        _this.selectedPhoto = new ImageSnippet(event.target.result, file);
                        _this.selectedPhoto.pending = true;
                        _this.imageService.uploadPhoto(_this.selectedPhoto.file, _this.candidate.id).subscribe(function (res) {
                            _this.isPhotoDeleted = false;
                            console.log(res);
                            _this.onSuccess();
                        }, function (err) {
                            _this.onError();
                        });
                    });
                    reader.readAsDataURL(file);
                };
                CandidateUpdateComponent.prototype.removePhoto = function () {
                    var _this = this;
                    this.imageService.deletePhoto(this.candidate.id).subscribe(function (res) {
                        _this.isPhotoDeleted = true;
                        _this.selectedPhoto = undefined;
                        _this.candidate.photo = undefined;
                        console.log(res);
                    }, function (err) {
                        console.log(err);
                    });
                };
                CandidateUpdateComponent.prototype.processFile = function (fileInput, fileType) {
                    var _this = this;
                    var file = fileInput.files[0];
                    var reader = new FileReader();
                    reader.addEventListener('load', function (event) {
                        _this.fileInputResume.nativeElement.value = null;
                        _this.fileInputDocument.nativeElement.value = null;
                        if (fileType === _this.IS_RESUME) {
                            _this.selectedFile = new FileSnippet(event.target.result, file);
                            _this.fileService.uploadFile(_this.selectedFile.file, _this.candidate.id, fileType).subscribe(function (res) {
                                _this.onFileSuccess();
                            }, function (err) {
                                _this.onFileError();
                            });
                        }
                        else if (fileType === _this.IS_DOCUMENT) {
                            _this.selectedFileDocument = new FileSnippet(event.target.result, file);
                            _this.fileService.uploadFile(_this.selectedFileDocument.file, _this.candidate.id, fileType).subscribe(function (res) {
                                _this.getCandidateDocuments();
                            }, function (err) {
                            });
                        }
                    });
                    reader.readAsDataURL(file);
                };
                CandidateUpdateComponent.prototype.removeFile = function ($event) {
                    var _this = this;
                    $event.preventDefault();
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (params) { return _this.fileService.getCandidateDocuments(params['id']); }))
                        .subscribe(function (documentCandidate) {
                        var candidateResume = documentCandidate.filter(function (doc) { return doc.is_resume = _this.IS_RESUME; }).pop();
                        _this.fileService.deleteFileByName(_this.candidate.id, candidateResume.id).subscribe(function (res) {
                            _this.selectedFile = undefined;
                            _this.candidateResume = undefined;
                            _this.getCandidateDocuments();
                            console.log(res);
                        }, function (err) {
                            console.log(err);
                        });
                    });
                };
                CandidateUpdateComponent.prototype.removeCandidateDocumentByName = function ($event, fileName) {
                    var _this = this;
                    $event.preventDefault();
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (params) { return _this.fileService.getCandidateDocuments(params['id']); }))
                        .subscribe(function (documentCandidate) {
                        _this.deletedCandidateDocument = documentCandidate.filter(function (doc) { return doc.file_name === fileName; }).pop();
                        _this.fileService.deleteFileByName(_this.candidate.id, _this.deletedCandidateDocument.id).subscribe(function (res) {
                            _this.getCandidateDocuments();
                        }, function (err) {
                            console.log(err);
                        });
                    });
                };
                CandidateUpdateComponent.prototype.openModal = function (template) {
                    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
                };
                CandidateUpdateComponent.prototype.confirm = function () {
                    var _this = this;
                    this.candidateService.delete(this.candidate.id).then(function (item) {
                        _this.router.navigate(["/all-candidates"]);
                    });
                    this.modalRef.hide();
                };
                CandidateUpdateComponent.prototype.decline = function () {
                    this.modalRef.hide();
                };
                return CandidateUpdateComponent;
            }());
            CandidateUpdateComponent.ctorParameters = function () { return [
                { type: _services_candidate_service__WEBPACK_IMPORTED_MODULE_2__["CandidateService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
                { type: _services_cities_service__WEBPACK_IMPORTED_MODULE_5__["CitiesService"] },
                { type: _services_skills_service__WEBPACK_IMPORTED_MODULE_6__["SkillsService"] },
                { type: _services_skills_candidate_service__WEBPACK_IMPORTED_MODULE_7__["SkillsCandidateService"] },
                { type: _services_image_service__WEBPACK_IMPORTED_MODULE_9__["ImageService"] },
                { type: _services_file_service__WEBPACK_IMPORTED_MODULE_10__["FileService"] },
                { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_11__["BsModalService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInputResume', { static: false })
            ], CandidateUpdateComponent.prototype, "fileInputResume", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInputDocument', { static: false })
            ], CandidateUpdateComponent.prototype, "fileInputDocument", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('photoInput', { static: false })
            ], CandidateUpdateComponent.prototype, "photoInput", void 0);
            CandidateUpdateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-candidate-update',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./candidate-update.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/candidate-update/candidate-update.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./candidate-update.component.css */ "./src/app/components/candidate-update/candidate-update.component.css")).default]
                })
            ], CandidateUpdateComponent);
            /***/ 
        }),
        /***/ "./src/app/components/candidate/candidate.component.css": 
        /*!**************************************************************!*\
          !*** ./src/app/components/candidate/candidate.component.css ***!
          \**************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".item-card {\n  border: 1px solid rgba(0,0,0,.125);\n  padding-bottom: 20px;\n  padding-top: 20px;\n  margin: 10px;\n  text-align: -moz-center;\n  text-align: -webkit-center;\n  align-items: center;\n}\n.item-wrap {\n  display: inline-block;\n  max-width: 22%;\n  width: 100%;\n}\n.entity-item-round {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  text-align: center;\n  margin-top: 5px;\n  width: 150px;\n  height: 150px;\n  background-color:\n    #f5f5f5;\n  border-radius: 50% !important;\n  border: 1px solid\n  #eaeaea;\n}\n.entity-item-round i {\n  line-height: 65px;\n  font-size: 40px;\n  color:\n    #C4C4C4;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jYW5kaWRhdGUvY2FuZGlkYXRlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQ0FBa0M7RUFDbEMsb0JBQW9CO0VBQ3BCLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osdUJBQXVCO0VBQ3ZCLDBCQUEwQjtFQUMxQixtQkFBbUI7QUFDckI7QUFDQTtFQUNFLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsV0FBVztBQUNiO0FBQ0E7RUFDRSxhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLFlBQVk7RUFDWixhQUFhO0VBQ2I7V0FDUztFQUNULDZCQUE2QjtFQUM3QjtTQUNPO0FBQ1Q7QUFDQTtFQUNFLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2Y7V0FDUztBQUNYIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jYW5kaWRhdGUvY2FuZGlkYXRlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaXRlbS1jYXJkIHtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLDAsMCwuMTI1KTtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gIHBhZGRpbmctdG9wOiAyMHB4O1xuICBtYXJnaW46IDEwcHg7XG4gIHRleHQtYWxpZ246IC1tb3otY2VudGVyO1xuICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5pdGVtLXdyYXAge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1heC13aWR0aDogMjIlO1xuICB3aWR0aDogMTAwJTtcbn1cbi5lbnRpdHktaXRlbS1yb3VuZCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgd2lkdGg6IDE1MHB4O1xuICBoZWlnaHQ6IDE1MHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOlxuICAgICNmNWY1ZjU7XG4gIGJvcmRlci1yYWRpdXM6IDUwJSAhaW1wb3J0YW50O1xuICBib3JkZXI6IDFweCBzb2xpZFxuICAjZWFlYWVhO1xufVxuLmVudGl0eS1pdGVtLXJvdW5kIGkge1xuICBsaW5lLWhlaWdodDogNjVweDtcbiAgZm9udC1zaXplOiA0MHB4O1xuICBjb2xvcjpcbiAgICAjQzRDNEM0O1xufVxuIl19 */");
            /***/ 
        }),
        /***/ "./src/app/components/candidate/candidate.component.ts": 
        /*!*************************************************************!*\
          !*** ./src/app/components/candidate/candidate.component.ts ***!
          \*************************************************************/
        /*! exports provided: CandidateComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CandidateComponent", function () { return CandidateComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _services_candidate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/candidate.service */ "./src/app/services/candidate.service.ts");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            /* harmony import */ var _services_pager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/pager.service */ "./src/app/services/pager.service.ts");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            var CandidateComponent = /** @class */ (function () {
                function CandidateComponent(http, pagerService, candidateService) {
                    this.http = http;
                    this.pagerService = pagerService;
                    this.candidateService = candidateService;
                    // pager object
                    this.pager = {};
                    this.candidateUrl = 'web/candidates?per-page=100000&page-count=44';
                }
                CandidateComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.getCandidates();
                    this.http.get(this.candidateUrl).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (response) { return response.json(); }))
                        .subscribe(function (data) {
                        // set items to json response
                        _this.allItems = data;
                        // initialize to page 1
                        _this.setPage(1);
                    });
                };
                CandidateComponent.prototype.getCandidates = function () {
                    var _this = this;
                    this.candidateService.getData().then(function (candidates) { return _this.candidates = candidates; });
                };
                CandidateComponent.prototype.setPage = function (page) {
                    // get pager object from service
                    this.pager = this.pagerService.getPager(this.allItems.length, page);
                    // get current page of items
                    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
                };
                return CandidateComponent;
            }());
            CandidateComponent.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_3__["Http"] },
                { type: _services_pager_service__WEBPACK_IMPORTED_MODULE_4__["PagerService"] },
                { type: _services_candidate_service__WEBPACK_IMPORTED_MODULE_2__["CandidateService"] }
            ]; };
            CandidateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-candidate',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./candidate.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/candidate/candidate.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./candidate.component.css */ "./src/app/components/candidate/candidate.component.css")).default]
                })
            ], CandidateComponent);
            /***/ 
        }),
        /***/ "./src/app/components/connect-customer-modal/connect-customer-modal.component.css": 
        /*!****************************************************************************************!*\
          !*** ./src/app/components/connect-customer-modal/connect-customer-modal.component.css ***!
          \****************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".modal-header {\n    background: #01919D;\n    color: white;\n    padding: 0.5rem 1rem!important;\n    align-items: center!important;\n}\n.modal-header h5 {\n    font-size: 13px;\n    margin: 0px;\n}\n.custom-control {\n    position: relative;\n    display: block;\n    min-height: 1.5rem;\n    padding-left: 1.5rem;\n}\n/******BUTTONS******/\n.btn-crm {\n    border-radius: 0!important;\n    height: 30px;\n    min-width: 120px;\n  }\n.btn-crm-primary{\n    font-size: 11px!important;\n    font-weight: 700!important;\n    border-radius: 0!important;\n    border: 1px solid #01747f !important;\n    height: 30px;\n    background-color: #01919D!important;\n    color: white!important;\n  }\n.btn-crm-default{\n    font-size: 11px!important;\n    font-weight: 700!important;\n    border-radius: 0!important;\n    border: 1px solid #aeaeae !important;\n    height: 30px;\n  }\n.btn-crm-default:hover {\n    background: #f5f5f5;\n    outline: 0;\n    box-shadow: 0 0 0 0.15rem rgba(255, 177, 6, 0.25);\n  }\n.btn-crm-default_red {\n    border-color: red !important;\n    color: red !important;\n  }\n/******UPLOAD RESUME AND DOCUMENTS******/\n.fa-add-document:before {\n      content: \"\\f055\";\n      line-height: 1;\n      padding: 1.5px;\n      color: #58d5e0;\n    }\n.add-document-button,.remove-document-button {\n      color: #58d5e0;\n    }\n.add-document-label {\n      display: block;\n      width: 100%;\n      padding: .25rem;\n      clear: both;\n      font-weight: 400;\n      color: #58d5e0;\n      text-align: inherit;\n      white-space: nowrap;\n      background-color: transparent;\n      border: 0;\n    }\n.add-document-label span {\n      margin-left: 10px;\n      color: #58d5e0;\n    }\n.input-document {\n      display: none;\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb25uZWN0LWN1c3RvbWVyLW1vZGFsL2Nvbm5lY3QtY3VzdG9tZXItbW9kYWwuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osOEJBQThCO0lBQzlCLDZCQUE2QjtBQUNqQztBQUNBO0lBQ0ksZUFBZTtJQUNmLFdBQVc7QUFDZjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsb0JBQW9CO0FBQ3hCO0FBQ0Esb0JBQW9CO0FBQ3BCO0lBQ0ksMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixnQkFBZ0I7RUFDbEI7QUFDQTtJQUNFLHlCQUF5QjtJQUN6QiwwQkFBMEI7SUFDMUIsMEJBQTBCO0lBQzFCLG9DQUFvQztJQUNwQyxZQUFZO0lBQ1osbUNBQW1DO0lBQ25DLHNCQUFzQjtFQUN4QjtBQUNBO0lBQ0UseUJBQXlCO0lBQ3pCLDBCQUEwQjtJQUMxQiwwQkFBMEI7SUFDMUIsb0NBQW9DO0lBQ3BDLFlBQVk7RUFDZDtBQUNBO0lBQ0UsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixpREFBaUQ7RUFDbkQ7QUFDQTtJQUNFLDRCQUE0QjtJQUM1QixxQkFBcUI7RUFDdkI7QUFDRSx3Q0FBd0M7QUFDeEM7TUFDRSxnQkFBZ0I7TUFDaEIsY0FBYztNQUNkLGNBQWM7TUFDZCxjQUFjO0lBQ2hCO0FBQ0E7TUFDRSxjQUFjO0lBQ2hCO0FBQ0E7TUFDRSxjQUFjO01BQ2QsV0FBVztNQUNYLGVBQWU7TUFDZixXQUFXO01BQ1gsZ0JBQWdCO01BQ2hCLGNBQWM7TUFDZCxtQkFBbUI7TUFDbkIsbUJBQW1CO01BQ25CLDZCQUE2QjtNQUM3QixTQUFTO0lBQ1g7QUFDQTtNQUNFLGlCQUFpQjtNQUNqQixjQUFjO0lBQ2hCO0FBQ0E7TUFDRSxhQUFhO0lBQ2YiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2Nvbm5lY3QtY3VzdG9tZXItbW9kYWwvY29ubmVjdC1jdXN0b21lci1tb2RhbC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1vZGFsLWhlYWRlciB7XG4gICAgYmFja2dyb3VuZDogIzAxOTE5RDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogMC41cmVtIDFyZW0haW1wb3J0YW50O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIhaW1wb3J0YW50O1xufVxuLm1vZGFsLWhlYWRlciBoNSB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIG1hcmdpbjogMHB4O1xufVxuLmN1c3RvbS1jb250cm9sIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWluLWhlaWdodDogMS41cmVtO1xuICAgIHBhZGRpbmctbGVmdDogMS41cmVtO1xufVxuLyoqKioqKkJVVFRPTlMqKioqKiovXG4uYnRuLWNybSB7XG4gICAgYm9yZGVyLXJhZGl1czogMCFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAzMHB4O1xuICAgIG1pbi13aWR0aDogMTIwcHg7XG4gIH1cbiAgLmJ0bi1jcm0tcHJpbWFyeXtcbiAgICBmb250LXNpemU6IDExcHghaW1wb3J0YW50O1xuICAgIGZvbnQtd2VpZ2h0OiA3MDAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1yYWRpdXM6IDAhaW1wb3J0YW50O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMTc0N2YgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDMwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAxOTE5RCFpbXBvcnRhbnQ7XG4gICAgY29sb3I6IHdoaXRlIWltcG9ydGFudDtcbiAgfVxuICAuYnRuLWNybS1kZWZhdWx0e1xuICAgIGZvbnQtc2l6ZTogMTFweCFpbXBvcnRhbnQ7XG4gICAgZm9udC13ZWlnaHQ6IDcwMCFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLXJhZGl1czogMCFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2FlYWVhZSAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMzBweDtcbiAgfVxuICAuYnRuLWNybS1kZWZhdWx0OmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjZjVmNWY1O1xuICAgIG91dGxpbmU6IDA7XG4gICAgYm94LXNoYWRvdzogMCAwIDAgMC4xNXJlbSByZ2JhKDI1NSwgMTc3LCA2LCAwLjI1KTtcbiAgfVxuICAuYnRuLWNybS1kZWZhdWx0X3JlZCB7XG4gICAgYm9yZGVyLWNvbG9yOiByZWQgIWltcG9ydGFudDtcbiAgICBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XG4gIH1cbiAgICAvKioqKioqVVBMT0FEIFJFU1VNRSBBTkQgRE9DVU1FTlRTKioqKioqL1xuICAgIC5mYS1hZGQtZG9jdW1lbnQ6YmVmb3JlIHtcbiAgICAgIGNvbnRlbnQ6IFwiXFxmMDU1XCI7XG4gICAgICBsaW5lLWhlaWdodDogMTtcbiAgICAgIHBhZGRpbmc6IDEuNXB4O1xuICAgICAgY29sb3I6ICM1OGQ1ZTA7XG4gICAgfVxuICAgIC5hZGQtZG9jdW1lbnQtYnV0dG9uLC5yZW1vdmUtZG9jdW1lbnQtYnV0dG9uIHtcbiAgICAgIGNvbG9yOiAjNThkNWUwO1xuICAgIH1cbiAgICAuYWRkLWRvY3VtZW50LWxhYmVsIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBwYWRkaW5nOiAuMjVyZW07XG4gICAgICBjbGVhcjogYm90aDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICBjb2xvcjogIzU4ZDVlMDtcbiAgICAgIHRleHQtYWxpZ246IGluaGVyaXQ7XG4gICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgICBib3JkZXI6IDA7XG4gICAgfVxuICAgIC5hZGQtZG9jdW1lbnQtbGFiZWwgc3BhbiB7XG4gICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgIGNvbG9yOiAjNThkNWUwO1xuICAgIH1cbiAgICAuaW5wdXQtZG9jdW1lbnQge1xuICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9Il19 */");
            /***/ 
        }),
        /***/ "./src/app/components/connect-customer-modal/connect-customer-modal.component.ts": 
        /*!***************************************************************************************!*\
          !*** ./src/app/components/connect-customer-modal/connect-customer-modal.component.ts ***!
          \***************************************************************************************/
        /*! exports provided: ConnectCustomerModalComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectCustomerModalComponent", function () { return ConnectCustomerModalComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
            /* harmony import */ var src_app_services_customer_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/customer.service */ "./src/app/services/customer.service.ts");
            /* harmony import */ var src_app_services_contacts_customer_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/contacts-customer.service */ "./src/app/services/contacts-customer.service.ts");
            /* harmony import */ var src_app_services_file_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/file.service */ "./src/app/services/file.service.ts");
            /* harmony import */ var src_app_services_mail_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/mail.service */ "./src/app/services/mail.service.ts");
            /* harmony import */ var src_app_services_contacts_candidate_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/contacts-candidate.service */ "./src/app/services/contacts-candidate.service.ts");
            var FileSnippet = /** @class */ (function () {
                function FileSnippet(src, file) {
                    this.src = src;
                    this.file = file;
                    this.pending = false;
                    this.status = 'init';
                }
                return FileSnippet;
            }());
            var ConnectCustomerModalComponent = /** @class */ (function () {
                function ConnectCustomerModalComponent(bsModalRef, customerService, contactsCustomerService, fileService, mailService, contactsCandidateService) {
                    this.bsModalRef = bsModalRef;
                    this.customerService = customerService;
                    this.contactsCustomerService = contactsCustomerService;
                    this.fileService = fileService;
                    this.mailService = mailService;
                    this.contactsCandidateService = contactsCandidateService;
                    this.customers_names = [];
                    this.choosen_customer_contacts_values = [];
                    this.transformed_customer_contacts_names = [];
                    this.customer_contacts_values = [];
                    this.IS_DOCUMENT = 0;
                }
                ConnectCustomerModalComponent.prototype.ngOnInit = function () {
                    this.getCustomers();
                };
                ConnectCustomerModalComponent.prototype.getCustomers = function () {
                    var _this = this;
                    this.customerService.getData().then(function (customers) {
                        _this.customers_names = customers.map(function (customer) { return customer.name; });
                        console.log(_this.customers_names);
                    });
                };
                ConnectCustomerModalComponent.prototype.getCustomerIdByName = function (customerName, successCalback) {
                    this.customerService.getData().then(function (customers) {
                        successCalback(customers.find(function (customer) { return customer.name == customerName; }).id);
                    });
                };
                ConnectCustomerModalComponent.prototype.getAttachedDocuments = function () {
                    var _this = this;
                    this.fileService.getCandidateDocuments(this.selectedCandidateId).then(function (candidateDocuments) {
                        _this.attached_documents = candidateDocuments.map(function (candidateDocument) { return candidateDocument.file_name; });
                    });
                };
                ConnectCustomerModalComponent.prototype.onSelect = function (event) {
                    var _this = this;
                    this.getCustomerIdByName(event.item, function (id) {
                        _this.contactsCustomerService.getCustomerContacts(id).then(function (customerContacts) {
                            customerContacts.forEach(function (customerContact) { return _this.customer_contacts_values.push({ value: customerContact.value, name: customerContact.contact_type }); });
                            _this.customer_contacts_names = customerContacts.map(function (customerContact) { return customerContact.contact_type; });
                            _this.customer_contacts_names.forEach(function (contact_name) { return _this.contactsCandidateService.getCandidateContactTypeName(contact_name).then(function (contact) { return _this.transformed_customer_contacts_names.push({ name: contact.name, id: contact.id }); }); });
                            _this.getAttachedDocuments();
                        });
                    });
                };
                ConnectCustomerModalComponent.prototype.onSelectCommunication = function (event) {
                    this.choosen_customer_contacts_values = this.customer_contacts_values.filter(function (customerContactValue) { return customerContactValue.name == event.item.id; });
                };
                ConnectCustomerModalComponent.prototype.onFormSubmit = function (sendCustomer) {
                    this.mailService.sendMail(this.selected_customer, undefined, this.selected_contact_name, this.selected_contact_value, this.selected_attached_document, this.customer_letter_body);
                    this.bsModalRef.hide();
                };
                ConnectCustomerModalComponent.prototype.addDocument = function (fileInput) {
                    var _this = this;
                    var file = fileInput.files[0];
                    var reader = new FileReader();
                    reader.addEventListener('load', function (event) {
                        _this.fileInputDocument.nativeElement.value = null;
                        _this.selectedAttachedFile = new FileSnippet(event.target.result, file);
                        _this.fileService.uploadFile(_this.selectedAttachedFile.file, _this.selectedCandidateId, _this.IS_DOCUMENT).subscribe(function (res) {
                            _this.getAttachedDocuments();
                            console.log(res);
                        }, function (err) {
                            console.log(err);
                        });
                    });
                    reader.readAsDataURL(file);
                };
                return ConnectCustomerModalComponent;
            }());
            ConnectCustomerModalComponent.ctorParameters = function () { return [
                { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__["BsModalRef"] },
                { type: src_app_services_customer_service__WEBPACK_IMPORTED_MODULE_3__["CustomerService"] },
                { type: src_app_services_contacts_customer_service__WEBPACK_IMPORTED_MODULE_4__["ContactsCustomerService"] },
                { type: src_app_services_file_service__WEBPACK_IMPORTED_MODULE_5__["FileService"] },
                { type: src_app_services_mail_service__WEBPACK_IMPORTED_MODULE_6__["MailService"] },
                { type: src_app_services_contacts_candidate_service__WEBPACK_IMPORTED_MODULE_7__["ContactsCandidateService"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInputDocument', { static: false })
            ], ConnectCustomerModalComponent.prototype, "fileInputDocument", void 0);
            ConnectCustomerModalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-connect-customer-modal',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./connect-customer-modal.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/connect-customer-modal/connect-customer-modal.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./connect-customer-modal.component.css */ "./src/app/components/connect-customer-modal/connect-customer-modal.component.css")).default]
                })
            ], ConnectCustomerModalComponent);
            /***/ 
        }),
        /***/ "./src/app/components/detail/detail.component.css": 
        /*!********************************************************!*\
          !*** ./src/app/components/detail/detail.component.css ***!
          \********************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGV0YWlsL2RldGFpbC5jb21wb25lbnQuY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/components/detail/detail.component.ts": 
        /*!*******************************************************!*\
          !*** ./src/app/components/detail/detail.component.ts ***!
          \*******************************************************/
        /*! exports provided: DetailComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailComponent", function () { return DetailComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _services_candidate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/candidate.service */ "./src/app/services/candidate.service.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            var DetailComponent = /** @class */ (function () {
                function DetailComponent(candidateService, route, location, router) {
                    this.candidateService = candidateService;
                    this.route = route;
                    this.location = location;
                    this.router = router;
                }
                DetailComponent.prototype.ngOnInit = function () {
                    this.getCandidate();
                };
                DetailComponent.prototype.getCandidate = function () {
                    var _this = this;
                    //    this.route.params.pipe(switchMap((params: Params) => this.candidateService.getDetail(params['id'])));
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (params) { return _this.candidateService.getDetail(params['id']); }))
                        .subscribe(function (candidate) { return _this.candidate = candidate; });
                };
                DetailComponent.prototype.goBack = function () {
                    this.location.back();
                };
                return DetailComponent;
            }());
            DetailComponent.ctorParameters = function () { return [
                { type: _services_candidate_service__WEBPACK_IMPORTED_MODULE_2__["CandidateService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
                { type: _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
            ]; };
            DetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-detail',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./detail.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/detail/detail.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./detail.component.css */ "./src/app/components/detail/detail.component.css")).default]
                })
            ], DetailComponent);
            /***/ 
        }),
        /***/ "./src/app/components/search-candidate/search-candidate.component.css": 
        /*!****************************************************************************!*\
          !*** ./src/app/components/search-candidate/search-candidate.component.css ***!
          \****************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".candidate-search {\n    padding: 42px 10px 31px 30px!important;\n  }\n  #searchCandidate {\n    width: 35rem;\n  }\n  .candidate-search input {\n    background: #F6F6F6;\n    border: 1px solid #E1E1E1;\n    font-size: 14px;\n    padding: 12px;\n    height: 40px;\n  }\n  .candidate-search input::-webkit-input-placeholder {\n    text-transform: uppercase;\n    color: #757575;\n  }\n  .candidate-search input::-moz-placeholder {\n    text-transform: uppercase;\n    color: #757575;\n  }\n  .candidate-search input::-ms-input-placeholder {\n    text-transform: uppercase;\n    color: #757575;\n  }\n  .candidate-search input::placeholder {\n    text-transform: uppercase;\n    color: #757575;\n  }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zZWFyY2gtY2FuZGlkYXRlL3NlYXJjaC1jYW5kaWRhdGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHNDQUFzQztFQUN4QztFQUNBO0lBQ0UsWUFBWTtFQUNkO0VBQ0E7SUFDRSxtQkFBbUI7SUFDbkIseUJBQXlCO0lBQ3pCLGVBQWU7SUFDZixhQUFhO0lBQ2IsWUFBWTtFQUNkO0VBQ0E7SUFDRSx5QkFBeUI7SUFDekIsY0FBYztFQUNoQjtFQUhBO0lBQ0UseUJBQXlCO0lBQ3pCLGNBQWM7RUFDaEI7RUFIQTtJQUNFLHlCQUF5QjtJQUN6QixjQUFjO0VBQ2hCO0VBSEE7SUFDRSx5QkFBeUI7SUFDekIsY0FBYztFQUNoQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2VhcmNoLWNhbmRpZGF0ZS9zZWFyY2gtY2FuZGlkYXRlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FuZGlkYXRlLXNlYXJjaCB7XG4gICAgcGFkZGluZzogNDJweCAxMHB4IDMxcHggMzBweCFpbXBvcnRhbnQ7XG4gIH1cbiAgI3NlYXJjaENhbmRpZGF0ZSB7XG4gICAgd2lkdGg6IDM1cmVtO1xuICB9XG4gIC5jYW5kaWRhdGUtc2VhcmNoIGlucHV0IHtcbiAgICBiYWNrZ3JvdW5kOiAjRjZGNkY2O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFMUUxRTE7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIHBhZGRpbmc6IDEycHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICB9XG4gIC5jYW5kaWRhdGUtc2VhcmNoIGlucHV0OjpwbGFjZWhvbGRlciB7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjogIzc1NzU3NTtcbiAgfVxuIl19 */");
            /***/ 
        }),
        /***/ "./src/app/components/search-candidate/search-candidate.component.ts": 
        /*!***************************************************************************!*\
          !*** ./src/app/components/search-candidate/search-candidate.component.ts ***!
          \***************************************************************************/
        /*! exports provided: SearchCandidateComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchCandidateComponent", function () { return SearchCandidateComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            // import { debounceTime, map } from 'rxjs/operators'; 
            // import 'rxjs/add/operator/debounceTime';
            var SearchCandidateComponent = /** @class */ (function () {
                function SearchCandidateComponent() {
                    this.search = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
                }
                SearchCandidateComponent.prototype.ngOnInit = function () {
                };
                SearchCandidateComponent.prototype.ngAfterViewInit = function () {
                    var _this = this;
                    // server-side search
                    Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["fromEvent"])(this.input.nativeElement, 'keyup')
                        .pipe(
                    // filter(Boolean),
                    Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["debounceTime"])(1500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["distinctUntilChanged"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (text) {
                        _this.search.emit(_this.input.nativeElement.value);
                    }))
                        .subscribe();
                };
                return SearchCandidateComponent;
            }());
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
            ], SearchCandidateComponent.prototype, "search", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('input', { static: false })
            ], SearchCandidateComponent.prototype, "input", void 0);
            SearchCandidateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-search-candidate',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./search-candidate.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/search-candidate/search-candidate.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./search-candidate.component.css */ "./src/app/components/search-candidate/search-candidate.component.css")).default]
                })
            ], SearchCandidateComponent);
            /***/ 
        }),
        /***/ "./src/app/components/send-candidate-modal/send-candidate-modal.component.css": 
        /*!************************************************************************************!*\
          !*** ./src/app/components/send-candidate-modal/send-candidate-modal.component.css ***!
          \************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".modal-header {\n    background: #01919D;\n    color: white;\n    padding: 0.5rem 1rem!important;\n    align-items: center!important;\n}\n.modal-header h5 {\n    font-size: 13px;\n    margin: 0px;\n}\n.custom-control {\n    position: relative;\n    display: block;\n    min-height: 1.5rem;\n    padding-left: 1.5rem;\n}\n/******BUTTONS******/\n.btn-crm {\n    border-radius: 0!important;\n    height: 30px;\n    min-width: 120px;\n  }\n.btn-crm-primary{\n    font-size: 11px!important;\n    font-weight: 700!important;\n    border-radius: 0!important;\n    border: 1px solid #01747f !important;\n    height: 30px;\n    background-color: #01919D!important;\n    color: white!important;\n  }\n.btn-crm-default{\n    font-size: 11px!important;\n    font-weight: 700!important;\n    border-radius: 0!important;\n    border: 1px solid #aeaeae !important;\n    height: 30px;\n  }\n.btn-crm-default:hover {\n    background: #f5f5f5;\n    outline: 0;\n    box-shadow: 0 0 0 0.15rem rgba(255, 177, 6, 0.25);\n  }\n.btn-crm-default_red {\n    border-color: red !important;\n    color: red !important;\n  }\n/******UPLOAD RESUME AND DOCUMENTS******/\n.fa-add-document:before {\n      content: \"\\f055\";\n      line-height: 1;\n      padding: 1.5px;\n      color: #58d5e0;\n    }\n.add-document-button,.remove-document-button {\n      color: #58d5e0;\n    }\n.add-document-label {\n      display: block;\n      width: 100%;\n      padding: .25rem;\n      clear: both;\n      font-weight: 400;\n      color: #58d5e0;\n      text-align: inherit;\n      white-space: nowrap;\n      background-color: transparent;\n      border: 0;\n    }\n.add-document-label span {\n      margin-left: 10px;\n      color: #58d5e0;\n    }\n.input-document {\n      display: none;\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zZW5kLWNhbmRpZGF0ZS1tb2RhbC9zZW5kLWNhbmRpZGF0ZS1tb2RhbC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksbUJBQW1CO0lBQ25CLFlBQVk7SUFDWiw4QkFBOEI7SUFDOUIsNkJBQTZCO0FBQ2pDO0FBQ0E7SUFDSSxlQUFlO0lBQ2YsV0FBVztBQUNmO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixvQkFBb0I7QUFDeEI7QUFDQSxvQkFBb0I7QUFDcEI7SUFDSSwwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLGdCQUFnQjtFQUNsQjtBQUNBO0lBQ0UseUJBQXlCO0lBQ3pCLDBCQUEwQjtJQUMxQiwwQkFBMEI7SUFDMUIsb0NBQW9DO0lBQ3BDLFlBQVk7SUFDWixtQ0FBbUM7SUFDbkMsc0JBQXNCO0VBQ3hCO0FBQ0E7SUFDRSx5QkFBeUI7SUFDekIsMEJBQTBCO0lBQzFCLDBCQUEwQjtJQUMxQixvQ0FBb0M7SUFDcEMsWUFBWTtFQUNkO0FBQ0E7SUFDRSxtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLGlEQUFpRDtFQUNuRDtBQUNBO0lBQ0UsNEJBQTRCO0lBQzVCLHFCQUFxQjtFQUN2QjtBQUNFLHdDQUF3QztBQUN4QztNQUNFLGdCQUFnQjtNQUNoQixjQUFjO01BQ2QsY0FBYztNQUNkLGNBQWM7SUFDaEI7QUFDQTtNQUNFLGNBQWM7SUFDaEI7QUFDQTtNQUNFLGNBQWM7TUFDZCxXQUFXO01BQ1gsZUFBZTtNQUNmLFdBQVc7TUFDWCxnQkFBZ0I7TUFDaEIsY0FBYztNQUNkLG1CQUFtQjtNQUNuQixtQkFBbUI7TUFDbkIsNkJBQTZCO01BQzdCLFNBQVM7SUFDWDtBQUNBO01BQ0UsaUJBQWlCO01BQ2pCLGNBQWM7SUFDaEI7QUFDQTtNQUNFLGFBQWE7SUFDZiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2VuZC1jYW5kaWRhdGUtbW9kYWwvc2VuZC1jYW5kaWRhdGUtbW9kYWwuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tb2RhbC1oZWFkZXIge1xuICAgIGJhY2tncm91bmQ6ICMwMTkxOUQ7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHBhZGRpbmc6IDAuNXJlbSAxcmVtIWltcG9ydGFudDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyIWltcG9ydGFudDtcbn1cbi5tb2RhbC1oZWFkZXIgaDUge1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBtYXJnaW46IDBweDtcbn1cbi5jdXN0b20tY29udHJvbCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1pbi1oZWlnaHQ6IDEuNXJlbTtcbiAgICBwYWRkaW5nLWxlZnQ6IDEuNXJlbTtcbn1cbi8qKioqKipCVVRUT05TKioqKioqL1xuLmJ0bi1jcm0ge1xuICAgIGJvcmRlci1yYWRpdXM6IDAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMzBweDtcbiAgICBtaW4td2lkdGg6IDEyMHB4O1xuICB9XG4gIC5idG4tY3JtLXByaW1hcnl7XG4gICAgZm9udC1zaXplOiAxMXB4IWltcG9ydGFudDtcbiAgICBmb250LXdlaWdodDogNzAwIWltcG9ydGFudDtcbiAgICBib3JkZXItcmFkaXVzOiAwIWltcG9ydGFudDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMDE3NDdmICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAzMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMTkxOUQhaW1wb3J0YW50O1xuICAgIGNvbG9yOiB3aGl0ZSFpbXBvcnRhbnQ7XG4gIH1cbiAgLmJ0bi1jcm0tZGVmYXVsdHtcbiAgICBmb250LXNpemU6IDExcHghaW1wb3J0YW50O1xuICAgIGZvbnQtd2VpZ2h0OiA3MDAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1yYWRpdXM6IDAhaW1wb3J0YW50O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNhZWFlYWUgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDMwcHg7XG4gIH1cbiAgLmJ0bi1jcm0tZGVmYXVsdDpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogI2Y1ZjVmNTtcbiAgICBvdXRsaW5lOiAwO1xuICAgIGJveC1zaGFkb3c6IDAgMCAwIDAuMTVyZW0gcmdiYSgyNTUsIDE3NywgNiwgMC4yNSk7XG4gIH1cbiAgLmJ0bi1jcm0tZGVmYXVsdF9yZWQge1xuICAgIGJvcmRlci1jb2xvcjogcmVkICFpbXBvcnRhbnQ7XG4gICAgY29sb3I6IHJlZCAhaW1wb3J0YW50O1xuICB9XG4gICAgLyoqKioqKlVQTE9BRCBSRVNVTUUgQU5EIERPQ1VNRU5UUyoqKioqKi9cbiAgICAuZmEtYWRkLWRvY3VtZW50OmJlZm9yZSB7XG4gICAgICBjb250ZW50OiBcIlxcZjA1NVwiO1xuICAgICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgICBwYWRkaW5nOiAxLjVweDtcbiAgICAgIGNvbG9yOiAjNThkNWUwO1xuICAgIH1cbiAgICAuYWRkLWRvY3VtZW50LWJ1dHRvbiwucmVtb3ZlLWRvY3VtZW50LWJ1dHRvbiB7XG4gICAgICBjb2xvcjogIzU4ZDVlMDtcbiAgICB9XG4gICAgLmFkZC1kb2N1bWVudC1sYWJlbCB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgcGFkZGluZzogLjI1cmVtO1xuICAgICAgY2xlYXI6IGJvdGg7XG4gICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgY29sb3I6ICM1OGQ1ZTA7XG4gICAgICB0ZXh0LWFsaWduOiBpbmhlcml0O1xuICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgICAgYm9yZGVyOiAwO1xuICAgIH1cbiAgICAuYWRkLWRvY3VtZW50LWxhYmVsIHNwYW4ge1xuICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICBjb2xvcjogIzU4ZDVlMDtcbiAgICB9XG4gICAgLmlucHV0LWRvY3VtZW50IHtcbiAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/components/send-candidate-modal/send-candidate-modal.component.ts": 
        /*!***********************************************************************************!*\
          !*** ./src/app/components/send-candidate-modal/send-candidate-modal.component.ts ***!
          \***********************************************************************************/
        /*! exports provided: SendCandidateModalComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendCandidateModalComponent", function () { return SendCandidateModalComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
            /* harmony import */ var src_app_services_contacts_candidate_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/contacts-candidate.service */ "./src/app/services/contacts-candidate.service.ts");
            /* harmony import */ var src_app_services_file_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/file.service */ "./src/app/services/file.service.ts");
            /* harmony import */ var src_app_services_mail_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/mail.service */ "./src/app/services/mail.service.ts");
            var FileSnippet = /** @class */ (function () {
                function FileSnippet(src, file) {
                    this.src = src;
                    this.file = file;
                    this.pending = false;
                    this.status = 'init';
                }
                return FileSnippet;
            }());
            var SendCandidateModalComponent = /** @class */ (function () {
                function SendCandidateModalComponent(bsModalRef, contactCandidateService, fileService, mailService) {
                    this.bsModalRef = bsModalRef;
                    this.contactCandidateService = contactCandidateService;
                    this.fileService = fileService;
                    this.mailService = mailService;
                    this.candidate_contacts_values = [];
                    this.transformed_candidate_contacts_names = [];
                    this.choosen_candidate_contacts_values = [];
                    this.IS_DOCUMENT = 0;
                }
                SendCandidateModalComponent.prototype.ngOnInit = function () {
                    this.getCandidateContacts();
                    this.getCandidateContactsNames();
                    this.getAttachedDocuments();
                };
                SendCandidateModalComponent.prototype.getCandidateContacts = function () {
                    var _this = this;
                    this.contactCandidateService.getCandidateContacts(this.selectedCandidateId).then(function (contactsValues) { return contactsValues.forEach(function (contactValue) { return _this.candidate_contacts_values.push(contactValue); }); });
                };
                SendCandidateModalComponent.prototype.getCandidateContactsNames = function () {
                    var _this = this;
                    this.contactCandidateService.getCandidateContacts(this.selectedCandidateId).then(function (contacts) { return contacts.forEach(function (contact) { return _this.contactCandidateService.getCandidateContactTypeName(contact.contact_type).then(function (contactName) { return _this.transformed_candidate_contacts_names.push(contactName); }); }); });
                };
                SendCandidateModalComponent.prototype.onSelectCommunication = function (event) {
                    console.log(this.candidate_contacts_values);
                    this.choosen_candidate_contacts_values = this.candidate_contacts_values.filter(function (candidateContactValue) { return candidateContactValue.contact_type == event.item.id; });
                };
                SendCandidateModalComponent.prototype.getAttachedDocuments = function () {
                    var _this = this;
                    this.fileService.getCandidateDocuments(this.selectedCandidateId).then(function (candidateDocuments) {
                        _this.attached_documents = candidateDocuments.map(function (candidateDocument) { return candidateDocument.file_name; });
                    });
                };
                SendCandidateModalComponent.prototype.addDocument = function (fileInput) {
                    var _this = this;
                    var file = fileInput.files[0];
                    var reader = new FileReader();
                    reader.addEventListener('load', function (event) {
                        _this.fileInputDocument.nativeElement.value = null;
                        _this.selectedAttachedFile = new FileSnippet(event.target.result, file);
                        _this.fileService.uploadFile(_this.selectedAttachedFile.file, _this.selectedCandidateId, _this.IS_DOCUMENT).subscribe(function (res) {
                            _this.getAttachedDocuments();
                            console.log(res);
                        }, function (err) {
                            console.log(err);
                        });
                    });
                    reader.readAsDataURL(file);
                };
                SendCandidateModalComponent.prototype.onFormSubmit = function (sendCustomer) {
                    this.mailService.sendMail(undefined, undefined, this.selected_contact_name, this.selected_contact_value, this.selected_attached_document, this.candidate_letter_body);
                    this.bsModalRef.hide();
                };
                return SendCandidateModalComponent;
            }());
            SendCandidateModalComponent.ctorParameters = function () { return [
                { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__["BsModalRef"] },
                { type: src_app_services_contacts_candidate_service__WEBPACK_IMPORTED_MODULE_3__["ContactsCandidateService"] },
                { type: src_app_services_file_service__WEBPACK_IMPORTED_MODULE_4__["FileService"] },
                { type: src_app_services_mail_service__WEBPACK_IMPORTED_MODULE_5__["MailService"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInputDocument', { static: false })
            ], SendCandidateModalComponent.prototype, "fileInputDocument", void 0);
            SendCandidateModalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-send-candidate-modal',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./send-candidate-modal.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/send-candidate-modal/send-candidate-modal.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./send-candidate-modal.component.css */ "./src/app/components/send-candidate-modal/send-candidate-modal.component.css")).default]
                })
            ], SendCandidateModalComponent);
            /***/ 
        }),
        /***/ "./src/app/components/send-customer-modal/send-customer-modal.component.css": 
        /*!**********************************************************************************!*\
          !*** ./src/app/components/send-customer-modal/send-customer-modal.component.css ***!
          \**********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".modal-header {\n    background: #01919D;\n    color: white;\n    padding: 0.5rem 1rem!important;\n    align-items: center!important;\n}\n.modal-header h5 {\n    font-size: 13px;\n    margin: 0px;\n}\n.custom-control {\n    position: relative;\n    display: block;\n    min-height: 1.5rem;\n    padding-left: 1.5rem;\n}\n/******BUTTONS******/\n.btn-crm {\n    border-radius: 0!important;\n    height: 30px;\n    min-width: 120px;\n  }\n.btn-crm-primary{\n    font-size: 11px!important;\n    font-weight: 700!important;\n    border-radius: 0!important;\n    border: 1px solid #01747f !important;\n    height: 30px;\n    background-color: #01919D!important;\n    color: white!important;\n  }\n.btn-crm-default{\n    font-size: 11px!important;\n    font-weight: 700!important;\n    border-radius: 0!important;\n    border: 1px solid #aeaeae !important;\n    height: 30px;\n  }\n.btn-crm-default:hover {\n    background: #f5f5f5;\n    outline: 0;\n    box-shadow: 0 0 0 0.15rem rgba(255, 177, 6, 0.25);\n  }\n.btn-crm-default_red {\n    border-color: red !important;\n    color: red !important;\n  }\n/******UPLOAD RESUME AND DOCUMENTS******/\n.fa-add-document:before {\n      content: \"\\f055\";\n      line-height: 1;\n      padding: 1.5px;\n      color: #58d5e0;\n    }\n.add-document-button,.remove-document-button {\n      color: #58d5e0;\n    }\n.add-document-label {\n      display: block;\n      width: 100%;\n      padding: .25rem;\n      clear: both;\n      font-weight: 400;\n      color: #58d5e0;\n      text-align: inherit;\n      white-space: nowrap;\n      background-color: transparent;\n      border: 0;\n    }\n.add-document-label span {\n      margin-left: 10px;\n      color: #58d5e0;\n    }\n.input-document {\n      display: none;\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zZW5kLWN1c3RvbWVyLW1vZGFsL3NlbmQtY3VzdG9tZXItbW9kYWwuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osOEJBQThCO0lBQzlCLDZCQUE2QjtBQUNqQztBQUNBO0lBQ0ksZUFBZTtJQUNmLFdBQVc7QUFDZjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsb0JBQW9CO0FBQ3hCO0FBQ0Esb0JBQW9CO0FBQ3BCO0lBQ0ksMEJBQTBCO0lBQzFCLFlBQVk7SUFDWixnQkFBZ0I7RUFDbEI7QUFDQTtJQUNFLHlCQUF5QjtJQUN6QiwwQkFBMEI7SUFDMUIsMEJBQTBCO0lBQzFCLG9DQUFvQztJQUNwQyxZQUFZO0lBQ1osbUNBQW1DO0lBQ25DLHNCQUFzQjtFQUN4QjtBQUNBO0lBQ0UseUJBQXlCO0lBQ3pCLDBCQUEwQjtJQUMxQiwwQkFBMEI7SUFDMUIsb0NBQW9DO0lBQ3BDLFlBQVk7RUFDZDtBQUNBO0lBQ0UsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixpREFBaUQ7RUFDbkQ7QUFDQTtJQUNFLDRCQUE0QjtJQUM1QixxQkFBcUI7RUFDdkI7QUFDRSx3Q0FBd0M7QUFDeEM7TUFDRSxnQkFBZ0I7TUFDaEIsY0FBYztNQUNkLGNBQWM7TUFDZCxjQUFjO0lBQ2hCO0FBQ0E7TUFDRSxjQUFjO0lBQ2hCO0FBQ0E7TUFDRSxjQUFjO01BQ2QsV0FBVztNQUNYLGVBQWU7TUFDZixXQUFXO01BQ1gsZ0JBQWdCO01BQ2hCLGNBQWM7TUFDZCxtQkFBbUI7TUFDbkIsbUJBQW1CO01BQ25CLDZCQUE2QjtNQUM3QixTQUFTO0lBQ1g7QUFDQTtNQUNFLGlCQUFpQjtNQUNqQixjQUFjO0lBQ2hCO0FBQ0E7TUFDRSxhQUFhO0lBQ2YiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3NlbmQtY3VzdG9tZXItbW9kYWwvc2VuZC1jdXN0b21lci1tb2RhbC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1vZGFsLWhlYWRlciB7XG4gICAgYmFja2dyb3VuZDogIzAxOTE5RDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogMC41cmVtIDFyZW0haW1wb3J0YW50O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIhaW1wb3J0YW50O1xufVxuLm1vZGFsLWhlYWRlciBoNSB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIG1hcmdpbjogMHB4O1xufVxuLmN1c3RvbS1jb250cm9sIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWluLWhlaWdodDogMS41cmVtO1xuICAgIHBhZGRpbmctbGVmdDogMS41cmVtO1xufVxuLyoqKioqKkJVVFRPTlMqKioqKiovXG4uYnRuLWNybSB7XG4gICAgYm9yZGVyLXJhZGl1czogMCFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAzMHB4O1xuICAgIG1pbi13aWR0aDogMTIwcHg7XG4gIH1cbiAgLmJ0bi1jcm0tcHJpbWFyeXtcbiAgICBmb250LXNpemU6IDExcHghaW1wb3J0YW50O1xuICAgIGZvbnQtd2VpZ2h0OiA3MDAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1yYWRpdXM6IDAhaW1wb3J0YW50O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMTc0N2YgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDMwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAxOTE5RCFpbXBvcnRhbnQ7XG4gICAgY29sb3I6IHdoaXRlIWltcG9ydGFudDtcbiAgfVxuICAuYnRuLWNybS1kZWZhdWx0e1xuICAgIGZvbnQtc2l6ZTogMTFweCFpbXBvcnRhbnQ7XG4gICAgZm9udC13ZWlnaHQ6IDcwMCFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLXJhZGl1czogMCFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2FlYWVhZSAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMzBweDtcbiAgfVxuICAuYnRuLWNybS1kZWZhdWx0OmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjZjVmNWY1O1xuICAgIG91dGxpbmU6IDA7XG4gICAgYm94LXNoYWRvdzogMCAwIDAgMC4xNXJlbSByZ2JhKDI1NSwgMTc3LCA2LCAwLjI1KTtcbiAgfVxuICAuYnRuLWNybS1kZWZhdWx0X3JlZCB7XG4gICAgYm9yZGVyLWNvbG9yOiByZWQgIWltcG9ydGFudDtcbiAgICBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XG4gIH1cbiAgICAvKioqKioqVVBMT0FEIFJFU1VNRSBBTkQgRE9DVU1FTlRTKioqKioqL1xuICAgIC5mYS1hZGQtZG9jdW1lbnQ6YmVmb3JlIHtcbiAgICAgIGNvbnRlbnQ6IFwiXFxmMDU1XCI7XG4gICAgICBsaW5lLWhlaWdodDogMTtcbiAgICAgIHBhZGRpbmc6IDEuNXB4O1xuICAgICAgY29sb3I6ICM1OGQ1ZTA7XG4gICAgfVxuICAgIC5hZGQtZG9jdW1lbnQtYnV0dG9uLC5yZW1vdmUtZG9jdW1lbnQtYnV0dG9uIHtcbiAgICAgIGNvbG9yOiAjNThkNWUwO1xuICAgIH1cbiAgICAuYWRkLWRvY3VtZW50LWxhYmVsIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBwYWRkaW5nOiAuMjVyZW07XG4gICAgICBjbGVhcjogYm90aDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICBjb2xvcjogIzU4ZDVlMDtcbiAgICAgIHRleHQtYWxpZ246IGluaGVyaXQ7XG4gICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgICBib3JkZXI6IDA7XG4gICAgfVxuICAgIC5hZGQtZG9jdW1lbnQtbGFiZWwgc3BhbiB7XG4gICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgIGNvbG9yOiAjNThkNWUwO1xuICAgIH1cbiAgICAuaW5wdXQtZG9jdW1lbnQge1xuICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9Il19 */");
            /***/ 
        }),
        /***/ "./src/app/components/send-customer-modal/send-customer-modal.component.ts": 
        /*!*********************************************************************************!*\
          !*** ./src/app/components/send-customer-modal/send-customer-modal.component.ts ***!
          \*********************************************************************************/
        /*! exports provided: SendCustomerModalComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendCustomerModalComponent", function () { return SendCustomerModalComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
            /* harmony import */ var src_app_services_customer_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/customer.service */ "./src/app/services/customer.service.ts");
            /* harmony import */ var src_app_services_contacts_customer_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/contacts-customer.service */ "./src/app/services/contacts-customer.service.ts");
            /* harmony import */ var src_app_services_file_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/file.service */ "./src/app/services/file.service.ts");
            /* harmony import */ var src_app_services_mail_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/mail.service */ "./src/app/services/mail.service.ts");
            /* harmony import */ var src_app_services_contacts_candidate_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/contacts-candidate.service */ "./src/app/services/contacts-candidate.service.ts");
            /* harmony import */ var src_app_services_vacancy_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/vacancy.service */ "./src/app/services/vacancy.service.ts");
            var FileSnippet = /** @class */ (function () {
                function FileSnippet(src, file) {
                    this.src = src;
                    this.file = file;
                    this.pending = false;
                    this.status = 'init';
                }
                return FileSnippet;
            }());
            var SendCustomerModalComponent = /** @class */ (function () {
                // @Input() selectedCandidate : Candidate;
                function SendCustomerModalComponent(bsModalRef, customerService, vacancyService, contactsCustomerService, fileService, mailService, contactsCandidateService) {
                    this.bsModalRef = bsModalRef;
                    this.customerService = customerService;
                    this.vacancyService = vacancyService;
                    this.contactsCustomerService = contactsCustomerService;
                    this.fileService = fileService;
                    this.mailService = mailService;
                    this.contactsCandidateService = contactsCandidateService;
                    this.customers_names = [];
                    this.vacancies_names = [];
                    this.customer_contact = [];
                    this.customer_contact_value = [];
                    this.attached_document = [];
                    this.customerId = '';
                    this.customer_contacts_values = [];
                    this.transformed_customer_contacts_names = [];
                    this.choosen_customer_contacts_values = [];
                    this.IS_DOCUMENT = 0;
                }
                SendCustomerModalComponent.prototype.ngOnInit = function () {
                    this.getCustomers();
                    this.getVacancies();
                };
                SendCustomerModalComponent.prototype.getCustomers = function () {
                    var _this = this;
                    this.customerService.getData().then(function (customers) {
                        _this.customers_names = customers.map(function (customer) { return customer.name; });
                        console.log(_this.customers_names);
                    });
                };
                SendCustomerModalComponent.prototype.getVacancies = function () {
                    var _this = this;
                    this.vacancyService.getData().then(function (vacancies) {
                        _this.vacancies_names = vacancies.map(function (vacancy) { return vacancy.name; });
                    });
                };
                SendCustomerModalComponent.prototype.getCustomerIdByName = function (customerName, successCalback) {
                    this.customerService.getData().then(function (customers) {
                        successCalback(customers.find(function (customer) { return customer.name == customerName; }).id);
                    });
                };
                SendCustomerModalComponent.prototype.getCustomerContacts = function (id) {
                    this.contactsCustomerService.getCustomerContacts(id).then(function (contacts) {
                        console.log(contacts);
                    });
                };
                SendCustomerModalComponent.prototype.getAttachedDocuments = function () {
                    var _this = this;
                    this.fileService.getCandidateDocuments(this.selectedCandidateId).then(function (candidateDocuments) {
                        _this.attached_documents = candidateDocuments.map(function (candidateDocument) { return candidateDocument.file_name; });
                    });
                };
                SendCustomerModalComponent.prototype.onSelect = function (event) {
                    var _this = this;
                    this.getCustomerIdByName(event.item, function (id) {
                        _this.contactsCustomerService.getCustomerContacts(id).then(function (customerContacts) {
                            customerContacts.forEach(function (customerContact) { return _this.customer_contacts_values.push({ value: customerContact.value, name: customerContact.contact_type }); });
                            _this.customer_contacts_names = customerContacts.map(function (customerContact) { return customerContact.contact_type; });
                            _this.customer_contacts_names.forEach(function (contact_name) { return _this.contactsCandidateService.getCandidateContactTypeName(contact_name).then(function (contact) { return _this.transformed_customer_contacts_names.push({ name: contact.name, id: contact.id }); }); });
                            _this.getAttachedDocuments();
                        });
                    });
                };
                SendCustomerModalComponent.prototype.onSelectCommunication = function (event) {
                    this.choosen_customer_contacts_values = this.customer_contacts_values.filter(function (customerContactValue) { return customerContactValue.name == event.item.id; });
                };
                SendCustomerModalComponent.prototype.onFormSubmit = function (sendCustomer) {
                    this.mailService.sendMail(this.selected_customer, this.selected_vacancy, this.selected_contact_name, this.selected_contact_value, this.selected_attached_document, this.customer_letter_body);
                    this.bsModalRef.hide();
                };
                SendCustomerModalComponent.prototype.addDocument = function (fileInput) {
                    var _this = this;
                    var file = fileInput.files[0];
                    var reader = new FileReader();
                    reader.addEventListener('load', function (event) {
                        _this.fileInputDocument.nativeElement.value = null;
                        _this.selectedAttachedFile = new FileSnippet(event.target.result, file);
                        _this.fileService.uploadFile(_this.selectedAttachedFile.file, _this.selectedCandidateId, _this.IS_DOCUMENT).subscribe(function (res) {
                            _this.getAttachedDocuments();
                            console.log(res);
                        }, function (err) {
                            console.log(err);
                        });
                    });
                    reader.readAsDataURL(file);
                };
                return SendCustomerModalComponent;
            }());
            SendCustomerModalComponent.ctorParameters = function () { return [
                { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_2__["BsModalRef"] },
                { type: src_app_services_customer_service__WEBPACK_IMPORTED_MODULE_3__["CustomerService"] },
                { type: src_app_services_vacancy_service__WEBPACK_IMPORTED_MODULE_8__["VacancyService"] },
                { type: src_app_services_contacts_customer_service__WEBPACK_IMPORTED_MODULE_4__["ContactsCustomerService"] },
                { type: src_app_services_file_service__WEBPACK_IMPORTED_MODULE_5__["FileService"] },
                { type: src_app_services_mail_service__WEBPACK_IMPORTED_MODULE_6__["MailService"] },
                { type: src_app_services_contacts_candidate_service__WEBPACK_IMPORTED_MODULE_7__["ContactsCandidateService"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInputDocument', { static: false })
            ], SendCustomerModalComponent.prototype, "fileInputDocument", void 0);
            SendCustomerModalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-send-customer-modal',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./send-customer-modal.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/send-customer-modal/send-customer-modal.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./send-customer-modal.component.css */ "./src/app/components/send-customer-modal/send-customer-modal.component.css")).default]
                })
            ], SendCustomerModalComponent);
            /***/ 
        }),
        /***/ "./src/app/components/vacancy-info/vacancy-info.component.css": 
        /*!********************************************************************!*\
          !*** ./src/app/components/vacancy-info/vacancy-info.component.css ***!
          \********************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".entity-buttons-wrapper .btn {\n    height: 40px;\n  \n    margin-right: 12px;\n    font-weight: 500 !important;\n    font-size: 14px !important;\n    text-transform: uppercase;\n    color:\n      rgba(0, 0, 0, 0.87);\n  }\n  .btn-crm {\n    border-radius: 0!important;\n    height: 30px;\n    min-width: 120px;\n  }\n  .btn-crm-primary{\n    font-size: 11px!important;\n    font-weight: 700!important;\n    border-radius: 0!important;\n    border: 1px solid #01747f !important;\n    height: 30px;\n    background-color: #01919D!important;\n    color: white!important;\n  }\n  .btn-crm-default{\n    font-size: 11px!important;\n    font-weight: 700!important;\n    border-radius: 0!important;\n    border: 1px solid #aeaeae !important;\n    height: 30px;\n  }\n  .btn-crm-default:hover {\n    background: #f5f5f5;\n    outline: 0;\n    box-shadow: 0 0 0 0.15rem rgba(255, 177, 6, 0.25);\n  }\n  .btn-crm-default_red {\n    border-color: red !important;\n    color: red !important;\n  }\n  .entity-name {\n    font-size: 20px;\n    color: #151319;\n    padding-top: 24px;\n  }\n  .entity-details .item-title {\n    font-size: 16px;\n    line-height: 30px;\n    color: #6A6A6A;\n  }\n  .entity-details .item-value{\n    font-weight: normal;\n    font-size: 16px;\n    line-height: 30px;\n    color: #151319;\n  }\n  .entity-details .skill {\n    display: inline;\n    margin-right: 10px;\n    background: rgba(225, 225, 225, 0.6);\n    border: 1px solid #C4C4C4;\n    font-size: 10px;\n    text-transform: uppercase;\n    color: #151319;\n  }\n  span.skill {\n    margin: 1px 2px;\n    padding: 1px 5px;\n    border: 1px solid #aeaeae;\n    font-size: 12px;\n    background: #f5f5f5;\n    display: inline-block;\n    font-weight: 500;\n  }\n  /****PDF PREVIEW POSITIONING****/\n  .buttons-left, .buttons-right {\n  display: inline-block;\n  width: 100%;\n  vertical-align: bottom;\n}\n  .buttons-left a {\n  margin-right: 10px;\n}\n  .buttons-right {\n  text-align: right;\n  padding-top: 13px;  \n}\n  .document__dropdown-menu i {\n  width: 22px;\n  height: 22px;\n  margin-right: 0;\n}\n  .document__dropdown-menu i::before {\n  line-height: 22px;\n  font-size: 12px;\n}\n  .document__dropdown-item {\n  padding: 0;\n  margin-right: 10px;\n}\n  .btn-file {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 1rem;\n  padding: 0;\n  min-width: 82px;\n  position: relative;\n  overflow: hidden;\n}\n  input[type=\"file\"] {\n  position: absolute;\n  opacity: 0;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy92YWNhbmN5LWluZm8vdmFjYW5jeS1pbmZvLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZOztJQUVaLGtCQUFrQjtJQUNsQiwyQkFBMkI7SUFDM0IsMEJBQTBCO0lBQzFCLHlCQUF5QjtJQUN6Qjt5QkFDcUI7RUFDdkI7RUFDQTtJQUNFLDBCQUEwQjtJQUMxQixZQUFZO0lBQ1osZ0JBQWdCO0VBQ2xCO0VBQ0E7SUFDRSx5QkFBeUI7SUFDekIsMEJBQTBCO0lBQzFCLDBCQUEwQjtJQUMxQixvQ0FBb0M7SUFDcEMsWUFBWTtJQUNaLG1DQUFtQztJQUNuQyxzQkFBc0I7RUFDeEI7RUFDQTtJQUNFLHlCQUF5QjtJQUN6QiwwQkFBMEI7SUFDMUIsMEJBQTBCO0lBQzFCLG9DQUFvQztJQUNwQyxZQUFZO0VBQ2Q7RUFDQTtJQUNFLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsaURBQWlEO0VBQ25EO0VBQ0E7SUFDRSw0QkFBNEI7SUFDNUIscUJBQXFCO0VBQ3ZCO0VBQ0E7SUFDRSxlQUFlO0lBQ2YsY0FBYztJQUNkLGlCQUFpQjtFQUNuQjtFQUNBO0lBQ0UsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0VBQ2hCO0VBRUE7SUFDRSxtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixjQUFjO0VBQ2hCO0VBQ0E7SUFDRSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG9DQUFvQztJQUNwQyx5QkFBeUI7SUFDekIsZUFBZTtJQUNmLHlCQUF5QjtJQUN6QixjQUFjO0VBQ2hCO0VBQ0E7SUFDRSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLHlCQUF5QjtJQUN6QixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLHFCQUFxQjtJQUNyQixnQkFBZ0I7RUFDbEI7RUFDRixnQ0FBZ0M7RUFDaEM7RUFDRSxxQkFBcUI7RUFDckIsV0FBVztFQUNYLHNCQUFzQjtBQUN4QjtFQUNBO0VBQ0Usa0JBQWtCO0FBQ3BCO0VBQ0E7RUFDRSxpQkFBaUI7RUFDakIsaUJBQWlCO0FBQ25CO0VBQ0E7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQWU7QUFDakI7RUFDQTtFQUNFLGlCQUFpQjtFQUNqQixlQUFlO0FBQ2pCO0VBQ0E7RUFDRSxVQUFVO0VBQ1Ysa0JBQWtCO0FBQ3BCO0VBQ0E7RUFDRSxpREFBaUQ7RUFDakQsZUFBZTtFQUNmLFVBQVU7RUFDVixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGdCQUFnQjtBQUNsQjtFQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixNQUFNO0VBQ04sUUFBUTtFQUNSLFNBQVM7RUFDVCxPQUFPO0VBQ1AsV0FBVztFQUNYLFlBQVk7QUFDZCIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdmFjYW5jeS1pbmZvL3ZhY2FuY3ktaW5mby5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVudGl0eS1idXR0b25zLXdyYXBwZXIgLmJ0biB7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICBcbiAgICBtYXJnaW4tcmlnaHQ6IDEycHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMCAhaW1wb3J0YW50O1xuICAgIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6XG4gICAgICByZ2JhKDAsIDAsIDAsIDAuODcpO1xuICB9XG4gIC5idG4tY3JtIHtcbiAgICBib3JkZXItcmFkaXVzOiAwIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDMwcHg7XG4gICAgbWluLXdpZHRoOiAxMjBweDtcbiAgfVxuICAuYnRuLWNybS1wcmltYXJ5e1xuICAgIGZvbnQtc2l6ZTogMTFweCFpbXBvcnRhbnQ7XG4gICAgZm9udC13ZWlnaHQ6IDcwMCFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLXJhZGl1czogMCFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzAxNzQ3ZiAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMzBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDE5MTlEIWltcG9ydGFudDtcbiAgICBjb2xvcjogd2hpdGUhaW1wb3J0YW50O1xuICB9XG4gIC5idG4tY3JtLWRlZmF1bHR7XG4gICAgZm9udC1zaXplOiAxMXB4IWltcG9ydGFudDtcbiAgICBmb250LXdlaWdodDogNzAwIWltcG9ydGFudDtcbiAgICBib3JkZXItcmFkaXVzOiAwIWltcG9ydGFudDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYWVhZWFlICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAzMHB4O1xuICB9XG4gIC5idG4tY3JtLWRlZmF1bHQ6aG92ZXIge1xuICAgIGJhY2tncm91bmQ6ICNmNWY1ZjU7XG4gICAgb3V0bGluZTogMDtcbiAgICBib3gtc2hhZG93OiAwIDAgMCAwLjE1cmVtIHJnYmEoMjU1LCAxNzcsIDYsIDAuMjUpO1xuICB9XG4gIC5idG4tY3JtLWRlZmF1bHRfcmVkIHtcbiAgICBib3JkZXItY29sb3I6IHJlZCAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiByZWQgIWltcG9ydGFudDtcbiAgfVxuICAuZW50aXR5LW5hbWUge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBjb2xvcjogIzE1MTMxOTtcbiAgICBwYWRkaW5nLXRvcDogMjRweDtcbiAgfSAgXG4gIC5lbnRpdHktZGV0YWlscyAuaXRlbS10aXRsZSB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xuICAgIGNvbG9yOiAjNkE2QTZBO1xuICB9XG4gIFxuICAuZW50aXR5LWRldGFpbHMgLml0ZW0tdmFsdWV7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgY29sb3I6ICMxNTEzMTk7XG4gIH1cbiAgLmVudGl0eS1kZXRhaWxzIC5za2lsbCB7XG4gICAgZGlzcGxheTogaW5saW5lO1xuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDIyNSwgMjI1LCAyMjUsIDAuNik7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0M0QzRDNDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjogIzE1MTMxOTtcbiAgfVxuICBzcGFuLnNraWxsIHtcbiAgICBtYXJnaW46IDFweCAycHg7XG4gICAgcGFkZGluZzogMXB4IDVweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYWVhZWFlO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBiYWNrZ3JvdW5kOiAjZjVmNWY1O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBmb250LXdlaWdodDogNTAwO1xuICB9XG4vKioqKlBERiBQUkVWSUVXIFBPU0lUSU9OSU5HKioqKi9cbi5idXR0b25zLWxlZnQsIC5idXR0b25zLXJpZ2h0IHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogMTAwJTtcbiAgdmVydGljYWwtYWxpZ246IGJvdHRvbTtcbn1cbi5idXR0b25zLWxlZnQgYSB7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbi5idXR0b25zLXJpZ2h0IHtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIHBhZGRpbmctdG9wOiAxM3B4OyAgXG59XG4uZG9jdW1lbnRfX2Ryb3Bkb3duLW1lbnUgaSB7XG4gIHdpZHRoOiAyMnB4O1xuICBoZWlnaHQ6IDIycHg7XG4gIG1hcmdpbi1yaWdodDogMDtcbn1cbi5kb2N1bWVudF9fZHJvcGRvd24tbWVudSBpOjpiZWZvcmUge1xuICBsaW5lLWhlaWdodDogMjJweDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuLmRvY3VtZW50X19kcm9wZG93bi1pdGVtIHtcbiAgcGFkZGluZzogMDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuLmJ0bi1maWxlIHtcbiAgZm9udC1mYW1pbHk6IFJvYm90bywgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBzYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDFyZW07XG4gIHBhZGRpbmc6IDA7XG4gIG1pbi13aWR0aDogODJweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuaW5wdXRbdHlwZT1cImZpbGVcIl0ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG9wYWNpdHk6IDA7XG4gIHRvcDogMDtcbiAgcmlnaHQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbiJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/components/vacancy-info/vacancy-info.component.ts": 
        /*!*******************************************************************!*\
          !*** ./src/app/components/vacancy-info/vacancy-info.component.ts ***!
          \*******************************************************************/
        /*! exports provided: VacancyInfoComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VacancyInfoComponent", function () { return VacancyInfoComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_services_shared_event_bus_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/shared/event-bus.service */ "./src/app/services/shared/event-bus.service.ts");
            /* harmony import */ var src_app_services_cities_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/cities.service */ "./src/app/services/cities.service.ts");
            /* harmony import */ var src_app_services_company_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/company.service */ "./src/app/services/company.service.ts");
            /* harmony import */ var _vacancy_update_vacancy_update_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../vacancy-update/vacancy-update.component */ "./src/app/components/vacancy-update/vacancy-update.component.ts");
            /* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
            /* harmony import */ var src_app_services_skills_vacancy_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/skills-vacancy.service */ "./src/app/services/skills-vacancy.service.ts");
            /* harmony import */ var src_app_services_skills_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/skills.service */ "./src/app/services/skills.service.ts");
            /* harmony import */ var src_app_services_vacancy_documents_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/vacancy-documents.service */ "./src/app/services/vacancy-documents.service.ts");
            var FileSnippet = /** @class */ (function () {
                function FileSnippet(src, file) {
                    this.src = src;
                    this.file = file;
                    this.pending = false;
                    this.status = 'init';
                }
                return FileSnippet;
            }());
            var VacancyInfoComponent = /** @class */ (function () {
                function VacancyInfoComponent(eventBusService, citiesService, companyService, modalService, skillsVacancyService, skillsService, vacancyDocumentService) {
                    this.eventBusService = eventBusService;
                    this.citiesService = citiesService;
                    this.companyService = companyService;
                    this.modalService = modalService;
                    this.skillsVacancyService = skillsVacancyService;
                    this.skillsService = skillsService;
                    this.vacancyDocumentService = vacancyDocumentService;
                    this.vacancySkills = [];
                }
                VacancyInfoComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.eventBusService.on('selectedVacancy', function (data) {
                        if (data) {
                            _this.selectedVacancy = data;
                            _this.getVacancyCity(_this.selectedVacancy.location.toString());
                            _this.getVacancyCompany(_this.selectedVacancy.company_id);
                            _this.getVacancySkills(_this.selectedVacancy.id);
                            _this.getVacancyDocuments(_this.selectedVacancy.id);
                        }
                    });
                };
                VacancyInfoComponent.prototype.ngOnChanges = function (changes) {
                    var _this = this;
                    this.eventBusService.on('selectedVacancy', function (data) {
                        if (data) {
                            _this.selectedVacancy = data;
                            _this.getVacancyCity(_this.selectedVacancy.location.toString());
                            _this.getVacancyCompany(_this.selectedVacancy.company_id);
                        }
                    });
                };
                VacancyInfoComponent.prototype.getVacancyCity = function (id) {
                    var _this = this;
                    this.citiesService.getDetail(id).then(function (city) { return _this.city = city; });
                };
                VacancyInfoComponent.prototype.getVacancyCompany = function (company_id) {
                    var _this = this;
                    this.companyService.getDetail(company_id).then(function (company) {
                        _this.company = company;
                    });
                };
                VacancyInfoComponent.prototype.getVacancySkills = function (id) {
                    var _this = this;
                    this.vacancySkills = [];
                    this.skillsVacancyService.getAllVacanciesSkills(id).then(function (skillsVacancy) {
                        skillsVacancy.relatedSkills.forEach(function (skillVacancy) { return _this.skillsService.getDetail(skillVacancy.skill_id).then(function (skill) { return _this.vacancySkills.push(skill.name); }); });
                    });
                };
                VacancyInfoComponent.prototype.openVacancyUpdateModal = function () {
                    var initialState = {
                        selectedVacancyId: this.selectedVacancy.id,
                        selectedVacancy: this.selectedVacancy,
                        title: 'Редактировать вакансию'
                    };
                    this.bsModalRef = this.modalService.show(_vacancy_update_vacancy_update_component__WEBPACK_IMPORTED_MODULE_5__["VacancyUpdateComponent"], { initialState: initialState });
                    this.bsModalRef.content.selectedVacancyId = this.selectedVacancy.id;
                };
                VacancyInfoComponent.prototype.getVacancyDocuments = function (vacancyId) {
                    var _this = this;
                    this.selectedVacancyDocument = undefined;
                    this.vacancyDocumentService.getVacancyDocuments(vacancyId).then(function (documentsVacancy) {
                        _this.vacancyDocuments = documentsVacancy;
                        if (!_this.uploadedFile && _this.vacancyDocuments[0]) {
                            _this.selectedVacancyDocument = _this.vacancyDocuments[0].file_name;
                        }
                        else if (_this.uploadedFile) {
                            _this.selectedVacancyDocument = _this.uploadedFile.file.name;
                            _this.uploadedFile = undefined;
                        }
                    });
                };
                VacancyInfoComponent.prototype.previewDocument = function (event, filename) {
                    event.preventDefault();
                    this.selectedVacancyDocument = filename;
                };
                VacancyInfoComponent.prototype.removeVacancyDocumentByName = function (event, fileName) {
                    var _this = this;
                    event.preventDefault();
                    this.vacancyDocumentService.getVacancyDocuments(this.selectedVacancy.id)
                        .then(function (documentVacancy) {
                        _this.deletedVacancyDocument = documentVacancy.filter(function (doc) { return doc.file_name === fileName; }).pop();
                        _this.vacancyDocumentService.deleteFileByName(_this.selectedVacancy.id, _this.deletedVacancyDocument.id).subscribe(function (res) {
                            _this.getVacancyDocuments(_this.selectedVacancy.id);
                        }, function (err) {
                            console.log(err);
                        });
                    });
                };
                VacancyInfoComponent.prototype.processFile = function (fileInput) {
                    var _this = this;
                    var file = fileInput.files[0];
                    var reader = new FileReader();
                    reader.addEventListener('load', function (event) {
                        _this.fileInput.nativeElement.value = null;
                        _this.uploadedFile = new FileSnippet(event.target.result, file);
                        _this.uploadedFile.pending = true;
                        _this.vacancyDocumentService.uploadFile(_this.uploadedFile.file, _this.selectedVacancy.id).subscribe(function (res) {
                            _this.selectedVacancyDocument = _this.uploadedFile.file.name;
                            _this.getVacancyDocuments(_this.selectedVacancy.id);
                        }, function (err) {
                            console.log(err);
                        });
                    });
                    reader.readAsDataURL(file);
                };
                return VacancyInfoComponent;
            }());
            VacancyInfoComponent.ctorParameters = function () { return [
                { type: src_app_services_shared_event_bus_service__WEBPACK_IMPORTED_MODULE_2__["EventBusService"] },
                { type: src_app_services_cities_service__WEBPACK_IMPORTED_MODULE_3__["CitiesService"] },
                { type: src_app_services_company_service__WEBPACK_IMPORTED_MODULE_4__["CompanyService"] },
                { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_6__["BsModalService"] },
                { type: src_app_services_skills_vacancy_service__WEBPACK_IMPORTED_MODULE_7__["SkillsVacancyService"] },
                { type: src_app_services_skills_service__WEBPACK_IMPORTED_MODULE_8__["SkillsService"] },
                { type: src_app_services_vacancy_documents_service__WEBPACK_IMPORTED_MODULE_9__["VacancyDocumentsService"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInput', { static: false })
            ], VacancyInfoComponent.prototype, "fileInput", void 0);
            VacancyInfoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-vacancy-info',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./vacancy-info.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/vacancy-info/vacancy-info.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./vacancy-info.component.css */ "./src/app/components/vacancy-info/vacancy-info.component.css")).default]
                })
            ], VacancyInfoComponent);
            /***/ 
        }),
        /***/ "./src/app/components/vacancy-list/vacancy-list.component.css": 
        /*!********************************************************************!*\
          !*** ./src/app/components/vacancy-list/vacancy-list.component.css ***!
          \********************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".selected {\n    background-color: #CFD8DC !important;\n    color: white;\n  }\n.vacancies {\n    margin: 0 0 2em 0;\n    list-style-type: none;\n    padding: 0;\n}\n.vacancies li {\n    cursor: pointer;\n    position: relative;\n    left: 0;\n    margin: .5em;\n    padding: .3em 0;\n    height: 1.6em;\n    border-radius: 4px;\n  }\n.vacancies li.selected:hover {\n    background-color: #BBD8DC !important;\n    color: white;\n  }\n.vacancies li:hover {\n    color: #607D8B;\n    background-color: #DDD;\n    left: .1em;\n  }\n.entity_list__item {\n    display:flex;\n    background: #fff;\n    min-height: 100px;\n    /*padding-bottom: 5rem !important;*/\n    /*padding-top: 5rem !important;*/\n    border-bottom: 1px solid #dee2e6 !important;\n    margin: 0 !important;\n  }\n.entity_list__item a {\n    color: black !important;\n    text-decoration: none;\n    background-color: transparent;\n  }\n.entity_list__item .entity-title {\n    font-weight: 500;\n  }\n.entity_list__item .entity-sub {\n    font-size: 14px;\n    color:\n      #616161;\n    font-weight: normal;\n  }    \n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy92YWNhbmN5LWxpc3QvdmFjYW5jeS1saXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxvQ0FBb0M7SUFDcEMsWUFBWTtFQUNkO0FBQ0Y7SUFDSSxpQkFBaUI7SUFDakIscUJBQXFCO0lBQ3JCLFVBQVU7QUFDZDtBQUNBO0lBQ0ksZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixPQUFPO0lBQ1AsWUFBWTtJQUNaLGVBQWU7SUFDZixhQUFhO0lBQ2Isa0JBQWtCO0VBQ3BCO0FBQ0E7SUFDRSxvQ0FBb0M7SUFDcEMsWUFBWTtFQUNkO0FBQ0E7SUFDRSxjQUFjO0lBQ2Qsc0JBQXNCO0lBQ3RCLFVBQVU7RUFDWjtBQUNBO0lBQ0UsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsbUNBQW1DO0lBQ25DLGdDQUFnQztJQUNoQywyQ0FBMkM7SUFDM0Msb0JBQW9CO0VBQ3RCO0FBQ0E7SUFDRSx1QkFBdUI7SUFDdkIscUJBQXFCO0lBQ3JCLDZCQUE2QjtFQUMvQjtBQUNBO0lBQ0UsZ0JBQWdCO0VBQ2xCO0FBQ0E7SUFDRSxlQUFlO0lBQ2Y7YUFDUztJQUNULG1CQUFtQjtFQUNyQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdmFjYW5jeS1saXN0L3ZhY2FuY3ktbGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNlbGVjdGVkIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjQ0ZEOERDICFpbXBvcnRhbnQ7XG4gICAgY29sb3I6IHdoaXRlO1xuICB9XG4udmFjYW5jaWVzIHtcbiAgICBtYXJnaW46IDAgMCAyZW0gMDtcbiAgICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG4gICAgcGFkZGluZzogMDtcbn0gIFxuLnZhY2FuY2llcyBsaSB7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBsZWZ0OiAwO1xuICAgIG1hcmdpbjogLjVlbTtcbiAgICBwYWRkaW5nOiAuM2VtIDA7XG4gICAgaGVpZ2h0OiAxLjZlbTtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIH1cbiAgLnZhY2FuY2llcyBsaS5zZWxlY3RlZDpob3ZlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0JCRDhEQyAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgfVxuICAudmFjYW5jaWVzIGxpOmhvdmVyIHtcbiAgICBjb2xvcjogIzYwN0Q4QjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjREREO1xuICAgIGxlZnQ6IC4xZW07XG4gIH1cbiAgLmVudGl0eV9saXN0X19pdGVtIHtcbiAgICBkaXNwbGF5OmZsZXg7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBtaW4taGVpZ2h0OiAxMDBweDtcbiAgICAvKnBhZGRpbmctYm90dG9tOiA1cmVtICFpbXBvcnRhbnQ7Ki9cbiAgICAvKnBhZGRpbmctdG9wOiA1cmVtICFpbXBvcnRhbnQ7Ki9cbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RlZTJlNiAhaW1wb3J0YW50O1xuICAgIG1hcmdpbjogMCAhaW1wb3J0YW50O1xuICB9XG4gIC5lbnRpdHlfbGlzdF9faXRlbSBhIHtcbiAgICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIH1cbiAgLmVudGl0eV9saXN0X19pdGVtIC5lbnRpdHktdGl0bGUge1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIH1cbiAgLmVudGl0eV9saXN0X19pdGVtIC5lbnRpdHktc3ViIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6XG4gICAgICAjNjE2MTYxO1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIH0gICAgIl19 */");
            /***/ 
        }),
        /***/ "./src/app/components/vacancy-list/vacancy-list.component.ts": 
        /*!*******************************************************************!*\
          !*** ./src/app/components/vacancy-list/vacancy-list.component.ts ***!
          \*******************************************************************/
        /*! exports provided: VacancyListComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VacancyListComponent", function () { return VacancyListComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_services_vacancy_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/vacancy.service */ "./src/app/services/vacancy.service.ts");
            /* harmony import */ var src_app_services_cities_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/cities.service */ "./src/app/services/cities.service.ts");
            /* harmony import */ var src_app_services_company_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/company.service */ "./src/app/services/company.service.ts");
            /* harmony import */ var src_app_services_shared_event_bus_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/shared/event-bus.service */ "./src/app/services/shared/event-bus.service.ts");
            /* harmony import */ var src_app_services_shared_event_class__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/shared/event.class */ "./src/app/services/shared/event.class.ts");
            var VacancyListComponent = /** @class */ (function () {
                function VacancyListComponent(vacancyService, citiesService, companyService, eventBusService) {
                    this.vacancyService = vacancyService;
                    this.citiesService = citiesService;
                    this.companyService = companyService;
                    this.eventBusService = eventBusService;
                    this.vacancies = [];
                }
                VacancyListComponent.prototype.ngOnInit = function () {
                    this.getVacancies();
                };
                VacancyListComponent.prototype.ngOnChanges = function (changes) {
                    console.log("vacancy list", changes);
                    this.getVacancies();
                };
                VacancyListComponent.prototype.getVacancies = function () {
                    var _this = this;
                    this.vacancyService.getData().then(function (vacancies) {
                        _this.vacancies = vacancies;
                        _this.vacancies.forEach(function (vacancy) {
                            _this.citiesService.getDetail(vacancy.location).then(function (city) {
                                vacancy.city_name = city.name;
                            });
                            _this.companyService.getDetail(vacancy.company_id).then(function (company) {
                                vacancy.company_name = company.name;
                            });
                        });
                    });
                };
                VacancyListComponent.prototype.onSelect = function (vacancy) {
                    this.selectedVacancy = vacancy;
                    this.eventBusService.emit(new src_app_services_shared_event_class__WEBPACK_IMPORTED_MODULE_6__["EventData"]('selectedVacancy', vacancy));
                };
                return VacancyListComponent;
            }());
            VacancyListComponent.ctorParameters = function () { return [
                { type: src_app_services_vacancy_service__WEBPACK_IMPORTED_MODULE_2__["VacancyService"] },
                { type: src_app_services_cities_service__WEBPACK_IMPORTED_MODULE_3__["CitiesService"] },
                { type: src_app_services_company_service__WEBPACK_IMPORTED_MODULE_4__["CompanyService"] },
                { type: src_app_services_shared_event_bus_service__WEBPACK_IMPORTED_MODULE_5__["EventBusService"] }
            ]; };
            VacancyListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-vacancy-list',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./vacancy-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/vacancy-list/vacancy-list.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./vacancy-list.component.css */ "./src/app/components/vacancy-list/vacancy-list.component.css")).default]
                })
            ], VacancyListComponent);
            /***/ 
        }),
        /***/ "./src/app/components/vacancy-update/vacancy-update.component.css": 
        /*!************************************************************************!*\
          !*** ./src/app/components/vacancy-update/vacancy-update.component.css ***!
          \************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".modal-header {\n    background: #01919D;\n    color: white;\n    padding: 0.5rem 1rem!important;\n    align-items: center!important;\n}\n.modal-header h5 {\n    font-size: 13px;\n    margin: 0px;\n}\n.custom-control {\n    position: relative;\n    display: block;\n    min-height: 1.5rem;\n    padding-left: 1.5rem;\n}\n/******BUTTONS******/\n.btn-crm {\n    border-radius: 0!important;\n    height: 30px;\n    min-width: 120px;\n  }\n.btn-crm-primary{\n    font-size: 11px!important;\n    font-weight: 700!important;\n    border-radius: 0!important;\n    border: 1px solid #01747f !important;\n    height: 30px;\n    background-color: #01919D!important;\n    color: white!important;\n  }\n.btn-crm-default{\n    font-size: 11px!important;\n    font-weight: 700!important;\n    border-radius: 0!important;\n    border: 1px solid #aeaeae !important;\n    height: 30px;\n  }\n.btn-crm-default:hover {\n    background: #f5f5f5;\n    outline: 0;\n    box-shadow: 0 0 0 0.15rem rgba(255, 177, 6, 0.25);\n  }\n.btn-crm-default_red {\n    border-color: red !important;\n    color: red !important;\n  }\n::ng-deep .cdk-global-overlay-wrapper,  ::ng-deep .cdk-overlay-container {\n     z-index: 9999!important; \n  }\n.vacancy-update__input-wrap mat-form-field {\n    min-width: 100%;\n  }\n/******UPLOAD RESUME AND DOCUMENTS******/\n.fa-add-document:before {\n    content: \"\\f055\";\n    line-height: 1;\n    padding: 1.5px;\n    color: #58d5e0;\n  }\n.add-document-button,.remove-document-button {\n    color: #58d5e0;\n  }\n.add-document-label {\n    display: block;\n    width: 100%;\n    padding: .25rem;\n    clear: both;\n    font-weight: 400;\n    color: #58d5e0;\n    text-align: inherit;\n    white-space: nowrap;\n    background-color: transparent;\n    border: 0;\n  }\n.add-document-label span {\n    margin-left: 10px;\n    color: #58d5e0;\n  }\n.input-document {\n    display: none;\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy92YWNhbmN5LXVwZGF0ZS92YWNhbmN5LXVwZGF0ZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksbUJBQW1CO0lBQ25CLFlBQVk7SUFDWiw4QkFBOEI7SUFDOUIsNkJBQTZCO0FBQ2pDO0FBQ0E7SUFDSSxlQUFlO0lBQ2YsV0FBVztBQUNmO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixvQkFBb0I7QUFDeEI7QUFDQSxvQkFBb0I7QUFDcEI7SUFDSSwwQkFBMEI7SUFDMUIsWUFBWTtJQUNaLGdCQUFnQjtFQUNsQjtBQUNBO0lBQ0UseUJBQXlCO0lBQ3pCLDBCQUEwQjtJQUMxQiwwQkFBMEI7SUFDMUIsb0NBQW9DO0lBQ3BDLFlBQVk7SUFDWixtQ0FBbUM7SUFDbkMsc0JBQXNCO0VBQ3hCO0FBQ0E7SUFDRSx5QkFBeUI7SUFDekIsMEJBQTBCO0lBQzFCLDBCQUEwQjtJQUMxQixvQ0FBb0M7SUFDcEMsWUFBWTtFQUNkO0FBQ0E7SUFDRSxtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLGlEQUFpRDtFQUNuRDtBQUNBO0lBQ0UsNEJBQTRCO0lBQzVCLHFCQUFxQjtFQUN2QjtBQUVBO0tBQ0csdUJBQXVCO0VBQzFCO0FBRUE7SUFDRSxlQUFlO0VBQ2pCO0FBQ0Esd0NBQXdDO0FBQ3hDO0lBQ0UsZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxjQUFjO0lBQ2QsY0FBYztFQUNoQjtBQUNBO0lBQ0UsY0FBYztFQUNoQjtBQUNBO0lBQ0UsY0FBYztJQUNkLFdBQVc7SUFDWCxlQUFlO0lBQ2YsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQiw2QkFBNkI7SUFDN0IsU0FBUztFQUNYO0FBQ0E7SUFDRSxpQkFBaUI7SUFDakIsY0FBYztFQUNoQjtBQUNBO0lBQ0UsYUFBYTtFQUNmIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy92YWNhbmN5LXVwZGF0ZS92YWNhbmN5LXVwZGF0ZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1vZGFsLWhlYWRlciB7XG4gICAgYmFja2dyb3VuZDogIzAxOTE5RDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogMC41cmVtIDFyZW0haW1wb3J0YW50O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIhaW1wb3J0YW50O1xufVxuLm1vZGFsLWhlYWRlciBoNSB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIG1hcmdpbjogMHB4O1xufVxuLmN1c3RvbS1jb250cm9sIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWluLWhlaWdodDogMS41cmVtO1xuICAgIHBhZGRpbmctbGVmdDogMS41cmVtO1xufVxuLyoqKioqKkJVVFRPTlMqKioqKiovXG4uYnRuLWNybSB7XG4gICAgYm9yZGVyLXJhZGl1czogMCFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAzMHB4O1xuICAgIG1pbi13aWR0aDogMTIwcHg7XG4gIH1cbiAgLmJ0bi1jcm0tcHJpbWFyeXtcbiAgICBmb250LXNpemU6IDExcHghaW1wb3J0YW50O1xuICAgIGZvbnQtd2VpZ2h0OiA3MDAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1yYWRpdXM6IDAhaW1wb3J0YW50O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMTc0N2YgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDMwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAxOTE5RCFpbXBvcnRhbnQ7XG4gICAgY29sb3I6IHdoaXRlIWltcG9ydGFudDtcbiAgfVxuICAuYnRuLWNybS1kZWZhdWx0e1xuICAgIGZvbnQtc2l6ZTogMTFweCFpbXBvcnRhbnQ7XG4gICAgZm9udC13ZWlnaHQ6IDcwMCFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLXJhZGl1czogMCFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2FlYWVhZSAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMzBweDtcbiAgfVxuICAuYnRuLWNybS1kZWZhdWx0OmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjZjVmNWY1O1xuICAgIG91dGxpbmU6IDA7XG4gICAgYm94LXNoYWRvdzogMCAwIDAgMC4xNXJlbSByZ2JhKDI1NSwgMTc3LCA2LCAwLjI1KTtcbiAgfVxuICAuYnRuLWNybS1kZWZhdWx0X3JlZCB7XG4gICAgYm9yZGVyLWNvbG9yOiByZWQgIWltcG9ydGFudDtcbiAgICBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XG4gIH1cblxuICA6Om5nLWRlZXAgLmNkay1nbG9iYWwtb3ZlcmxheS13cmFwcGVyLCAgOjpuZy1kZWVwIC5jZGstb3ZlcmxheS1jb250YWluZXIge1xuICAgICB6LWluZGV4OiA5OTk5IWltcG9ydGFudDsgXG4gIH0gXG5cbiAgLnZhY2FuY3ktdXBkYXRlX19pbnB1dC13cmFwIG1hdC1mb3JtLWZpZWxkIHtcbiAgICBtaW4td2lkdGg6IDEwMCU7XG4gIH1cbiAgLyoqKioqKlVQTE9BRCBSRVNVTUUgQU5EIERPQ1VNRU5UUyoqKioqKi9cbiAgLmZhLWFkZC1kb2N1bWVudDpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXFxmMDU1XCI7XG4gICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgcGFkZGluZzogMS41cHg7XG4gICAgY29sb3I6ICM1OGQ1ZTA7XG4gIH1cbiAgLmFkZC1kb2N1bWVudC1idXR0b24sLnJlbW92ZS1kb2N1bWVudC1idXR0b24ge1xuICAgIGNvbG9yOiAjNThkNWUwO1xuICB9XG4gIC5hZGQtZG9jdW1lbnQtbGFiZWwge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IC4yNXJlbTtcbiAgICBjbGVhcjogYm90aDtcbiAgICBmb250LXdlaWdodDogNDAwO1xuICAgIGNvbG9yOiAjNThkNWUwO1xuICAgIHRleHQtYWxpZ246IGluaGVyaXQ7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXI6IDA7XG4gIH1cbiAgLmFkZC1kb2N1bWVudC1sYWJlbCBzcGFuIHtcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICBjb2xvcjogIzU4ZDVlMDtcbiAgfVxuICAuaW5wdXQtZG9jdW1lbnQge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/components/vacancy-update/vacancy-update.component.ts": 
        /*!***********************************************************************!*\
          !*** ./src/app/components/vacancy-update/vacancy-update.component.ts ***!
          \***********************************************************************/
        /*! exports provided: VacancyUpdateComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VacancyUpdateComponent", function () { return VacancyUpdateComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ckeditor/ckeditor5-build-classic */ "./node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js");
            /* harmony import */ var _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/ __webpack_require__.n(_ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2__);
            /* harmony import */ var src_app_services_vacancy_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/vacancy.service */ "./src/app/services/vacancy.service.ts");
            /* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
            /* harmony import */ var _services_cities_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/cities.service */ "./src/app/services/cities.service.ts");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var src_app_services_skills_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/skills.service */ "./src/app/services/skills.service.ts");
            /* harmony import */ var src_app_services_skills_vacancy_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/skills-vacancy.service */ "./src/app/services/skills-vacancy.service.ts");
            /* harmony import */ var src_app_services_shared_event_class__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/shared/event.class */ "./src/app/services/shared/event.class.ts");
            /* harmony import */ var src_app_services_shared_event_bus_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/services/shared/event-bus.service */ "./src/app/services/shared/event-bus.service.ts");
            /* harmony import */ var src_app_services_vacancy_documents_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/app/services/vacancy-documents.service */ "./src/app/services/vacancy-documents.service.ts");
            var FileSnippet = /** @class */ (function () {
                function FileSnippet(src, file) {
                    this.src = src;
                    this.file = file;
                    this.pending = false;
                    this.status = 'init';
                }
                return FileSnippet;
            }());
            var VacancyUpdateComponent = /** @class */ (function () {
                function VacancyUpdateComponent(vacancyService, bsModalRef, citiesService, route, skillsService, skillsVacancyService, eventBusService, vacancyDocumentService) {
                    this.vacancyService = vacancyService;
                    this.bsModalRef = bsModalRef;
                    this.citiesService = citiesService;
                    this.route = route;
                    this.skillsService = skillsService;
                    this.skillsVacancyService = skillsVacancyService;
                    this.eventBusService = eventBusService;
                    this.vacancyDocumentService = vacancyDocumentService;
                    this.Editor = _ckeditor_ckeditor5_build_classic__WEBPACK_IMPORTED_MODULE_2__;
                    this.selectedSkills = Array();
                    this.skillsVacancy = [];
                }
                VacancyUpdateComponent.prototype.ngOnInit = function () {
                    this.getCities();
                    this.getSkills();
                    this.getSkillsVacancies(this.selectedVacancyId);
                };
                VacancyUpdateComponent.prototype.getCities = function () {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function (params) { return _this.citiesService.getData(); }))
                        .subscribe(function (cities) { return _this.cities = cities; });
                };
                VacancyUpdateComponent.prototype.getSkills = function () {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function (params) { return _this.skillsService.getData(); }))
                        .subscribe(function (skills) {
                        _this.skills = skills;
                    });
                };
                VacancyUpdateComponent.prototype.getSkillsVacancies = function (id) {
                    var _this = this;
                    this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function (params) { return _this.skillsVacancyService.getData(id); }))
                        .subscribe(function (skillsVacancy) {
                        _this.skillsVacancy = skillsVacancy;
                        _this.isSelectedSkill = true;
                        var vacSkillIds = _this.skillsVacancy.map(function (i) { return i.skill_id; });
                        _this.skills.forEach(function (item) {
                            if (vacSkillIds.includes(item.id)) {
                                _this.selectedSkills.push(item);
                            }
                        });
                    });
                    console.log(this.selectedSkills);
                    console.dir(this.selectedSkills);
                };
                VacancyUpdateComponent.prototype.onFormSubmit = function (vacancyUpdate) {
                    var _this = this;
                    console.log(vacancyUpdate.form.value);
                    var vacancy = {
                        id: vacancyUpdate.form.value.vacancyId,
                        name: vacancyUpdate.form.value.vacancy_position,
                        location: vacancyUpdate.form.value.location,
                        salary: vacancyUpdate.form.value.vacancy_salary,
                        v_duties: vacancyUpdate.form.value.duties,
                        w_conditions: vacancyUpdate.form.value.conditions,
                        v_requirements: vacancyUpdate.form.value.requirements,
                        v_description: vacancyUpdate.form.value.description,
                    };
                    (function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, this.skillsVacancyService.removeAllSkillsVacancy(this.selectedVacancyId)];
                                case 1:
                                    _a.sent();
                                    return [4 /*yield*/, this.selectedSkills.forEach(function (skill) { return _this.skillsVacancyService.addSkillVacancy({ skill_id: skill.id, vacancy_id: _this.selectedVacancyId }); })];
                                case 2:
                                    _a.sent();
                                    this.eventBusService.emit(new src_app_services_shared_event_class__WEBPACK_IMPORTED_MODULE_10__["EventData"]('selectedVacancy', vacancy));
                                    return [2 /*return*/];
                            }
                        });
                    }); })();
                    this.vacancyService.update(vacancy);
                };
                VacancyUpdateComponent.prototype.addDocument = function (fileInput) {
                    var _this = this;
                    var file = fileInput.files[0];
                    var reader = new FileReader();
                    reader.addEventListener('load', function (event) {
                        _this.fileInputDocument.nativeElement.value = null;
                        _this.selectedAttachedFile = new FileSnippet(event.target.result, file);
                        _this.vacancyDocumentService.uploadFile(_this.selectedAttachedFile.file, _this.selectedVacancyId).subscribe(function (res) {
                            // this.getAttachedDocuments();
                            console.log(res);
                        }, function (err) {
                            console.log(err);
                        });
                    });
                    reader.readAsDataURL(file);
                };
                return VacancyUpdateComponent;
            }());
            VacancyUpdateComponent.ctorParameters = function () { return [
                { type: src_app_services_vacancy_service__WEBPACK_IMPORTED_MODULE_3__["VacancyService"] },
                { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__["BsModalRef"] },
                { type: _services_cities_service__WEBPACK_IMPORTED_MODULE_5__["CitiesService"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] },
                { type: src_app_services_skills_service__WEBPACK_IMPORTED_MODULE_8__["SkillsService"] },
                { type: src_app_services_skills_vacancy_service__WEBPACK_IMPORTED_MODULE_9__["SkillsVacancyService"] },
                { type: src_app_services_shared_event_bus_service__WEBPACK_IMPORTED_MODULE_11__["EventBusService"] },
                { type: src_app_services_vacancy_documents_service__WEBPACK_IMPORTED_MODULE_12__["VacancyDocumentsService"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('fileInputDocument', { static: false })
            ], VacancyUpdateComponent.prototype, "fileInputDocument", void 0);
            VacancyUpdateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-vacancy-update',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./vacancy-update.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/vacancy-update/vacancy-update.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./vacancy-update.component.css */ "./src/app/components/vacancy-update/vacancy-update.component.css")).default]
                })
            ], VacancyUpdateComponent);
            /***/ 
        }),
        /***/ "./src/app/date_adapters/date.adapter.ts": 
        /*!***********************************************!*\
          !*** ./src/app/date_adapters/date.adapter.ts ***!
          \***********************************************/
        /*! exports provided: AppDateAdapter, APP_DATE_FORMATS */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppDateAdapter", function () { return AppDateAdapter; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APP_DATE_FORMATS", function () { return APP_DATE_FORMATS; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
            var AppDateAdapter = /** @class */ (function (_super) {
                __extends(AppDateAdapter, _super);
                function AppDateAdapter() {
                    return _super !== null && _super.apply(this, arguments) || this;
                }
                AppDateAdapter.prototype.parse = function (value) {
                    if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
                        var str = value.split('/');
                        var year = Number(str[2]);
                        var month = Number(str[1]) - 1;
                        var date = Number(str[0]);
                        return new Date(year, month, date);
                    }
                    var timestamp = typeof value === 'number' ? value : Date.parse(value);
                    return isNaN(timestamp) ? null : new Date(timestamp);
                };
                AppDateAdapter.prototype.format = function (date, displayFormat) {
                    if (displayFormat == "input") {
                        var day = date.getDate();
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        console.log(this.convert(year + '-' + this._to2digit(month) + '-' + this._to2digit(day)));
                        return this.convert(year + '-' + this._to2digit(month) + '-' + this._to2digit(day));
                    }
                    else if (displayFormat == "inputMonth") {
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        console.log(this.convert(year + '-' + this._to2digit(month)));
                        return this.convert(year + '-' + this._to2digit(month));
                    }
                    else {
                        console.log(this.convert(date.toDateString()));
                        return this.convert(date.toDateString());
                    }
                    // const isoDateString: string = date.toISOString();
                    // const isoDate = new Date(isoDateString);
                    // const mySQLDateString = isoDate.toJSON().slice(0, 19).replace('T', ' ');
                    // return mySQLDateString;
                };
                AppDateAdapter.prototype.convert = function (str) {
                    var date = new Date(str), mnth = ("0" + (date.getMonth() + 1)).slice(-2), day = ("0" + date.getDate()).slice(-2);
                    return [date.getFullYear(), mnth, day].join("-");
                };
                AppDateAdapter.prototype._to2digit = function (n) {
                    return ('00' + n).slice(-2);
                };
                return AppDateAdapter;
            }(_angular_material__WEBPACK_IMPORTED_MODULE_1__["NativeDateAdapter"]));
            var APP_DATE_FORMATS = {
                parse: {
                    dateInput: { month: 'short', year: 'numeric', day: 'numeric' }
                },
                display: {
                    dateInput: 'input',
                    monthYearLabel: 'inputMonth',
                    dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
                    monthYearA11yLabel: { year: 'numeric', month: 'long' },
                }
            };
            /***/ 
        }),
        /***/ "./src/app/index.ts": 
        /*!**************************!*\
          !*** ./src/app/index.ts ***!
          \**************************/
        /*! exports provided: PagerService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _services_pager_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services/pager.service */ "./src/app/services/pager.service.ts");
            /* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PagerService", function () { return _services_pager_service__WEBPACK_IMPORTED_MODULE_1__["PagerService"]; });
            /***/ 
        }),
        /***/ "./src/app/models/candidate.ts": 
        /*!*************************************!*\
          !*** ./src/app/models/candidate.ts ***!
          \*************************************/
        /*! exports provided: Candidate */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Candidate", function () { return Candidate; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            var Candidate = /** @class */ (function () {
                function Candidate() {
                }
                return Candidate;
            }());
            /***/ 
        }),
        /***/ "./src/app/models/vacancy.ts": 
        /*!***********************************!*\
          !*** ./src/app/models/vacancy.ts ***!
          \***********************************/
        /*! exports provided: Vacancy */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Vacancy", function () { return Vacancy; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            var Vacancy = /** @class */ (function () {
                function Vacancy() {
                }
                return Vacancy;
            }());
            /***/ 
        }),
        /***/ "./src/app/pipes/safehtml.pipe.ts": 
        /*!****************************************!*\
          !*** ./src/app/pipes/safehtml.pipe.ts ***!
          \****************************************/
        /*! exports provided: SafehtmlPipe */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SafehtmlPipe", function () { return SafehtmlPipe; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            var SafehtmlPipe = /** @class */ (function () {
                function SafehtmlPipe(sanitized) {
                    this.sanitized = sanitized;
                }
                SafehtmlPipe.prototype.transform = function (value) {
                    var args = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        args[_i - 1] = arguments[_i];
                    }
                    return this.sanitized.bypassSecurityTrustHtml(value);
                };
                return SafehtmlPipe;
            }());
            SafehtmlPipe.ctorParameters = function () { return [
                { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] }
            ]; };
            SafehtmlPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
                    name: 'safehtml'
                })
            ], SafehtmlPipe);
            /***/ 
        }),
        /***/ "./src/app/pipes/search-pipe.pipe.ts": 
        /*!*******************************************!*\
          !*** ./src/app/pipes/search-pipe.pipe.ts ***!
          \*******************************************/
        /*! exports provided: SearchPipePipe */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPipePipe", function () { return SearchPipePipe; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _components_candidate_list_candidate_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/candidate-list/candidate-list.component */ "./src/app/components/candidate-list/candidate-list.component.ts");
            var SearchPipePipe = /** @class */ (function () {
                function SearchPipePipe(candidateListComponent) {
                    this.candidateListComponent = candidateListComponent;
                    /*****Candidate Statuses*****/
                    this.STATUS_ALL = 0;
                    this.STATUS_NEW = 1;
                    this.STATUS_CONSIDERED = 2;
                    this.STATUS_TODAY = 3;
                    this.STATUS_OFFER = 4;
                    this.STATUS_SELECTED = 5;
                    this.STATUS_BLACKLISTED = 6;
                }
                SearchPipePipe.prototype.transform = function (value) {
                    var args = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        args[_i - 1] = arguments[_i];
                    }
                    if (!args[0]) {
                        return value;
                    }
                    if (value && value.length) {
                        return value.filter(function (el) {
                            return (el.lastname.toLowerCase().indexOf(args[0].toLocaleLowerCase()) > -1)
                                || (el.name.toLowerCase().indexOf(args[0].toLocaleLowerCase()) > -1)
                                || (el.fathername.toLowerCase().indexOf(args[0].toLocaleLowerCase()) > -1);
                        });
                    }
                    else {
                        if (args[1] === undefined || args[1] === this.STATUS_ALL) {
                            this.candidateListComponent.getCandidates();
                        }
                        else if (args[1] !== undefined || args[1] !== this.STATUS_ALL) {
                            value = undefined;
                            this.candidateListComponent.getCandidatesWithStatuses(args[1]);
                        }
                    }
                };
                return SearchPipePipe;
            }());
            SearchPipePipe.ctorParameters = function () { return [
                { type: _components_candidate_list_candidate_list_component__WEBPACK_IMPORTED_MODULE_2__["CandidateListComponent"], decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return _components_candidate_list_candidate_list_component__WEBPACK_IMPORTED_MODULE_2__["CandidateListComponent"]; }),] }] }
            ]; };
            SearchPipePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
                    name: 'searchPipe'
                }),
                tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return _components_candidate_list_candidate_list_component__WEBPACK_IMPORTED_MODULE_2__["CandidateListComponent"]; })))
            ], SearchPipePipe);
            /***/ 
        }),
        /***/ "./src/app/pipes/selected-candidate.pipe.ts": 
        /*!**************************************************!*\
          !*** ./src/app/pipes/selected-candidate.pipe.ts ***!
          \**************************************************/
        /*! exports provided: SelectedCandidatePipe */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectedCandidatePipe", function () { return SelectedCandidatePipe; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _components_candidate_list_candidate_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/candidate-list/candidate-list.component */ "./src/app/components/candidate-list/candidate-list.component.ts");
            /* harmony import */ var _services_shared_event_bus_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/shared/event-bus.service */ "./src/app/services/shared/event-bus.service.ts");
            /* harmony import */ var src_app_services_shared_event_class__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/shared/event.class */ "./src/app/services/shared/event.class.ts");
            var SelectedCandidatePipe = /** @class */ (function () {
                function SelectedCandidatePipe(candidateListComponent, eventBusService) {
                    this.candidateListComponent = candidateListComponent;
                    this.eventBusService = eventBusService;
                }
                SelectedCandidatePipe.prototype.transform = function (value) {
                    var args = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        args[_i - 1] = arguments[_i];
                    }
                    if (value && value.length) {
                        this.candidateListComponent.selectedCandidate = value[0];
                        this.eventBusService.emit(new src_app_services_shared_event_class__WEBPACK_IMPORTED_MODULE_4__["EventData"]('selectedCandidate', value[0]));
                    }
                    return value;
                };
                return SelectedCandidatePipe;
            }());
            SelectedCandidatePipe.ctorParameters = function () { return [
                { type: _components_candidate_list_candidate_list_component__WEBPACK_IMPORTED_MODULE_2__["CandidateListComponent"], decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return _components_candidate_list_candidate_list_component__WEBPACK_IMPORTED_MODULE_2__["CandidateListComponent"]; }),] }] },
                { type: _services_shared_event_bus_service__WEBPACK_IMPORTED_MODULE_3__["EventBusService"] }
            ]; };
            SelectedCandidatePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
                    name: 'selectedCandidate'
                }),
                tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return _components_candidate_list_candidate_list_component__WEBPACK_IMPORTED_MODULE_2__["CandidateListComponent"]; })))
            ], SelectedCandidatePipe);
            /***/ 
        }),
        /***/ "./src/app/services/candidate.service.ts": 
        /*!***********************************************!*\
          !*** ./src/app/services/candidate.service.ts ***!
          \***********************************************/
        /*! exports provided: CandidateService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CandidateService", function () { return CandidateService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var CandidateService = /** @class */ (function () {
                function CandidateService(http) {
                    this.http = http;
                    this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
                    this.candidateUrl = 'web/candidates'; // URL to web api
                    this.expandRelatedStatuses = '?expand=relatedStatuses';
                }
                CandidateService.prototype.getData = function () {
                    return this.http.get(this.candidateUrl)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                CandidateService.prototype.getCandidatesWithStatuses = function () {
                    return this.http.get("" + this.candidateUrl + this.expandRelatedStatuses)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                CandidateService.prototype.getDetail = function (id) {
                    return this.http.get(this.candidateUrl + "/" + id)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                CandidateService.prototype.update = function (candidate) {
                    var url = this.candidateUrl + "/" + candidate.id;
                    return this.http
                        .put(url, JSON.stringify(candidate), { headers: this.headers })
                        .toPromise()
                        .then(function () { return candidate; })
                        .catch(this.handleError);
                };
                CandidateService.prototype.create = function (candidate) {
                    var url = "" + this.candidateUrl;
                    return this.http
                        .post(url, JSON.stringify({
                        lastname: candidate.lastname,
                        name: candidate.name,
                        fathername: candidate.fathername,
                        location: candidate.location,
                        company: candidate.company,
                        position: candidate.position,
                        salary: candidate.salary,
                        date_birth: candidate.date_birth,
                        wish: candidate.wish,
                        notes: candidate.notes
                    }), { headers: this.headers })
                        .toPromise()
                        .then(function (res) { return res.json(); })
                        .catch(this.handleError);
                };
                CandidateService.prototype.delete = function (candidate_id) {
                    var url = this.candidateUrl + "/" + candidate_id;
                    return this.http
                        .delete(url, { headers: this.headers })
                        .toPromise()
                        .then(function (res) { return res.json(); })
                        .catch(this.handleError);
                };
                CandidateService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                return CandidateService;
            }());
            CandidateService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            CandidateService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], CandidateService);
            /***/ 
        }),
        /***/ "./src/app/services/cities.service.ts": 
        /*!********************************************!*\
          !*** ./src/app/services/cities.service.ts ***!
          \********************************************/
        /*! exports provided: CitiesService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CitiesService", function () { return CitiesService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var CitiesService = /** @class */ (function () {
                function CitiesService(http) {
                    this.http = http;
                    this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
                    this.citiesUrl = 'web/cities'; // URL to web api
                }
                CitiesService.prototype.getData = function () {
                    return this.http.get(this.citiesUrl)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                CitiesService.prototype.getDetail = function (id) {
                    return this.http.get(this.citiesUrl + "/" + id)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                CitiesService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                CitiesService.prototype.update = function (city) {
                    var url = this.citiesUrl + "/" + city.id;
                    return this.http
                        .put(url, JSON.stringify(city), { headers: this.headers })
                        .toPromise()
                        .then(function () { return city; })
                        .catch(this.handleError);
                };
                return CitiesService;
            }());
            CitiesService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            CitiesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], CitiesService);
            /***/ 
        }),
        /***/ "./src/app/services/company.service.ts": 
        /*!*********************************************!*\
          !*** ./src/app/services/company.service.ts ***!
          \*********************************************/
        /*! exports provided: CompanyService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanyService", function () { return CompanyService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var CompanyService = /** @class */ (function () {
                function CompanyService(http) {
                    this.http = http;
                    this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
                    this.companyUrl = 'web/companies'; // URL to web api
                    this.expandRelatedStatuses = '?expand=relatedStatuses';
                }
                CompanyService.prototype.getDetail = function (id) {
                    return this.http.get(this.companyUrl + "/" + id)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                CompanyService.prototype.getAllCompanies = function () {
                    return this.http.get("" + this.companyUrl)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                CompanyService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                return CompanyService;
            }());
            CompanyService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            CompanyService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], CompanyService);
            /***/ 
        }),
        /***/ "./src/app/services/contacts-candidate.service.ts": 
        /*!********************************************************!*\
          !*** ./src/app/services/contacts-candidate.service.ts ***!
          \********************************************************/
        /*! exports provided: ContactsCandidateService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsCandidateService", function () { return ContactsCandidateService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var ContactsCandidateService = /** @class */ (function () {
                function ContactsCandidateService(http) {
                    this.http = http;
                    this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
                    this.candidateUrl = "web/candidates/";
                    this.contactsCandidateUrl = "/contacts-candidate";
                }
                ContactsCandidateService.prototype.getCandidateContacts = function (candidate_id) {
                    return this.http
                        .get(this.candidateUrl + ("" + candidate_id) + this.contactsCandidateUrl)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                ContactsCandidateService.prototype.getCandidateContactTypeName = function (contact_id) {
                    var contactsCandidateTypeUrl = 'web/contacts-type/';
                    return this.http
                        .get("" + contactsCandidateTypeUrl + contact_id)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                ContactsCandidateService.prototype.addContactCandidate = function (contactCandidate) {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var candidateId;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    candidateId = contactCandidate.candidate_id;
                                    return [4 /*yield*/, this.http
                                            .post(this.candidateUrl + candidateId + this.contactsCandidateUrl, JSON.stringify({
                                            candidate_id: candidateId,
                                            contact_type: contactCandidate.contact_type,
                                            value: contactCandidate.value,
                                        }), { headers: this.headers })
                                            .toPromise()
                                            .then(function (res) { console.log('addedContactValue', contactCandidate.value); return res.json(); })
                                            .catch(this.handleError)];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        });
                    });
                };
                ContactsCandidateService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                return ContactsCandidateService;
            }());
            ContactsCandidateService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            ContactsCandidateService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], ContactsCandidateService);
            /***/ 
        }),
        /***/ "./src/app/services/contacts-customer.service.ts": 
        /*!*******************************************************!*\
          !*** ./src/app/services/contacts-customer.service.ts ***!
          \*******************************************************/
        /*! exports provided: ContactsCustomerService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsCustomerService", function () { return ContactsCustomerService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var ContactsCustomerService = /** @class */ (function () {
                function ContactsCustomerService(http) {
                    this.http = http;
                    this.headers = new Headers({ 'Content-Type': 'application/json' });
                    this.candidateUrl = "web/customers/";
                    this.contactsCandidateUrl = "/contacts-customer";
                }
                ContactsCustomerService.prototype.getCustomerContacts = function (customer_id) {
                    return this.http
                        .get(this.candidateUrl + ("" + customer_id) + this.contactsCandidateUrl)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                ContactsCustomerService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                return ContactsCustomerService;
            }());
            ContactsCustomerService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            ContactsCustomerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], ContactsCustomerService);
            /***/ 
        }),
        /***/ "./src/app/services/customer.service.ts": 
        /*!**********************************************!*\
          !*** ./src/app/services/customer.service.ts ***!
          \**********************************************/
        /*! exports provided: CustomerService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerService", function () { return CustomerService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var CustomerService = /** @class */ (function () {
                function CustomerService(http) {
                    this.http = http;
                    this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
                    this.customerUrl = 'web/customers'; // URL to web api
                }
                CustomerService.prototype.getData = function () {
                    return this.http.get(this.customerUrl)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                CustomerService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                return CustomerService;
            }());
            CustomerService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            CustomerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], CustomerService);
            /***/ 
        }),
        /***/ "./src/app/services/file.service.ts": 
        /*!******************************************!*\
          !*** ./src/app/services/file.service.ts ***!
          \******************************************/
        /*! exports provided: FileService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileService", function () { return FileService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var FileService = /** @class */ (function () {
                function FileService(http) {
                    this.http = http;
                    this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
                }
                FileService.prototype.getCandidateResume = function (candidate_id) {
                    var resumeUrl = 'web/candidates';
                    return this.http.get(resumeUrl + '/' + ("" + candidate_id) + '/documents-candidate')
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                FileService.prototype.getCandidateDocuments = function (candidate_id) {
                    var resumeUrl = 'web/candidates';
                    return this.http.get(resumeUrl + '/' + ("" + candidate_id) + '/documents-candidate')
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                FileService.prototype.uploadFile = function (file, candidate_id, fileType) {
                    var formData = new FormData();
                    var resumeCreateUrl = 'web/candidates';
                    formData.append('file', file);
                    this.headers.set('file-type', fileType.toString());
                    return this.http.post(resumeCreateUrl + '/' + ("" + candidate_id) + '/documents-candidate', formData, { headers: this.headers });
                };
                FileService.prototype.deleteFile = function (candidate_id) {
                    var fileDeleteUrl = 'web/candidates';
                    return this.http.delete(fileDeleteUrl + '/' + ("" + candidate_id) + '/documents-candidate', { headers: this.headers });
                };
                FileService.prototype.deleteFileByName = function (candidate_id, deletedDocumentId) {
                    var fileDeleteUrl = 'web/candidates';
                    return this.http.delete(fileDeleteUrl + '/' + ("" + candidate_id) + '/documents-candidate/' + ("" + deletedDocumentId), { headers: this.headers });
                };
                FileService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                return FileService;
            }());
            FileService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            FileService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], FileService);
            /***/ 
        }),
        /***/ "./src/app/services/image.service.ts": 
        /*!*******************************************!*\
          !*** ./src/app/services/image.service.ts ***!
          \*******************************************/
        /*! exports provided: ImageService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageService", function () { return ImageService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var ImageService = /** @class */ (function () {
                function ImageService(http) {
                    this.http = http;
                }
                ImageService.prototype.uploadPhoto = function (photo, candidate_id) {
                    var formData = new FormData();
                    var photoCreateUrl = 'web/candidates';
                    formData.append('photo', photo);
                    return this.http.post(photoCreateUrl + '/' + ("" + candidate_id) + '/photo', formData);
                };
                ImageService.prototype.deletePhoto = function (candidate_id) {
                    var photoDeleteUrl = 'web/candidates';
                    return this.http.delete(photoDeleteUrl + '/' + ("" + candidate_id) + '/photo');
                };
                return ImageService;
            }());
            ImageService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            ImageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], ImageService);
            /***/ 
        }),
        /***/ "./src/app/services/mail.service.ts": 
        /*!******************************************!*\
          !*** ./src/app/services/mail.service.ts ***!
          \******************************************/
        /*! exports provided: MailService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MailService", function () { return MailService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var MailService = /** @class */ (function () {
                function MailService(http) {
                    this.http = http;
                    this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
                }
                MailService.prototype.sendMail = function (customer, vacancy, contact_name, contact_value, attached_document, letter_body) {
                    if (customer === void 0) { customer = ''; }
                    if (vacancy === void 0) { vacancy = ''; }
                    var mailUrl = 'web/mail';
                    return this.http
                        .post(mailUrl, JSON.stringify({
                        customer: customer,
                        vacancy: vacancy,
                        contact_name: contact_name,
                        contact_value: contact_value,
                        attached_document: attached_document,
                        letter_body: letter_body,
                    }), { headers: this.headers })
                        .toPromise()
                        .then(function (res) { return res.json(); })
                        .catch(this.handleError);
                };
                MailService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                return MailService;
            }());
            MailService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            MailService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], MailService);
            /***/ 
        }),
        /***/ "./src/app/services/pager.service.ts": 
        /*!*******************************************!*\
          !*** ./src/app/services/pager.service.ts ***!
          \*******************************************/
        /*! exports provided: PagerService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagerService", function () { return PagerService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            var PagerService = /** @class */ (function () {
                function PagerService() {
                }
                PagerService.prototype.getPager = function (totalItems, currentPage, pageSize) {
                    if (currentPage === void 0) { currentPage = 1; }
                    if (pageSize === void 0) { pageSize = 10; }
                    // calculate total pages
                    var totalPages = Math.ceil(totalItems / pageSize);
                    // ensure current page isn't out of range
                    if (currentPage < 1) {
                        currentPage = 1;
                    }
                    else if (currentPage > totalPages) {
                        currentPage = totalPages;
                    }
                    var startPage, endPage;
                    if (totalPages <= 10) {
                        // less than 10 total pages so show all
                        startPage = 1;
                        endPage = totalPages;
                    }
                    else {
                        // more than 10 total pages so calculate start and end pages
                        if (currentPage <= 6) {
                            startPage = 1;
                            endPage = 10;
                        }
                        else if (currentPage + 4 >= totalPages) {
                            startPage = totalPages - 9;
                            endPage = totalPages;
                        }
                        else {
                            startPage = currentPage - 5;
                            endPage = currentPage + 4;
                        }
                    }
                    // calculate start and end item indexes
                    var startIndex = (currentPage - 1) * pageSize;
                    var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
                    // create an array of pages to ng-repeat in the pager control
                    var pages = Array.from(Array((endPage + 1) - startPage).keys()).map(function (i) { return startPage + i; });
                    // return object with all pager properties required by the view
                    return {
                        totalItems: totalItems,
                        currentPage: currentPage,
                        pageSize: pageSize,
                        totalPages: totalPages,
                        startPage: startPage,
                        endPage: endPage,
                        startIndex: startIndex,
                        endIndex: endIndex,
                        pages: pages
                    };
                };
                return PagerService;
            }());
            /***/ 
        }),
        /***/ "./src/app/services/shared/event-bus.service.ts": 
        /*!******************************************************!*\
          !*** ./src/app/services/shared/event-bus.service.ts ***!
          \******************************************************/
        /*! exports provided: EventBusService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventBusService", function () { return EventBusService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
            /* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
            var EventBusService = /** @class */ (function () {
                function EventBusService() {
                    this.subject$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
                }
                EventBusService.prototype.emit = function (event) {
                    this.subject$.next(event);
                };
                EventBusService.prototype.on = function (eventName, action) {
                    return this.subject$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function (e) { return e.name === eventName; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (e) { return e["value"]; })).subscribe(action);
                };
                return EventBusService;
            }());
            EventBusService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], EventBusService);
            /***/ 
        }),
        /***/ "./src/app/services/shared/event.class.ts": 
        /*!************************************************!*\
          !*** ./src/app/services/shared/event.class.ts ***!
          \************************************************/
        /*! exports provided: EventData */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventData", function () { return EventData; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            var EventData = /** @class */ (function () {
                function EventData(name, value) {
                    this.name = name;
                    this.value = value;
                }
                return EventData;
            }());
            /***/ 
        }),
        /***/ "./src/app/services/skills-candidate.service.ts": 
        /*!******************************************************!*\
          !*** ./src/app/services/skills-candidate.service.ts ***!
          \******************************************************/
        /*! exports provided: SkillsCandidateService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SkillsCandidateService", function () { return SkillsCandidateService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var SkillsCandidateService = /** @class */ (function () {
                function SkillsCandidateService(http) {
                    this.http = http;
                    this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
                    this.candidateUrl = "web/candidates/";
                    this.skillsCandidateUrl = "/skills-candidate";
                }
                SkillsCandidateService.prototype.getAllCandidatesSkills = function (candidate_id) {
                    return this.http.get(this.candidateUrl + ("" + candidate_id) + '?expand=relatedSkills&fields=relatedSkills')
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                SkillsCandidateService.prototype.getData = function (candidate_id) {
                    console.log(this.candidateUrl + candidate_id + this.skillsCandidateUrl);
                    return this.http.get(this.candidateUrl + candidate_id + this.skillsCandidateUrl)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                SkillsCandidateService.prototype.addSkillCandidate = function (skillCandidate) {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var skillId, candidateId;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    skillId = skillCandidate.skill_id;
                                    candidateId = skillCandidate.candidate_id;
                                    return [4 /*yield*/, this.http
                                            .post(this.candidateUrl + candidateId + this.skillsCandidateUrl, JSON.stringify({
                                            skill_id: skillId,
                                            candidate_id: candidateId,
                                        }), { headers: this.headers })
                                            .toPromise()
                                            .then(function (res) { console.log('addedSkillId', skillId); return res.json(); })
                                            .catch(this.handleError)];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        });
                    });
                };
                SkillsCandidateService.prototype.removeSkillCandidate = function (id, candidate_id) {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var deletedId;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    deletedId = "" + id;
                                    return [4 /*yield*/, this.http
                                            .delete(this.candidateUrl + candidate_id + this.skillsCandidateUrl + '/' + deletedId)
                                            .toPromise()
                                            .then(function () { return console.log("DeletedSkillId", deletedId); }).catch(this.handleError)];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        });
                    });
                };
                SkillsCandidateService.prototype.removeAllSkillsCandidate = function (candidate_id) {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, this.getData(candidate_id)
                                        .then(function (skillCandidate) { return skillCandidate.forEach(function (skill) { return _this.http
                                        .delete(_this.candidateUrl + candidate_id + _this.skillsCandidateUrl + '/' + skill.id)
                                        .toPromise()
                                        .then(function (res) { console.log('deletedSkillId', skill.id, 'deleted Skill_id', skill.skill_id); return res.json(); })
                                        .catch(_this.handleError); }); })];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        });
                    });
                };
                SkillsCandidateService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                return SkillsCandidateService;
            }());
            SkillsCandidateService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            SkillsCandidateService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], SkillsCandidateService);
            /***/ 
        }),
        /***/ "./src/app/services/skills-vacancy.service.ts": 
        /*!****************************************************!*\
          !*** ./src/app/services/skills-vacancy.service.ts ***!
          \****************************************************/
        /*! exports provided: SkillsVacancyService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SkillsVacancyService", function () { return SkillsVacancyService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var SkillsVacancyService = /** @class */ (function () {
                function SkillsVacancyService(http) {
                    this.http = http;
                    this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
                    this.vacancyUrl = "web/vacancies/";
                    this.skillsVacancyUrl = "/skills-vacancy";
                }
                SkillsVacancyService.prototype.getAllVacanciesSkills = function (vacancy_id) {
                    return this.http.get(this.vacancyUrl + ("" + vacancy_id) + '?expand=relatedSkills&fields=relatedSkills')
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                SkillsVacancyService.prototype.getData = function (vacancy_id) {
                    console.log(this.vacancyUrl + vacancy_id + this.skillsVacancyUrl);
                    return this.http.get(this.vacancyUrl + vacancy_id + this.skillsVacancyUrl)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                SkillsVacancyService.prototype.addSkillVacancy = function (skillVacancy) {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var skillId, vacancyId;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    skillId = skillVacancy.skill_id;
                                    vacancyId = skillVacancy.vacancy_id;
                                    return [4 /*yield*/, this.http
                                            .post(this.vacancyUrl + vacancyId + this.skillsVacancyUrl, JSON.stringify({
                                            skill_id: skillId,
                                            vacancy_id: vacancyId,
                                        }), { headers: this.headers })
                                            .toPromise()
                                            .then(function (res) { console.log('addedSkillId', skillId); return res.json(); })
                                            .catch(this.handleError)];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        });
                    });
                };
                SkillsVacancyService.prototype.removeSkillVacancy = function (id, vacancy_id) {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var deletedId;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    deletedId = "" + id;
                                    return [4 /*yield*/, this.http
                                            .delete(this.vacancyUrl + vacancy_id + this.skillsVacancyUrl + '/' + deletedId)
                                            .toPromise()
                                            .then(function () { return console.log("DeletedSkillId", deletedId); }).catch(this.handleError)];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        });
                    });
                };
                SkillsVacancyService.prototype.removeAllSkillsVacancy = function (vacancy_id) {
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, this.getData(vacancy_id)
                                        .then(function (skillVacancy) { return skillVacancy.forEach(function (skill) { return _this.http
                                        .delete(_this.vacancyUrl + vacancy_id + _this.skillsVacancyUrl + '/' + skill.id)
                                        .toPromise()
                                        .then(function (res) { console.log('deletedSkillId', skill.id, 'deleted Skill_id', skill.skill_id); return res.json(); })
                                        .catch(_this.handleError); }); })];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        });
                    });
                };
                SkillsVacancyService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                return SkillsVacancyService;
            }());
            SkillsVacancyService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            SkillsVacancyService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], SkillsVacancyService);
            /***/ 
        }),
        /***/ "./src/app/services/skills.service.ts": 
        /*!********************************************!*\
          !*** ./src/app/services/skills.service.ts ***!
          \********************************************/
        /*! exports provided: SkillsService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SkillsService", function () { return SkillsService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var SkillsService = /** @class */ (function () {
                function SkillsService(http) {
                    this.http = http;
                    this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
                    this.skillsUrl = 'web/skills'; // URL to web api
                }
                SkillsService.prototype.getData = function () {
                    return this.http.get(this.skillsUrl)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                SkillsService.prototype.getDetail = function (id) {
                    return this.http.get(this.skillsUrl + "/" + id)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                SkillsService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                SkillsService.prototype.update = function (skill) {
                    var url = this.skillsUrl + "/" + skill.id;
                    return this.http
                        .put(url, JSON.stringify(skill), { headers: this.headers })
                        .toPromise()
                        .then(function () { return skill; })
                        .catch(this.handleError);
                };
                return SkillsService;
            }());
            SkillsService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            SkillsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], SkillsService);
            /***/ 
        }),
        /***/ "./src/app/services/vacancy-documents.service.ts": 
        /*!*******************************************************!*\
          !*** ./src/app/services/vacancy-documents.service.ts ***!
          \*******************************************************/
        /*! exports provided: VacancyDocumentsService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VacancyDocumentsService", function () { return VacancyDocumentsService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var VacancyDocumentsService = /** @class */ (function () {
                function VacancyDocumentsService(http) {
                    this.http = http;
                    this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
                }
                VacancyDocumentsService.prototype.getVacancyDocuments = function (vacancy_id) {
                    var vacancyUrl = 'web/vacancies';
                    return this.http.get(vacancyUrl + '/' + ("" + vacancy_id) + '/documents-vacancy')
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                VacancyDocumentsService.prototype.uploadFile = function (file, vacancy_id) {
                    var formData = new FormData();
                    var vacancyDocumentCreateUrl = 'web/vacancies';
                    formData.append('file', file);
                    return this.http.post(vacancyDocumentCreateUrl + '/' + ("" + vacancy_id) + '/documents-vacancy', formData, { headers: this.headers });
                };
                VacancyDocumentsService.prototype.deleteFileByName = function (vacancy_id, deletedDocumentId) {
                    var fileDeleteUrl = 'web/vacancies';
                    return this.http.delete(fileDeleteUrl + '/' + ("" + vacancy_id) + '/documents-vacancy/' + ("" + deletedDocumentId), { headers: this.headers });
                };
                VacancyDocumentsService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                return VacancyDocumentsService;
            }());
            VacancyDocumentsService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            VacancyDocumentsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], VacancyDocumentsService);
            /***/ 
        }),
        /***/ "./src/app/services/vacancy.service.ts": 
        /*!*********************************************!*\
          !*** ./src/app/services/vacancy.service.ts ***!
          \*********************************************/
        /*! exports provided: VacancyService */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VacancyService", function () { return VacancyService; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
            var VacancyService = /** @class */ (function () {
                function VacancyService(http) {
                    this.http = http;
                    this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'application/json' });
                    this.vacancyUrl = 'web/vacancies'; // URL to web api
                }
                VacancyService.prototype.getData = function () {
                    return this.http.get(this.vacancyUrl)
                        .toPromise()
                        .then(function (response) { return response.json(); })
                        .catch(this.handleError);
                };
                VacancyService.prototype.update = function (vacancy) {
                    var url = this.vacancyUrl + "/" + vacancy.id;
                    return this.http
                        .put(url, JSON.stringify(vacancy), { headers: this.headers })
                        .toPromise()
                        .then(function () { return vacancy; })
                        .catch(this.handleError);
                };
                VacancyService.prototype.create = function (vacancy) {
                    var url = "" + this.vacancyUrl;
                    return this.http
                        .post(url, JSON.stringify({
                        name: vacancy.name,
                        company_id: vacancy.company_id,
                        location: vacancy.location,
                        salary: vacancy.salary,
                        //skills here
                        v_duties: vacancy.v_duties,
                        w_conditions: vacancy.w_conditions,
                        v_requirements: vacancy.v_requirements,
                        v_description: vacancy.v_description,
                    }), { headers: this.headers })
                        .toPromise()
                        .then(function (res) { return res.json(); })
                        .catch(this.handleError);
                };
                VacancyService.prototype.delete = function (vacancy_id) {
                    var url = this.vacancyUrl + "/" + vacancy_id;
                    return this.http
                        .delete(url, { headers: this.headers })
                        .toPromise()
                        .then(function (res) { return res.json(); })
                        .catch(this.handleError);
                };
                VacancyService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                return VacancyService;
            }());
            VacancyService.ctorParameters = function () { return [
                { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] }
            ]; };
            VacancyService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
                    providedIn: 'root'
                })
            ], VacancyService);
            /***/ 
        }),
        /***/ "./src/environments/environment.ts": 
        /*!*****************************************!*\
          !*** ./src/environments/environment.ts ***!
          \*****************************************/
        /*! exports provided: environment */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function () { return environment; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            // This file can be replaced during build by using the `fileReplacements` array.
            // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
            // The list of file replacements can be found in `angular.json`.
            var environment = {
                production: false
            };
            /*
             * For easier debugging in development mode, you can import the following file
             * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
             *
             * This import should be commented out in production mode because it will have a negative impact
             * on performance if an error is thrown.
             */
            // import 'zone.js/dist/zone-error';  // Included with Angular CLI.
            /***/ 
        }),
        /***/ "./src/main.ts": 
        /*!*********************!*\
          !*** ./src/main.ts ***!
          \*********************/
        /*! no exports provided */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
            /* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/ __webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_1__);
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
            /* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
            /* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
            if (_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].production) {
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["enableProdMode"])();
            }
            Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_4__["AppModule"])
                .catch(function (err) { return console.error(err); });
            /***/ 
        }),
        /***/ 0: 
        /*!***************************!*\
          !*** multi ./src/main.ts ***!
          \***************************/
        /*! no static exports found */
        /***/ (function (module, exports, __webpack_require__) {
            module.exports = __webpack_require__(/*! /var/www/html/recruiter-rest/angular/src/main.ts */ "./src/main.ts");
            /***/ 
        })
    }, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es2015.js.map
//# sourceMappingURL=main-es5.js.map
//# sourceMappingURL=main-es5.js.map