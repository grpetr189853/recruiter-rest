<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 10:16
 */

namespace app\crm\interfaces;


interface ICandidate
{
    public function getName();

}