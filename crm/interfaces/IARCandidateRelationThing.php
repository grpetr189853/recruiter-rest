<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 04.04.2019
 * Time: 10:57
 */

namespace app\crm\interfaces;


interface IARCandidateRelationThing
{
    public static function tableName(): string;
    public function afterFind(): void;
    public function beforeSave($insert): bool;

}