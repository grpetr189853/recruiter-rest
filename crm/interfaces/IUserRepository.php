<?php
namespace app\crm\interfaces;

use app\crm\entities\user\User;

interface IUserRepository
{
    public function get($id): User;
    public function add(User $entity):void;
    public function save(User $entity): void;
    public function remove(User $entity): void;
    public function getBy(array $condition): User;
    public function nextId(): IEntityId;
    public function getAll(): array;
}