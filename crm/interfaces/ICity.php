<?php


namespace app\crm\interfaces;


interface ICity
{
    public function getId(): string ;
    public function getName(): string;
    public function getCountryId() : string;
}