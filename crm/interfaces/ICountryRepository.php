<?php


namespace app\crm\interfaces;

use app\crm\entities\Country;

interface ICountryRepository
{
    public function get($id): Country;
    public function getBy(array $condition);
}