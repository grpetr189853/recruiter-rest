<?php
/**
 * Created by PhpStorm.
 * User: Dmytro
 * Date: 28.03.2019
 * Time: 15:54
 */

namespace app\crm\interfaces;


interface IARVacancy
{
    public function insert();
    public function update();
    public function delete();

}