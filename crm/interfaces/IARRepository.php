<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 16:06
 */

namespace app\crm\interfaces;


interface IARRepository
{
    public function get($id): IARCandidate;
    public function add(IARCandidate $entity):void;
    public function save(IARCandidate $entity): void;
    public function remove(IARCandidate $entity): void;
    public function getBy(array $condition): IARCandidate;
    public function nextId(): IEntityId;
}