<?php


namespace app\crm\interfaces;


use app\crm\entities\candidate\Documents;

interface IDocumentService
{
    public function setDocuments(Documents $current_documents, array $documents): Documents;
}