<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 16.04.2019
 * Time: 14:29
 */
namespace app\crm\interfaces;

use app\crm\entities\group\Group;

interface IGroupRepository
{
    public function get($name): Group;
    public function add(Group $entity):void;
    public function save(Group $entity): void;
    public function getBy(array $condition): Group;
    public function getAll(): array;
}