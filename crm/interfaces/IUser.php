<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 10:16
 */
namespace app\crm\interfaces;


use app\crm\entities\candidate\Info;
use app\crm\entities\candidate\Name;
use app\crm\entities\user\UserId;

interface IUser
{
    public function getId(): UserId;
    public function getName(): Name;
    public function getEmail();
    public function getInfo(): Info;
    public function getDateBirth(): \DateTimeImmutable;
    public function getLocation();
    public function getGroupName();
    public function getPassword();
    public function getRepeatPassword();
    public function getStatus();
}