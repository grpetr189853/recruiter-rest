<?php


namespace app\crm\interfaces;


interface ICountry
{
    public function getId(): string ;
    public function getName(): string;
}