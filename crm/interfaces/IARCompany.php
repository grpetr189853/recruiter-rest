<?php


namespace app\crm\interfaces;


interface IARCompany
{
    public function insert();
    public function update();
    public function delete();
}