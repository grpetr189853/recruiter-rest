<?php


namespace app\crm\interfaces;


use app\crm\entities\candidate\Candidate;

interface ICandidateRepository
{
    public function get($id): Candidate;
    public function add(Candidate $entity):void;
    public function save(Candidate $entity): void;
    public function remove(Candidate $entity): void;
    public function getBy(array $condition): Candidate;
    public function nextId(): IEntityId;
    public function getAll(): array;
}