<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 10:47
 */

namespace app\crm\interfaces;


interface IEventDispatcher
{
    public function dispatchAll(array $events): void;
    public function dispatch($event): void;
}