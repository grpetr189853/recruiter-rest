<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 05.04.2019
 * Time: 15:10
 */

namespace app\crm\tests\unit\candidates;


use app\crm\entities\candidate\Candidate;
use app\crm\entities\candidate\CandidateId;
use app\crm\entities\candidate\ContactCandidate;
use app\crm\entities\candidate\Info;
use app\crm\entities\candidate\Name;
use app\crm\entities\candidate\SkillCandidate;
use Ramsey\Uuid\Uuid;
use app\crm\entities\ContactAbstract as CA;

class CandidateBuilder
{

    private $contacts     = [];
    private $skills       = [];

    public function __construct()
    {
        $this->contacts[] = new ContactCandidate(CA::PHONE,'+3805000000001');
        $this->contacts[] = new ContactCandidate(CA::EMAIL,'my@gmail.com');
        $this->contacts[] = new ContactCandidate(CA::SKYPE,'jaskins.gaskins');

        $this->skills[]   = new SkillCandidate(1);
        $this->skills[]   = new SkillCandidate(2);
        $this->skills[]   = new SkillCandidate(3);
    }

    public static function instance()
    {
        return new self();
    }

    public function withContacts(array $contacts)
    {
        $this->contacts = $contacts;
        return $this;
    }

    public function withSkills(array $skills)
    {
        $this->skills = $skills;
        return $this;
    }


    public function build()
    {
        $uuid = Uuid::uuid4()->toString();
        $id = new CandidateId($uuid);
        $name = new Name('Dmytro','Mytsko', "Ivanovich");
        $info = new Info(3500, 'Sunk International Ltd', 'Senior Pascal Developer', new \DateTimeImmutable('1987-04-28'));

        $candidate = Candidate::create($id, $name, $info, 1, $this->contacts, $this->skills);

        return $candidate;
    }
}