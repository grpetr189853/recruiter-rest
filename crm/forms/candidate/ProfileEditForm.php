<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 15.04.2019
 * Time: 10:10
 */

namespace app\crm\forms\candidate;


use app\crm\interfaces\ICandidate;
use yii\base\Model;
use yii\helpers\ArrayHelper;


class ProfileEditForm extends Model
{

    public $lastname;
    public $name;
    public $fathername;
    public $salary;
    public $location;
    public $current_company;
    public $current_position;
    public $birth;
    public $photo;
    public $notes;

    public $contacts=[];
    public $skills=[];
    public $documents=[];

    public function __construct(ICandidate $candidate, array $config = [])
    {
        $this->name             = $candidate->getName()->getFirst();
        $this->lastname         = $candidate->getName()->getLast();
        $this->fathername       = $candidate->getName()->getFather();
        $this->birth            = $candidate->getBirthDate()->format('Y-m-d');
        $this->location         = $candidate->location;

        $this->salary           = $candidate->getSalary();
        $this->current_company  = $candidate->getCompany();
        $this->current_position = $candidate->getPosition();
        $this->skills           = ArrayHelper::map($candidate->getSkills(),'skill_id','skill.name');
        $this->contacts         = $candidate->getContacts();
        $this->photo            = $candidate->getPhoto();
        $this->notes            = $candidate->getNotes();
        $this->documents        = $candidate->getDocuments();
        parent::__construct($config);
    }


    public function rules(): array
    {
        return [
            [['name','lastname','fathername','current_company','current_position','birth', 'photo', 'notes'], 'string'],
            [['salary','location'], 'integer'],
            [['skills','contacts', 'documents'],'safe'],
        ];
    }
}