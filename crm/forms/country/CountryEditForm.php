<?php
namespace app\crm\forms\country;

use app\crm\interfaces\ICountry;
use yii\base\Model;

class CountryEditForm extends Model
{
    public $id;
    public $name;

    private $country;

    public function __construct(ICountry $country, $config = [])
    {
        $this->country = $country;
        $this->id = $country->getId();
        $this->name = $country->getName();
        parent::__construct($config);
    }
    public function rules()
    {
        return [
            [[ 'name', ], 'string'],
            [['id'], 'integer'],
            [['name'], 'required'],
        ];
    }
}