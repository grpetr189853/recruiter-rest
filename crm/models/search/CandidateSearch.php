<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 22.04.2019
 * Time: 17:30
 */

namespace app\crm\models\search;


use app\crm\entities\candidate\Candidate;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper as AH;

class CandidateSearch extends Model
{

    public $name;
    public $lastname;
    public $position;
    public $location;
    public $salary;
    public $skills;
    public $country;
    public function search($params)
    {
        $query = Candidate::find()
            ->from('candidates c')
            ->leftJoin('skills_candidate sk','sk.candidate_id = c.id')
            ->leftJoin('cities cc','cc.id = c.location')
            ->leftJoin('countries ccc','ccc.id = cc.country_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere([
            'lastname'  => AH::getValue($params,'CandidateSearch.lastname'),
            'name'      => AH::getValue($params,'CandidateSearch.name'),
        ]);

        $query->andFilterWhere(['like', 'position',    AH::getValue($params,'CandidateSearch.position')]);
        $query->andFilterWhere(['in',   'location',    AH::getValue($params,'CandidateSearch.location')]);
        $query->andFilterWhere(['in',   'sk.skill_id', AH::getValue($params,'CandidateSearch.skills')]);
        $query->andFilterWhere(['in',   'cc.country_id', AH::getValue($params,'CandidateSearch.country')]);

        return $dataProvider;
    }


    public function rules(): array
    {
        return [
            [['name','lastname','position'], 'string'],
            [['salary','location'], 'integer'],
            [['skills','country'],'safe'],
        ];
    }

}