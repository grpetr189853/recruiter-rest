<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 29.03.2019
 * Time: 13:57
 */

namespace app\crm\entities;


abstract class ContactAbstract
{
    const PHONE     = 1;
    const EMAIL     = 2;
    const SKYPE     = 3;
    const LINKEDIN  = 4;
    const GITHUB    = 5;

    protected $type;
    protected $value;
}