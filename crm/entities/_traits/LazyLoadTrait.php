<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 16:31
 */

namespace app\crm\entities\_traits;

use ProxyManager\Factory\LazyLoadingValueHolderFactory;

trait LazyLoadTrait
{

    protected static function getLazyFactory()
    {
        return \Yii::createObject(LazyLoadingValueHolderFactory::class);
    }
}