<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 15:31
 */
namespace app\crm\entities\group;

use app\crm\entities\_traits\LazyLoadTrait;
use app\crm\entities\candidate\Name;
use app\crm\interfaces\IARGroup;
use app\crm\interfaces\IGroup;
use app\crm\interfaces\IEntity;
use app\crm\interfaces\IGroupEntity;
use yii\db\ActiveRecord;
use yii\db\Expression;

/***
 * Class Group
 * @package app\crm\entities\group
 *
 */
class Group extends ActiveRecord implements IEntity, IGroup, IARGroup, IGroupEntity
{
    use LazyLoadTrait;

    /***@var $name ***/
    private $name;

    /***@var $name ***/
    private $description;

    public static function create(string $name): self
    {
        $group = new static();
        $group->name = $name;

        return $group;
    }

    public function rename(Name $name): void
    {
        $this->name = $name;
    }

    public function remove(): void
    {
        if(false) {
            throw new \DomainException('Some reason why can not delete user');
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function export(): array
    {
        $data = [];
        return $data;
    }

    public function getType(): string
    {
        return "group";
    }

    /**
     * @return array
     */
    public function releaseEvents(): array
    {
        // TODO: Implement releaseEvents() method.
        return [];
    }


    /***##SERVICE METHODS **/

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auth_item}}';
    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];

    }

    public function beforeSave($insert): bool
    {
        $this->setAttribute('name', $this->getName());

        return parent::beforeSave($insert);
    }

    public function addRole()
    {
        $auth = \Yii::$app->authManager;
        $admin = $auth->createRole($this->getName());
        $admin->description = $this->description;
        $auth->add($admin);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновление',
        ];
    }
}