<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 14:24
 */

namespace app\crm\entities;

use Assert\Assertion;

abstract class Id
{
    protected $id;

    /***
     * Id constructor.
     * @param null $id
     */
    public function __construct($id = null)
    {
        Assertion::notEmpty($id);
        $this->id = $id;
    }

    /***
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /***
     * @param Id $other
     * @return bool
     */
    public function isEqualTo(self $other): bool
    {
        return $this->getId() === $other->getId();
    }
}