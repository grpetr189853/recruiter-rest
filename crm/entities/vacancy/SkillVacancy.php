<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 03.04.2019
 * Time: 15:33
 */

namespace app\crm\entities\vacancy;


use app\crm\entities\_traits\InstantiateTrait;
use app\crm\entities\Skill;
use app\crm\interfaces\IARCandidateRelationThing;
use app\crm\interfaces\ISkill;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use Assert\Assertion;

/***
 * Class SkillVacancy
 * @package app\crm\entities\vacancy
 *
 * @property Skill $skill
 *
 */
class SkillVacancy extends ActiveRecord implements ISkill, IARCandidateRelationThing
{
    use InstantiateTrait;

    private $skill_id;
    private $vacancy_id;
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
/*
    public function __construct(int $skill_id)
    {
        $this->skill_id = $skill_id;

        parent::__construct();
    }
*/

    public static function create(int $skill_id,string $vacancy_id): self
    {
        $skill_vacancy = new static();
        $skill_vacancy->skill_id = $skill_id;
        $skill_vacancy->vacancy_id = $vacancy_id;
        return $skill_vacancy;
    }

    public function getSkillId()
    {
        return $this->skill_id;
    }

    public function getCandidateId()
    {
        return $this->vacancy_id;
    }


    public function isEqualTo(ISkill $skill): bool
    {
        return $this->skill_id === $skill->skill_id;
    }

    /***### - ###***/
    public static function tableName(): string
    {
        return '{{%skills_vacancy}}';
    }

    public function afterFind(): void
    {
        $this->skill_id = $this->getAttribute('skill_id');
        $this->vacancy_id = $this->getAttribute('vacancy_id');
        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {

        return parent::beforeSave($insert);
    }

    public function getSkill(): ActiveQuery
    {
        return $this->hasOne(Skill::className(), ['id'=>'skill_id']);
    }

    public function rules()
    {
        return [
            [['skill_id','vacancy_id'], 'safe','on' => self::SCENARIO_CREATE],
            [['skill_id','vacancy_id'], 'safe','on' => self::SCENARIO_UPDATE],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['skill_id','vacancy_id'],
            self::SCENARIO_UPDATE => ['skill_id','vacancy_id'],
        ];
    }


}
