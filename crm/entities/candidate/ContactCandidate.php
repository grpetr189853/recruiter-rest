<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 03.04.2019
 * Time: 10:32
 */

namespace app\crm\entities\candidate;


use app\crm\entities\_traits\InstantiateTrait;
use app\crm\entities\candidate\related\ContactType;
use app\crm\interfaces\IARCandidateRelationThing;
use app\crm\interfaces\IContact;
use app\crm\interfaces\IEntityId;
use yii\db\ActiveRecord;
use Assert\Assertion;

class ContactCandidate extends ActiveRecord implements IContact, IARCandidateRelationThing
{
    use InstantiateTrait;

    private $contact_type;
    private $value;
    private $candidate_id;
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    /*
    public function __construct(int $type, string $value)
    {
        Assertion::notEmpty($type);
        Assertion::notEmpty($value);

        $this->type = $type;
        $this->value= $value;
        parent::__construct();
    }
    */
    public function create(int $contact_type, string $value,int $candidate_id): self
    {
        $contact_candidate = new static();
        $contact_candidate->contact_type = $contact_type;
        $contact_candidate->value = $value;
        $contact_candidate->candidate_id = $candidate_id;
        return $contact_candidate;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getContactType()
    {
        return $this->hasOne(ContactType::className(), ['id'=>'contact_type']);
    }

    public static function tableName(): string
    {
        return '{{%contacts_candidate}}';
    }

    public function afterFind(): void
    {
        $this->contact_type  = $this->getAttribute('contact_type');
        $this->value = $this->getAttribute('value');
        $this->candidate_id = $this->getAttribute('candidate_id');
        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {
//        $this->setAttribute('contact_type',  $this->type);
//        $this->setAttribute('value', $this->value);
        return parent::beforeSave($insert);
    }

    public function rules()
    {
        return [
            [['contact_type','value','candidate_id'], 'safe','on' => self::SCENARIO_CREATE],
            [['contact_type','value','candidate_id'], 'safe','on' => self::SCENARIO_UPDATE],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['contact_type','value','candidate_id'],
            self::SCENARIO_UPDATE => ['contact_type','value','candidate_id'],
        ];
    }
}