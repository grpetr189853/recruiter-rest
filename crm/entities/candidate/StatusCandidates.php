<?php


namespace app\crm\entities\candidate;

use app\crm\entities\_traits\InstantiateTrait;
use app\crm\interfaces\IARCandidateRelationThing;
use yii\db\ActiveRecord;

class StatusCandidates extends ActiveRecord implements IARCandidateRelationThing
{
    use InstantiateTrait;
    private $candidate_id;
    private $status;
    const STATUS_ALL = 0;
    const STATUS_NEW = 1;
    const STATUS_CONSIDERED = 2;
    const STATUS_TODAY = 3;
    const STATUS_OFFER = 4;
    const STATUS_SELECTED = 5;
    const STATUS_BLACKLISTED = 6;

    public function create(string $candidate_id, int $status): self {
        $status_candidate = new static();
        $status_candidate->candidate_id = $candidate_id;
        $status_candidate->status = $status;
        return $status_candidate;
    }

    public function getCandidateId(): string {
        return $this->candidate_id;
    }

    public function getStatus(): int {
        return $this->status;
    }

    public static function tableName(): string
    {
        return '{{%additional_statuses}}';
    }

    public function afterFind(): void
    {
        $this->candidate_id = $this->getAttribute('candidate_id', $this->candidate_id);
        $this->status = $this->getAttribute('status', $this->status);
        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {
        return parent::beforeSave($insert);
    }

}