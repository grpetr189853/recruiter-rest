<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 12:07
 */

namespace app\crm\entities\candidate;


use app\crm\entities\_traits\InstantiateTrait;
use app\crm\entities\_traits\LazyLoadTrait;
use app\crm\interfaces\IEntity;
use yii\db\ActiveRecord;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;



class YiiCandidate extends ActiveRecord
{

    use LazyLoadTrait, InstantiateTrait;


    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%candidates}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {

    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function afterFind(): void
    {


        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {
        $this->setAttribute('id', 'fgdgdfgdfgdfg');
        $this->setAttribute('lastname', 'ffff');
        $this->setAttribute('name', 'fdgdfgdfgdfg');


        return parent::beforeSave($insert);
    }

}