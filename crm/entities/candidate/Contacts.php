<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 03.04.2019
 * Time: 11:12
 */

namespace app\crm\entities\candidate;


use app\crm\entities\Contact;
use app\crm\interfaces\IContact;

class Contacts
{
    /***@var Contact[] ***/
    private $contacts = [];

    public function __construct(array $contacts)
    {
        if(!$contacts){
            //throw new \DomainException('May be give me one contact?');
        }
        foreach ($contacts as $contact){
            $this->add($contact);
        }
    }

    public function add(IContact $contact): void
    {
        foreach ($this->contacts as $item) {
            if(false) { //TODO:
                throw new \DomainException('Contact already exists.');
            }
        }
        $this->contacts[] = $contact;
    }

    public function remove($index): IContact
    {
        if(!isset($this->contacts[$index])){
            throw new \DomainException('Contact not found.');
        }
        $contact = $this->contacts[$index];
        unset($this->contacts[$index]);

        return $contact;
    }

    public function getAll(): array
    {
        return $this->contacts;
    }


}