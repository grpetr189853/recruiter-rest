<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 16:36
 */

namespace app\crm\entities\candidate;

use Assert\Assertion;

class Name
{
    private $first;
    private $last;
    private $father;

    public function __construct(string $last, string $first, string $father=null)
    {
        Assertion::notEmpty($last);
        Assertion::notEmpty($first);

        $this->last = $last;
        $this->first = $first;
        $this->father = $father;
    }

    public function getFull(): string
    {
        return trim($this->last . ' ' . $this->first . ' ' . $this->father);
    }

    public function getFirst(): string
    {
        return $this->first ?? '';
    }

    public function getFather(): string
    {
        return $this->father ?? '';
    }

    public function getLast(): string
    {
        return $this->last ?? '';
    }
}