<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 15:31
 */
namespace app\crm\entities\user;

use app\crm\entities\_traits\LazyLoadTrait;
use app\crm\entities\candidate\Info;
use app\crm\entities\candidate\Name;
use app\crm\entities\group\Group;
use app\crm\entities\City;
use app\crm\interfaces\IARUser;
use app\crm\interfaces\IUser;
use app\crm\interfaces\IEntity;
use app\crm\interfaces\IGroupEntity;
use dektrium\user\helpers\Password;
use phpDocumentor\Reflection\Types\This;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\Application as WebApplication;

/***
 * Class User
 * @package app\crm\entities\user
 *
 */
class User extends ActiveRecord implements IEntity, IUser, IARUser, IGroupEntity
{
    use LazyLoadTrait;

    /***@var $id UserId***/
    private $id;

    /***@var $name Name***/
    private $name;

    private $username;

    /***@var $date_birth \DateTimeImmutable***/
    private $date_birth;

    private $location_id;

    private $email;

    private $group_name;

    private $password;

    private $repeat_password;

    private $status;

    private $info;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const  ADMIN_USERNAME = 'admin@admin.com';

    public static function create(Name $name,
                                  string $date_birth,
                                  string $location_id = null,
                                  string $email,
                                  string $group_name,
                                  string $password,
                                  string $repeat_password,
                                  int $status,
                                  Info $info  ): self
    {
        $user = new static();
        $user->name = $name;
        $user->date_birth = $date_birth;
        $user->location_id  = $location_id;
        $user->email = $email;
        $user->group_name = $group_name;
        $user->password = $password;
        $user->repeat_password  = $repeat_password;
        $user->status = $status;
        $user->info = $info;

        return $user;
    }

    public function rename(Name $name): void
    {
        $this->name = $name;
    }

    public function remove(): void
    {
        if(false) {
            throw new \DomainException('Some reason why can not delete user');
        }
    }

    public function getId(): UserId
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = new UserId($id);
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getDateBirth(): \DateTimeImmutable
    {
        return $this->info->getBirth();
    }

    public function getLocation()
    {
        return $this->location_id;
    }

    public function setLocation($location_id)
    {
        $this->location_id = $location_id;
    }

    public function getEmail():string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        return $this->email = $email;
    }

    public function getGroupName(): string
    {
        return $this->group_name;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        return $this->password = $password;
    }

    public function getRepeatPassword()
    {
        return $this->repeat_password;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        return $this->status = $status;
    }

    public function getInfo(): Info
    {
        return $this->info;
    }

    public function setInfo(Info $info):void
    {
        $this->info = $info;
    }

    public function export(): array
    {
        $data = [];
        return $data;
    }


    public function getType(): string
    {
        return "user";
    }

    /**
     * @return array
     */
    public function releaseEvents(): array
    {
        // TODO: Implement releaseEvents() method.
        return [];
    }


    /***##SERVICE METHODS **/

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];

    }

    public function afterFind(): void
    {
        $this->setId($this->getAttribute('id'));

        $this->name = new Name(
            $this->getAttribute('lastname'),
            $this->getAttribute('name')
        );

        $this->setUsername($this->getAttribute('username'));

        $this->info = new Info(
            null,
            null,
            null,
            new \DateTimeImmutable($this->getAttribute('date_birth'))
        );

        $this->date_birth =  new \DateTimeImmutable(
            $this->getAttribute('date_birth')
        );
        $this->location_id = $this->getAttribute('location_id');
        $this->email = $this->getAttribute('email');

        $this->status = $this->getAttribute('status');

        $roles = \Yii::$app->authManager->getRolesByUser($this->id->getId());
        $role = pos($roles);
        $this->group_name = $role->name;

        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {
        $this->setAttribute('lastname', $this->name->getLast());
        $this->setAttribute('name', $this->name->getFirst());
        $this->setAttribute('date_birth', $this->getDateBirth()->format('Y-m-d'));
        $this->setAttribute('location_id', $this->location_id);

        $this->setAttribute('password_hash', Password::hash($this->password));
        $this->setAttribute('status', $this->getStatus());
        $this->setAttribute('auth_key', \Yii::$app->security->generateRandomString());

        $this->setAttribute('username', $this->email);
        $this->setAttribute('email', $this->email);

        if($insert) {
            $this->setAttribute('confirmed_at', time());

            if (\Yii::$app instanceof WebApplication) {
                $this->setAttribute('registration_ip', \Yii::$app->request->userIP);
            }
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->setId($this->getPrimaryKey());

        if($insert) {
            $auth = \Yii::$app->authManager;
            $authorRole = $auth->getRole($this->group_name);
            $auth->assign($authorRole, $this->getId()->getId());
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function getGroup()
    {
        $roles = \Yii::$app->authManager->getRolesByUser($this->id->getId());
        $role = pos($roles);
        return $role;
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'location_id']);
    }

    public function checkEmail() {
        $model = self::findOne(['email' => $email]);
        if ($model) {
            $model->setAttributes($data);
            return $model->save();
        } else {
            return false;
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'name' => 'Имя',
            'lastname' => 'Фамилия',
            'date_birth' => 'Дата рождение',
            'location_id' => 'Место проживания',
            'email' => 'E-mail',
            'group_id' => 'Группа',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновление',
        ];
    }

    public function canDelete() {
        return ($this->getUsername() != self::ADMIN_USERNAME);
    }

    public function canUpdate() {
        return ($this->getUsername() != self::ADMIN_USERNAME);
    }
}