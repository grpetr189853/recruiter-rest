<?php
/**
 * Created by PhpStorm.
 * User: Dmytro
 * Date: 03.04.2019
 * Time: 10:32
 */

namespace app\crm\entities\customer;


use app\crm\entities\_traits\InstantiateTrait;
use app\crm\entities\candidate\related\ContactType;
use app\crm\interfaces\IARCustomerRelationThing;
use app\crm\interfaces\IContact;
use yii\db\ActiveRecord;
use Assert\Assertion;

class ContactCustomer extends ActiveRecord implements IContact, IARCustomerRelationThing
{
    use InstantiateTrait;

    private $type;
    private $value;

    public function __construct(int $type, string $value)
    {
        Assertion::notEmpty($type);
        Assertion::notEmpty($value);

        $this->type = $type;
        $this->value= $value;
        parent::__construct();
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getType()
    {
        return $this->hasOne(ContactType::className(), ['id'=>'contact_type']);
    }

    public static function tableName(): string
    {
        return '{{%contacts_customer}}';
    }

    public function afterFind(): void
    {
        $this->type  = $this->getAttribute('contact_type');
        $this->value = $this->getAttribute('value');

        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {
        $this->setAttribute('contact_type',  $this->type);
        $this->setAttribute('value', $this->value);
        return parent::beforeSave($insert);
    }


}