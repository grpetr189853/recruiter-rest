<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 04.04.2019
 * Time: 13:54
 */

namespace app\crm\entities;


use app\crm\interfaces\ICountry;
use yii\db\ActiveRecord;

/***
 * @property int id
 * @property string $name
 *
 ***/
class Country extends ActiveRecord implements ICountry
{
    private $id;
    private $name;

    public static function create(string $name) : self
    {
        $country = new static();
        $country->name = $name;
        return $country;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function remove(): void
    {
        $this->delete();
    }

    public static function tableName()
    {
        return '{{%countries}}';
    }

    public function beforeSave($insert)
    {
        $this->setAttribute('name', $this->getName());

        return parent::beforeSave($insert);
    }
    public function afterFind()
    {
        $this->setId($this->getAttribute('id'));

        $this->setName($this->getAttribute('name'));

        parent::afterFind();
    }
}