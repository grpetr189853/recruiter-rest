<?php
/**
 * Created by PhpStorm.
 * User: Dmytro
 * Date: 28.03.2019
 * Time: 11:13
 */

namespace app\crm\services\customer;

use app\crm\entities\customer\Customer;
use app\crm\entities\candidate\Name;
use app\crm\forms\candidate\ProfileEditForm;
use app\crm\interfaces\ICustomerRepository;
use app\crm\interfaces\ICustomerService;
use app\crm\interfaces\IContact;
use app\crm\interfaces\IContactService;
use app\crm\interfaces\IEntityId;
use app\crm\interfaces\IEventDispatcher;
use app\crm\repositories\NotFoundException;
use app\crm\services\dto\CustomerCreateDTO;
use app\crm\services\dto\NameDTO;

class CustomerService implements ICustomerService, IContactService
{
    /***@var $repo ICustomerRepository***/
    private $customers;

    /***@var $dispatcher IEventDispatcher***/
    private $dispatcher;

    /***@var $contact IContactService****/
    private $contact;

    public function __construct(ICustomerRepository $repo, IEventDispatcher $dispatcher, IContactService $contact)
    {
        $this->customers   = $repo;
        $this->dispatcher   = $dispatcher;
        $this->contact      = $contact;
    }

    public function create(CustomerCreateDTO $dto): void
    {
        $customer = Customer::create(
            $this->customers->nextId(),
            $dto->getName(),
            $dto->location,
            $dto->getContacts()
        );
        $this->customers->add($customer);

        //$this->dispatcher->dispatch($caustomer->releaseEvents());
    }

    public function rename(IEntityId $id, NameDTO $name): void
    {
        $customer = $this->customers->get($id->getId());
        $name = new Name($name->first, $name->last, $name->father);
        $customer->rename($name);
        try {
            $this->customer->save($customer);
        }
            // db related exceptions
        catch (\yii\db\Exception $exception) {die;}
        catch (\DomainException $exception) {die;}

        // any exception throwin by yii
        catch (\yii\base\Exception $exception){die;}

        // any php exception
        catch (\Exception $exception){die;}
    }

    public function remove(IEntityId $id): void
    {
        $customer = $this->customers->get($id->getId());
        $customer->remove();
        $this->customers->remove($customer);
        $this->dispatcher->dispatch($customer->releaseEvents());
    }

    public function addContact(IEntityId $entity_id, IContact $contact): void
    {
        $customer = $this->customers->get($entity_id);
        $this->contact->addContact($contact);
        $this->customers->save($customer);
        $this->dispatcher->dispatch($customer->releaseEvents());

    }

    public function changeContact(IEntityId $entity_id, IContact $contact): void
    {
        $this->contact->changeContact($entity_id, $contact);
    }

    public function removeContact(IEntityId $entity_id, IContact $contact): void
    {
        // TODO: Implement removeContact() method.
    }

    private function toName(NameDTO $name)
    {
        $name = new Name(
            $name->last,
            $name->first,
            $name->father
        );

        return $name;
    }

    public function getAll($condition=[]): array
    {
        try{
            $customers = $this->customers->getAll($condition);
        }
        catch (NotFoundException $e) {
            //TODO: log
            $customers = [];
        }

        return $customers;
    }

    public function getAllJson(): array
    {
        $customers = $this->customers->getAllJson();
        return $customers;
    }

    public function getBy(array $condition)
    {
        return $this->customers->getBy($condition);
    }

    private function setContacts($formContacts, $currentContacts)
    {
        $new_contacts   = $this->contact->setContacts($currentContacts, $formContacts);
        return $new_contacts;
    }
}