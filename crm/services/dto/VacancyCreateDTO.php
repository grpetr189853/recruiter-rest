<?php
/**
 * Created by PhpStorm.
 * User: Dmytro
 * Date: 29.03.2019
 * Time: 12:20
 */

namespace app\crm\services\dto;


use app\crm\entities\vacancy\DocumentVacancy;
use app\crm\entities\vacancy\SkillVacancy;

class VacancyCreateDTO
{
    public $id;
    public $name;
    public $location;
    public $salary;
    public $customers;
    public $company;
    public $skills  = [];
    public $v_duties;
    public $w_conditions;
    public $v_requirements;
    public $v_description;
    public $documents = [];

    public function load(array $params)
    {
        $this->id = ($params['id']) ?? '';
        $this->name = ($params['name']) ?? '';
        $this->location = (is_numeric($params['location'])) ? $params['location'] : null ;
        $this->salary = (is_numeric($params['salary'])) ? $params['salary'] : 0;
        $this->customers = ($params['customers']) ?? null;
        $this->company = ($params['company']) ?? '';//(is_array($params['company'])) ? $params['company'] : [];
        $this->skills = (is_array($params['skills'])) ? $params['skills'] : [];
        $this->v_duties      = ($params['v_duties']) ?? '';
        $this->w_conditions      = ($params['w_conditions']) ?? '';
        $this->v_requirements      = ($params['v_requirements']) ?? '';
        $this->v_description      = ($params['v_description']) ?? '';
        $this->documents    = (is_array($params['documents'])) ? $params['documents'] : [];
    }

    /***
     * @return String
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getSkills(): array
    {
        $skills=[];
        foreach ($this->skills as $item) {
            $skills[] = new SkillVacancy($item);
        }

        return $skills;
    }

    public function getDocuments(): array
    {
        $documents = [];
        $filteredArrayOfDocuments = array_filter($this->documents);
        if(!empty($filteredArrayOfDocuments)){
            foreach ($this->documents as $document){
                $documents[] = new DocumentVacancy(basename($document),'pdf',$this->id);
            }
        }

        return $documents;
    }
}