<?php


namespace app\crm\services\dto;


class CityCreateDTO
{
    public $country_id;
    public $name;

    public function load(array $params)
    {
        $this->name = ($params['name']) ?? '';
        $this->country_id  = (is_numeric($params['country_id'])) ? $params['country_id'] : null;
    }
}