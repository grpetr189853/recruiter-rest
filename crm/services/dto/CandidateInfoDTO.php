<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 02.04.2019
 * Time: 11:52
 */

namespace app\crm\services\dto;


class CandidateInfoDTO
{
    public $salary;
    public $company;
    public $position;
    public $location;
}