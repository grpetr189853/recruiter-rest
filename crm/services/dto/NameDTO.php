<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 29.03.2019
 * Time: 12:18
 */

namespace app\crm\services\dto;


class NameDTO
{
    public $first;
    public $father;
    public $last;
}