<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 16.04.2019
 * Time: 14:28
 */

namespace app\crm\services;


use app\crm\forms\city\CityEditForm;
use app\crm\interfaces\ICityRepository;
use app\crm\repositories\NotFoundException;
use app\crm\services\dto\CityCreateDTO;
use app\crm\entities\City;

class CityService
{
    /**@var $cities \app\crm\interfaces\ICityRepository */
    private $cities;


    public function __construct(ICityRepository $repo)
    {
        $this->cities = $repo;
    }

    public function create(CityCreateDTO $dto)
    {
        $city = City::create(
            $dto->name,
            $dto->country_id
        );
        $this->cities->add($city);

        return $city->id;
    }

    public function edit($id, CityEditForm $form)
    {
        $city = $this->cities->getBy(['id' => $id]);
        $city->setName($form->name);
        $city->setCountryId($form->country_id);

        try {
            $this->cities->save($city);
        }
        catch (\yii\db\IntegrityException  $ie) {
            die($ie->getMessage());
        }
        catch (\DomainException $de) {
            die($de->getMessage());
        }
    }

    public function getAll($condition=[])
    {

        try{
            $cities =$this->cities->getAll($condition);
        }
        catch (NotFoundException $e) {
            //TODO: log
            $cities = [];
        }


        return $cities;
    }
    public function getBy(array $condition)
    {
        return $this->cities->getBy($condition);
    }

    public function remove($id): void
    {
        $city = $this->cities->get($id);
        $city->remove();
        $this->cities->remove($city);
    }
}