<?php


namespace app\crm\services;


use app\crm\entities\Country;
use app\crm\forms\country\CountryEditForm;
use app\crm\interfaces\ICountryRepository;
use app\crm\repositories\NotFoundException;
use app\crm\services\dto\CountryCreateDTO;

class CountryService
{
    /**@var $countries \app\crm\interfaces\ICountryRepository */
    private $countries;

    public function __construct(ICountryRepository $repo)
    {
        $this->countries = $repo;
    }

    public function create(CountryCreateDTO $dto)
    {
        $country = Country::create(
            $dto->name
        );
        $this->countries->add($country);

        return $country->id;
    }

    public function edit($id, CountryEditForm $form)
    {
        $country = $this->countries->getBy(['id' => $id]);
        $country->setName($form->name);

        try {
            $this->countries->save($country);
        }
        catch (\yii\db\IntegrityException  $ie) {
            die($ie->getMessage());
        }
        catch (\DomainException $de) {
            die($de->getMessage());
        }
    }

    public function getAll($condition=[])
    {
        try{
            $countries =$this->countries->getAll($condition);
        }
        catch (NotFoundException $e) {
            //TODO: log
            $countries = [];
        }


        return $countries;
    }

    public function getBy(array $condition)
    {
        return $this->countries->getBy($condition);
    }

    public function remove($id): void
    {
        $country = $this->countries->get($id);
        $country->remove();
        $this->countries->remove($country);
    }
}