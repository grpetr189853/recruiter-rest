<?php


namespace app\crm\services;


use app\crm\entities\candidate\DocumentCandidate;
use app\crm\entities\candidate\Documents;
use app\crm\interfaces\IDocumentService;

class DocumentService implements IDocumentService
{

    public function setDocuments(Documents $curr_documents, array $formDocuments) : Documents
    {

        $reflector = new \ReflectionFunction($curr_documents->getProxyInitializer());
        $new_documents = [];
        if(!empty($formDocuments[0])) {
            /**создадим  из массива*/
            foreach ($formDocuments as $key => $formDocument) {
                // TODO: redo it
                    $new_documents[] = new DocumentCandidate(basename($formDocument), 'pdf', $reflector->getClosureThis()->id);
            }
        }
        return new Documents($new_documents);
    }

}