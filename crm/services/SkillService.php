<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 16.04.2019
 * Time: 14:28
 */

namespace app\crm\services;


use app\crm\entities\candidate\related\Skills;
use app\crm\entities\candidate\SkillCandidate;
use app\crm\entities\Skill;
use app\crm\forms\skill\SkillEditForm;
use app\crm\interfaces\IRepository;
use app\crm\interfaces\ISkillEntity;
use app\crm\interfaces\ISkillRepository;
use app\crm\interfaces\ISkillService;
use app\crm\repositories\NotFoundException;
use app\crm\services\dto\SkillCreateDTO;

class SkillService implements ISkillService
{

    /**@var $skills \app\crm\interfaces\ISkillRepository */
    private $skills;

    public function __construct(ISkillRepository $repository)
    {
        $this->skills = $repository;
    }

    public function create(SkillCreateDTO $dto)
    {
        $skill = Skill::create(
            $dto->name
        );
        $this->skills->add($skill);

        return $skill->id;
    }

    public function edit($id, SkillEditForm $form)
    {
        $skill = $this->skills->getBy(['id' => $id]);
        $skill->setName($form->name);

        try {
            $this->skills->save($skill);
        }
        catch (\yii\db\IntegrityException  $ie) {
            die($ie->getMessage());
        }
        catch (\DomainException $de) {
            die($de->getMessage());
        }
    }

    public function getAll($condition=[])
    {
        try{
            $skills =$this->skills->getAll($condition);
        }
        catch (NotFoundException $e) {
            //TODO: log
            $skills = [];
        }


        return $skills;
    }

    public function getBy(array $condition)
    {
        return $this->skills->getBy($condition);
    }

    public function setSkills(Skills $current_skills, array $skills): Skills
    {
        if(!empty($skills)) {

            $new_skills=[];

            /**создадим скиллы из массива*/
            foreach ($skills as $skill){
                $new_skills[] = new SkillCandidate((int)$skill);
            }

            /**удалим скилл если его нету в новом массиве*/
            foreach ($current_skills->getAll() as $key => $old_skill) {
                if (!in_array($old_skill->skill_id, $skills)) {
                    $current_skills->remove($key);
                }
            }
            /**добавим скилл */
            foreach ($new_skills as $new_skill) {
                try {
                    $current_skills->add($new_skill);
                }
                catch (\DomainException $de) {
                    //TODO:
                }
            }
        } else {
            /**удалим все*/
            foreach ($current_skills->getAll()  as $key=>$skill) {
                $current_skills->remove($key);
            }
        }
        return $current_skills;
    }

    public function remove($id): void
    {
        $skill = $this->skills->get($id);
        $skill->remove();
        $this->skills->remove($skill);
    }
}