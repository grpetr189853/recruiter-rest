<?php


namespace app\crm\repositories\lib;

use app\crm\entities\Country;
use app\crm\interfaces\ICountryRepository;
use app\crm\interfaces\IRepository;
use app\crm\repositories\NotFoundException;
use http\Exception;

class CountryRepository implements IRepository,ICountryRepository
{
    /**
     * @param $id
     * @return mixed
     */
    public function get($id): Country
    {
        return $this->getBy(['id'=>$id]);
    }

    /**
     * @param $entity
     */
    public function add($entity):void
    {
        try{
            $entity->insert();
        }
        catch (\Exception $e){
            throw new \RuntimeException('Inserting error. '. $e->getMessage());
        }
    }

    /***
     * @param Country $entity
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function save($entity): void
    {
        if ($entity->update() === false) {
            throw new \RuntimeException('Saving error');
        }
    }

    /**
     * @param $entity
     */
    public function remove($entity): void
    {
        // TODO: Implement remove() method.
    }

    /**
     * @param array $condition
     * @return mixed
     */
    public function getBy(array $condition): Country
    {
        if(!$country = Country::find()->andWhere($condition)->limit(1)->one()){
            throw new NotFoundException('Country not found');
        }

        return $country;
    }

    public function getAll(array $condition)
    {
        if(!$countries = Country::find()->andWhere($condition)->all()){
            throw new NotFoundException('Country not found');
        }

        return $countries;
    }

    /**
     * @return mixed
     */
    public function nextId()
    {
        // TODO: Implement nextId() method.
    }
}