<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 16.04.2019
 * Time: 14:25
 */

namespace app\crm\repositories\lib;


use app\crm\entities\Skill;
use app\crm\interfaces\IRepository;
use app\crm\interfaces\ISkillRepository;
use app\crm\repositories\NotFoundException;
use http\Exception;

class SkillRepository implements IRepository, ISkillRepository
{

    /**
     * @param $id
     * @return mixed
     */
    public function get($id) : Skill
    {
        return $this->getBy(['id'=>$id]);
    }

    /**
     * @param $entity
     */
    public function add($entity): void
    {
        try {
            $entity->insert();
        }
        catch (Exception $e) {
            throw new \RuntimeException('Inserting error. '. $e->getMessage());
        }
    }

    /**
     * @param $entity
     */
    public function save($entity): void
    {
        if ($entity->update() === false) {
            throw new \RuntimeException('Saving error');
        }
    }

    /**
     * @param $entity
     */
    public function remove($entity): void
    {
        // TODO: Implement remove() method.
    }

    /**
     * @param array $condition
     * @return mixed
     */
    public function getBy(array $condition) : Skill
    {
        if(!$skill = Skill::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('Skill not found');
        }

        return $skill;
    }

    /**
     * @param array $condition
     * @return mixed
     */
    public function getAll(array $condition)
    {

        if(!$cities = Skill::find()->andWhere($condition)->all()) {
            throw new NotFoundException('Skill not found');
        }

        return $cities;
    }

    /**
     * @return mixed
     */
    public function nextId()
    {
        // TODO: Implement nextId() method.
    }
}