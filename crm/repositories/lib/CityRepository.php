<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 16.04.2019
 * Time: 13:57
 */

namespace app\crm\repositories\lib;


use app\crm\entities\City;
use app\crm\interfaces\ICityRepository;
use app\crm\interfaces\IRepository;
use app\crm\repositories\NotFoundException;
use http\Exception;

class CityRepository implements IRepository, ICityRepository
{

    /**
     * @param $id
     * @return mixed
     */
    public function get($id) : City
    {
        return $this->getBy(['id'=>$id]);
    }

    /**
     * @param $entity
     */
    public function add($entity): void
    {
        try {
            $entity->insert();
        }
        catch (Exception $e) {
            throw new \RuntimeException('Inserting error. '. $e->getMessage());
        }
    }

    /***
     * @param City $entity
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function save($entity): void
    {
        if ($entity->update() === false) {
            throw new \RuntimeException('Saving error');
        }
    }
    /**
     * @param $entity
     */
    public function remove($entity): void
    {
        // TODO: Implement remove() method.
    }

    /**
     * @param array $condition
     * @return mixed
     */
    public function getBy(array $condition): City
    {
        if(!$city = City::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('City not found');
        }

        return $city;
    }

    public function getAll($condition=[]): array
    {

        if(!$cities = City::find()->andWhere($condition)->all()) {
            throw new NotFoundException('City not found');
        }

        return $cities;
    }


    /**
     * @return mixed
     */
    public function nextId()
    {
        // TODO: Implement nextId() method.
    }
}