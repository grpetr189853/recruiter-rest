<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 16.04.2019
 * Time: 13:57
 */
namespace app\crm\repositories\lib;


use app\crm\entities\Group;
use app\crm\interfaces\IGroupRepository;
use app\crm\interfaces\IRepository;
use app\crm\repositories\NotFoundException;

class GroupRepository implements IRepository, IGroupRepository
{

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        // TODO: Implement get() method.
    }

    /**
     * @param $entity
     */
    public function add($entity): void
    {
        // TODO: Implement add() method.
    }

    /**
     * @param $entity
     */
    public function save($entity): void
    {
        // TODO: Implement save() method.
    }

    /**
     * @param $entity
     */
    public function remove($entity): void
    {
        // TODO: Implement remove() method.
    }

    /**
     * @param array $condition
     * @return mixed
     */
    public function getBy(array $condition)
    {
        // TODO: Implement getBy() method.
    }

    public function getAll($condition=[]): array
    {

        if(!$groups = Group::find()->andWhere($condition)->all()) {
            throw new NotFoundException('Group not found');
        }

        return $groups;
    }


    /**
     * @return mixed
     */
    public function nextId()
    {
        // TODO: Implement nextId() method.
    }
}