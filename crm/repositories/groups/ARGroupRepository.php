<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 29.03.2019
 * Time: 10:42
 */
namespace app\crm\repositories\groups;

use app\crm\entities\group\Group;
use app\crm\entities\group\GroupId;
use app\crm\interfaces\IGroupRepository;
use app\crm\interfaces\IEntityId;
use app\crm\repositories\NotFoundException;
use Ramsey\Uuid\Uuid;
use yii\db\Exception;
use yii\rbac\Item;

class ARGroupRepository implements IGroupRepository
{
    /**
     * @param $name
     * @return Group
     */
    public function get($name): Group
    {
        return $this->getBy(['name' => $name]);
    }

    /***
     * @param Group $entity
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function save(Group $entity): void
    {
        if ($entity->update() === false) {
            throw new \RuntimeException('Saving error');
        }
    }
    
    /**
     * @param array $condition
     * @return Group
     */
    public function getBy(array $condition): Group
    {
        if(!$group = Group::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('Group not found');
        }

        return $group;
    }
    
    /***
     * @param Group $entity
     * @throws \Exception
     * @throws \Throwable
     */
    public function add(Group $entity): void
    {
        try {
            $entity->addRole();
        }
        catch (Exception $e) {
            throw new \RuntimeException('Inserting error. '. $e->getMessage());
        }
    }

    public function getAll($condition=[], $search=''): array
    {
        if(!$groups = Group::find()
            ->andWhere($condition)
            ->andWhere(['type' => Item::TYPE_ROLE])
            ->andWhere(['like', 'name', $search])
            ->orderBy(['created_at' => SORT_DESC])->all()) {
            throw new NotFoundException('Groups not found');
        }

        return $groups;
    }

    public function getAllJson()
    {
        $condition = [];
        if (!$groups = Group::find()->andWhere($condition)->asArray()->all()) {
            throw new NotFoundException('Groups not found');
        }

        return $groups;
    }
}