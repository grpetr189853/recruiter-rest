<?php
/**
 * Created by PhpStorm.
 * User: Dmytro
 * Date: 29.03.2019
 * Time: 10:42
 */

namespace app\crm\repositories\vacancies;

use app\crm\entities\vacancy\Vacancy;
use app\crm\entities\vacancy\VacancyId;
use app\crm\interfaces\IVacancyRepository;
use app\crm\interfaces\IEntityId;
use app\crm\repositories\NotFoundException;
use Ramsey\Uuid\Uuid;
use yii\db\Exception;

class ARVacancyRepository implements IVacancyRepository
{

    /***
     * @return IEntityId
     * @throws \Exception
     */
    public function nextId(): IEntityId
    {
        return new VacancyId(Uuid::uuid4()->toString());
    }

    /**
     * @param $id
     * @return Vacancy
     */
    public function get($id): Vacancy
    {
        return $this->getBy(['id'=>$id]);
    }

    /***
     * @param Vacancy $entity
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function save(Vacancy $entity): void
    {
        if ($entity->update() === false) {
            throw new \RuntimeException('Saving error');
        }
    }

    /***
     * @param Vacancy $entity
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function remove(Vacancy $entity): void
    {
        if (!$entity->delete()) {
            throw new \RuntimeException('Removing error');
        }
    }

    /**
     * @param array $condition
     * @return Vacancy
     */
    public function getBy(array $condition): Vacancy
    {
        if(!$vacancy = Vacancy::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('Vacancy not found');
        }

        return $vacancy;
    }

    /***
     * @param Vacancy $entity
     * @throws \Exception
     * @throws \Throwable
     */
    public function add(Vacancy $entity): void
    {
        try {
            $entity->insert();
        }
        catch (Exception $e) {
            throw new \RuntimeException('Inserting error. '. $e->getMessage());
        }

        /*if (!$entity->insert()) {
            throw new \RuntimeException('saving error');
        }*/
    }

    public function getAll($condition=[]): array
    {

        if(!$vacancies = Vacancy::find()->andWhere($condition)->orderBy(['created_at' => SORT_DESC])->all()) {
            throw new NotFoundException('Vacancies not found');
        }

        return $vacancies;
    }

    public function getAllJson()
    {
        $condition=[];
        if(!$vacancies = Vacancy::find()->andWhere($condition)->asArray()->all()) {
            throw new NotFoundException('Vacancies not found');
        }

        return $vacancies;
    }
}