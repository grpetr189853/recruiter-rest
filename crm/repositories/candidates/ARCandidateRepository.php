<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 29.03.2019
 * Time: 10:42
 */

namespace app\crm\repositories\candidates;


use app\crm\entities\candidate\Candidate;
use app\crm\entities\candidate\CandidateId;
use app\crm\interfaces\IARCandidate;
use app\crm\interfaces\IARRepository;
use app\crm\interfaces\ICandidateRepository;
use app\crm\interfaces\IEntityId;
use app\crm\interfaces\IEventDispatcher;
use app\crm\repositories\NotFoundException;
use Ramsey\Uuid\Uuid;
use yii\db\ActiveRecord;
use yii\db\Exception;

class ARCandidateRepository implements ICandidateRepository
{

    /***
     * @return IEntityId
     * @throws \Exception
     */
    public function nextId(): IEntityId
    {
        return new CandidateId(Uuid::uuid4()->toString());
    }

    /**
     * @param $id
     * @return Candidate
     */
    public function get($id): Candidate
    {
        return $this->getBy(['id'=>$id]);
    }

    /***
     * @param Candidate $entity
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function save(Candidate $entity): void
    {
        if ($entity->update() === false) {
            throw new \RuntimeException('Saving error');
        }
    }

    /***
     * @param Candidate $entity
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function remove(Candidate $entity): void
    {
        if (!$entity->delete()) {
            throw new \RuntimeException('Removing error');
        }
    }

    /**
     * @param array $condition
     * @return Candidate
     */
    public function getBy(array $condition): Candidate
    {
        if(!$candidate = Candidate::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('Candidate not found');
        }

        return $candidate;
    }

    /***
     * @param Candidate $entity
     * @throws \Exception
     * @throws \Throwable
     */
    public function add(Candidate $entity): void
    {
        try {
            $entity->insert();
        }
        catch (Exception $e) {
            throw new \RuntimeException('Inserting error. '. $e->getMessage());
        }

        /*if (!$entity->insert()) {
            throw new \RuntimeException('saving error');
        }*/
    }

    public function getAll($condition=[], $search=''): array
    {

        if(!$candidates = Candidate::find()
            ->andWhere($condition)
            ->andFilterWhere([
                'or',
                ['like', 'name', $search],
                ['like', 'lastname', $search],
                ['like', 'fathername', $search]
            ])
            ->orderBy(['date_created' => SORT_DESC])->all()) {
            throw new NotFoundException('Candidates not found');
        }

        return $candidates;
    }

    public function getAllJson()
    {
        $condition=[];
        if(!$candidates = Candidate::find()->andWhere($condition)->with(['city','relatedSkills.skill','relatedContacts'])->asArray()->all()) {
            throw new NotFoundException('Candidates not found');
        }

        return $candidates;
    }

}